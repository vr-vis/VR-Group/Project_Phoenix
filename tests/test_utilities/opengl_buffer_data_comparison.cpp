//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include "test_utilities/opengl_buffer_data_comparison.hpp"

#include <string>

#include "phx/resources/types/image.hpp"

namespace test_utilities {

std::string OpenGLBufferComparison::GetFormatString(phx::Image* image) {
  if (image == nullptr) return "";
  switch (image->GetBitsPerPixel()) {
    case 8:
      return "Byte";
    case 24:
      return "RGB";
    case 32:
      return "RGBA";
    default:
      return "Unknown format";
  }
}

void OpenGLBufferComparison::OutputRefImageMissingInfo(
    const std::string& file1) {
  std::cout << "The test image has been written to" << std::endl
            << "     " << file1 << std::endl
            << std::endl;

  std::cout
      << "Please check the test image and check whether it indeed contains "
      << std::endl
      << "the correct result." << std::endl
      << "If the test image is correct, please input \"write\" and confirm"
      << std::endl
      << "to create the reference image from the test image." << std::endl
      << "Input anything else to cancel (in which case, the test image "
         "file is retained)."
      << std::endl;
}

std::string OpenGLBufferComparison::ReadInput() {
  std::string response;
#ifdef IS_BUILD_SERVER
  // on the build server, answer with whatever is in cin already
  // this makes it possible to "fake" the answer from the outside,
  // but if nobody faked anything, the build server will simply answer
  // the empty string
  if (std::cin.rdbuf()->in_avail() == 0) {
    response = "";
  } else {
    std::getline(std::cin, response);
  }
#else
  std::getline(std::cin, response);
#endif
  return response;
}

void OpenGLBufferComparison::RemoveTempFiles(
    const std::string& temp_file1, const std::string& temp_file2,
    const std::string& temp_file_diff) {
  std::cout << "The temporary file(s) are now removed." << std::endl
            << std::endl;
  std::remove(temp_file1.c_str());
  if (temp_file2 != "") {
    std::remove(temp_file2.c_str());
  }
  if (temp_file_diff != "") {
    std::remove(temp_file_diff.c_str());
  }
}

void OpenGLBufferComparison::OutputUpdateRefImageInfo(
    const std::string& filename_reference_image, bool overwrite) {
  std::cout << std::endl
            << "Reference image" << std::endl
            << "     " << filename_reference_image << std::endl;
  if (overwrite) {
    std::cout << " has been overwritten with the new image buffer."
              << std::endl;
  } else {
    std::cout << " has been created with the new image buffer." << std::endl;
  }
}

void OpenGLBufferComparison::OutputComparisonInfo(
    double minimumSimilarity, double similarity, phx::Image* ref_image,
    phx::OpenGLImageBufferData<phx::OpenGLImageBufferDataType_Float32>*
        diff_image,
    const std::string& test_image_name, const std::string& ref_image_name,
    const std::string& diff_image_name) {
  std::cout << "Similarity should be " << minimumSimilarity << ", but is only "
            << (similarity < 0 ? "(undefined, reference image does not exist)"
                               : std::to_string(similarity))
            << std::endl
            << std::endl
            << "Images have been written to " << std::endl
            << "     [Test image]        " << test_image_name << std::endl
            << "     [Reference image]   "
            << (ref_image != nullptr ? ref_image_name : "(does not exist)")
            << "\n"
            << "     [Difference image]  "
            << (diff_image != nullptr ? diff_image_name : "(does not exist)")
            << std::endl
            << std::endl;

  std::cout
      << "Please compare the images and find out whether there is an error."
      << std::endl
      << "If the test image is correct (and the reference image is "
         "wrong/outdated),"
      << std::endl
      << "please input \"overwrite\" and confirm to overwrite the reference "
         "image"
      << std::endl
      << "with the test image." << std::endl
      << "Input anything else to cancel to avoid changing the reference image "
      << std::endl
      << "(in that case, the temporary image files above are retained)."
      << std::endl;
}

std::string OpenGLBufferComparison::GenerateRandomString(std::size_t length) {
  std::string result;
  result.reserve(length);
  std::default_random_engine generator;
  generator.seed(std::random_device()());
  std::uniform_int_distribution<int> distribution(0, 61);
  for (std::size_t i = 0; i < length; ++i) {
    int random = distribution(generator);
    char random_char;
    if (random < 10)
      random_char = static_cast<char>(random + 48);
    else if (random < 36)
      random_char = static_cast<char>(random + 55);
    else
      random_char = static_cast<char>(random + 61);
    result.insert(result.end(), random_char);
  }
  return result;
}

}  // namespace test_utilities

//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef TESTS_TEST_UTILITIES_GLM_MAT4_HPP_
#define TESTS_TEST_UTILITIES_GLM_MAT4_HPP_

#include <algorithm>
#include <limits>
#include <sstream>
#include <string>

#include "catch/catch.hpp"

#include "phx/suppress_warnings.hpp"

SUPPRESS_WARNINGS_BEGIN
#include "glm/glm.hpp"
SUPPRESS_WARNINGS_END

#include "phx/utility/stream_helpers.hpp"

#include "approx.hpp"

namespace Catch {

template <>
struct StringMaker<glm::mat4> {
  static std::string convert(const glm::mat4& matrix) {
    std::ostringstream sstr;
    sstr << matrix;
    return sstr.str();
  }
};

}  // namespace Catch

namespace test_utilities {

template <>
inline bool Approx<glm::mat4>::operator==(const glm::mat4& rhs) {
  bool result = true;
  for (int row = 0; row < 4; ++row) {
    for (int col = 0; col < 4; ++col) {
      const auto this_element = value_[row][col];
      auto this_approx_element = Catch::Detail::Approx(this_element);
      this_approx_element.epsilon(epsilon_);
      this_approx_element.margin(margin_);
      this_approx_element.scale(scale_);
      const auto other_element = rhs[row][col];
      result &= (other_element == this_approx_element);
    }
  }
  return result;
}

}  // namespace test_utilities

#endif  // TESTS_TEST_UTILITIES_GLM_MAT4_HPP_

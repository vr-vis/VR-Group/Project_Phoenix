//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef TESTS_TEST_UTILITIES_DUMMY_MESH_GENERATOR_HPP_
#define TESTS_TEST_UTILITIES_DUMMY_MESH_GENERATOR_HPP_

#include <memory>
#include <utility>
#include <vector>

#include "phx/suppress_warnings.hpp"

SUPPRESS_WARNINGS_BEGIN
#include "glm/vec3.hpp"
SUPPRESS_WARNINGS_END

#include "phx/resources/resource_load_strategy.hpp"

namespace phx {
class Mesh;

class DummyMeshLoader : public ResourceLoadStrategy {
 public:
  DummyMeshLoader() = delete;
  DummyMeshLoader(std::vector<glm::vec3> &&vertices,
                  std::vector<glm::vec3> &&normals,
                  std::vector<unsigned int> &&indices);
  DummyMeshLoader(const DummyMeshLoader &) = default;
  DummyMeshLoader(DummyMeshLoader &&) = default;
  ~DummyMeshLoader() override = default;

  DummyMeshLoader &operator=(const DummyMeshLoader &) = default;
  DummyMeshLoader &operator=(DummyMeshLoader &&) = default;

  std::unique_ptr<Resource> Load(
      const ResourceDeclaration &declaration) override;

 protected:
 private:
  std::unique_ptr<phx::Mesh> mesh_;
};
}  // namespace phx

#endif  // TESTS_TEST_UTILITIES_DUMMY_MESH_GENERATOR_HPP_

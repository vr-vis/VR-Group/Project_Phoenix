//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include "dummy_mesh_generator.hpp"

#include <memory>
#include <utility>
#include <vector>

#include "phx/resources/types/mesh.hpp"

namespace phx {

DummyMeshLoader::DummyMeshLoader(std::vector<glm::vec3> &&vertices,
                                 std::vector<glm::vec3> &&normals,
                                 std::vector<unsigned int> &&indices)
    : mesh_{std::make_unique<phx::Mesh>()} {
  mesh_->SetVertices(std::move(vertices));
  mesh_->SetNormals(std::move(normals));
  mesh_->SetIndices(std::move(indices));
  mesh_->SetTextureCoords(
      std::vector<glm::vec2>(mesh_->GetVertices().size(), glm::vec2()));
}

std::unique_ptr<phx::Resource> DummyMeshLoader::Load(
    const ResourceDeclaration &) {
  auto new_mesh = std::make_unique<phx::Mesh>();
  *new_mesh = *mesh_;
  return new_mesh;
}

}  // namespace phx

//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef TESTS_TEST_UTILITIES_LOG_CAPTURE_HPP_
#define TESTS_TEST_UTILITIES_LOG_CAPTURE_HPP_

#include <sstream>
#include <string>

#include "catch/catch.hpp"

#include "spdlog/sinks/sink.h"

#include "phx/suppress_warnings.hpp"

namespace test_utilities {

class LogCapture : public spdlog::sinks::sink {
 public:
  void log(const spdlog::details::log_msg& msg) override;
  void flush() override;

  bool operator==(const std::string& other) const;

  std::string ToString() const;

 private:
  std::stringstream log_stream_;
};

}  // namespace test_utilities

namespace Catch {

template <>
struct StringMaker<test_utilities::LogCapture> {
  static std::string convert(const test_utilities::LogCapture& log_capture) {
    return log_capture.ToString();
  }
};

}  // namespace Catch

#endif  // TESTS_TEST_UTILITIES_LOG_CAPTURE_HPP_

//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include "test_utilities/log_capture.hpp"

#include <sstream>
#include <string>

namespace test_utilities {
void LogCapture::log(const spdlog::details::log_msg& msg) {
  log_stream_ << msg.raw.c_str();
}

void LogCapture::flush() { log_stream_.flush(); }

std::string LogCapture::ToString() const {
  return "\"" + log_stream_.str() + "\"";
}

bool LogCapture::operator==(const std::string& other) const {
  return log_stream_.str() == other;
}

}  // namespace test_utilities

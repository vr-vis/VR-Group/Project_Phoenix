//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef TESTS_TEST_UTILITIES_APPROX_HPP_
#define TESTS_TEST_UTILITIES_APPROX_HPP_

#include <algorithm>
#include <limits>
#include <sstream>
#include <string>

#include "catch/catch.hpp"

#include "phx/suppress_warnings.hpp"

SUPPRESS_WARNINGS_BEGIN
#include "glm/glm.hpp"
SUPPRESS_WARNINGS_END

namespace test_utilities {

template <typename T>
class Approx {
 public:
  explicit Approx(const T& value) : value_(value) {}

  Approx& epsilon(float new_epsilon) {
    epsilon_ = new_epsilon;
    return *this;
  }

  Approx& margin(float new_margin) {
    margin_ = new_margin;
    return *this;
  }

  Approx& scale(float new_scale) {
    scale_ = new_scale;
    return *this;
  }

  bool operator==(const T&);
  friend bool operator==(const T& lhs, const Approx<T>& rhs) {
    return rhs == lhs;
  }
  const T& GetValue() const { return value_; }
  float GetEpsilon() const { return epsilon_; }
  float GetMargin() const { return margin_; }
  float GetScale() const { return scale_; }

 private:
  float epsilon_{std::numeric_limits<float>::epsilon() * 100.0f};
  float margin_{0.0f};
  float scale_{1.0f};
  const T& value_;
};

}  // namespace test_utilities

namespace Catch {

template <typename T>
struct StringMaker<test_utilities::Approx<T>> {
  static std::string convert(const test_utilities::Approx<T>& approx) {
    std::ostringstream sstr;
    sstr << "Approx: epsilon=" << approx.GetEpsilon()
         << " margin=" << approx.GetMargin() << " scale=" << approx.GetScale()
         << "\n"
         << StringMaker<T>::convert(approx.GetValue());
    return sstr.str();
  }
};

}  // namespace Catch

#endif  // TESTS_TEST_UTILITIES_APPROX_HPP_

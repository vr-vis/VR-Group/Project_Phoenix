//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include <cstddef>
#include <cstdio>
#include <limits>
#include <memory>
#include <string>

#include "catch/catch.hpp"

#include "phx/rendering/backend/opengl_image_buffer_data.hpp"
#include "phx/resources/resource_manager.hpp"
#include "phx/resources/resource_utils.hpp"

#include "test_utilities/opengl_buffer_data_comparison.hpp"
#include "test_utilities/reference_image_path.hpp"

std::unique_ptr<phx::Image> CreateCircularGradientImage(std::size_t width,
                                                        std::size_t height);
std::unique_ptr<phx::Image> CreateCircularGradientImage(std::size_t width,
                                                        std::size_t height) {
  auto image = std::make_unique<phx::Image>(
      std::array<std::size_t, 2>{{width, height}}, 24);
  const float center_x = static_cast<float>(width) * 0.5f;
  const float center_y = static_cast<float>(height) * 0.5f;
  const float max_dist =
      std::sqrt(static_cast<float>(width * width + height * height)) * 0.5f;
  for (std::size_t y = 0; y < height; ++y) {
    float dist_to_center_y = static_cast<float>(y) - center_y;
    for (std::size_t x = 0; x < width; ++x) {
      // some gradient to the center
      float dist_to_center_x = static_cast<float>(x) - center_x;
      float dist_to_center = std::sqrt(dist_to_center_x * dist_to_center_x +
                                       dist_to_center_y * dist_to_center_y);
      float color = 1.f - (dist_to_center / max_dist);
      unsigned char color_byte =
          static_cast<unsigned char>(std::round(color * 255.f));
      image->SetPixelColor({{x, y}}, {{color_byte, color_byte, color_byte}});
    }
  }
  return image;
}

SCENARIO(
    "A buffer can be compared to a reference image. If they are different (or "
    "the reference image does not exist), the reference image can, upon user "
    "request, automatically be overwritten with the buffer.",
    "[tests][test::utils]") {
  GIVEN("A buffer with gray pixels") {
    phx::OpenGLImageBufferData<phx::OpenGLImageBufferDataType_RGB> buffer(1024,
                                                                          768);
    for (std::size_t y = 0; y < 768; ++y)
      for (std::size_t x = 0; x < 1024; ++x)
        buffer.SetPixel(x, y, {128, 128, 128});

    WHEN(
        "It is compared to a reference image path that contains the same "
        "image") {
      const std::string ref_image_filename = "test_reference_image.png";
      const std::string filename_with_path =
          test_utilities::reference_image_root + ref_image_filename;

      buffer.SaveToFilePNG(filename_with_path);

      THEN("Everything's fine") {
        test_utilities::OpenGLBufferComparison::
            REQUIRE_REFERENCE_IMAGE_SIMILARITY(buffer, ref_image_filename, 1.0);
      }

      std::remove(filename_with_path.c_str());
    }

    WHEN("It is compared to a reference image path that does not exist") {
      const std::string ref_image_filename = "test_reference_image2.png";
      const std::string filename_with_path =
          test_utilities::reference_image_root + ref_image_filename;
      // make sure it does not exist
      std::remove(filename_with_path.c_str());

      THEN(
          "The user is asked whether it should be created. If they deny, "
          "the test fails.") {
        // fake user response to accept overwriting
        std::streambuf* backup = std::cin.rdbuf();
        std::istringstream oss("write\n");
        std::cin.rdbuf(oss.rdbuf());

        test_utilities::OpenGLBufferComparison::
            REQUIRE_REFERENCE_IMAGE_SIMILARITY(buffer, ref_image_filename, 1.0);

        // restore cin
        std::cin.rdbuf(backup);

        THEN(
            "If they accept, the reference image now exists and is identical "
            "to the buffer") {
          auto img = phx::ResourceUtils::LoadResourceFromFile<phx::Image>(
              filename_with_path, {}, true);

          REQUIRE(img != nullptr);
          auto test_ref_image_buffer = phx::OpenGLImageBufferData<
              phx::OpenGLImageBufferDataType_RGB>::CreateFromImage(img.Get());
          test_utilities::OpenGLBufferComparison::REQUIRE_SIMILARITY(
              buffer, *test_ref_image_buffer.get(), 1.0);

          // clean up
          std::remove(filename_with_path.c_str());
        }
      }
    }

    WHEN("It is compared to a reference image path whose image is different") {
      const std::string ref_image_filename = "test_reference_image3.png";
      const std::string filename_with_path =
          test_utilities::reference_image_root + ref_image_filename;
      // create a different image there
      auto ref_image = CreateCircularGradientImage(1024, 768);
      ref_image->Save(filename_with_path);

      THEN(
          "The user is asked whether the reference image should be "
          "overwritten. If they deny, the test fails.") {
        // fake user response to accept overwriting
        std::streambuf* backup = std::cin.rdbuf();
        std::istringstream oss("overwrite\n");
        std::cin.rdbuf(oss.rdbuf());

        test_utilities::OpenGLBufferComparison::
            REQUIRE_REFERENCE_IMAGE_SIMILARITY(buffer, ref_image_filename, 1.0);

        // restore cin
        std::cin.rdbuf(backup);

        THEN(
            "If they accept, the reference image now exists and is identical "
            "to the buffer") {
          auto img = phx::ResourceUtils::LoadResourceFromFile<phx::Image>(
              filename_with_path, {}, true);
          REQUIRE(img != nullptr);
          auto test_ref_image_buffer = phx::OpenGLImageBufferData<
              phx::OpenGLImageBufferDataType_RGB>::CreateFromImage(img.Get());
          test_utilities::OpenGLBufferComparison::REQUIRE_SIMILARITY(
              buffer, *test_ref_image_buffer.get(), 1.0);

          // clean up
          std::remove(filename_with_path.c_str());
        }
      }
    }
  }
}

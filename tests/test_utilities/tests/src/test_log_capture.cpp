//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include <iostream>
#include <memory>

#include "catch/catch.hpp"

#include "phx/core/logger.hpp"

#include "test_utilities/log_capture.hpp"

SCENARIO("LogCapture captures standard output", "[tests][test::utils]") {
  GIVEN("A LogCapture") {
    auto log_capture = std::make_shared<test_utilities::LogCapture>();
    phx::logger = std::make_shared<spdlog::logger>("logcapture", log_capture);
    phx::logger->set_pattern("%v");

    WHEN("I log \"something\".") {
      phx::info("something");
      THEN("\"something\" is captured.") {
        REQUIRE(*log_capture == "something");
      }
      WHEN("I log \" else\".") {
        phx::info(" else");
        THEN("\"something else\" is captured.") {
          REQUIRE(*log_capture == "something else");
        }
      }
    }
    WHEN("I log two lines.") {
      phx::info("two\n");
      phx::info("lines");
      THEN("two lines are captured") { REQUIRE(*log_capture == "two\nlines"); }
    }
  }
}

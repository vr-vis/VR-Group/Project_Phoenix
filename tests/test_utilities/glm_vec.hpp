//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef TESTS_TEST_UTILITIES_GLM_VEC_HPP_
#define TESTS_TEST_UTILITIES_GLM_VEC_HPP_

#include <algorithm>
#include <limits>
#include <sstream>
#include <string>

#include "catch/catch.hpp"

#include "phx/suppress_warnings.hpp"

SUPPRESS_WARNINGS_BEGIN
#include "glm/glm.hpp"
SUPPRESS_WARNINGS_END

#include "phx/utility/stream_helpers.hpp"

#include "approx.hpp"

namespace Catch {

template <>
struct StringMaker<glm::vec2> {
  static std::string convert(const glm::vec2& vector) {
    std::ostringstream sstr;
    sstr << vector;
    return sstr.str();
  }
};

template <>
struct StringMaker<glm::vec3> {
  static std::string convert(const glm::vec3& vector) {
    std::ostringstream sstr;
    sstr << vector;
    return sstr.str();
  }
};

template <>
struct StringMaker<glm::vec4> {
  static std::string convert(const glm::vec4& vector) {
    std::ostringstream sstr;
    sstr << vector;
    return sstr.str();
  }
};

}  // namespace Catch

namespace test_utilities {

template <class VectorType>
bool CompareVectors(VectorType a, VectorType b, int rows, float eps,
                    float margin, float scale) {
  bool result = true;
  for (int row = 0; row < rows; ++row) {
    const auto this_element = a[row];
    auto this_approx_element = Catch::Detail::Approx(this_element);
    this_approx_element.epsilon(eps);
    this_approx_element.margin(margin);
    this_approx_element.scale(scale);
    const auto other_element = b[row];
    result &= (other_element == this_approx_element);
  }
  return result;
}

template <>
inline bool Approx<glm::vec2>::operator==(const glm::vec2& rhs) {
  return CompareVectors<glm::vec2>(value_, rhs, 2, epsilon_, margin_, scale_);
}

template <>
inline bool Approx<glm::vec3>::operator==(const glm::vec3& rhs) {
  return CompareVectors<glm::vec3>(value_, rhs, 3, epsilon_, margin_, scale_);
}

template <>
inline bool Approx<glm::vec4>::operator==(const glm::vec4& rhs) {
  return CompareVectors<glm::vec4>(value_, rhs, 4, epsilon_, margin_, scale_);
}

}  // namespace test_utilities

#endif  // TESTS_TEST_UTILITIES_GLM_VEC_HPP_

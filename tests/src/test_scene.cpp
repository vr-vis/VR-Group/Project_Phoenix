//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include "catch/catch.hpp"

#include "phx/core/entity.hpp"
#include "phx/core/runtime_component.hpp"
#include "phx/core/scene.hpp"

SCENARIO("A scene keeps track of entities.", "[phx][phx::Scene]") {
  GIVEN("An empty scene") {
    phx::Scene scene;

    THEN("It should have 1 entity (the virtual platform).") {
      REQUIRE(scene.GetNumberOfEntities() == 1);
      REQUIRE(
          scene.GetEntities()[0]
              ->GetFirstComponent<phx::RuntimeComponent<phx::USER_PLATFORM>>());
    }

    WHEN("We create an entity in the scene") {
      phx::Entity* entity = scene.CreateEntity();

      THEN("the scene has 2 entities.") {
        REQUIRE(scene.GetNumberOfEntities() == 2);
      }
      THEN("the entity vector is of size 2 and contains the entity.") {
        REQUIRE(scene.GetEntities().size() == 2);
        REQUIRE(scene.GetEntities()[1] == entity);
        REQUIRE(scene.GetEntities()[1]->GetScene() == &scene);
      }
      THEN("we get a non-null pointer to the newly created entity.") {
        REQUIRE(entity != nullptr);
      }

      WHEN("we try to delete with an invalid handle.") {
        scene.RemoveEntity(reinterpret_cast<phx::Entity*>(42));
        THEN("the scene still has 2 entities.") {
          REQUIRE(scene.GetNumberOfEntities() == 2);
        }
      }

      WHEN("we delete the entity from the scene.") {
        scene.RemoveEntity(entity);
        THEN("the scene has 1 entity (the virtual platform).") {
          REQUIRE(scene.GetNumberOfEntities() == 1);
          REQUIRE(scene.GetEntities()[0]
                      ->GetFirstComponent<
                          phx::RuntimeComponent<phx::USER_PLATFORM>>());
        }
      }
    }
  }
}

class TestComponent1 : public phx::Component {};
class TestComponent2 : public phx::Component {};

SCENARIO("A scene provides access to entities with specified components.",
         "[phx][phx::Scene]") {
  GIVEN(
      "A scene containing an entity with a TestComponent1, another with a "
      "TestComponent2, and a third with both TestComponent1 and "
      "TestComponent2.") {
    phx::Scene scene;

    auto entity1 = scene.CreateEntity();
    auto entity1component1 = entity1->AddComponent<TestComponent1>();

    auto entity2 = scene.CreateEntity();
    auto entity2component2 = entity2->AddComponent<TestComponent2>();

    auto entity3 = scene.CreateEntity();
    auto entity3component1 = entity3->AddComponent<TestComponent1>();
    auto entity3component2 = entity3->AddComponent<TestComponent2>();

    WHEN("We request the entities with TestComponent1.") {
      auto entities = scene.GetEntitiesWithComponents<TestComponent1>();
      THEN("The two entities with TestComponent1 are returned.") {
        REQUIRE(entities.size() == 2);
        REQUIRE(entities[0] == entity1);
        REQUIRE(entities[1] == entity3);
      }
    }

    WHEN("We request the entities with TestComponent2.") {
      auto entities = scene.GetEntitiesWithComponents<TestComponent2>();
      THEN("The two entities with TestComponent2 are returned.") {
        REQUIRE(entities.size() == 2);
        REQUIRE(entities[0] == entity2);
        REQUIRE(entities[1] == entity3);
      }
    }

    WHEN(
        "We request the entities with both TestComponent1 and "
        "TestComponent2.") {
      auto entities =
          scene.GetEntitiesWithComponents<TestComponent1, TestComponent2>();
      THEN(
          "The entity with both TestComponent1 and TestComponent2 is "
          "returned.") {
        REQUIRE(entities.size() == 1);
        REQUIRE(entities[0] == entity3);
      }
    }

    WHEN("We request the TestComponent1 components.") {
      auto components = scene.GetFirstComponents<TestComponent1>();
      THEN("The two TestComponent1 are returned.") {
        REQUIRE(components.size() == 2);
        REQUIRE(components[0] == entity1component1);
        REQUIRE(components[1] == entity3component1);
      }
    }

    WHEN("We request the TestComponent2 components.") {
      auto components = scene.GetFirstComponents<TestComponent2>();
      THEN("The two TestComponent2 are returned.") {
        REQUIRE(components.size() == 2);
        REQUIRE(components[0] == entity2component2);
        REQUIRE(components[1] == entity3component2);
      }
    }

    WHEN("We request both the TestComponent1 and TestComponent2 components.") {
      auto components =
          scene.GetFirstComponentsMany<TestComponent1, TestComponent2>();
      THEN("The TestComponent1 and TestComponent2 are returned.") {
        REQUIRE(components.size() == 1);
        REQUIRE(std::get<0>(components[0]) == entity3component1);
        REQUIRE(std::get<1>(components[0]) == entity3component2);
      }
    }
  }
}

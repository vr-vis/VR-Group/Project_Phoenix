//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include <string>

#include "catch/catch.hpp"

#include "phx/core/entity.hpp"
#include "phx/core/scene.hpp"
#include "phx/rendering/components/mesh_handle.hpp"
#include "phx/rendering/components/transform.hpp"
#include "phx/resources/loaders/scene_loader.hpp"

#include "mocks/opengl_mock.hpp"
#include "trompeloeil.hpp"

extern template struct trompeloeil::reporter<trompeloeil::specialized>;

SCENARIO("The scene loader can load a model", "[phx][phx::SceneLoader]") {
  OPENGL_MOCK_ALLOW_ANY_CALL
  GIVEN("An empty scene") {
    phx::Scene scene;
    WHEN("We load a model into the scene") {
      std::string mesh_file{"models/2MeshTest/2meshTest.obj"};
      bool success = phx::SceneLoader::InsertModelIntoScene(mesh_file, &scene);

      THEN(
          "the scene contains 2 entities + the virtual platform + the root "
          "entity") {
        REQUIRE(success);
        REQUIRE(scene.GetEntities().size() == 4u);
      }

      THEN(
          "the scene contains exactly one root entity with the same name like "
          "the model + mesh entites are children of that entity") {
        unsigned int nameCounter = 0;
        bool meshHasWrongParent = false;

        auto entityVec = scene.GetEntities();
        for (auto currentEntity : entityVec) {
          if (currentEntity->GetName() == "2meshTest") {
            nameCounter++;
          }
          if (currentEntity->GetFirstComponent<phx::MeshHandle>() != nullptr) {
            auto parentEntity =
                currentEntity->GetFirstComponent<phx::Transform>()
                    ->GetParent()
                    ->GetEntity();
            if (parentEntity->GetName() != "2meshTest") {
              meshHasWrongParent = true;
            }
          }
        }
        REQUIRE(nameCounter == 1u);
        REQUIRE(meshHasWrongParent == false);
      }
    }
  }
}

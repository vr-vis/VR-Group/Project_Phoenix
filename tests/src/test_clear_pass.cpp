//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include "phx/suppress_warnings.hpp"

SUPPRESS_WARNINGS_BEGIN
#include "GL/glew.h"
#include "glm/vec2.hpp"
SUPPRESS_WARNINGS_END

#include "trompeloeil.hpp"

#include "catch/catch.hpp"

#include "phx/rendering/backend/render_target.hpp"
#include "phx/rendering/render_passes/clear_pass.hpp"

#include "mocks/opengl_mock.hpp"

extern template struct trompeloeil::reporter<trompeloeil::specialized>;

SCENARIO("The clear pass clears the active color and depth buffers.",
         "[phx][phx::ClearPass]") {
  OPENGL_MOCK_ALLOW_ANY_CALL
  GIVEN("A ClearPass") {
    phx::RenderTarget renderTarget(glm::uvec2(1024, 768));
    phx::ClearPass clearPass(&renderTarget);

    WHEN("We set the clear color") {
      THEN("glClearColor is called as desired") {
        REQUIRE_CALL(open_gl_mock, glClearColor(0.1f, 0.2f, 0.3f, 0.4f))
            .TIMES(1);
        clearPass.SetClearColor({0.1f, 0.2f, 0.3f, 0.4f});
      }
    }

    WHEN("We execute the clear pass.") {
      THEN("glClear is called as desired") {
        REQUIRE_CALL(open_gl_mock,
                     glClear(static_cast<GLbitfield>(GL_COLOR_BUFFER_BIT |
                                                     GL_DEPTH_BUFFER_BIT)))
            .TIMES(1);
        clearPass.Execute();
      }
    }
  }
}

//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include <memory>

#include "catch/catch.hpp"

#include "trompeloeil.hpp"

#include "phx/core/engine.hpp"
#include "phx/input/device.hpp"
#include "phx/input/device_system.hpp"

using trompeloeil::_;
using trompeloeil::ne;

extern template struct trompeloeil::reporter<trompeloeil::specialized>;

class MockDevice : public phx::Device {
 public:
  MAKE_MOCK0(Update, void());
};

class MockDevice2 : public MockDevice {
 public:
  MAKE_MOCK0(Update, void());
};

SCENARIO("The device system manages and updates its devices",
         "[phx][phx::DeviceSystem]") {
  GIVEN("an device system") {
    phx::Engine engine;
    auto device_system = engine.CreateSystem<phx::DeviceSystem>();

    WHEN("We add devices") {
      MockDevice* device1 = device_system->AddDevice<MockDevice>();
      MockDevice2* device2 = device_system->AddDevice<MockDevice2>();

      THEN("their update method is called when the system is updated") {
        REQUIRE_CALL(*device1, Update()).TIMES(1);
        REQUIRE_CALL(*device2, Update()).TIMES(1);
        device_system->Update(phx::FrameTimer::TimeInfo());
      }

      THEN("we can get these devices by type") {
        auto devices = device_system->GetDevices<MockDevice>();
        REQUIRE(devices.size() == 2);
        REQUIRE(std::find(devices.begin(), devices.end(), device1) !=
                devices.end());
        REQUIRE(std::find(devices.begin(), devices.end(), device2) !=
                devices.end());

        auto devices2 = device_system->GetDevices<MockDevice2>();
        REQUIRE(devices2.size() == 1);
        REQUIRE(devices2[0] == device2);
      }

      THEN("devices can be removed again") {
        device_system->RemoveDevice(device1);
        REQUIRE(device_system->GetDevices<MockDevice>().size() == 1);
      }
    }
  }
}

//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include <memory>

#include "catch/catch.hpp"
#include "trompeloeil.hpp"

#include "phx/core/engine.hpp"
#include "phx/core/logger.hpp"
#include "phx/core/scene.hpp"
#include "phx/scripting/behavior.hpp"
#include "phx/scripting/behavior_system.hpp"
#include "phx/scripting/generic_behavior.hpp"
#include "test_utilities/log_capture.hpp"

extern template struct trompeloeil::reporter<trompeloeil::specialized>;

class BehaviorMock : public phx::Behavior {
 public:
  MAKE_MOCK0(OnUpdate, void(), override);
};

SCENARIO(
    "The behavior system iterates through entities and processes their "
    "behavior components on each frame.",
    "[phx][phx::BehaviorSystem]") {
  GIVEN("An entity containing a mock behavior component.") {
    using trompeloeil::_;
    using trompeloeil::gt;

    phx::Engine engine;
    auto scene = std::make_shared<phx::Scene>();
    engine.SetScene(scene);
    auto entity = scene->CreateEntity();

    auto behavior_system = engine.CreateSystem<phx::BehaviorSystem>();

    WHEN("We create a mocked behavior") {
      auto behavior = entity->AddComponent<BehaviorMock>();
      WHEN("We update the behavior system 10 times.") {
        REQUIRE_CALL(*behavior, OnUpdate()).TIMES(10);
        for (auto i = 0; i < 10; ++i)
          behavior_system->Update(phx::FrameTimer::TimeInfo());
      }
    }
  }
}
SCENARIO("we can have generic behaviors", "[phx][phx::GenericBehavior]") {
  GIVEN("An engine with a behavior system and an entity") {
    phx::Engine engine;
    auto scene = std::make_shared<phx::Scene>();
    engine.SetScene(scene);
    auto entity = scene->CreateEntity();
    auto behavior_system = engine.CreateSystem<phx::BehaviorSystem>();

    WHEN("We create a generic behavior") {
      bool onConstructionCalled = false;
      bool onUpdateCalled = false;
      entity->AddComponent<phx::GenericBehavior>(
          [&onConstructionCalled](phx::Entity*) {
            onConstructionCalled = true;
          },
          [&onUpdateCalled](const phx::FrameTimer::TimeInfo*, phx::Entity*) {
            onUpdateCalled = true;
          });
      THEN("onConstruction should be called, but no onUpdate yet") {
        REQUIRE(onConstructionCalled);
        REQUIRE_FALSE(onUpdateCalled);
        WHEN("The behavior system is updated") {
          behavior_system->Update(phx::FrameTimer::TimeInfo());
          THEN("onUpdate is also called") {
            REQUIRE(onConstructionCalled);
            REQUIRE(onUpdateCalled);
          }
        }
      }
    }
    WHEN("We we name a generic behavior") {
      auto behavior = entity->AddComponent<phx::GenericBehavior>(
          [](phx::Entity*) {},
          [](const phx::FrameTimer::TimeInfo*, phx::Entity*) {}, "GenericName");
      THEN("it is loggable") {
        auto log_capture = std::make_shared<test_utilities::LogCapture>();
        phx::logger =
            std::make_shared<spdlog::logger>("logcapture", log_capture);
        phx::logger->set_pattern("%v");

        phx::info("{}", *behavior);
        REQUIRE(*log_capture == "GenericBehavior: GenericName");
      }
    }
  }
}

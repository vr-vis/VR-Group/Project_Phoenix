//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include <iostream>
#include <memory>
#include <string>

#include "catch/catch.hpp"

#include "mocks/opengl_mock.hpp"

#include "phx/core/logger.hpp"
#include "phx/resources/loaders/assimp_model_loader.hpp"
#include "phx/resources/resource_utils.hpp"
#include "phx/resources/types/mesh.hpp"
#include "phx/utility/aspects/nameable.hpp"

#include "test_utilities/log_capture.hpp"

extern template struct trompeloeil::reporter<trompeloeil::specialized>;

class TestLogger {
 public:
  TestLogger() : log_capture_{std::make_shared<test_utilities::LogCapture>()} {
    phx::logger = std::make_shared<spdlog::logger>("logcapture", log_capture_);
    phx::logger->set_pattern("%w");
  }

  test_utilities::LogCapture* GetCapture() const { return log_capture_.get(); }

 private:
  std::shared_ptr<test_utilities::LogCapture> log_capture_;
};

SCENARIO("The assimp loader loads models using the Assimp library.",
         "[phx][phx::AssimpLoader]") {
  OPENGL_MOCK_ALLOW_ANY_CALL
  GIVEN("A plain loader") {
    phx::CreateDefaultLogger();
    TestLogger test_logger;

    WHEN("We load an invalid file") {
      phx::ResourceUtils::LoadResourceFromFile<phx::Model>("invalid.obj");

      THEN("We get an error message printed to the console.") {
        std::string captured_message = test_logger.GetCapture()->ToString();
        std::cout << "Captured error message is: " << captured_message
                  << std::endl;
        REQUIRE(captured_message.find("Error loading model") !=
                std::string::npos);
      }
    }
    WHEN("We load the stanford bunny.") {
      auto resource = phx::ResourceUtils::LoadResourceFromFile<phx::Model>(
          "models/bunny.obj");
      auto mesh = resource->GetMesh();

      THEN("there is a mesh") {
        REQUIRE(mesh != nullptr);
        THEN("The mesh component has 2503 vertices.") {
          REQUIRE(mesh->GetVertices().size() == 2503);
        }
        THEN("The mesh component has 4968 * 3 indices.") {
          REQUIRE(mesh->GetIndices().size() == 4968 * 3);
        }
        THEN("The mesh component has 2503 normals.") {
          REQUIRE(mesh->GetNormals().size() == 2503);
        }
        THEN("The mesh component has 0 tangents.") {
          REQUIRE(mesh->GetTangents().size() == 0);
        }
        THEN("The mesh component has 0 bitangents.") {
          REQUIRE(mesh->GetBitangents().size() == 0);
        }
        THEN("The mesh component has 2503 texture coordinates.") {
          REQUIRE(mesh->GetTextureCoords().size() == 2503);
        }
        THEN("The mesh component is unnamed.") {
          REQUIRE(mesh->GetName() == phx::Mesh::UNNAMED);
        }
      }
    }
    WHEN("We load a unit cube.") {
      auto resource = phx::ResourceUtils::LoadResourceFromFile<phx::Model>(
          "models/cube/cube.obj");
      auto mesh = resource->GetMesh();
      auto bounding_box = mesh->GetBoundingBox();
      THEN("The lower bound should be at (-0.5,-0.5,-0.5)") {
        REQUIRE(bounding_box[0] == glm::vec3(-0.5f));
      }
      THEN("The upper bound should be at (0.5,0.5,0.5)") {
        REQUIRE(bounding_box[1] == glm::vec3(0.5f));
      }
    }
    WHEN("We load a specific material.") {
      const std::string material_name = "Material.001";
      auto resource = phx::ResourceUtils::LoadResourceFromFile<phx::Material>(
          "models/2MeshTest/2meshTest.obj", {{"material_name", material_name}});
      auto material = resource.Get();
      THEN("there is a material") {
        REQUIRE(material != nullptr);
        THEN("The material has the right name") {
          REQUIRE(material->GetName() == material_name);
        }
      }
    }
    WHEN("We load a named mesh in an OBJ-File with two models.") {
      const std::string mesh_name = "Cube_Cube.001";
      auto resource = phx::ResourceUtils::LoadResourceFromFile<phx::Model>(
          "models/2MeshTest/2meshTest.obj");
      auto mesh = resource->GetMesh(mesh_name);

      THEN("there is a mesh") {
        REQUIRE(mesh != nullptr);
        THEN("The mesh component has 24 vertices.") {
          REQUIRE(mesh->GetVertices().size() == 24);
        }
        THEN("The mesh component has 12 * 3 indices.") {
          REQUIRE(mesh->GetIndices().size() == 12 * 3);
        }
        THEN("The mesh component has 24 normals.") {
          REQUIRE(mesh->GetNormals().size() == 24);
        }
        THEN("The mesh component has 0 tangents.") {
          REQUIRE(mesh->GetTangents().size() == 0);
        }
        THEN("The mesh component has 0 bitangents.") {
          REQUIRE(mesh->GetBitangents().size() == 0);
        }
        THEN("The mesh component has 24 texture coordinates.") {
          REQUIRE(mesh->GetTextureCoords().size() == 24);
        }
        THEN("The mesh component is named like the mesh in the OBJ-File.") {
          REQUIRE(mesh->GetName() == mesh_name);
        }
      }
    }
  }
}

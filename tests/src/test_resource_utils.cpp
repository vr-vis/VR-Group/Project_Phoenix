//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include <fstream>
#include <string>

#include "catch/catch.hpp"

#include "phx/resources/resource_declaration.hpp"
#include "phx/resources/resource_utils.hpp"
#include "phx/resources/types/image.hpp"
#include "phx/resources_path.hpp"

namespace {
void CreateDummyFile(const std::string &file_name) {
  std::fstream out_file(file_name.c_str(), std::ios::out);
  // force fail test if file could not be created
  REQUIRE(out_file.is_open());
}
void DeleteDummyFile(const std::string &file_name) {
  std::remove(file_name.c_str());
}
}  // namespace

SCENARIO("Resource utils can extract file extensions.",
         "[phx][phx::ResourceUtils]") {
  GIVEN("A file name") {
    std::string file_name{"C:/some_path/to/file.34.ext"};

    THEN("It can extract the correct extension.") {
      std::string extension =
          phx::ResourceUtils::ExtractFileExtension(file_name);
      REQUIRE(extension == ".ext");
    }
  }
  GIVEN("A file name in capital case") {
    std::string file_name{"C:/some_path/to/file.34.EXT"};

    THEN("It can extract the correct lowercase extension.") {
      std::string extension =
          phx::ResourceUtils::ExtractFileExtension(file_name);
      REQUIRE(extension == ".ext");
    }
  }
}

SCENARIO("Resource utils can declarare a file resource.",
         "[phx][phx::ResourceUtils]") {
  GIVEN("A file name with absolute path") {
    std::string file_name{"C:/some_path/to/file.34.ext"};

    THEN("we get a declaration with a valid (unmodified) file name") {
      phx::ResourceDeclaration declaration =
          phx::ResourceUtils::DeclarationFromFile(file_name, {}, true);
      REQUIRE(declaration["TYPE"] == ".ext");
      REQUIRE(declaration["file_name"] == file_name);
    }
  }
  GIVEN("A file name and a search path") {
    std::string file_name{"textures/splash_progress.png"};

    THEN(
        "we get a declaration with a valid file name that has been pre-pended "
        "by the first hit in the search path") {
      phx::ResourceDeclaration declaration =
          phx::ResourceUtils::DeclarationFromFile(file_name);
      REQUIRE(declaration["TYPE"] == ".png");
      REQUIRE(declaration["file_name"] == phx::resources_root + file_name);
    }
  }
  GIVEN("Additional key, value pairs are provided") {
    std::string file_name{"textures/splash_progress.png"};
    std::string key = "some_key";
    int value = 42;

    THEN("the resulting declaration contains them") {
      phx::ResourceDeclaration declaration =
          phx::ResourceUtils::DeclarationFromFile(file_name, {{key, value}},
                                                  false);
      REQUIRE(declaration[key] == value);
    }
  }
}

SCENARIO("Resource utils find file resource outside the original search path",
         "[phx][phx::ResourceUtils]") {
  GIVEN("A file outside the original resource tree") {
    std::string dummy_file = "dummy.txt";
    std::string offset_search_path = "../";
    ::CreateDummyFile(offset_search_path + dummy_file);

    phx::ResourceUtils::AddResourceSearchPath(offset_search_path);
    THEN("the file is found and a valid declaration is returned") {
      phx::ResourceDeclaration declaration =
          phx::ResourceUtils::DeclarationFromFile(dummy_file, {}, false);
      CHECK(declaration["file_name"] == offset_search_path + dummy_file);
      ::DeleteDummyFile(offset_search_path + dummy_file);
    }
  }
}

SCENARIO("Resource utils can load a file resource.",
         "[phx][phx::ResourceUtils]") {
  GIVEN("A file name within the original resource tree") {
    std::string file_name{"textures/splash_progress.png"};

    THEN("It can load it") {
      phx::ResourcePointer<phx::Image> image =
          phx::ResourceUtils::LoadResourceFromFile<phx::Image>(file_name);
      REQUIRE(image != nullptr);
    }
  }
}

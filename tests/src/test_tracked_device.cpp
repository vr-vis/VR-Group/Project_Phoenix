//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include <memory>

#include "catch/catch.hpp"

#include "phx/display/hmd.hpp"
#include "phx/input/tracked_device.hpp"
#include "phx/input/vr_controller.hpp"

#include "mocks/openvr_mock.hpp"

#include "trompeloeil.hpp"

#undef _

extern template struct trompeloeil::reporter<trompeloeil::specialized>;
using trompeloeil::_;

SCENARIO("OpenVR is only initialized and shutdown once",
         "[phx][phx::TrackedDevice]") {
  OPENVR_MOCK_ALLOW_ANY_CALL

  WHEN("we initialize 2 derrived tracked devices") {
    THEN("openVR is initialized exactly once") {
      REQUIRE_CALL(openvr_mock.Get(),
                   VR_InitInternal2(_, vr::VRApplication_Scene, _))
          .TIMES(1)
          .RETURN(static_cast<uint32_t>(1337))
          .SIDE_EFFECT(*_1 = vr::VRInitError_None);
      auto device1 = std::make_unique<phx::HMD>();
      auto device2 = std::make_unique<phx::VRController>(
          phx::VRController::LEFT_CONTROLLER);

      WHEN("we destroy one") {
        THEN("openVR shutdown is not called") {
          FORBID_CALL(openvr_mock.Get(), VR_ShutdownInternal());
          device1.reset();
          WHEN("we destroy the second one") {
            THEN("openVR shutdown is called") {
              REQUIRE_CALL(openvr_mock.Get(), VR_ShutdownInternal()).TIMES(1);
              device2.reset();
            }
          }
        }
      }
    }
  }
}

#define ALLOW_EVENT_POLL_CALLS                             \
  ALLOW_CALL(openvr_mock.GetSystem(), PollNextEvent(_, _)) \
      .SIDE_EFFECT(*_1 = event)                            \
      .SIDE_EFFECT(*first_call = false)                    \
      .WITH(*first_call == true)                           \
      .RETURN(true);                                       \
  ALLOW_CALL(openvr_mock.GetSystem(), PollNextEvent(_, _)) \
      .WITH(*first_call == false)                          \
      .RETURN(false);

class ActualDevice : public phx::TrackedDevice {
 public:
  ActualDevice() : TrackedDevice() { id_ = 1; }
  void Update() override { TrackedDevice::Update(); }
  void UpdateDeviceIndex() override {}
  MAKE_MOCK1(OnOpenVREvent, void(const vr::VREvent_t&));
};

SCENARIO("OpenVR events are forwarded as signals to all tracked devices",
         "[phx][phx::TrackedDevice]") {
  OPENVR_MOCK_ALLOW_ANY_CALL
  GIVEN("a derrived tracked device and some OpenVR event") {
    auto device = std::make_unique<ActualDevice>();

    auto first_call = std::make_shared<bool>(true);
    vr::VREvent_t event;
    event.eventType = vr::EVREventType::VREvent_ButtonPress;
    WHEN("Update is called") {
      THEN("OnOpenVREvent is called") {
        *first_call = true;
        ALLOW_EVENT_POLL_CALLS
        REQUIRE_CALL(*device, OnOpenVREvent(_))
            .WITH(_1.eventType == vr::EVREventType::VREvent_ButtonPress);

        device->Update();
      }
    }
    WHEN("we create a second device") {
      auto device2 = std::make_unique<ActualDevice>();
      THEN("the onOpenVREvent is called for both devices") {
        *first_call = true;
        ALLOW_EVENT_POLL_CALLS
        REQUIRE_CALL(*device, OnOpenVREvent(_))
            .WITH(_1.eventType == vr::EVREventType::VREvent_ButtonPress);
        REQUIRE_CALL(*device2, OnOpenVREvent(_))
            .WITH(_1.eventType == vr::EVREventType::VREvent_ButtonPress);
        device->Update();
        device2->Update();

        WHEN("the first device is deleted") {
          device.reset();
          THEN("the second device still gets the event") {
            *first_call = true;
            ALLOW_EVENT_POLL_CALLS
            REQUIRE_CALL(*device2, OnOpenVREvent(_))
                .WITH(_1.eventType == vr::EVREventType::VREvent_ButtonPress);
            device2->Update();
          }
        }
      }
    }
  }
}

// the pose etc. is tested in integration_test_hmd

//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include "catch/catch.hpp"

#include "phx/resources/resource_utils.hpp"
#include "phx/resources/types/material.hpp"

#include "test_utilities/glm_vec.hpp"

SCENARIO("The Generic Material Loader can create Generic materials.",
         "[phx][phx::GenericMaterialLoader]") {
  GIVEN("A plain Generic Material Loader") {
    WHEN("We load a red material") {
      auto materialResource = phx::ResourceUtils::LoadGenericMaterial(
          "generic_material", glm::vec3(1.f, 0.f, 0.f));

      THEN("We have a material with the given name") {
        REQUIRE(materialResource != nullptr);
        REQUIRE(materialResource->GetName() == "generic_material");
      }

      THEN(
          "The material is red, and specular color, ambient color and "
          "shininess are set to default values.") {
        auto diffuseColor = materialResource->GetDiffuseColor();
        REQUIRE(diffuseColor == glm::vec3(1.0, 0.0, 0.0));

        auto specularColor = materialResource->GetSpecularColor();
        auto specularColorDefault =
            phx::Material::GetDefault()->GetSpecularColor();
        REQUIRE(specularColor == specularColorDefault);

        auto ambientColor = materialResource->GetAmbientColor();
        auto ambientColorDefault =
            phx::Material::GetDefault()->GetAmbientColor();
        REQUIRE(ambientColor == ambientColorDefault);

        float shininess = materialResource->GetShininess();
        REQUIRE(shininess == 64.0f);  //  default shininess value
      }
    }

    WHEN(
        "We load a different, more exotic material with some arbitrary "
        "values") {
      auto secondMaterial = phx::ResourceUtils::LoadGenericMaterial(
          "generic_material_2", glm::vec3(0.4f, 0.3f, 0.55f),
          glm::vec3(0.48f, 0.42f, 0.88f), glm::vec3(0.11f, 0.94f, 0.73f), 40.f);

      THEN("We still get a material with the given name") {
        REQUIRE(secondMaterial != nullptr);
        REQUIRE(secondMaterial->GetName() == "generic_material_2");
      }

      THEN("All the values in the material are set to the right value.") {
        auto diffuseColor = secondMaterial->GetDiffuseColor();
        REQUIRE(diffuseColor == glm::vec3(0.4f, 0.3f, 0.55f));

        auto specularColor = secondMaterial->GetSpecularColor();
        REQUIRE(specularColor == glm::vec3(0.48f, 0.42f, 0.88f));

        auto ambientColor = secondMaterial->GetAmbientColor();
        REQUIRE(ambientColor == glm::vec3(0.11f, 0.94f, 0.73f));

        float shininess = secondMaterial->GetShininess();
        REQUIRE(shininess == 40.f);
      }
    }
  }
}

//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include <cstddef>

#include <memory>

#include "catch/catch.hpp"

#include "phx/suppress_warnings.hpp"

SUPPRESS_WARNINGS_BEGIN
#include "glm/glm.hpp"
SUPPRESS_WARNINGS_END

#include "phx/core/entity.hpp"
#include "phx/core/logger.hpp"
#include "phx/core/scene.hpp"
#include "phx/rendering/components/transform.hpp"

#include "test_utilities/glm_mat4.hpp"
#include "test_utilities/glm_quat.hpp"
#include "test_utilities/glm_vec.hpp"
#include "test_utilities/log_capture.hpp"

SCENARIO(
    "The transform component represents a 4x4 transformation matrix and "
    "provides functionality to translate, rotate and scale it.",
    "[phx][phx::Transform]") {
  GIVEN("A scene with an entity and a new transform component.") {
    auto scene = std::make_shared<phx::Scene>();
    auto entity = scene->CreateEntity();
    phx::Transform* transform = entity->AddComponent<phx::Transform>();

    WHEN("We query its local translation.") {
      auto translation = transform->GetLocalTranslation();
      THEN("It should be equal to the zero vector.") {
        REQUIRE(translation == glm::vec3());
      }
    }
    WHEN("We query its local rotation.") {
      auto rotation = transform->GetLocalRotation();
      THEN("It should be equal to the unit quaternion.") {
        REQUIRE(rotation == glm::quat());
      }
    }
    WHEN("We query its local rotation in Euler angles.") {
      auto rotation = transform->GetLocalRotationEuler();
      THEN("It should be equal to the zero vector.") {
        REQUIRE(rotation == glm::vec3());
      }
    }
    WHEN("We query its local scale.") {
      auto scale = transform->GetLocalScale();
      THEN("It should be equal to the unit vector.") {
        REQUIRE(scale == glm::vec3(1.0f));
      }
    }
    WHEN("We query its local transform matrix.") {
      auto matrix = transform->GetLocalMatrix();
      THEN("It should be equal to the identity matrix.") {
        REQUIRE(matrix == glm::mat4());
      }
    }

    WHEN("We set its local translation to [1.0, 2.0, 3.0].") {
      transform->SetLocalTranslation(glm::vec3(1.0f, 2.0f, 3.0f));
      THEN("The translation vector should be equal to [1.0, 2.0, 3.0].") {
        REQUIRE(transform->GetLocalTranslation() ==
                glm::vec3(1.0f, 2.0f, 3.0f));
      }
      THEN(
          "The transform matrix should be equal to the corresponding "
          "translation matrix.") {
        const glm::mat4 translation_matrix(1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f,
                                           0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f,
                                           1.0f, 2.0f, 3.0f, 1.0f);
        REQUIRE(transform->GetLocalMatrix() == translation_matrix);
      }
      WHEN("We translate it by [4.0, 5.0, 6.0].") {
        transform->Translate(glm::vec3(4.0f, 5.0f, 6.0f));
        THEN("The translation vector should be equal to [5.0, 7.0, 9.0].") {
          REQUIRE(transform->GetLocalTranslation() ==
                  glm::vec3(5.0f, 7.0f, 9.0f));
        }
        THEN(
            "The transform matrix should be equal to the corresponding "
            "translation matrix.") {
          const glm::mat4 translation_matrix(1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f,
                                             0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f,
                                             5.0f, 7.0f, 9.0f, 1.0f);
          REQUIRE(transform->GetLocalMatrix() == translation_matrix);
        }
      }
      WHEN("We reset it.") {
        transform->Reset();
        THEN(
            "The local translation vector should be equal to the zero "
            "vector.") {
          REQUIRE(transform->GetLocalTranslation() == glm::vec3());
        }
        THEN(
            "The local transform matrix should be equal to the identity "
            "matrix.") {
          REQUIRE(transform->GetLocalMatrix() == glm::mat4());
        }
      }
    }
    WHEN("We set its local rotation to [0.43967, 0.36042, 0.82236, 0.02225].") {
      transform->SetLocalRotation(
          glm::quat(0.43967f, 0.36042f, 0.82236f, 0.02225f));
      THEN(
          "The rotation vector be equal to [0.43967, 0.36042, 0.82236, "
          "0.02225].") {
        auto rotation = transform->GetLocalRotation();
        REQUIRE(test_utilities::Approx<glm::quat>(rotation) ==
                glm::quat(0.43967f, 0.36042f, 0.82236f, 0.02225f));
      }
      THEN(
          "The transform matrix should be equal to the corresponding "
          "rotation matrix.") {
        const glm::mat4 rotation_matrix(
            -0.35354f, 0.61235f, -0.70709f, 0.0f, 0.57322, 0.73920f, 0.35352f,
            0.0f, 0.73917f, -0.28033f, -0.61235f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f);
        REQUIRE(test_utilities::Approx<glm::mat4>(
                    transform->GetLocalMatrix()) == rotation_matrix);
      }
      WHEN("We reset it.") {
        transform->Reset();
        THEN("The local rotation should be equal to the identity quaternion.") {
          REQUIRE(transform->GetLocalRotation() == glm::quat());
        }
        THEN(
            "The local transform matrix should be equal to the identity "
            "matrix.") {
          REQUIRE(transform->GetLocalMatrix() == glm::mat4());
        }
      }
    }
    WHEN("We set its local rotation in Euler angles to [30.0, 45.0, 60.0].") {
      transform->SetLocalRotationEuler(glm::vec3(30.0f, 45.0f, 60.0f));
      THEN(
          "The local rotation vector should be equal to [30.0f, 45.0f, "
          "60.0f].") {
        auto rotation = transform->GetLocalRotationEuler();
        REQUIRE(test_utilities::Approx<glm::vec3>(rotation) ==
                glm::vec3(30.0f, 45.0f, 60.0f));
      }
      THEN(
          "The local transform matrix should be equal to the corresponding "
          "rotation matrix.") {
        const glm::mat4 rotation_matrix(
            0.35356f, 0.61237f, -0.70710f, 0.0f, -0.57322, 0.73919f, 0.35355f,
            0.0f, 0.73919f, 0.28033f, 0.61237f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f);
        REQUIRE(test_utilities::Approx<glm::mat4>(
                    transform->GetLocalMatrix()) == rotation_matrix);
      }
      WHEN("We rotate it in Euler angles by [5.0, 10.0, 15.0].") {
        transform->RotateEuler(glm::vec3(5.0f, 10.0f, 15.0f));
        THEN(
            "The local rotation vector should be equal to [51.351, 43.540, "
            "85.105].") {
          auto rotation = transform->GetLocalRotationEuler();
          REQUIRE(test_utilities::Approx<glm::vec3>(rotation) ==
                  glm::vec3(51.351f, 43.540f, 85.105f));
        }
        THEN(
            "The local transform matrix should be equal to the corresponding "
            "rotation matrix.") {
          const glm::mat4 rotation_matrix(0.06185f, 0.72225f, -0.68886f, 0.0f,
                                          -0.57637f, 0.58931f, 0.56613f, 0.0f,
                                          0.81484f, 0.36202f, 0.45274f, 0.0f,
                                          0.0f, 0.0f, 0.0f, 1.0f);
          REQUIRE(test_utilities::Approx<glm::mat4>(
                      transform->GetLocalMatrix()) == rotation_matrix);
        }
      }
      WHEN("We reset it.") {
        transform->Reset();
        THEN("The local rotation vector should be equal to the zero vector.") {
          REQUIRE(transform->GetLocalRotationEuler() == glm::vec3());
        }
        THEN(
            "The local transform matrix should be equal to the identity "
            "matrix.") {
          REQUIRE(transform->GetLocalMatrix() == glm::mat4());
        }
      }
    }
    WHEN("We set its local scale to [1.0, 2.0, 3.0].") {
      transform->SetLocalScale(glm::vec3(1.0f, 2.0f, 3.0f));
      THEN("The scale vector should be equal to [1.0, 2.0, 3.0].") {
        REQUIRE(transform->GetLocalScale() == glm::vec3(1.0f, 2.0f, 3.0f));
      }
      THEN(
          "The local transform matrix should be equal to the corresponding "
          "scale matrix.") {
        const glm::mat4 scale_matrix(1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 2.0f, 0.0f,
                                     0.0f, 0.0f, 0.0f, 3.0f, 0.0f, 0.0f, 0.0f,
                                     0.0f, 1.0f);
        REQUIRE(transform->GetLocalMatrix() == scale_matrix);
      }
      WHEN("We scale it by [4.0, 5.0, 6.0].") {
        transform->Scale(glm::vec3(4.0f, 5.0f, 6.0f));
        THEN("The local scale vector should be equal to [5.0, 7.0, 9.0].") {
          REQUIRE(transform->GetLocalScale() == glm::vec3(5.0f, 7.0f, 9.0f));
        }
        THEN(
            "The local transform matrix should be equal to the corresponding "
            "scale matrix.") {
          const glm::mat4 scale_matrix(5.0f, 0.0f, 0.0f, 0.0f, 0.0f, 7.0f, 0.0f,
                                       0.0f, 0.0f, 0.0f, 9.0f, 0.0f, 0.0f, 0.0f,
                                       0.0f, 1.0f);
          REQUIRE(transform->GetLocalMatrix() == scale_matrix);
        }
      }
      WHEN("We reset it.") {
        transform->Reset();
        THEN(
            "The local translation vector should be equal to the zero "
            "vector.") {
          REQUIRE(transform->GetLocalTranslation() == glm::vec3());
        }
        THEN(
            "The local transform matrix should be equal to the identity "
            "matrix.") {
          REQUIRE(transform->GetLocalMatrix() == glm::mat4());
        }
      }
    }
    WHEN("We look at [0.0, 0.0, -1.0] with up vector [0.0, 1.0, 0.0]") {
      transform->LookAt(glm::vec3(0.0, 0.0, -1.0), glm::vec3(0.0, 1.0, 0.0));
      THEN(
          "The local rotation quaternion should be equal to the identity "
          "quaternion.") {
        REQUIRE(transform->GetLocalRotation() == glm::quat());
      }
      THEN(
          "The local transform matrix should be equal to the identity "
          "matrix.") {
        REQUIRE(transform->GetLocalMatrix() == glm::mat4());
      }
    }
  }
}

SCENARIO(
    "The transform component enables hierarchical arrangement of transforms.",
    "[phx][phx::Transform]") {
  GIVEN("A hierarchy of transforms: A( B, C( D ) )") {
    auto scene = std::make_shared<phx::Scene>();
    auto entity1 = scene->CreateEntity();
    auto entity2 = scene->CreateEntity();
    auto entity3 = scene->CreateEntity();
    auto entity4 = scene->CreateEntity();
    phx::Transform* A = entity1->AddComponent<phx::Transform>();
    phx::Transform* B = entity2->AddComponent<phx::Transform>();
    phx::Transform* C = entity3->AddComponent<phx::Transform>();
    phx::Transform* D = entity4->AddComponent<phx::Transform>();
    B->SetParent(A);
    C->SetParent(A);
    D->SetParent(C);

    WHEN("We set A's local translation to (1, 0, 0).") {
      A->SetLocalTranslation(glm::vec3(1.f, 0.f, 0.f));
      THEN("The local translation of B, C and D is still (0, 0, 0).") {
        REQUIRE(B->GetLocalTranslation() == glm::vec3());
        REQUIRE(C->GetLocalTranslation() == glm::vec3());
        REQUIRE(D->GetLocalTranslation() == glm::vec3());
      }
      THEN("The global translation of B, C and D is (1, 0, 0).") {
        REQUIRE(B->GetGlobalTranslation() == glm::vec3(1.f, 0.f, 0.f));
        REQUIRE(C->GetGlobalTranslation() == glm::vec3(1.f, 0.f, 0.f));
        REQUIRE(D->GetGlobalTranslation() == glm::vec3(1.f, 0.f, 0.f));
      }
      THEN(
          "The global transform matrix of B, C and D should be equal to the "
          "corresponding translation matrix.") {
        const glm::mat4 trans_matrix(1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f,
                                     0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f,
                                     0.0f, 1.0f);
        REQUIRE(B->GetGlobalMatrix() == trans_matrix);
        REQUIRE(C->GetGlobalMatrix() == trans_matrix);
        REQUIRE(D->GetGlobalMatrix() == trans_matrix);
      }
      WHEN("We set C's global translation to (2, 1, 0).") {
        C->SetGlobalTranslation(glm::vec3(2.f, 1.f, 0.f));
        THEN("It's local translation is (1, 1, 0)") {
          REQUIRE(C->GetLocalTranslation() == glm::vec3(1.f, 1.f, 0.f));
        }
        THEN("B's global translation is still (1, 0, 0).") {
          REQUIRE(B->GetGlobalTranslation() == glm::vec3(1.f, 0.f, 0.f));
        }
        THEN("D's global translation is (2, 1, 0).") {
          REQUIRE(D->GetGlobalTranslation() == glm::vec3(2.f, 1.f, 0.f));
        }
      }
    }

    WHEN("We set A's local rotation to the (20, 30, 40) euler angles.") {
      A->SetLocalRotationEuler(glm::vec3(20.f, 30.f, 40.f));
      THEN("The local rotation of B, C and D is still (0, 0, 0).") {
        REQUIRE(B->GetLocalRotationEuler() == glm::vec3());
        REQUIRE(C->GetLocalRotationEuler() == glm::vec3());
        REQUIRE(D->GetLocalRotationEuler() == glm::vec3());
      }
      THEN("The global rotation of B, C and D is (20, 30, 40).") {
        REQUIRE(
            test_utilities::Approx<glm::quat>(B->GetGlobalRotationEuler()) ==
            glm::vec3(20.f, 30.f, 40.f));
        REQUIRE(
            test_utilities::Approx<glm::quat>(C->GetGlobalRotationEuler()) ==
            glm::vec3(20.f, 30.f, 40.f));
        REQUIRE(
            test_utilities::Approx<glm::quat>(D->GetGlobalRotationEuler()) ==
            glm::vec3(20.f, 30.f, 40.f));
      }
      THEN(
          "The global transform matrix of B, C and D should be equal to the "
          "corresponding rotation matrix.") {
        const glm::mat4 rot_matrix = glm::mat4_cast(
            glm::quat(glm::radians(glm::vec3(20.f, 30.f, 40.f))));
        REQUIRE(test_utilities::Approx<glm::mat4>(B->GetGlobalMatrix()) ==
                rot_matrix);
        REQUIRE(test_utilities::Approx<glm::mat4>(C->GetGlobalMatrix()) ==
                rot_matrix);
        REQUIRE(test_utilities::Approx<glm::mat4>(D->GetGlobalMatrix()) ==
                rot_matrix);
      }
      WHEN("We set C's global rotation to (20, 30, 50).") {
        C->SetGlobalRotationEuler(glm::vec3(20.f, 30.f, 50.f));
        THEN("B's global rotation is still (20, 30, 40).") {
          REQUIRE(
              test_utilities::Approx<glm::vec3>(B->GetGlobalRotationEuler()) ==
              glm::vec3(20.f, 30.f, 40.f));
        }
        THEN("D's global rotation is (20, 30, 50).") {
          REQUIRE(
              test_utilities::Approx<glm::vec3>(D->GetGlobalRotationEuler()) ==
              glm::vec3(20.f, 30.f, 50.f));
        }
      }
    }

    WHEN("We set A's local scale to (1, 2, 3).") {
      A->SetLocalScale(glm::vec3(1.f, 2.f, 3.f));
      THEN("The local scale of B, C and D is still (1, 1, 1).") {
        REQUIRE(B->GetLocalScale() == glm::vec3(1.f, 1.f, 1.f));
        REQUIRE(C->GetLocalScale() == glm::vec3(1.f, 1.f, 1.f));
        REQUIRE(D->GetLocalScale() == glm::vec3(1.f, 1.f, 1.f));
      }
      THEN("The global scale of B, C and D is (1, 2, 3).") {
        REQUIRE(B->GetGlobalScale() == glm::vec3(1.f, 2.f, 3.f));
        REQUIRE(C->GetGlobalScale() == glm::vec3(1.f, 2.f, 3.f));
        REQUIRE(D->GetGlobalScale() == glm::vec3(1.f, 2.f, 3.f));
      }
      THEN(
          "The global transform matrix of B, C and D should be equal to the "
          "corresponding scale matrix.") {
        const glm::mat4 scale_matrix(1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 2.0f, 0.0f,
                                     0.0f, 0.0f, 0.0f, 3.0f, 0.0f, 0.0f, 0.0f,
                                     0.0f, 1.0f);
        REQUIRE(B->GetGlobalMatrix() == scale_matrix);
        REQUIRE(C->GetGlobalMatrix() == scale_matrix);
        REQUIRE(D->GetGlobalMatrix() == scale_matrix);
      }
      WHEN("We set C's global scale to (2, 4, 6).") {
        C->SetGlobalScale(glm::vec3(2.f, 4.f, 6.f));
        THEN("It's local scale is (2, 2, 2)") {
          REQUIRE(C->GetLocalScale() == glm::vec3(2.f, 2.f, 2.f));
        }
        THEN("B's global scale is still (1, 2, 3).") {
          REQUIRE(B->GetGlobalScale() == glm::vec3(1.f, 2.f, 3.f));
        }
        THEN("D's global scale is (2, 4, 6).") {
          REQUIRE(D->GetGlobalScale() == glm::vec3(2.f, 4.f, 6.f));
        }
      }
    }

    WHEN("We set A's global translation to some arbitrary values.") {
      A->SetGlobalTranslation(glm::vec3(18.0f, 0.5f, 13.37f));
      A->SetGlobalRotationEuler(glm::vec3(12.f, 0.f, 42.f));
      A->SetGlobalScale(glm::vec3(1.f, 2.f, 4.f));
      THEN("The local matrix of B, C and D is still identity.") {
        REQUIRE(B->GetLocalMatrix() == glm::mat4());
        REQUIRE(C->GetLocalMatrix() == glm::mat4());
        REQUIRE(D->GetLocalMatrix() == glm::mat4());
      }
      THEN("The global matrix of B, C and D is the same as A's.") {
        REQUIRE(B->GetGlobalMatrix() == A->GetGlobalMatrix());
        REQUIRE(C->GetGlobalMatrix() == A->GetGlobalMatrix());
        REQUIRE(D->GetGlobalMatrix() == A->GetGlobalMatrix());
      }
      WHEN("We set C's global matrix to identity.") {
        C->SetGlobalMatrix(glm::mat4());
        THEN("It's local matrix is the inverse of A's.") {
          REQUIRE(C->GetLocalMatrix() == glm::inverse(A->GetLocalMatrix()));
        }
        THEN("B's global matrix is still the same as A's.") {
          REQUIRE(B->GetGlobalMatrix() == A->GetGlobalMatrix());
        }
        THEN("D's global matrix is identity.") {
          REQUIRE(test_utilities::Approx<glm::mat4>(D->GetGlobalMatrix()) ==
                  glm::mat4());
        }

        WHEN(
            "We set C's parent to bue nullptr, maintaining its global "
            "position.") {
          C->SetParent(nullptr);
          THEN("C's global and local matrices are identity.") {
            REQUIRE(test_utilities::Approx<glm::mat4>(C->GetLocalMatrix()) ==
                    glm::mat4());
            REQUIRE(test_utilities::Approx<glm::mat4>(C->GetGlobalMatrix()) ==
                    glm::mat4());
          }
          THEN("D's global and local matrices are identity.") {
            REQUIRE(test_utilities::Approx<glm::mat4>(D->GetLocalMatrix()) ==
                    glm::mat4());
            REQUIRE(test_utilities::Approx<glm::mat4>(D->GetGlobalMatrix()) ==
                    glm::mat4());
          }
        }

        WHEN(
            "We set C's parent to bue nullptr, not maintaining its global "
            "position.") {
          C->SetParent(nullptr, false);
          THEN(
              "C's global and local matrices are the inverse of A's global "
              "matrix.") {
            REQUIRE(test_utilities::Approx<glm::mat4>(C->GetLocalMatrix()) ==
                    glm::inverse(A->GetGlobalMatrix()));
            REQUIRE(test_utilities::Approx<glm::mat4>(C->GetGlobalMatrix()) ==
                    glm::inverse(A->GetGlobalMatrix()));
          }
          THEN("D's global matrix is the inverse of A's global matrix.") {
            REQUIRE(test_utilities::Approx<glm::mat4>(D->GetGlobalMatrix()) ==
                    glm::inverse(A->GetGlobalMatrix()));
          }
        }
      }
    }
  }
}

SCENARIO("The transform component checks if its parent is in the same scene.",
         "[phx][phx::Transform]") {
  GIVEN(
      "A scene s1 with two entities e1, e2 with transform components t1, t2.") {
    auto s1 = phx::Scene();
    auto e1 = s1.CreateEntity();
    auto t1 = e1->AddComponent<phx::Transform>();
    auto e2 = s1.CreateEntity();
    auto t2 = e2->AddComponent<phx::Transform>();

    WHEN("We set t2 to be the parent of t1.") {
      t1->SetParent(t2);
      THEN("t2 is the parent of t1.") {
        REQUIRE(t1->GetParent() == t2);
        REQUIRE(t2->GetChild(0) == t1);
      }
    }

    GIVEN("A second scene s2 with an entity e3 with a transform t3.") {
      auto s2 = phx::Scene();
      auto e3 = s2.CreateEntity();
      auto t3 = e3->AddComponent<phx::Transform>();

      WHEN("We set t3 to be the parent of t1.") {
        auto log_capture = std::make_shared<test_utilities::LogCapture>();
        phx::logger =
            std::make_shared<spdlog::logger>("logcapture", log_capture);
        phx::logger->set_pattern("%v");

        t1->SetParent(t3);
        THEN("An error is logged.") {
          REQUIRE(*log_capture ==
                  "The transform to be set as parent is not within the same "
                  "scene.");
        }
        THEN("t3 is not the parent of t1.") {
          REQUIRE(t1->GetParent() != t3);
          REQUIRE(t3->GetChildCount() == 0);
        }
      }
    }
  }
}

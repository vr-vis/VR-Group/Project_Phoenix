//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef TESTS_SRC_MOCKS_OPENVR_MOCK_HPP_
#define TESTS_SRC_MOCKS_OPENVR_MOCK_HPP_

#include <string>

#include "phx/suppress_warnings.hpp"

SUPPRESS_WARNINGS_BEGIN
#ifdef OPENVR_MOCK_BUILD
#define VR_API_EXPORT
#endif
#include "openvr.h"  //NOLINT

#include "trompeloeil.hpp"
SUPPRESS_WARNINGS_END

#include "mocks/openvr_mock_export.hpp"

using trompeloeil::_;

class OPENVR_MOCK_EXPORT OpenVRMockInternal {
 public:
  MAKE_MOCK0(VR_IsHmdPresent, bool());
  MAKE_MOCK2(VR_GetGenericInterface, void*(const char*, vr::EVRInitError*));
  MAKE_MOCK3(VR_InitInternal2,
             uint32_t(vr::EVRInitError*, vr::EVRApplicationType, const char*));
  MAKE_MOCK0(VR_ShutdownInternal, void());
  MAKE_MOCK1(VR_IsInterfaceVersionValid, bool(const char*));
  MAKE_MOCK0(VR_GetInitToken, uint32_t());
};

namespace vr {

class OPENVR_MOCK_EXPORT IVRSystemMock : public IVRSystem {
 public:
  virtual ~IVRSystemMock() = default;
  MAKE_MOCK2(GetRecommendedRenderTargetSize, void(uint32_t*, uint32_t*));
  MAKE_MOCK5(GetProjectionRaw, void(EVREye, float*, float*, float*, float*));
  MAKE_MOCK4(ComputeDistortion,
             bool(EVREye, float, float, DistortionCoordinates_t*));
  MAKE_MOCK2(GetTimeSinceLastVsync, bool(float*, uint64_t*));
  MAKE_MOCK0(GetD3D9AdapterIndex, int32_t());
  MAKE_MOCK1(GetDXGIOutputInfo, void(int32_t*));
  MAKE_MOCK3(GetOutputDevice, void(uint64_t*, ETextureType, VkInstance_T*));
  MAKE_MOCK0(IsDisplayOnDesktop, bool());
  MAKE_MOCK1(SetDisplayVisibility, bool(bool));
  MAKE_MOCK4(GetDeviceToAbsoluteTrackingPose,
             void(ETrackingUniverseOrigin, float, TrackedDevicePose_t*,
                  uint32_t));
  MAKE_MOCK0(ResetSeatedZeroPose, void());
  MAKE_MOCK0(GetSeatedZeroPoseToStandingAbsoluteTrackingPose, HmdMatrix34_t());
  MAKE_MOCK0(GetRawZeroPoseToStandingAbsoluteTrackingPose, HmdMatrix34_t());
  MAKE_MOCK4(GetSortedTrackedDeviceIndicesOfClass,
             uint32_t(ETrackedDeviceClass, TrackedDeviceIndex_t*, uint32_t,
                      TrackedDeviceIndex_t));
  MAKE_MOCK1(GetTrackedDeviceActivityLevel,
             EDeviceActivityLevel(TrackedDeviceIndex_t));
  MAKE_MOCK3(ApplyTransform,
             void(TrackedDevicePose_t*, const TrackedDevicePose_t*,
                  const HmdMatrix34_t*));
  MAKE_MOCK1(GetTrackedDeviceIndexForControllerRole,
             TrackedDeviceIndex_t(vr::ETrackedControllerRole));
  MAKE_MOCK1(GetControllerRoleForTrackedDeviceIndex,
             ETrackedControllerRole(vr::TrackedDeviceIndex_t));
  MAKE_MOCK1(GetTrackedDeviceClass,
             ETrackedDeviceClass(vr::TrackedDeviceIndex_t));
  MAKE_MOCK1(IsTrackedDeviceConnected, bool(vr::TrackedDeviceIndex_t));
  MAKE_MOCK3(GetBoolTrackedDeviceProperty,
             bool(vr::TrackedDeviceIndex_t, ETrackedDeviceProperty,
                  ETrackedPropertyError*));
  MAKE_MOCK3(GetFloatTrackedDeviceProperty,
             float(vr::TrackedDeviceIndex_t, ETrackedDeviceProperty,
                   ETrackedPropertyError*));
  MAKE_MOCK3(GetInt32TrackedDeviceProperty,
             int32_t(vr::TrackedDeviceIndex_t, ETrackedDeviceProperty,
                     ETrackedPropertyError*));
  MAKE_MOCK3(GetUint64TrackedDeviceProperty,
             uint64_t(vr::TrackedDeviceIndex_t, ETrackedDeviceProperty,
                      ETrackedPropertyError*));
  MAKE_MOCK3(GetMatrix34TrackedDeviceProperty,
             HmdMatrix34_t(vr::TrackedDeviceIndex_t, ETrackedDeviceProperty,
                           ETrackedPropertyError*));
  MAKE_MOCK5(GetStringTrackedDeviceProperty,
             uint32_t(vr::TrackedDeviceIndex_t, ETrackedDeviceProperty, char*,
                      uint32_t, ETrackedPropertyError*));
  MAKE_MOCK1(GetPropErrorNameFromEnum, const char*(ETrackedPropertyError));
  MAKE_MOCK2(PollNextEvent, bool(VREvent_t*, uint32_t));
  MAKE_MOCK4(PollNextEventWithPose, bool(ETrackingUniverseOrigin, VREvent_t*,
                                         uint32_t, vr::TrackedDevicePose_t*));
  MAKE_MOCK1(GetEventTypeNameFromEnum, const char*(EVREventType));
  MAKE_MOCK2(GetHiddenAreaMesh, HiddenAreaMesh_t(EVREye, EHiddenAreaMeshType));
  MAKE_MOCK3(GetControllerState, bool(vr::TrackedDeviceIndex_t,
                                      vr::VRControllerState_t*, uint32_t));
  MAKE_MOCK5(GetControllerStateWithPose,
             bool(vr::ETrackingUniverseOrigin, vr::TrackedDeviceIndex_t,
                  vr::VRControllerState_t*, uint32_t,
                  vr::TrackedDevicePose_t*));
  MAKE_MOCK3(TriggerHapticPulse,
             void(vr::TrackedDeviceIndex_t, uint32_t, unsigned short));
  MAKE_MOCK1(GetButtonIdNameFromEnum, const char*(EVRButtonId));
  MAKE_MOCK1(GetControllerAxisTypeNameFromEnum,
             const char*(EVRControllerAxisType));
  MAKE_MOCK0(CaptureInputFocus, bool());
  MAKE_MOCK0(ReleaseInputFocus, void());
  MAKE_MOCK0(IsInputFocusCapturedByAnotherProcess, bool());
  MAKE_MOCK4(DriverDebugRequest,
             uint32_t(vr::TrackedDeviceIndex_t, const char*, char*, uint32_t));
  MAKE_MOCK1(PerformFirmwareUpdate,
             vr::EVRFirmwareError(vr::TrackedDeviceIndex_t));
  MAKE_MOCK0(AcknowledgeQuit_Exiting, void());
  MAKE_MOCK0(AcknowledgeQuit_UserPrompt, void());

  // somehow returning structs is a problem, so this workaround
  // TODO(WJ): should be reworked once this is solved in trompeloeil
  // https://github.com/rollbear/trompeloeil/issues/69
  MAKE_MOCK3(GetProjectionMatrixArray, float*(vr::EVREye, float, float));
  vr::HmdMatrix44_t GetProjectionMatrix(vr::EVREye, float, float);
  MAKE_MOCK1(GetEyeToHeadTransformArray, float*(EVREye));
  HmdMatrix34_t GetEyeToHeadTransform(EVREye);
  MAKE_MOCK6(GetArrayTrackedDeviceProperty,
             uint32_t(vr::TrackedDeviceIndex_t, ETrackedDeviceProperty,
                      PropertyTypeTag_t, void*, uint32_t,
                      ETrackedPropertyError*));
  MAKE_MOCK0(IsInputAvailable, bool());
  MAKE_MOCK0(IsSteamVRDrawingControllers, bool());
  MAKE_MOCK0(ShouldApplicationPause, bool());
  MAKE_MOCK0(ShouldApplicationReduceRenderingWork, bool());
};

class OPENVR_MOCK_EXPORT IVRCompositorMock : public IVRCompositor {
 public:
  virtual ~IVRCompositorMock() = default;
  MAKE_MOCK1(SetTrackingSpace, void(ETrackingUniverseOrigin));
  MAKE_MOCK0(GetTrackingSpace, ETrackingUniverseOrigin());
  MAKE_MOCK4(WaitGetPoses, EVRCompositorError(TrackedDevicePose_t*, uint32_t,
                                              TrackedDevicePose_t*, uint32_t));
  MAKE_MOCK4(GetLastPoses, EVRCompositorError(TrackedDevicePose_t*, uint32_t,
                                              TrackedDevicePose_t*, uint32_t));
  MAKE_MOCK3(GetLastPoseForTrackedDeviceIndex,
             EVRCompositorError(TrackedDeviceIndex_t, TrackedDevicePose_t*,
                                TrackedDevicePose_t*));
  MAKE_MOCK4(Submit,
             EVRCompositorError(EVREye, const Texture_t*,
                                const VRTextureBounds_t*, EVRSubmitFlags));
  MAKE_MOCK0(ClearLastSubmittedFrame, void());
  MAKE_MOCK0(PostPresentHandoff, void());
  MAKE_MOCK2(GetFrameTiming, bool(Compositor_FrameTiming*, uint32_t));
  MAKE_MOCK2(GetFrameTimings, uint32_t(Compositor_FrameTiming*, uint32_t));
  MAKE_MOCK0(GetFrameTimeRemaining, float());
  MAKE_MOCK2(GetCumulativeStats, void(Compositor_CumulativeStats*, uint32_t));
  MAKE_MOCK6(FadeToColor, void(float, float, float, float, float, bool));
  MAKE_MOCK1(GetCurrentFadeColor, HmdColor_t(bool));
  MAKE_MOCK2(FadeGrid, void(float, bool));
  MAKE_MOCK0(GetCurrentGridAlpha, float());
  MAKE_MOCK2(SetSkyboxOverride, EVRCompositorError(const Texture_t*, uint32_t));
  MAKE_MOCK0(ClearSkyboxOverride, void());
  MAKE_MOCK0(CompositorBringToFront, void());
  MAKE_MOCK0(CompositorGoToBack, void());
  MAKE_MOCK0(CompositorQuit, void());
  MAKE_MOCK0(IsFullscreen, bool());
  MAKE_MOCK0(GetCurrentSceneFocusProcess, uint32_t());
  MAKE_MOCK0(GetLastFrameRenderer, uint32_t());
  MAKE_MOCK0(CanRenderScene, bool());
  MAKE_MOCK0(ShowMirrorWindow, void());
  MAKE_MOCK0(HideMirrorWindow, void());
  MAKE_MOCK0(IsMirrorWindowVisible, bool());
  MAKE_MOCK0(CompositorDumpImages, void());
  MAKE_MOCK0(ShouldAppRenderWithLowResources, bool());
  MAKE_MOCK1(ForceInterleavedReprojectionOn, void(bool));
  MAKE_MOCK0(ForceReconnectProcess, void());
  MAKE_MOCK1(SuspendRendering, void(bool));
  MAKE_MOCK3(GetMirrorTextureD3D11,
             EVRCompositorError(vr::EVREye, void*, void**));
  MAKE_MOCK1(ReleaseMirrorTextureD3D11, void(void*));
  MAKE_MOCK3(GetMirrorTextureGL,
             vr::EVRCompositorError(vr::EVREye, vr::glUInt_t*,
                                    vr::glSharedTextureHandle_t*));
  MAKE_MOCK2(ReleaseSharedGLTexture,
             bool(vr::glUInt_t, vr::glSharedTextureHandle_t));
  MAKE_MOCK1(LockGLSharedTextureForAccess, void(vr::glSharedTextureHandle_t));
  MAKE_MOCK1(UnlockGLSharedTextureForAccess, void(vr::glSharedTextureHandle_t));
  MAKE_MOCK2(GetVulkanInstanceExtensionsRequired, uint32_t(char*, uint32_t));
  MAKE_MOCK3(GetVulkanDeviceExtensionsRequired,
             uint32_t(VkPhysicalDevice_T*, char*, uint32_t));
  MAKE_MOCK1(SetExplicitTimingMode, void(bool));
  MAKE_MOCK0(SubmitExplicitTimingData, EVRCompositorError());
  MAKE_MOCK1(SetExplicitTimingMode, void(EVRCompositorTimingMode));
};

class OPENVR_MOCK_EXPORT IVRRenderModelsMock : public IVRRenderModels {
 public:
  virtual ~IVRRenderModelsMock() = default;
  MAKE_MOCK2(LoadRenderModel_Async,
             EVRRenderModelError(const char*, RenderModel_t**));
  MAKE_MOCK1(FreeRenderModel, void(RenderModel_t*));
  MAKE_MOCK2(LoadTexture_Async,
             EVRRenderModelError(TextureID_t, RenderModel_TextureMap_t**));
  MAKE_MOCK1(FreeTexture, void(RenderModel_TextureMap_t*));
  MAKE_MOCK3(LoadTextureD3D11_Async,
             EVRRenderModelError(TextureID_t, void*, void**));
  MAKE_MOCK2(LoadIntoTextureD3D11_Async,
             EVRRenderModelError(TextureID_t, void*));
  MAKE_MOCK1(FreeTextureD3D11, void(void*));
  MAKE_MOCK3(GetRenderModelName,
             uint32_t(uint32_t, VR_OUT_STRING() char*, uint32_t));
  MAKE_MOCK0(GetRenderModelCount, uint32_t());
  MAKE_MOCK1(GetComponentCount, uint32_t(const char*));
  MAKE_MOCK4(GetComponentName,
             uint32_t(const char*, uint32_t, VR_OUT_STRING() char*, uint32_t));

  MAKE_MOCK2(GetComponentButtonMask, uint64_t(const char*, const char*));
  MAKE_MOCK4(GetComponentRenderModelName,
             uint32_t(const char*, const char*, VR_OUT_STRING() char*,
                      uint32_t));
  MAKE_MOCK5(GetComponentState,
             bool(const char*, const char*, const vr::VRControllerState_t*,
                  const RenderModel_ControllerMode_State_t*,
                  RenderModel_ComponentState_t*));

  MAKE_MOCK2(RenderModelHasComponent, bool(const char*, const char*));
  MAKE_MOCK4(GetRenderModelThumbnailURL,
             uint32_t(const char*, VR_OUT_STRING() char*, uint32_t,
                      vr::EVRRenderModelError*));
  MAKE_MOCK4(GetRenderModelOriginalPath,
             uint32_t(const char*, VR_OUT_STRING() char*, uint32_t,
                      vr::EVRRenderModelError*));
  MAKE_MOCK1(GetRenderModelErrorNameFromEnum,
             const char*(vr::EVRRenderModelError));
};

}  // namespace vr

class OPENVR_MOCK_EXPORT OpenVRMock {
 public:
  OpenVRMock()
      : mock_(new OpenVRMockInternal),
        ivr_system_mock_(new vr::IVRSystemMock),
        ivr_compositor_mock_(new vr::IVRCompositorMock),
        ivr_render_models_mock_(new vr::IVRRenderModelsMock) {}
  OpenVRMock(const OpenVRMock&) = delete;
  OpenVRMock(OpenVRMock&&) = default;
  ~OpenVRMock() {
    delete mock_;
    delete ivr_system_mock_;
    delete ivr_compositor_mock_;
    delete ivr_render_models_mock_;
  }

  OpenVRMock& operator=(const OpenVRMock&) = delete;
  OpenVRMock& operator=(OpenVRMock&&) = delete;

  OpenVRMockInternal& Get() { return *mock_; }
  vr::IVRSystemMock& GetSystem() const { return *ivr_system_mock_; }
  vr::IVRCompositorMock& GetCompositor() { return *ivr_compositor_mock_; }
  vr::IVRRenderModelsMock& GetRenderModels() {
    return *ivr_render_models_mock_;
  }

  vr::RenderModel_t* CreateDummyTriangleModel(unsigned int id) {
    vr::RenderModel_t* model = new vr::RenderModel_t();

    model->rIndexData = &model_indices_[0];
    model->unVertexCount = 3;
    model->unTriangleCount = 1;
    model->diffuseTextureId = static_cast<int>(id);

    model_vertices_[0].vPosition = {{-0.1f, -0.1f, 0.f}};
    model_vertices_[0].vNormal = {{0.f, 0.f, 1.f}};
    model_vertices_[0].rfTextureCoord[0] = 0.f;
    model_vertices_[0].rfTextureCoord[1] = 0.f;

    model_vertices_[1].vPosition = {{+0.1f, -0.1f, 0.f}};
    model_vertices_[1].vNormal = {{0.f, 0.f, 1.f}};
    model_vertices_[1].rfTextureCoord[0] = 1.f;
    model_vertices_[1].rfTextureCoord[1] = 0.f;

    model_vertices_[2].vPosition = {{0.f, +0.1f, 0.f}};
    model_vertices_[2].vNormal = {{0.f, 0.f, 1.f}};
    model_vertices_[2].rfTextureCoord[0] = 0.5f;
    model_vertices_[2].rfTextureCoord[1] = 1.f;

    model->rVertexData = &model_vertices_[0];

    return model;
  }

  vr::RenderModel_TextureMap_t* CreateDummyTexture(unsigned int id) {
    vr::RenderModel_TextureMap_t* texture = new vr::RenderModel_TextureMap_t();
    texture->unHeight = 1u;
    texture->unWidth = 1u;
    if (id == 1u)
      texture->rubTextureMapData = &green_pixel_[0];
    else
      texture->rubTextureMapData = &blue_pixel_[0];
    return texture;
  }

  static vr::HmdMatrix44_t toHMDMatrix44_t(float mat[16]);
  static vr::HmdMatrix34_t toHMDMatrix34_t(float mat[12]);

  // Matrix4x4
  float projection_matrix_[16] = {1.483f, 0.0f, 0.0f,  0.0f, 0.0f,  1.483f,
                                  0.0f,   0.0f, 0.0f,  0.0f, -1.0f, -0.02f,
                                  0.0f,   0.0f, -1.0f, 0.0f};
  // Matrix 3x4
  float eye_to_head_left_[12] = {1.0f, 0.0f, 0.0f, 0.03f, 0.0f, 1.0f,
                                 0.0f, 0.0f, 0.0f, 0.0f,  1.0f, 0.0f};
  float eye_to_head_right_[12] = {1.0f, 0.0f, 0.0f, -0.03f, 0.0f, 1.0f,
                                  0.0f, 0.0f, 0.0f, 0.0f,   1.0f, 0.0f};
  float head_transformation_[12] = {1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f,
                                    0.0f, 1.8f, 0.0f, 0.0f, 1.0f, 0.0f};
  float l_cont_transformation_[12] = {1.0f, 0.0f, 0.0f, -0.2f, 0.0f, 1.0f,
                                      0.0f, 1.6f, 0.0f, 0.0f,  1.0f, -0.6f};
  float r_cont_transformation_[12] = {1.0f, 0.0f, 0.0f, 0.2f, 0.0f, 1.0f,
                                      0.0f, 1.6f, 0.0f, 0.0f, 1.0f, -0.6f};

  unsigned int index_right_controller_ = 1u;
  unsigned int index_left_controller_ = 2u;

  char name_left_controller_[vr::k_unMaxPropertyStringSize] = "controller_left";
  char name_right_controller_[vr::k_unMaxPropertyStringSize] =
      "controller_right";

  uint8_t green_pixel_[4] = {0, 255, 0, 255};
  uint8_t blue_pixel_[4] = {0, 0, 255, 255};

  uint16_t model_indices_[3] = {0, 1, 2};
  vr::RenderModel_Vertex_t model_vertices_[3];

 private:
  OpenVRMockInternal* mock_;
  vr::IVRSystemMock* ivr_system_mock_;
  vr::IVRCompositorMock* ivr_compositor_mock_;
  vr::IVRRenderModelsMock* ivr_render_models_mock_;
};

extern OPENVR_MOCK_EXPORT OpenVRMock openvr_mock;

#define OPENVR_MOCK_ALLOW_ANY_CALL                                             \
  ALLOW_CALL(openvr_mock.Get(), VR_IsHmdPresent()).RETURN(true);               \
  ALLOW_CALL(openvr_mock.GetSystem(), GetRecommendedRenderTargetSize(_, _))    \
      .SIDE_EFFECT(*_1 = 1000)                                                 \
      .SIDE_EFFECT(*_2 = 1000);                                                \
  ALLOW_CALL(openvr_mock.GetSystem(), GetProjectionMatrixArray(_, _, _))       \
      .RETURN(openvr_mock.projection_matrix_);                                 \
  ALLOW_CALL(openvr_mock.GetSystem(),                                          \
             GetEyeToHeadTransformArray(vr::EVREye::Eye_Left))                 \
      .RETURN(openvr_mock.eye_to_head_left_);                                  \
  ALLOW_CALL(openvr_mock.GetSystem(),                                          \
             GetEyeToHeadTransformArray(vr::EVREye::Eye_Right))                \
      .RETURN(openvr_mock.eye_to_head_right_);                                 \
  ALLOW_CALL(openvr_mock.GetSystem(),                                          \
             GetSortedTrackedDeviceIndicesOfClass(_, _, _, _))                 \
      .RETURN(2u)                                                              \
      .SIDE_EFFECT(*_2 = openvr_mock.index_right_controller_)                  \
      .SIDE_EFFECT(*(_2 + 1) = openvr_mock.index_left_controller_);            \
  ALLOW_CALL(openvr_mock.GetSystem(), GetControllerRoleForTrackedDeviceIndex(  \
                                          openvr_mock.index_left_controller_)) \
      .RETURN(vr::TrackedControllerRole_LeftHand);                             \
  ALLOW_CALL(openvr_mock.GetSystem(),                                          \
             GetControllerRoleForTrackedDeviceIndex(                           \
                 openvr_mock.index_right_controller_))                         \
      .RETURN(vr::TrackedControllerRole_RightHand);                            \
  ALLOW_CALL(openvr_mock.GetSystem(), GetControllerRoleForTrackedDeviceIndex(  \
                                          vr::k_unTrackedDeviceIndex_Hmd))     \
      .RETURN(vr::TrackedControllerRole_Invalid);                              \
  ALLOW_CALL(openvr_mock.GetCompositor(), WaitGetPoses(_, _, _, _))            \
      .RETURN(vr::EVRCompositorError::VRCompositorError_None)                  \
      .SIDE_EFFECT(                                                            \
          _1[vr::k_unTrackedDeviceIndex_Hmd].mDeviceToAbsoluteTracking =       \
              OpenVRMock::toHMDMatrix34_t(openvr_mock.head_transformation_))   \
      .SIDE_EFFECT(_1[vr::k_unTrackedDeviceIndex_Hmd].bPoseIsValid = true)     \
      .SIDE_EFFECT(                                                            \
          _1[openvr_mock.index_right_controller_].mDeviceToAbsoluteTracking =  \
              OpenVRMock::toHMDMatrix34_t(openvr_mock.r_cont_transformation_)) \
      .SIDE_EFFECT(_1[openvr_mock.index_right_controller_].bPoseIsValid =      \
                       true)                                                   \
      .SIDE_EFFECT(                                                            \
          _1[openvr_mock.index_left_controller_].mDeviceToAbsoluteTracking =   \
              OpenVRMock::toHMDMatrix34_t(openvr_mock.l_cont_transformation_)) \
      .SIDE_EFFECT(_1[openvr_mock.index_left_controller_].bPoseIsValid =       \
                       true);                                                  \
  ALLOW_CALL(openvr_mock.GetSystem(),                                          \
             GetControllerStateWithPose(vr::TrackingUniverseStanding,          \
                                        openvr_mock.index_right_controller_,   \
                                        _, _, _))                              \
      .RETURN(true)                                                            \
      .SIDE_EFFECT(                                                            \
          _5->mDeviceToAbsoluteTracking =                                      \
              OpenVRMock::toHMDMatrix34_t(openvr_mock.r_cont_transformation_)) \
      .SIDE_EFFECT(_5->bPoseIsValid = true);                                   \
  ALLOW_CALL(                                                                  \
      openvr_mock.GetSystem(),                                                 \
      GetControllerStateWithPose(vr::TrackingUniverseStanding,                 \
                                 openvr_mock.index_left_controller_, _, _, _)) \
      .RETURN(true)                                                            \
      .SIDE_EFFECT(                                                            \
          _5->mDeviceToAbsoluteTracking =                                      \
              OpenVRMock::toHMDMatrix34_t(openvr_mock.l_cont_transformation_)) \
      .SIDE_EFFECT(_5->bPoseIsValid = true);                                   \
  ALLOW_CALL(openvr_mock.GetCompositor(), Submit(_, _, _, _))                  \
      .RETURN(vr::EVRCompositorError::VRCompositorError_None);                 \
  ALLOW_CALL(openvr_mock.Get(), VR_GetGenericInterface(_, _))                  \
      .WITH(std::string(vr::IVRSystem_Version) == std::string(_1))             \
      .RETURN(static_cast<void*>(&openvr_mock.GetSystem()));                   \
  ALLOW_CALL(openvr_mock.Get(), VR_GetGenericInterface(_, _))                  \
      .WITH(std::string(vr::IVRRenderModels_Version) == std::string(_1))       \
      .RETURN(static_cast<void*>(&openvr_mock.GetRenderModels()));             \
  ALLOW_CALL(openvr_mock.Get(), VR_GetGenericInterface(_, _))                  \
      .WITH(std::string(vr::IVRCompositor_Version) == std::string(_1))         \
      .RETURN(static_cast<void*>(&openvr_mock.GetCompositor()));               \
  ALLOW_CALL(openvr_mock.Get(),                                                \
             VR_InitInternal2(_, vr::VRApplication_Scene, _))                  \
      .RETURN(static_cast<uint32_t>(1337))                                     \
      .SIDE_EFFECT(*_1 = vr::VRInitError_None);                                \
  ALLOW_CALL(openvr_mock.Get(), VR_ShutdownInternal());                        \
  ALLOW_CALL(openvr_mock.Get(), VR_IsInterfaceVersionValid(_)).RETURN(true);   \
  ALLOW_CALL(openvr_mock.Get(), VR_GetInitToken()).RETURN(1337);               \
  ALLOW_CALL(openvr_mock.GetRenderModels(), LoadRenderModel_Async(_, _))       \
      .WITH(std::string(_1) == std::string(openvr_mock.name_left_controller_)) \
      .SIDE_EFFECT(*_2 = openvr_mock.CreateDummyTriangleModel(                 \
                       openvr_mock.index_left_controller_))                    \
      .RETURN(vr::VRRenderModelError_None);                                    \
  ALLOW_CALL(openvr_mock.GetRenderModels(), LoadRenderModel_Async(_, _))       \
      .WITH(std::string(_1) ==                                                 \
            std::string(openvr_mock.name_right_controller_))                   \
      .SIDE_EFFECT(*_2 = openvr_mock.CreateDummyTriangleModel(                 \
                       openvr_mock.index_right_controller_))                   \
      .RETURN(vr::VRRenderModelError_None);                                    \
  ALLOW_CALL(openvr_mock.GetSystem(),                                          \
             GetStringTrackedDeviceProperty(                                   \
                 openvr_mock.index_left_controller_,                           \
                 vr::Prop_RenderModelName_String, _, _, _))                    \
      .SIDE_EFFECT(std::string(openvr_mock.name_left_controller_)              \
                       .copy(_3, vr::k_unMaxPropertyStringSize))               \
      .RETURN(vr::k_unMaxPropertyStringSize);                                  \
  ALLOW_CALL(openvr_mock.GetSystem(),                                          \
             GetStringTrackedDeviceProperty(                                   \
                 openvr_mock.index_right_controller_,                          \
                 vr::Prop_RenderModelName_String, _, _, _))                    \
      .SIDE_EFFECT(std::string(openvr_mock.name_right_controller_)             \
                       .copy(_3, vr::k_unMaxPropertyStringSize))               \
      .RETURN(vr::k_unMaxPropertyStringSize);                                  \
  ALLOW_CALL(openvr_mock.GetRenderModels(), LoadTexture_Async(_, _))           \
      .RETURN(vr::VRRenderModelError_None)                                     \
      .SIDE_EFFECT(*_2 = openvr_mock.CreateDummyTexture(                       \
                       static_cast<unsigned int>(_1)));                        \
  ALLOW_CALL(openvr_mock.GetSystem(), PollNextEvent(_, _)).RETURN(false);      \
  ALLOW_CALL(openvr_mock.GetSystem(),                                          \
             GetInt32TrackedDeviceProperty(_, vr::Prop_Axis0Type_Int32, _))    \
      .RETURN(vr::k_eControllerAxis_None);                                     \
  ALLOW_CALL(openvr_mock.GetSystem(),                                          \
             GetInt32TrackedDeviceProperty(_, vr::Prop_Axis1Type_Int32, _))    \
      .RETURN(vr::k_eControllerAxis_None);                                     \
  ALLOW_CALL(openvr_mock.GetSystem(),                                          \
             GetInt32TrackedDeviceProperty(_, vr::Prop_Axis2Type_Int32, _))    \
      .RETURN(vr::k_eControllerAxis_None);                                     \
  ALLOW_CALL(openvr_mock.GetSystem(),                                          \
             GetInt32TrackedDeviceProperty(_, vr::Prop_Axis3Type_Int32, _))    \
      .RETURN(vr::k_eControllerAxis_None);                                     \
  ALLOW_CALL(openvr_mock.GetSystem(),                                          \
             GetInt32TrackedDeviceProperty(_, vr::Prop_Axis4Type_Int32, _))    \
      .RETURN(vr::k_eControllerAxis_None);

#endif  // TESTS_SRC_MOCKS_OPENVR_MOCK_HPP_

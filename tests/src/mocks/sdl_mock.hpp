//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef TESTS_SRC_MOCKS_SDL_MOCK_HPP_
#define TESTS_SRC_MOCKS_SDL_MOCK_HPP_

#include "phx/suppress_warnings.hpp"

SUPPRESS_WARNINGS_BEGIN
#include "SDL.h"
SUPPRESS_WARNINGS_END

#include "trompeloeil.hpp"

#include "mocks/sdl_mock_export.hpp"

class SDL_MOCK_EXPORT SdlMockInternal {
 public:
  MAKE_MOCK1(SDL_Init, int(Uint32 flags));  // NOLINT(readability/casting)
  MAKE_MOCK1(SDL_InitSubSystem,
             int(Uint32 flags));  // NOLINT(readability/casting)
  MAKE_MOCK1(SDL_VideoInit, int(const char* driver));
  MAKE_MOCK0(SDL_VideoQuit, void());
  MAKE_MOCK0(SDL_GetError, const char*());
  MAKE_MOCK2(SDL_GL_SetAttribute,
             int(SDL_GLattr, int));  // NOLINT(readability/casting)
  MAKE_MOCK6(SDL_CreateWindow,
             SDL_Window*(const char*, int, int, int, int, Uint32));
  MAKE_MOCK1(SDL_GL_CreateContext, SDL_GLContext(SDL_Window*));
  MAKE_MOCK1(SDL_GL_DeleteContext, void(SDL_GLContext));
  MAKE_MOCK1(SDL_DestroyWindow, void(SDL_Window*));
  MAKE_MOCK1(SDL_QuitSubSystem, void(Uint32));
  MAKE_MOCK1(SDL_GL_SwapWindow, void(SDL_Window*));
  MAKE_MOCK1(SDL_PollEvent, int(SDL_Event*));  // NOLINT(readability/casting)
  MAKE_MOCK3(SDL_GetWindowSize, void(SDL_Window*, int*, int*));
  MAKE_MOCK1(SDL_GL_SetSwapInterval, int(int));
  MAKE_MOCK1(SDL_ShowWindow, void(SDL_Window*));
  MAKE_MOCK1(SDL_HideWindow, void(SDL_Window*));
};

class SDL_MOCK_EXPORT SdlMock {
 public:
  SdlMock() : mock_(new SdlMockInternal) {}
  SdlMock(const SdlMock&) = delete;
  SdlMock(SdlMock&&) = default;

  ~SdlMock() { delete mock_; }

  SdlMock& operator=(const SdlMock&) = delete;
  SdlMock& operator=(SdlMock&&) = delete;

  SdlMockInternal& Get() { return *mock_; }

 private:
  SdlMockInternal* mock_{nullptr};
};

extern SDL_MOCK_EXPORT SdlMock sdl_mock;

#define SDL_MOCK_ALLOW_ANY_CALL                                              \
  ALLOW_CALL(sdl_mock.Get(), SDL_Init(SDL_INIT_VIDEO)).RETURN(0);            \
  ALLOW_CALL(sdl_mock.Get(), SDL_Init(SDL_INIT_EVENTS)).RETURN(0);           \
  ALLOW_CALL(sdl_mock.Get(), SDL_InitSubSystem(SDL_INIT_VIDEO)).RETURN(0);   \
  ALLOW_CALL(sdl_mock.Get(), SDL_InitSubSystem(SDL_INIT_EVENTS)).RETURN(0);  \
  ALLOW_CALL(sdl_mock.Get(), SDL_VideoInit(nullptr)).RETURN(0);              \
  ALLOW_CALL(sdl_mock.Get(), SDL_VideoQuit());                               \
  ALLOW_CALL(sdl_mock.Get(), SDL_GL_SetAttribute(_, _)).RETURN(0);           \
  ALLOW_CALL(sdl_mock.Get(), SDL_CreateWindow(_, _, _, _, _, _))             \
      .RETURN(reinterpret_cast<SDL_Window*>(1u));                            \
  ALLOW_CALL(sdl_mock.Get(), SDL_GL_CreateContext(ne(nullptr)))              \
      .RETURN(reinterpret_cast<SDL_GLContext>(1));                           \
  ALLOW_CALL(sdl_mock.Get(), SDL_GL_CreateContext(nullptr)).RETURN(nullptr); \
  ALLOW_CALL(sdl_mock.Get(), SDL_GL_DeleteContext(_));                       \
  ALLOW_CALL(sdl_mock.Get(), SDL_DestroyWindow(_));                          \
  ALLOW_CALL(sdl_mock.Get(), SDL_QuitSubSystem(_));                          \
  ALLOW_CALL(sdl_mock.Get(), SDL_GL_SwapWindow(ne(nullptr)));                \
  ALLOW_CALL(sdl_mock.Get(), SDL_GetWindowSize(ne(nullptr), _, _));          \
  ALLOW_CALL(sdl_mock.Get(), SDL_GL_SetSwapInterval(_)).RETURN(0);           \
  ALLOW_CALL(sdl_mock.Get(), SDL_PollEvent(_)).RETURN(0);                    \
  ALLOW_CALL(sdl_mock.Get(), SDL_ShowWindow(_));                             \
  ALLOW_CALL(sdl_mock.Get(), SDL_HideWindow(_));

#endif  // TESTS_SRC_MOCKS_SDL_MOCK_HPP_

//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include "mocks/sdl_mock.hpp"
#if defined __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wexit-time-destructors"
#pragma clang diagnostic ignored "-Wglobal-constructors"
#endif

SdlMock sdl_mock;

#if defined __clang__
#pragma clang diagnostic pop
#endif

extern "C" {
int SDL_Init(Uint32 flags) { return sdl_mock.Get().SDL_Init(flags); }
int SDL_InitSubSystem(Uint32 flags) {
  return sdl_mock.Get().SDL_InitSubSystem(flags);
}
int SDL_VideoInit(const char* driver) {
  return sdl_mock.Get().SDL_VideoInit(driver);
}
void SDL_VideoQuit() { sdl_mock.Get().SDL_VideoQuit(); }
const char* SDL_GetError() { return sdl_mock.Get().SDL_GetError(); }
int SDL_GL_SetAttribute(SDL_GLattr attr, int value) {
  return sdl_mock.Get().SDL_GL_SetAttribute(attr, value);
}
SDL_Window* SDL_CreateWindow(const char* title, int x, int y, int w, int h,
                             Uint32 flags) {
  return sdl_mock.Get().SDL_CreateWindow(title, x, y, w, h, flags);
}
SDL_GLContext SDL_GL_CreateContext(SDL_Window* window) {
  return sdl_mock.Get().SDL_GL_CreateContext(window);
}
void SDL_GL_DeleteContext(SDL_GLContext context) {
  sdl_mock.Get().SDL_GL_DeleteContext(context);
}
void SDL_DestroyWindow(SDL_Window* window) {
  sdl_mock.Get().SDL_DestroyWindow(window);
}
void SDL_QuitSubSystem(Uint32 flags) {
  sdl_mock.Get().SDL_QuitSubSystem(flags);
}
void SDL_GL_SwapWindow(SDL_Window* window) {
  sdl_mock.Get().SDL_GL_SwapWindow(window);
}
int SDL_PollEvent(SDL_Event* event) {
  return sdl_mock.Get().SDL_PollEvent(event);
}
void SDL_GetWindowSize(SDL_Window* window, int* w, int* h) {
  sdl_mock.Get().SDL_GetWindowSize(window, w, h);
}
int SDL_GL_SetSwapInterval(int interval) {
  return sdl_mock.Get().SDL_GL_SetSwapInterval(interval);
}
void SDL_ShowWindow(SDL_Window* window) {
  sdl_mock.Get().SDL_ShowWindow(window);
}
void SDL_HideWindow(SDL_Window* window) {
  sdl_mock.Get().SDL_HideWindow(window);
}
}

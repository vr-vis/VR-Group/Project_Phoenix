//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

<AUTO_GENERATION_WARNING>

#ifndef TESTS_SRC_MOCKS_OPEN_GL_MOCK_HPP_
#define TESTS_SRC_MOCKS_OPEN_GL_MOCK_HPP_

#ifdef OPENGL_MOCK_BUILD
#define GLEW_BUILD
#ifdef _MSC_VER
#define GLAPI __declspec(dllexport)
#else
#define GLAPI
#endif
#endif

#include "phx/suppress_warnings.hpp"

SUPPRESS_WARNINGS_BEGIN
#include "GL/glew.h"
SUPPRESS_WARNINGS_END

#include "trompeloeil.hpp"

#include "opengl_mock_export.hpp"

using trompeloeil::_;
using trompeloeil::ge;
using trompeloeil::gt;
using trompeloeil::ne;

class OPENGL_MOCK_EXPORT OpenGLMockInternal {
 public:
  MAKE_MOCK0(glewInit, GLenum());
  <MOCK_DEFINITION>
};

class OPENGL_MOCK_EXPORT OpenGLMockWrapper {
 public:
  OpenGLMockWrapper() : mock_(new OpenGLMockInternal) {}
  OpenGLMockWrapper(const OpenGLMockWrapper&) = delete;
  OpenGLMockWrapper(OpenGLMockWrapper&&) = default;

  ~OpenGLMockWrapper() { delete mock_; }

  OpenGLMockWrapper& operator=(const OpenGLMockWrapper&) = delete;
  OpenGLMockWrapper& operator=(OpenGLMockWrapper&&) = default;

  OpenGLMockInternal &Get() { return *mock_; }

 private:
  OpenGLMockInternal *mock_{nullptr};
};

extern OPENGL_MOCK_EXPORT OpenGLMockWrapper open_gl_mock_wrapper;
#define open_gl_mock open_gl_mock_wrapper.Get()

<ALLOW_CALL_DEFINITION>

#endif  // TESTS_SRC_MOCKS_OPEN_GL_MOCK_HPP_

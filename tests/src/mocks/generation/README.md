# Mock-Generation

If a method is missing and a linker error is thrown, you have to add the methods 
to the `functions_to_mock` list in the beginning of the python generator script.

It is generated automatically on rerunning cmake.

If you want to specify a specific `ALLOW_CALL` e.g. to have different needed return values, 
add it to `allow_calls_provided`. Otherwise a standard allow call is generated.

Either with its `glew` or its `gl` name, e.g., `glCreateProgram` or `__glewCompileShader`.

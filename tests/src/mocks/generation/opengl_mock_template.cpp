//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

<AUTO_GENERATION_WARNING>

#include "opengl_mock.hpp"

#if defined __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wexit-time-destructors"
#pragma clang diagnostic ignored "-Wglobal-constructors"
#endif

    OpenGLMockWrapper open_gl_mock_wrapper;

#if defined __clang__
#pragma clang diagnostic pop
#endif

SUPPRESS_WARNINGS_BEGIN_MISSING_DECLARATIONS

#ifdef __cplusplus
extern "C" {
#endif

GLboolean glewExperimental = false;

GLenum glewInit() { return open_gl_mock.glewInit(); }

<__GLEW_METHODS>

#ifdef __cplusplus
}
#endif

SUPPRESS_WARNINGS_END

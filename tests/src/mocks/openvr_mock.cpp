//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include "phx/suppress_warnings.hpp"

SUPPRESS_WARNINGS_BEGIN
#include "mocks/openvr_mock.hpp"
SUPPRESS_WARNINGS_END

#if defined __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wexit-time-destructors"
#pragma clang diagnostic ignored "-Wglobal-constructors"
#endif

OpenVRMock openvr_mock;

#if defined __clang__
#pragma clang diagnostic pop
#endif

namespace vr {

extern "C" {

bool VR_IsHmdPresent() { return openvr_mock.Get().VR_IsHmdPresent(); }

void *VR_GetGenericInterface(const char *pchInterfaceVersion,
                             EVRInitError *peError) {
  return openvr_mock.Get().VR_GetGenericInterface(pchInterfaceVersion, peError);
}

uint32_t VR_InitInternal2(EVRInitError *peError,
                          EVRApplicationType eApplicationType,
                          const char *pStartupInfo) {
  return openvr_mock.Get().VR_InitInternal2(peError, eApplicationType,
                                            pStartupInfo);
}
void VR_ShutdownInternal() { openvr_mock.Get().VR_ShutdownInternal(); }
bool VR_IsInterfaceVersionValid(const char *pchInterfaceVersion) {
  return openvr_mock.Get().VR_IsInterfaceVersionValid(pchInterfaceVersion);
}

uint32_t VR_GetInitToken() { return openvr_mock.Get().VR_GetInitToken(); }

HmdMatrix44_t IVRSystemMock::GetProjectionMatrix(vr::EVREye eye, float nearZ,
                                                 float farZ) {
  return OpenVRMock::toHMDMatrix44_t(
      GetProjectionMatrixArray(eye, nearZ, farZ));
}

vr::HmdMatrix34_t IVRSystemMock::GetEyeToHeadTransform(EVREye eye) {
  return OpenVRMock::toHMDMatrix34_t(GetEyeToHeadTransformArray(eye));
}

}  // extern "C"

}  // namespace vr

vr::HmdMatrix44_t OpenVRMock::toHMDMatrix44_t(float mat[16]) {
  return {{{mat[0], mat[1], mat[2], mat[3]},
           {mat[4], mat[5], mat[6], mat[7]},
           {mat[8], mat[9], mat[10], mat[11]},
           {mat[12], mat[13], mat[14], mat[15]}}};
}

vr::HmdMatrix34_t OpenVRMock::toHMDMatrix34_t(float mat[12]) {
  return {{{mat[0], mat[1], mat[2], mat[3]},
           {mat[4], mat[5], mat[6], mat[7]},
           {mat[8], mat[9], mat[10], mat[11]}}};
}

//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include <memory>
#include <string>
#include <utility>
#include <vector>

#include "catch/catch.hpp"

#include "phx/core/engine.hpp"
#include "phx/core/runtime_component.hpp"
#include "phx/display/hmd.hpp"
#include "phx/input/device_system.hpp"
#include "phx/rendering/backend/opengl_image_buffer_data.hpp"
#include "phx/rendering/components/light.hpp"
#include "phx/rendering/components/transform.hpp"
#include "phx/resources/loaders/scene_loader.hpp"
#include "phx/resources/resource_utils.hpp"
#include "phx/setup.hpp"

#include "mocks/openvr_mock.hpp"

#include "gl/texture.hpp"

#include "test_utilities/glm_mat4.hpp"
#include "test_utilities/opengl_buffer_data_comparison.hpp"

#include "trompeloeil.hpp"

extern template struct trompeloeil::reporter<trompeloeil::specialized>;
using trompeloeil::_;

namespace {

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wmissing-prototypes"
#endif

void SetupScene(phx::Engine* engine) {
  auto scene = engine->GetScene();
  phx::Entity* main_light = scene->CreateEntity();
  phx::Transform* light_transform = main_light->AddComponent<phx::Transform>();
  light_transform->SetLocalRotation(
      glm::angleAxis(0.1f * glm::pi<float>(), glm::vec3(0, 1, 0)));
  phx::Light* light = main_light->AddComponent<phx::Light>();
  light->SetType(phx::Light::Type::kDirectional);
  light->SetColor(glm::vec3(1.0f, 1.0f, 1.0f));
  light->SetIntensity(1.0f);

  phx::SceneLoader::InsertModelIntoScene("models/2MeshTest/2meshTest.obj",
                                         scene.get());
}

GLuint OpenVRTextureID(const vr::Texture_t* vr_texture) {
  if (vr_texture->eType != vr::TextureType_OpenGL ||
      vr_texture->eColorSpace != vr::ColorSpace_Gamma) {
    std::cerr << "vr_texture has unexpected type!" << std::endl;
    return 0;
  }
  return static_cast<GLuint>(reinterpret_cast<uintptr_t>(vr_texture->handle));
}

class EngineStopTestSystem : public phx::System {
 public:
  explicit EngineStopTestSystem(phx::Engine* engine) : phx::System(engine) {}

  void Update(const phx::FrameTimer::TimeInfo&) override {
    GetEngine()->Stop();
  }
};

void CheckSimilarity(GLuint submitted_image_id, std::string ref_image_name) {
  gl::texture_2d submitted_tex(submitted_image_id);
  auto buffer = phx::OpenGLImageBufferData<
      phx::OpenGLImageBufferDataType_RGB>::CreateFromTexture(&submitted_tex);
  test_utilities::OpenGLBufferComparison::REQUIRE_REFERENCE_IMAGE_SIMILARITY(
      *buffer, ref_image_name, 1.0);
}

void MoveUserPlatform(phx::Engine* engine) {
  auto platform_entities = engine->GetScene()
                               ->GetEntitiesWithComponents<
                                   phx::RuntimeComponent<phx::USER_PLATFORM>>();
  REQUIRE(platform_entities.size() == 1);
  auto transform_cmp =
      platform_entities[0]->GetFirstComponent<phx::Transform>();
  transform_cmp->Translate(glm::vec3(3.0f, 0.0f, 5.0f));
  transform_cmp->SetLocalRotationEuler(glm::vec3(0.f, 30.f, 0.f));
}

#ifdef __clang__
#pragma clang diagnostic pop
#endif

GLuint left_tex_id, right_tex_id;

}  // namespace

#define ALLOW_SUBMIT_CALLS                                     \
  ALLOW_CALL(openvr_mock.GetCompositor(),                      \
             Submit(vr::EVREye::Eye_Left, _, _, _))            \
      .SIDE_EFFECT(::left_tex_id = ::OpenVRTextureID(_2))      \
      .RETURN(vr::EVRCompositorError::VRCompositorError_None); \
  ALLOW_CALL(openvr_mock.GetCompositor(),                      \
             Submit(vr::EVREye::Eye_Right, _, _, _))           \
      .SIDE_EFFECT(::right_tex_id = ::OpenVRTextureID(_2))     \
      .RETURN(vr::EVRCompositorError::VRCompositorError_None);

SCENARIO("If no HMD is present nothing is rendered into it",
         "[phx][phx::HMD]") {
  ALLOW_CALL(openvr_mock.Get(), VR_IsHmdPresent()).RETURN(false);
  GIVEN("A standard engine that runs one frame") {
    std::unique_ptr<phx::Engine> engine = phx::Setup::CreateDefaultEngine();
    engine->CreateSystem<EngineStopTestSystem>();

    WHEN("We run the engine") {
      FORBID_CALL(openvr_mock.GetCompositor(), Submit(_, _, _, _));
      engine->Run();
      THEN("Submit() to HMD is never called") {}
    }
  }
}

SCENARIO("If a HMD is present we render for both eyes including controllers",
         "[phx][phx::HMD]") {
  OPENVR_MOCK_ALLOW_ANY_CALL
  ALLOW_CALL(openvr_mock.Get(), VR_IsHmdPresent()).RETURN(true);
  ALLOW_SUBMIT_CALLS

  GIVEN(
      "A standard engine that runs one frame and a scene plus a moved user "
      "platform") {
    std::unique_ptr<phx::Engine> engine = phx::Setup::CreateDefaultEngine();
    engine->CreateSystem<EngineStopTestSystem>();
    ::SetupScene(engine.get());
    ::MoveUserPlatform(engine.get());

    WHEN("We run the engine") {
      engine->Run();
      THEN("Submit() is called for both eyes submitting the expected images") {
        ::CheckSimilarity(::left_tex_id, "hmd_test_left.png");
        ::CheckSimilarity(::right_tex_id, "hmd_test_right.png");
      }
    }
  }
}

namespace {

glm::mat4 ToMat4(float mat[12]) {
  glm::mat4 result;
  for (int i = 0; i < 12; ++i) {
    result[i % 4][i / 4] = mat[i];
  }
  return result;
}
}  // namespace

SCENARIO("The Eye to distance is changed via vive's eye distance knob",
         "[phx][phx::HMD]") {
  GIVEN("A standard engine that runs one frame") {
    OPENVR_MOCK_ALLOW_ANY_CALL
    phx::Engine engine;
    phx::DeviceSystem* device_system = engine.CreateSystem<phx::DeviceSystem>();
    auto hmd = device_system->AddDevice<phx::HMD>();

    REQUIRE(hmd->GetEyeToHeadMatrix(phx::HMD::LEFT_EYE) ==
            ToMat4(openvr_mock.eye_to_head_left_));
    REQUIRE(hmd->GetEyeToHeadMatrix(phx::HMD::RIGHT_EYE) ==
            ToMat4(openvr_mock.eye_to_head_right_));

    WHEN("We change the eye distance while the engine is running") {
      float eye_to_head_left[12] = {1.0f, 0.0f, 0.0f, 0.03f, 0.0f, 1.0f,
                                    0.0f, 0.0f, 0.0f, 0.0f,  1.0f, 12.0f};
      float eye_to_head_right[12] = {1.0f, 0.0f, 0.0f, -0.03f, 0.0f, 1.0f,
                                     0.0f, 0.0f, 0.0f, 0.0f,   1.0f, 13.0f};
      ALLOW_CALL(openvr_mock.GetSystem(),
                 GetEyeToHeadTransformArray(vr::EVREye::Eye_Left))
          .LR_RETURN(eye_to_head_left);
      ALLOW_CALL(openvr_mock.GetSystem(),
                 GetEyeToHeadTransformArray(vr::EVREye::Eye_Right))
          .LR_RETURN(eye_to_head_right);
      THEN("The eye distances are different") {
        REQUIRE_FALSE(hmd->GetEyeToHeadMatrix(phx::HMD::LEFT_EYE) ==
                      ToMat4(eye_to_head_left));
        REQUIRE_FALSE(hmd->GetEyeToHeadMatrix(phx::HMD::RIGHT_EYE) ==
                      ToMat4(eye_to_head_right));
        WHEN("The HMD is Updated and an event is received") {
          vr::VREvent_t event;
          event.eventType = vr::VREvent_IpdChanged;
          auto first_call = std::make_shared<bool>(true);
          ALLOW_CALL(openvr_mock.GetSystem(), PollNextEvent(_, _))
              .SIDE_EFFECT(*_1 = event)
              .SIDE_EFFECT(*first_call = false)
              .WITH(*first_call == true)
              .RETURN(true);
          ALLOW_CALL(openvr_mock.GetSystem(), PollNextEvent(_, _))
              .WITH(*first_call == false)
              .RETURN(false);
          hmd->Update();
          THEN("The eye to head matrix has changed accordingly") {
            REQUIRE(hmd->GetEyeToHeadMatrix(phx::HMD::LEFT_EYE) ==
                    ToMat4(eye_to_head_left));
            REQUIRE(hmd->GetEyeToHeadMatrix(phx::HMD::RIGHT_EYE) ==
                    ToMat4(eye_to_head_right));
          }
        }
      }
    }
  }
}

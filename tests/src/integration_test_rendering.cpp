//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include <memory>
#include <utility>
#include <vector>

#include "catch/catch.hpp"

#include "phx/suppress_warnings.hpp"

#include "phx/core/frame_timer.hpp"
#include "phx/core/scene.hpp"
#include "phx/display/display_system_window.hpp"
#include "phx/display/window.hpp"
#include "phx/rendering/backend/opengl_image_buffer_data.hpp"
#include "phx/rendering/components/mesh_handle.hpp"
#include "phx/rendering/rendering_system.hpp"
#include "phx/resources/resource_manager.hpp"
#include "phx/resources/types/mesh.hpp"
#include "phx/setup.hpp"

SUPPRESS_WARNINGS_BEGIN
#include "mocks/openvr_mock.hpp"
SUPPRESS_WARNINGS_END

#include "test_utilities/dummy_mesh_generator.hpp"
#include "test_utilities/opengl_buffer_data_comparison.hpp"

#include "trompeloeil.hpp"

extern template struct trompeloeil::reporter<trompeloeil::specialized>;

SUPPRESS_WARNINGS_BEGIN_MISSING_DECLARATIONS

void SetupLightAndCamera(phx::Scene* scene) {
  phx::Entity* main_light = scene->CreateEntity();
  phx::Transform* light_transform = main_light->AddComponent<phx::Transform>();
  light_transform->SetLocalRotation(
      glm::angleAxis(0.2f * glm::pi<float>(), glm::vec3(0, 1, 0)));
  phx::Light* light = main_light->AddComponent<phx::Light>();
  light->SetType(phx::Light::Type::kDirectional);
  light->SetColor(glm::vec3(1.0f, 1.0f, 1.0f));
  light->SetIntensity(1.0f);

  phx::Entity* camera =
      scene->GetEntitiesWithComponents<phx::RenderTarget>()[0];
  auto camera_transform = camera->GetFirstComponent<phx::Transform>();
  auto camera_projection = camera->GetFirstComponent<phx::Projection>();
  camera_projection->SetPerspective(glm::radians(68.0f), 4.0f / 3.0f, 0.01f,
                                    1000.0f);
  camera_transform->SetLocalTranslation(glm::vec3(0, 0, 2.0));
  camera_transform->LookAt(glm::vec3(0, 0, 0));
}

void CreateTestTriangleComponent(phx::Entity* triangle) {
  std::vector<glm::vec3> vertices = {
      {0.0f, 0.0f, 0.0f}, {1.0f, 0.0f, 0.0f}, {1.0f, 1.0f, 0.0f}};
  std::vector<glm::vec3> normals = {
      {0.0f, 0.0f, 1.0f}, {0.0f, 0.0f, 1.0f}, {0.0f, 0.0f, 1.0f}};

  std::vector<unsigned int> indices = {0u, 1u, 2u};

  phx::ResourceManager::instance().RegisterResourceType(
      "staticmesh",
      std::make_unique<phx::DummyMeshLoader>(
          std::move(vertices), std::move(normals), std::move(indices)));
  auto mesh = phx::ResourceManager::instance().DeclareResource<phx::Mesh>(
      {{"TYPE", "staticmesh"}});
  mesh.Load();

  phx::MeshHandle* mesh_handle = triangle->AddComponent<phx::MeshHandle>();
  mesh_handle->SetMesh(mesh);
}

SUPPRESS_WARNINGS_END

SCENARIO("We can render a simple triangle", "[phx][phx::Rendering]") {
  ALLOW_CALL(openvr_mock.Get(), VR_IsHmdPresent()).RETURN(false);
  GIVEN("A complete triangle") {
    std::unique_ptr<phx::Engine> engine = phx::Setup::CreateDefaultEngine();
    auto rendering_system = engine->GetSystem<phx::RenderingSystem>();
    auto display_system = engine->GetSystem<phx::DisplaySystemWindow>();
    auto scene = engine->GetScene();

    SetupLightAndCamera(scene.get());

    phx::Entity* triangle = scene->CreateEntity();
    CreateTestTriangleComponent(triangle);

    auto material = phx::ResourceUtils::LoadGenericMaterial(
        "custommaterial", glm::vec3(0.2f, 0.2f, 1.0f), glm::vec3(1, 1, 1),
        glm::vec3(), 500.0f);

    triangle->AddComponent<phx::Transform>();
    phx::MaterialHandle* material_handle =
        triangle->AddComponent<phx::MaterialHandle>();
    material_handle->SetMaterial(material);

    WHEN("We render the scene") {
      rendering_system->Update(phx::FrameTimer::TimeInfo{});
      display_system->Update(phx::FrameTimer::TimeInfo{});
      THEN("the rendering matches our reference image") {
        phx::OpenGLImageBufferData<phx::OpenGLImageBufferDataType_RGB> buffer(
            1024, 768);
        buffer.ReadColorPixels(true);
        test_utilities::OpenGLBufferComparison::
            REQUIRE_REFERENCE_IMAGE_SIMILARITY(buffer, "simple_triangle.png",
                                               1.0);
      }
    }
  }
}

SCENARIO("If no material given a default red one is taken.",
         "[phx][phx::Rendering]") {
  ALLOW_CALL(openvr_mock.Get(), VR_IsHmdPresent()).RETURN(false);
  GIVEN("A triangle without material") {
    std::unique_ptr<phx::Engine> engine = phx::Setup::CreateDefaultEngine();
    auto rendering_system = engine->GetSystem<phx::RenderingSystem>();
    auto display_system = engine->GetSystem<phx::DisplaySystemWindow>();
    auto scene = engine->GetScene();

    SetupLightAndCamera(scene.get());

    phx::Entity* triangle = scene->CreateEntity();
    CreateTestTriangleComponent(triangle);
    triangle->AddComponent<phx::Transform>();

    WHEN("We render the scene") {
      rendering_system->Update(phx::FrameTimer::TimeInfo{});
      display_system->Update(phx::FrameTimer::TimeInfo{});
      THEN("the rendering matches our reference image") {
        phx::OpenGLImageBufferData<phx::OpenGLImageBufferDataType_RGB> buffer(
            1024, 768);
        buffer.ReadColorPixels(true);
        test_utilities::OpenGLBufferComparison::
            REQUIRE_REFERENCE_IMAGE_SIMILARITY(
                buffer, "triangle_without_material.png", 1.0);
      }
    }
  }
}

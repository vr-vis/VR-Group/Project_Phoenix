//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include <memory>

#include "catch/catch.hpp"

#include "phx/core/entity.hpp"
#include "phx/rendering/components/light.hpp"

#include "glm/glm.hpp"

SCENARIO("The light component describes a light source in space.",
         "[phx][phx::Light]") {
  GIVEN("A new light component.") {
    phx::Entity entity;
    phx::Light* light = entity.AddComponent<phx::Light>();

    WHEN("We set the type to spotlight.") {
      light->SetType(phx::Light::Type::kSpot);
      THEN("The type is set to spotlight.") {
        REQUIRE(light->GetType() == phx::Light::Type::kSpot);
      }
    }

    WHEN("We set the color to [1.0, 0.0, 0.0].") {
      light->SetColor(glm::vec3(1.0, 0.0, 0.0));
      THEN("The color is set to [1.0, 0.0, 0.0].") {
        REQUIRE(light->GetColor() == glm::vec3(1.0, 0.0, 0.0));
      }
    }

    WHEN("We set the intensity to 10.") {
      light->SetIntensity(10.0f);
      THEN("The intensity is set to 10.") {
        REQUIRE(light->GetIntensity() == 10.0f);
      }
    }

    WHEN("We set the range to 100.") {
      light->SetRange(100.0f);
      THEN("The range is set to 100.") { REQUIRE(light->GetRange() == 100.0f); }
    }

    WHEN("We set the spot angle to 60.") {
      light->SetSpotAngle(60.0f);
      THEN("The spot angle is set to 60.") {
        REQUIRE(light->GetSpotAngle() == 60.0f);
      }
    }
  }
}

//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include <memory>
#include <string>

#include "catch/catch.hpp"

#include "phx/suppress_warnings.hpp"

#include "phx/core/frame_timer.hpp"
#include "phx/core/scene.hpp"
#include "phx/display/display_system_window.hpp"
#include "phx/display/window.hpp"
#include "phx/rendering/backend/opengl_image_buffer_data.hpp"
#include "phx/rendering/components/mesh_handle.hpp"
#include "phx/rendering/components/mesh_render_settings.hpp"
#include "phx/rendering/rendering_system.hpp"
#include "phx/resources/loaders/assimp_model_loader.hpp"
#include "phx/resources/resource_manager.hpp"
#include "phx/resources/resource_utils.hpp"
#include "phx/resources/types/mesh.hpp"
#include "phx/setup.hpp"

#include "test_utilities/opengl_buffer_data_comparison.hpp"

SUPPRESS_WARNINGS_BEGIN
#include "mocks/openvr_mock.hpp"
SUPPRESS_WARNINGS_END

#include "trompeloeil.hpp"

extern template struct trompeloeil::reporter<trompeloeil::specialized>;

SUPPRESS_WARNINGS_BEGIN_MISSING_DECLARATIONS

phx::Entity* LoadBunny(glm::vec3 pos, float angleDeg,
                       const std::string& material_name, phx::Scene* scene) {
  auto bunny_mesh = phx::ResourceUtils::LoadResourceFromFile<phx::Mesh>(
      "models/bunny.obj", {{"mesh_index", 0}});

  phx::Entity* bunny = scene->CreateEntity();

  phx::MeshHandle* bunny_handle = bunny->AddComponent<phx::MeshHandle>();
  bunny_handle->SetMesh(bunny_mesh);

  phx::MaterialHandle* bunny_material_handle =
      bunny->AddComponent<phx::MaterialHandle>();

  auto bunny_material = phx::ResourceUtils::LoadResourceFromFile<phx::Material>(
      "models/bunny.obj", {{"material_name", material_name}});
  bunny_material_handle->SetMaterial(bunny_material);

  phx::Transform* bunny_transform = bunny->AddComponent<phx::Transform>();
  bunny_transform->SetLocalTranslation(pos);
  bunny_transform->SetLocalRotation(
      glm::angleAxis(angleDeg / 180.0f * glm::pi<float>(), glm::vec3(0, 1, 0)));

  return bunny;
}

void SetupLightAndCamera(phx::Scene* scene) {
  phx::Entity* main_light = scene->CreateEntity();
  phx::Transform* light_transform = main_light->AddComponent<phx::Transform>();
  light_transform->SetLocalRotation(
      glm::angleAxis(1.2f * glm::pi<float>(), glm::vec3(0, 1, 0)));
  phx::Light* light = main_light->AddComponent<phx::Light>();
  light->SetType(phx::Light::Type::kDirectional);
  light->SetColor(glm::vec3(1.0f, 1.0f, 1.0f));
  light->SetIntensity(1.0f);

  phx::Entity* camera =
      scene->GetEntitiesWithComponents<phx::RenderTarget>()[0];
  auto camera_transform = camera->GetFirstComponent<phx::Transform>();
  auto camera_projection = camera->GetFirstComponent<phx::Projection>();
  camera_projection->SetPerspective(glm::radians(68.0f), 4.0f / 3.0f, 0.01f,
                                    1000.0f);
  camera_transform->SetLocalTranslation(glm::vec3(0, 0, -0.3));
  camera_transform->LookAt(glm::vec3(0, 0, 0));
}

SCENARIO(
    "A model can be loaded and rendered with different trasformations and "
    "materials.",
    "[phx][phx::ModelLoading]") {
  ALLOW_CALL(openvr_mock.Get(), VR_IsHmdPresent()).RETURN(false);
  GIVEN("A complete scene with two differently colored bunnies.") {
    std::unique_ptr<phx::Engine> engine = phx::Setup::CreateDefaultEngine();
    auto scene = engine->GetScene();

    SetupLightAndCamera(scene.get());

    LoadBunny(glm::vec3(0.15f, -0.1f, 0.0f), 0.0f, "bunny", scene.get());
    LoadBunny(glm::vec3(-0.15f, -0.1f, 0.0f), 180.0f, "bunny2", scene.get());

    auto rendering_system = engine->GetSystem<phx::RenderingSystem>();
    auto display_system = engine->GetSystem<phx::DisplaySystemWindow>();

    WHEN("We render the scene") {
      rendering_system->Update(phx::FrameTimer::TimeInfo());
      display_system->Update(phx::FrameTimer::TimeInfo());
      THEN("the rendering matches our reference image") {
        phx::OpenGLImageBufferData<phx::OpenGLImageBufferDataType_RGB> buffer(
            1024, 768);
        buffer.ReadColorPixels(true);
        test_utilities::OpenGLBufferComparison::
            REQUIRE_REFERENCE_IMAGE_SIMILARITY(buffer, "model_loading.png",
                                               1.0);
      }
    }
  }
}

SCENARIO(
    "If no light and camera are give the model rendering takes default values",
    "[phx][phx::ModelLoading]") {
  ALLOW_CALL(openvr_mock.Get(), VR_IsHmdPresent()).RETURN(false);
  GIVEN("A complete scene with a bunny") {
    std::unique_ptr<phx::Engine> engine = phx::Setup::CreateDefaultEngine();
    auto scene = engine->GetScene();

    LoadBunny(glm::vec3(0.0f, -0.1f, -0.3f), 0.0f, "bunny", scene.get());

    auto rendering_system = engine->GetSystem<phx::RenderingSystem>();
    auto display_system = engine->GetSystem<phx::DisplaySystemWindow>();

    WHEN("We render the scene") {
      rendering_system->Update(phx::FrameTimer::TimeInfo{});
      display_system->Update(phx::FrameTimer::TimeInfo());
      THEN("the rendering matches our reference image") {
        phx::OpenGLImageBufferData<phx::OpenGLImageBufferDataType_RGB> buffer(
            1024, 768);
        buffer.ReadColorPixels(true);
        test_utilities::OpenGLBufferComparison::
            REQUIRE_REFERENCE_IMAGE_SIMILARITY(
                buffer, "model_loading_default_cam_light.png", 1.0);
      }
    }
  }
}

SCENARIO("We can render meshes as wireframe.",
         "[phx][phx::MeshRenderSettings]") {
  ALLOW_CALL(openvr_mock.Get(), VR_IsHmdPresent()).RETURN(false);
  GIVEN("A complete scene with a bunny") {
    std::unique_ptr<phx::Engine> engine = phx::Setup::CreateDefaultEngine();
    auto scene = engine->GetScene();

    auto bunny =
        LoadBunny(glm::vec3(0.0f, -0.1f, -0.3f), 0.0f, "bunny", scene.get());

    auto rendering_system = engine->GetSystem<phx::RenderingSystem>();
    auto display_system = engine->GetSystem<phx::DisplaySystemWindow>();

    WHEN("We set the rendering to be done in wireframe") {
      auto render_settings = bunny->AddComponent<phx::MeshRenderSettings>();
      render_settings->SetWireframeMode(true);
      THEN("the rendering matches our reference image") {
        rendering_system->Update(phx::FrameTimer::TimeInfo{});
        display_system->Update(phx::FrameTimer::TimeInfo());
        phx::OpenGLImageBufferData<phx::OpenGLImageBufferDataType_RGB> buffer(
            1024, 768);
        buffer.ReadColorPixels(true);
        test_utilities::OpenGLBufferComparison::
            REQUIRE_REFERENCE_IMAGE_SIMILARITY(
                buffer, "model_loading_wireframe.png", 1.0);
      }
    }
  }
}

SUPPRESS_WARNINGS_END

//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include <memory>
#include <vector>

#include "catch/catch.hpp"

#include "trompeloeil.hpp"

#include "mocks/sdl_mock.hpp"

#include "phx/core/logger.hpp"
#include "phx/display/display_system_window.hpp"
#include "phx/display/window.hpp"

using trompeloeil::_;
using trompeloeil::ne;

extern template struct trompeloeil::reporter<trompeloeil::specialized>;

#undef CreateWindow

SCENARIO(
    "The display system is a container and is responsible for the lifetime "
    "management of displays.",
    "[phx][phx::DisplaySystem]") {
  SDL_MOCK_ALLOW_ANY_CALL;

  WHEN("We construct a DisplaySystem.") {
    THEN(
        "SDL_VideoInit() is called and SDL_VideoQuit() when it goes out of "
        "scope.") {
      REQUIRE_CALL(sdl_mock.Get(), SDL_VideoInit(nullptr)).RETURN(0);
      REQUIRE_CALL(sdl_mock.Get(), SDL_VideoQuit());
      phx::Engine engine;
      phx::DisplaySystemWindow* displaySystem =
          engine.CreateSystem<phx::DisplaySystemWindow>();

      WHEN("We create a window.") {
        THEN("The window is created.") {
          REQUIRE_CALL(sdl_mock.Get(), SDL_CreateWindow(_, 0, 0, 640, 480, _))
              .RETURN(reinterpret_cast<SDL_Window*>(1u));
          REQUIRE_CALL(sdl_mock.Get(),
                       SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4))
              .RETURN(0);
          REQUIRE_CALL(sdl_mock.Get(),
                       SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 5))
              .RETURN(0);
          REQUIRE_CALL(sdl_mock.Get(),
                       SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK,
                                           SDL_GL_CONTEXT_PROFILE_CORE))
              .RETURN(0);
          REQUIRE_CALL(sdl_mock.Get(), SDL_GL_CreateContext(_))
              .RETURN(reinterpret_cast<SDL_GLContext>(1u));
          displaySystem->CreateWindow("TestWindow", glm::uvec2(0, 0),
                                      glm::uvec2(640, 480));

          WHEN("DisplaySystem Update is called") {
            THEN("SDL_GL_SwapWindow is called") {
              REQUIRE_CALL(sdl_mock.Get(), SDL_GL_SwapWindow(_));
              displaySystem->Update(phx::FrameTimer::TimeInfo());
            }
          }
        }
      }
    }
  }
}

//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include "catch/catch.hpp"

#include "phx/resources/types/image.hpp"
#include "phx/resources/resource_manager.hpp"
#include "phx/resources/resource_pointer.hpp"
#include "phx/resources/resource_utils.hpp"

SCENARIO(
    "The resource pointer makes the underlying resource available through the "
    "-> operator.",
    "[phx][phx::ResourcePointer]") {
  GIVEN("A resource pointer to an arbitrarily chosen image resource.") {
    phx::ResourcePointer<phx::Image> resource_pointer =
        phx::ResourceManager::instance().DeclareResource<phx::Image>(
            phx::ResourceUtils::DeclarationFromFile(
                "textures/splash_progress.png"));
    THEN(
        "We can can compare the pointer to nullptr using the != operator and "
        "they are the same, because the resource is not loaded.") {
      REQUIRE(resource_pointer == nullptr);
      REQUIRE(!resource_pointer);
    }
    WHEN("We load the resource.") {
      resource_pointer.Load();
      THEN(
          "We can compare the pointer to nullptr and they are not the "
          "same.") {
        REQUIRE(resource_pointer != nullptr);
        REQUIRE(resource_pointer);
      }
      THEN("We can access the image through the -> operator.") {
        REQUIRE(resource_pointer.operator->() == resource_pointer.Get());
        REQUIRE(resource_pointer.Get() != nullptr);
      }
    }
    WHEN("We initialize a new pointer to the same resource.") {
      phx::ResourcePointer<phx::Image> resource_pointer2 =
          phx::ResourceManager::instance().DeclareResource<phx::Image>(
              phx::ResourceUtils::DeclarationFromFile(
                  "textures/splash_progress.png"));
      THEN(
          "We can can compare the two pointers using the == operator and "
          "they are the same") {
        REQUIRE(resource_pointer == resource_pointer2);
      }
    }
    WHEN("We initialize a new pointer with a different resource.") {
      phx::ResourcePointer<phx::Image> resource_pointer2 =
          phx::ResourceManager::instance().DeclareResource<phx::Image>(
              phx::ResourceUtils::DeclarationFromFile("textures/splash.png"));
      THEN(
          "We can can compare the two pointers using the != operator and "
          "they are not the same") {
        REQUIRE(resource_pointer != resource_pointer2);
      }
    }
    WHEN("We initialize a new pointer with nullptr.") {
      phx::ResourcePointer<phx::Image> resource_pointer2 =
          phx::ResourcePointer<phx::Image>(nullptr);
      THEN(
          "We can can compare the two pointers using the != operator and "
          "they are not the same") {
        REQUIRE(resource_pointer != resource_pointer2);
      }
    }
  }
}

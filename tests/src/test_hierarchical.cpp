//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include <cstddef>

#include <memory>

#include "catch/catch.hpp"

#include "phx/utility/aspects/hierarchical.hpp"

class SomeHierarchical : public phx::Hierarchical<SomeHierarchical> {};

SCENARIO("The hierarchical has parents and childs.",
         "[phx][phx::Hierarchical]") {
  GIVEN("Two hierarchical objects.") {
    auto child = std::make_unique<SomeHierarchical>();
    auto parent = std::make_unique<SomeHierarchical>();
    WHEN("We query the first transform's parent.") {
      auto parent_ptr = child->GetParent();
      THEN("It should be nullptr.") { REQUIRE(parent_ptr == nullptr); }
    }

    WHEN("We set the first hierarchical's parent to the second.") {
      child->SetParent(parent.get());

      WHEN("We query the first hierarchical's parent.") {
        auto parent_ptr = child->GetParent();
        THEN("It should equal the second hierarchical.") {
          REQUIRE(parent_ptr == parent.get());
        }
      }

      WHEN("We query the second hierarchical's child count.") {
        auto child_count = parent->GetChildCount();
        THEN("Its size should equal 1.") { REQUIRE(child_count == 1); }
      }

      WHEN("We query the second hierarchical's first child.") {
        auto child_ptr = parent->GetChild(0);
        THEN("It should equal the first hierarchical.") {
          REQUIRE(child_ptr == child.get());
        }
      }

      WHEN("We iterate the second hierarchical's children.") {
        auto iteration_count = 0;
        for (auto it = parent->begin(); it != parent->end(); ++it)
          iteration_count++;
        THEN("It should consist of a single iteration.") {
          REQUIRE(iteration_count == 1);
        }
      }

      WHEN("We set the first hierarchical's parent to nullptr.") {
        child->SetParent(nullptr);
        WHEN("We query the hierarchical's parent.") {
          auto parent_ptr = child->GetParent();
          THEN("It should be nullptr.") { REQUIRE(parent_ptr == nullptr); }
        }
      }

      WHEN("We destroy the child.") {
        child.reset();
        THEN("The parent has no more children.") {
          REQUIRE(parent->GetChildCount() == 0);
        }
      }

      WHEN("We destroy the parent.") {
        parent.reset();
        THEN("The child has no more parent.") {
          REQUIRE(child->GetParent() == nullptr);
        }
      }
    }
  }
}

//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include "catch/catch.hpp"

#include "phx/suppress_warnings.hpp"

SUPPRESS_WARNINGS_BEGIN
#include "glm/glm.hpp"
SUPPRESS_WARNINGS_END

#include "phx/core/entity.hpp"
#include "phx/rendering/components/projection.hpp"

#include "test_utilities/glm_mat4.hpp"

SCENARIO("The projection component stores data for projections.",
         "[phx][phx::Projection]") {
  GIVEN("A new projection component.") {
    phx::Entity entity;
    phx::Projection* projection = entity.AddComponent<phx::Projection>();
    WHEN("we query the matrix.") {
      auto matrix = projection->GetMatrix();
      THEN("It should yield an orthogonal projection to the xz-plane.") {
        const glm::mat4 ortho(1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,
                              0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f);
        REQUIRE(matrix == ortho);
      }
    }

    WHEN(
        "We set some projection parameters for a perspective projection and "
        "query the matrix.") {
      projection->SetPerspective(glm::radians(68.0f), 4.0f / 3.0f, 0.01f,
                                 1000.0f);
      auto matrix = projection->GetMatrix();
      THEN("It should yield the appropriate projection matrix.") {
        const glm::mat4 ground_truth(1.11192f, 0.0f, 0.0f, 0.0f, 0.0f, 1.48256f,
                                     0.0f, 0.0f, 0.0f, 0.0f, -1.00002f, -1.0f,
                                     0.0f, 0.0f, -0.02f, 0.0f);
        REQUIRE(test_utilities::Approx<glm::mat4>(matrix).epsilon(0.01f) ==
                ground_truth);
      }
    }

    WHEN(
        "We set some projection parameters for an orthogonal projection and "
        "query the matrix.") {
      projection->SetOrthogonal(-100.0f, 100.0f, -100.0f, 100.0f, 0.01f,
                                1000.0f);
      auto matrix = projection->GetMatrix();
      THEN("It should yield the appropriate projection matrix.") {
        const glm::mat4 ground_truth(0.01, 0.0f, 0.0f, 0.0f, 0.0f, 0.01f, 0.0f,
                                     0.0f, 0.0f, 0.0f, -0.002f, 0.0f, 0.0f,
                                     0.0f, -1.0f, 1.0f);
        REQUIRE(test_utilities::Approx<glm::mat4>(matrix).epsilon(0.01f) ==
                ground_truth);
      }
    }
  }
}

//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include <memory>

#include "catch/catch.hpp"

#include "phx/core/engine.hpp"
#include "phx/input/input_system.hpp"

#include "trompeloeil.hpp"

#include "phx/suppress_warnings.hpp"

SUPPRESS_WARNINGS_BEGIN
#include "mocks/openvr_mock.hpp"
SUPPRESS_WARNINGS_END
#include "mocks/sdl_mock.hpp"

using trompeloeil::_;
using trompeloeil::ne;

extern template struct trompeloeil::reporter<trompeloeil::specialized>;

SCENARIO("The input system captures low-level input events and forwards them.",
         "[phx][phx::InputSystem]") {
  SDL_MOCK_ALLOW_ANY_CALL
  OPENVR_MOCK_ALLOW_ANY_CALL
  GIVEN("An engine with a DisplaySystem.") {
    phx::Engine engine;

    WHEN("I create an input system.") {
      THEN("SDL event subsystem is initialized.") {
        REQUIRE_CALL(sdl_mock.Get(), SDL_Init(SDL_INIT_EVENTS)).RETURN(0);
        engine.CreateSystem<phx::InputSystem>();
      }
    }

    GIVEN("An input system.") {
      phx::InputSystem* input_system = engine.CreateSystem<phx::InputSystem>();
      WHEN("The input system is updated.") {
        THEN("SDL is requested to poll the pending events.") {
          REQUIRE_CALL(sdl_mock.Get(), SDL_PollEvent(_)).RETURN(0);
          input_system->Update(phx::FrameTimer::TimeInfo());
        }
      }
    }
  }
}

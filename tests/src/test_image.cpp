//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include <algorithm>
#include <array>
#include <cstddef>
#include <memory>
#include "catch/catch.hpp"
#include "phx/rendering/backend/opengl_image_buffer_data.hpp"
#include "phx/resources/types/image.hpp"
#include "test_utilities/opengl_buffer_data_comparison.hpp"

SCENARIO("We can create an empty image.", "[phx][phx::Image]") {
  GIVEN("An empty image") {
    auto image = std::make_unique<phx::Image>(
        std::array<std::size_t, 2>{{320, 240}}, 24);

    WHEN("We check its dimensions") {
      auto dimensions = image->GetDimensions();
      std::size_t bits_per_pixel = image->GetBitsPerPixel();

      THEN("They match what we defined.") {
        REQUIRE(dimensions[0] == 320);
        REQUIRE(dimensions[1] == 240);
        REQUIRE(bits_per_pixel == 24);
      }
    }

    WHEN("We check its pixels") {
      auto pixels = image->GetPixels();

      THEN("All are black.") {
        REQUIRE(pixels.first != nullptr);

        std::array<std::uint8_t, 4> black_pixel = {{0, 0, 0, 0}};
        REQUIRE(image->GetPixelColor({{10, 10}}) == black_pixel);
        REQUIRE(image->GetPixelColor({{30, 100}}) == black_pixel);
        REQUIRE(image->GetPixelColor({{20, 90}}) == black_pixel);
        REQUIRE(image->GetPixelColor({{80, 230}}) == black_pixel);
      }
    }

    THEN("We can get its raw buffer pointer, e.g., for uploads to the GPU.") {
      const unsigned char* data_pointer = image->GetPixels().first;

      // check first and last byte, should be 0
      REQUIRE(data_pointer[0] == 0);
      REQUIRE(data_pointer[320 * 240 * 3 - 1] == 0);
    }
  }
}

SCENARIO("Images can be loaded from and saved to disk.", "[phx][phx::Image]") {
  GIVEN("An RGB image with some pixels set in some colors") {
    auto image = std::make_unique<phx::Image>(
        std::array<std::size_t, 2>{{320, 240}}, 24);
    image->SetPixelColor({{10, 10}}, {{255, 128, 64}});
    image->SetPixelColor({{50, 50}}, {{128, 128, 128}});

    WHEN("We save it as a PNG") {
      bool success = image->Save("test_image.png");
      REQUIRE(success);
      WHEN("We load the file again") {
        auto image_load = std::make_unique<phx::Image>(
            "test_image.png");  // Throws on failure.
      }
      std::remove("test_image.png");
    }

    WHEN("We save it as a BMP") {
      bool success = image->Save("test_image.bmp");
      REQUIRE(success);
      WHEN("We load it again") {
        auto image_load = std::make_unique<phx::Image>(
            "test_image.bmp");  // Throws on failure.
      }
      std::remove("test_image.bmp");
    }

    WHEN("We save it as a TGA") {
      bool success = image->Save("test_image.tga");
      REQUIRE(success);
      WHEN("We load it again") {
        auto image_load = std::make_unique<phx::Image>(
            "test_image.tga");  // Throws on failure.
      }
      std::remove("test_image.tga");
    }

    WHEN("We save it as a TIF") {
      bool success = image->Save("test_image.tif");
      REQUIRE(success);
      WHEN("We load it again") {
        auto image_load = std::make_unique<phx::Image>(
            "test_image.tif");  // Throws on failure.
      }
      std::remove("test_image.tif");
    }

    WHEN("We save it as a JPG") {
      bool success = image->Save("test_image.jpg");
      REQUIRE(success);
      WHEN("We load it again") {
        auto image_load = std::make_unique<phx::Image>("test_image.jpg");
        THEN("Loading worked and the images are highly similar") {
          REQUIRE(image_load != nullptr);
          auto buffer_load =
              phx::OpenGLImageBufferData<phx::OpenGLImageBufferDataType_RGB>::
                  CreateFromImage(image_load.get());
          auto buffer_save = phx::OpenGLImageBufferData<
              phx::OpenGLImageBufferDataType_RGB>::CreateFromImage(image.get());
          REQUIRE(buffer_load != nullptr);
          REQUIRE(buffer_save != nullptr);
          test_utilities::OpenGLBufferComparison::REQUIRE_SIMILARITY(
              *buffer_load.get(), *buffer_save.get(), 0.999);
        }
      }
      std::remove("test_image.jpg");
    }
  }

  GIVEN("An RGBA image with some pixels set in some colors") {
    auto image = std::make_unique<phx::Image>(
        std::array<std::size_t, 2>{{320, 240}}, 32);
    image->SetPixelColor({{10, 10}}, {{255, 128, 64, 255}});
    image->SetPixelColor({{50, 50}}, {{128, 128, 128, 255}});

    WHEN("We save it as a PNG") {
      bool success = image->Save("test_image_rgba.png");
      REQUIRE(success);

      WHEN("We load the file again") {
        auto image_load = std::make_unique<phx::Image>(
            "test_image_rgba.png");  // Throws on failure.
      }
      std::remove("test_image_rgba.png");
    }
  }

  GIVEN("An 8 bit (gray scale) image with some pixels set in some colors") {
    auto image =
        std::make_unique<phx::Image>(std::array<std::size_t, 2>{{320, 240}}, 8);
    image->SetPixelColor({{10, 10}}, {{192}});
    image->SetPixelColor({{50, 50}}, {{23}});

    WHEN("We save it as a PNG") {
      bool success = image->Save("test_image_8bit.png");
      REQUIRE(success);
      WHEN("We load the file again") {
        auto image_load = std::make_unique<phx::Image>(
            "test_image_8bit.png");  // Throws on failure.
      }
      std::remove("test_image_8bit.png");
    }
  }
}

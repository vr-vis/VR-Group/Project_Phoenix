//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include <array>
#include <utility>
#include <vector>

#include "catch/catch.hpp"

#include "phx/resources/types/mesh.hpp"

#define TEST_ATTRIBUTE(ATTRIBUTE_NAME, ATTRIBUTE_TYPE)           \
  WHEN("We query the " #ATTRIBUTE_NAME) {                        \
    auto& attributes = mesh.Get##ATTRIBUTE_NAME();               \
    THEN("The returned vector should be empty.") {               \
      REQUIRE(attributes.size() == 0);                           \
    }                                                            \
  }                                                              \
  WHEN("We add a vector of 10 " #ATTRIBUTE_NAME) {               \
    const std::size_t test_size{10};                             \
    std::vector<ATTRIBUTE_TYPE> attributes;                      \
    for (std::size_t i = 0; i < test_size; i++) {                \
      attributes.push_back(                                      \
          static_cast<ATTRIBUTE_TYPE>((static_cast<float>(i)))); \
    }                                                            \
    auto ref_values = attributes;                                \
    mesh.Set##ATTRIBUTE_NAME(std::move(attributes));             \
    THEN("It should contain our vector of " #ATTRIBUTE_NAME) {   \
      auto& atts = mesh.Get##ATTRIBUTE_NAME();                   \
      REQUIRE(atts.size() == test_size);                         \
      for (std::size_t i = 0; i < atts.size(); i++)              \
        REQUIRE(atts[i] == ref_values[i]);                       \
    }                                                            \
  }

SCENARIO("The mesh component keeps track of its attributes",
         "[phx][phx::Mesh]") {
  GIVEN("An empty mesh component") {
    phx::Mesh mesh;

    TEST_ATTRIBUTE(Vertices, glm::vec3);
    TEST_ATTRIBUTE(Normals, glm::vec3);
    TEST_ATTRIBUTE(TextureCoords, glm::vec2);
    TEST_ATTRIBUTE(Tangents, glm::vec3);
    TEST_ATTRIBUTE(Bitangents, glm::vec3);
    TEST_ATTRIBUTE(Indices, unsigned int);
  }
}

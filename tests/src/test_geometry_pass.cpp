//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include <iostream>
#include <string>
#include <utility>
#include <vector>

#include "catch/catch.hpp"

#include "phx/suppress_warnings.hpp"

SUPPRESS_WARNINGS_BEGIN
#include "GL/glew.h"

#include "glm/vec3.hpp"

#include "trompeloeil.hpp"
SUPPRESS_WARNINGS_END

#include "phx/core/entity.hpp"
#include "phx/rendering/backend/render_target.hpp"
#include "phx/rendering/components/projection.hpp"
#include "phx/rendering/components/transform.hpp"
#include "phx/rendering/render_passes/geometry_pass.hpp"

#include "mocks/opengl_mock.hpp"

#include "test_utilities/glm_mat4.hpp"

extern template struct trompeloeil::reporter<trompeloeil::specialized>;

SCENARIO(
    "The geometry pass collects all vertices in one vector and draws them.",
    "[phx][phx::GeometryPass]") {
  OPENGL_MOCK_ALLOW_ANY_CALL
  const GLuint vertex_buffer_id = 1u;
  const GLuint normal_buffer_id = 2u;
  const GLuint texCoord_buffer_id = 3u;
  const GLuint index_buffer_id = 4u;

  GIVEN("A geometry pass") {
    phx::Entity camera;
    camera.AddComponent<phx::Transform>();
    camera.AddComponent<phx::Projection>();
    auto render_target =
        camera.AddComponent<phx::RenderTarget>(glm::uvec2(1024, 768));
    phx::GeometryPass geometry_pass(render_target);

    WHEN("We initialize it.") {
      trompeloeil::sequence seq;
      REQUIRE_CALL(open_gl_mock, glCreateBuffers(1, _))
          .TIMES(1)
          .IN_SEQUENCE(seq)
          .SIDE_EFFECT(*_2 = 1);
      REQUIRE_CALL(open_gl_mock, glCreateBuffers(1, _))
          .TIMES(1)
          .IN_SEQUENCE(seq)
          .SIDE_EFFECT(*_2 = 2);
      REQUIRE_CALL(open_gl_mock, glCreateBuffers(1, _))
          .TIMES(1)
          .IN_SEQUENCE(seq)
          .SIDE_EFFECT(*_2 = 3);
      REQUIRE_CALL(open_gl_mock, glCreateBuffers(1, _))
          .TIMES(1)
          .IN_SEQUENCE(seq)
          .SIDE_EFFECT(*_2 = 4);
      geometry_pass.Initialize();
      THEN("It is valid.") { REQUIRE(geometry_pass.IsValid()); }

      THEN(
          "The arrays are bound and the shader is bound, even if nothing is "
          "drawn.") {
        REQUIRE_CALL(open_gl_mock, glBindVertexArray(ge(0u)));
        REQUIRE_CALL(open_gl_mock, glUseProgram(ge(0u)));
        geometry_pass.Execute();
      }

      WHEN("We add data for a mesh with 3 indices.") {
        phx::Entity entity;
        phx::Transform* transform = entity.AddComponent<phx::Transform>();
        phx::Mesh mesh;
        phx::Material material;
        mesh.SetIndices({0u, 1u, 2u});

        std::vector<phx::GeometryPass::RenderingInstance> geometry_pass_data;
        geometry_pass_data.push_back({&mesh, &material, transform});
        geometry_pass.SetData(geometry_pass_data);

        THEN("Then one triangle is drawn") {
          REQUIRE_CALL(
              open_gl_mock,
              glDrawElements(static_cast<GLenum>(GL_TRIANGLES), 3, _, _));
          geometry_pass.Execute();
        }
      }

      WHEN("We add data for a mesh with 3 vertices, normals, indices.") {
        phx::Entity entity;
        phx::Transform* transform = entity.AddComponent<phx::Transform>();
        phx::Mesh mesh;
        phx::Material material;
        mesh.SetVertices(
            {{0.0f, 0.0f, 0.0f}, {1.0f, 0.0f, 0.0f}, {1.0f, 1.0f, 0.0f}});
        mesh.SetNormals(
            {{0.0f, 0.0f, 1.0f}, {0.0f, 0.0f, 1.0f}, {0.0f, 0.0f, 1.0f}});
        mesh.SetTextureCoords({{0.0f, 0.0f}, {0.0f, 0.0f}, {0.0f, 0.0f}});
        mesh.SetIndices({0u, 1u, 2u});

        std::vector<phx::GeometryPass::RenderingInstance> geometry_pass_data;
        geometry_pass_data.push_back({&mesh, &material, transform});

        THEN(
            "3 vertices, 3 normals, 3 texCoords, 3 indices are uploaded and "
            "one triangle is "
            "drawn") {
          REQUIRE_CALL(open_gl_mock,
                       glNamedBufferSubData(vertex_buffer_id, 0,
                                            3 * sizeof(glm::vec3), _));
          REQUIRE_CALL(open_gl_mock,
                       glNamedBufferSubData(normal_buffer_id, 0,
                                            3 * sizeof(glm::vec3), _));
          REQUIRE_CALL(open_gl_mock,
                       glNamedBufferSubData(texCoord_buffer_id, 0,
                                            3 * sizeof(glm::vec2), _));
          REQUIRE_CALL(open_gl_mock,
                       glNamedBufferSubData(index_buffer_id, 0,
                                            3 * sizeof(unsigned int), _));
          geometry_pass.SetData(geometry_pass_data);

          REQUIRE_CALL(
              open_gl_mock,
              glDrawElements(static_cast<GLenum>(GL_TRIANGLES), 3, _, _));
          geometry_pass.Execute();
        }
      }
    }

    WHEN("We initialize it") {
      THEN("it creates one vertex and one fragment shader") {
        REQUIRE_CALL(open_gl_mock,
                     glCreateShader(static_cast<GLenum>(GL_VERTEX_SHADER)))
            .TIMES(1)
            .RETURN(1);
        REQUIRE_CALL(open_gl_mock,
                     glCreateShader(static_cast<GLenum>(GL_FRAGMENT_SHADER)))
            .TIMES(1)
            .RETURN(2);
        geometry_pass.Initialize();
      }
    }

    WHEN("We initialize it") {
      THEN(
          "it creates one vertex and one fragment shader, compiles both, "
          "attaches both to a program") {
        REQUIRE_CALL(open_gl_mock,
                     glCreateShader(static_cast<GLenum>(GL_VERTEX_SHADER)))
            .TIMES(1)
            .RETURN(1);
        REQUIRE_CALL(open_gl_mock,
                     glCreateShader(static_cast<GLenum>(GL_FRAGMENT_SHADER)))
            .TIMES(1)
            .RETURN(2);
        REQUIRE_CALL(open_gl_mock, glCompileShader(1u));
        REQUIRE_CALL(open_gl_mock, glCompileShader(2u));
        REQUIRE_CALL(open_gl_mock, glAttachShader(_, 1u));
        REQUIRE_CALL(open_gl_mock, glAttachShader(_, 2u));

        geometry_pass.Initialize();
      }

      THEN("it creates one vertex array, three buffers, and assigns them") {
        REQUIRE_CALL(open_gl_mock, glCreateVertexArrays(1, _))
            .TIMES(1)
            .SIDE_EFFECT(*_2 = 1);
        trompeloeil::sequence seq;
        REQUIRE_CALL(open_gl_mock, glCreateBuffers(1, _))
            .TIMES(1)
            .IN_SEQUENCE(seq)
            .SIDE_EFFECT(*_2 = 1);
        REQUIRE_CALL(open_gl_mock, glCreateBuffers(1, _))
            .TIMES(1)
            .IN_SEQUENCE(seq)
            .SIDE_EFFECT(*_2 = 2);
        REQUIRE_CALL(open_gl_mock, glCreateBuffers(1, _))
            .TIMES(1)
            .IN_SEQUENCE(seq)
            .SIDE_EFFECT(*_2 = 3);
        REQUIRE_CALL(open_gl_mock, glCreateBuffers(1, _))
            .TIMES(1)
            .IN_SEQUENCE(seq)
            .SIDE_EFFECT(*_2 = 4);
        REQUIRE_CALL(open_gl_mock, glVertexArrayVertexBuffer(1u, _, 1u, _, _));
        REQUIRE_CALL(open_gl_mock, glVertexArrayVertexBuffer(1u, _, 2u, _, _));
        REQUIRE_CALL(open_gl_mock, glVertexArrayVertexBuffer(1u, _, 3u, _, _));
        REQUIRE_CALL(open_gl_mock, glVertexArrayElementBuffer(1u, 4u));
        geometry_pass.Initialize();
      }
    }
  }
}

SCENARIO(
    "The geometry pass can draw several meshes with different transformations.",
    "[phx][phx::GeometryPass]") {
  OPENGL_MOCK_ALLOW_ANY_CALL
  const GLuint vertex_buffer_id = 1u;
  const GLuint normal_buffer_id = 2u;
  const GLuint index_buffer_id = 3u;

  GIVEN("An initialized geometry pass") {
    phx::Entity camera;
    camera.AddComponent<phx::Transform>();
    camera.AddComponent<phx::Projection>();
    auto render_target =
      camera.AddComponent<phx::RenderTarget>(glm::uvec2(1024, 768));
    phx::GeometryPass geometry_pass(render_target);
    geometry_pass.Initialize();

    WHEN("We add a meshes with two different transformations") {
      phx::Entity entity1;
      phx::Transform* transform1 = entity1.AddComponent<phx::Transform>();
      phx::Entity entity2;
      phx::Transform* transform2 = entity2.AddComponent<phx::Transform>();

      phx::Mesh mesh;
      phx::Material material;

      mesh.SetVertices(
          {{0.0f, 0.0f, 0.0f}, {1.0f, 0.0f, 0.0f}, {1.0f, 1.0f, 0.0f}});
      mesh.SetNormals(
          {{0.0f, 0.0f, 1.0f}, {0.0f, 0.0f, 1.0f}, {0.0f, 0.0f, 1.0f}});
      mesh.SetTextureCoords({{0.0f, 0.0f}, {0.0f, 0.0f}, {0.0f, 0.0f}});
      mesh.SetIndices({0u, 1u, 2u});

      transform1->SetGlobalTranslation(glm::vec3(-1, 0, 0));
      const glm::mat4 global_matrix1{transform1->GetGlobalMatrix()};
      transform2->SetGlobalTranslation(glm::vec3(1, 0, 0));
      const glm::mat4 local_matrix2{transform2->GetLocalMatrix()};

      phx::GeometryPass::RenderingInstance instance1{&mesh, &material,
                                                     transform1};
      phx::GeometryPass::RenderingInstance instance2{&mesh, &material,
                                                     transform2};

      geometry_pass.SetData({instance1, instance2}, {});
      THEN(
          "the rendering should be called twice with different "
          "transformations") {
        const GLuint model_matrix_location = 2u;
        std::string model_matrix_uniform_name = "model_matrix";
        trompeloeil::sequence seq;
        REQUIRE_CALL(open_gl_mock, glGetUniformLocation(_, _))
            .WITH(std::string(_2) == model_matrix_uniform_name)
            .IN_SEQUENCE(seq)
            .RETURN(model_matrix_location);
        REQUIRE_CALL(open_gl_mock, glProgramUniformMatrix4fv(
                                       _, model_matrix_location, _, _, _))
            .TIMES(1)
            .IN_SEQUENCE(seq)
            .WITH(*(reinterpret_cast<const glm::mat4*>(_5)) == global_matrix1);
        REQUIRE_CALL(open_gl_mock, glDrawElements(_, _, _, _))
            .TIMES(1)
            .IN_SEQUENCE(seq);
        REQUIRE_CALL(open_gl_mock, glGetUniformLocation(_, _))
            .WITH(std::string(_2) == model_matrix_uniform_name)
            .IN_SEQUENCE(seq)
            .RETURN(model_matrix_location);
        REQUIRE_CALL(open_gl_mock, glProgramUniformMatrix4fv(
                                       _, model_matrix_location, _, _, _))
            .TIMES(1)
            .IN_SEQUENCE(seq)
            .WITH(*(reinterpret_cast<const glm::mat4*>(_5)) == local_matrix2);
        REQUIRE_CALL(open_gl_mock, glDrawElements(_, _, _, _))
            .TIMES(1)
            .IN_SEQUENCE(seq);
        geometry_pass.Execute();
      }
    }
  }
}

//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include <iostream>
#include <memory>
#include <string>

#include "phx/suppress_warnings.hpp"

SUPPRESS_WARNINGS_BEGIN
#include "GL/glew.h"
SUPPRESS_WARNINGS_END

#include "trompeloeil.hpp"

#include "catch/catch.hpp"

#include "phx/rendering/backend/shader_program.hpp"
#include "phx/resources/resource_manager.hpp"
#include "phx/resources/resource_utils.hpp"

#include "mocks/opengl_mock.hpp"

extern template struct trompeloeil::reporter<trompeloeil::specialized>;

SCENARIO("We can load shaders.", "[phx][phx::Shader]") {
  OPENGL_MOCK_ALLOW_ANY_CALL
  GIVEN("An uninitialized Shader Object") {
    REQUIRE_CALL(open_gl_mock, glCreateProgram()).TIMES(1).RETURN(1u);
    phx::ShaderProgram shader_program;

    WHEN("we load an existing test fragment shader from the file system") {
      THEN(
          "the program is initialized and the shader is loaded from file and "
          "compiled.") {
        ALLOW_CALL(open_gl_mock, glGetShaderiv(1u, _, _))
            .WITH(_2 == GL_COMPILE_STATUS)
            .SIDE_EFFECT(*_3 = 1);
        REQUIRE_CALL(open_gl_mock, glCreateShader(_)).RETURN(1u);
        REQUIRE_CALL(open_gl_mock, glShaderSource(1u, 1, _, _))
            .WITH(std::string(*_3) == "test\n");
        REQUIRE_CALL(open_gl_mock, glCompileShader(1u));
        REQUIRE_CALL(open_gl_mock, glAttachShader(1u, 1u));

        auto fragment =
            phx::ResourceUtils::LoadResourceFromFile<phx::ShaderSource>(
                "shader/test.frag");

        bool success =
            shader_program.SetShader(phx::ShaderProgram::FRAGMENT, fragment);

        REQUIRE(success);
        REQUIRE(shader_program.is_valid());
      }
    }
  }
  GIVEN("An uninitialized Shader Object") {
    REQUIRE_CALL(open_gl_mock, glCreateProgram()).TIMES(1).RETURN(1u);
    phx::ShaderProgram shader_program;

    WHEN("we load all shaders from the filesystem with a given name") {
      THEN(
          "the program is initialized and the shader is loaded from file and "
          "compiled.") {
        ALLOW_CALL(open_gl_mock, glGetShaderiv(1u, _, _))
            .WITH(_2 == GL_COMPILE_STATUS)
            .SIDE_EFFECT(*_3 = 1);
        ALLOW_CALL(open_gl_mock, glCreateShader(_)).RETURN(1u);

        auto vertex =
            phx::ResourceUtils::LoadResourceFromFile<phx::ShaderSource>(
                "shader/phong.vert");
        auto fragment =
            phx::ResourceUtils::LoadResourceFromFile<phx::ShaderSource>(
                "shader/phong.frag");

        bool success =
            shader_program.SetShader(phx::ShaderProgram::VERTEX, vertex);
        success &=
            shader_program.SetShader(phx::ShaderProgram::FRAGMENT, fragment);

        REQUIRE(success);
        REQUIRE(shader_program.is_valid());
      }
    }
  }
}
SCENARIO("Shader Programs link themselves if they should be binded.",
         "[phx][phx::Shader]") {
  OPENGL_MOCK_ALLOW_ANY_CALL

  GIVEN("A phong shader program") {
    REQUIRE_CALL(open_gl_mock, glCreateProgram()).TIMES(1).RETURN(1u);
    phx::ShaderProgram shader_program;

    auto vertex = phx::ResourceUtils::LoadResourceFromFile<phx::ShaderSource>(
        "shader/phong.vert");
    auto fragment = phx::ResourceUtils::LoadResourceFromFile<phx::ShaderSource>(
        "shader/phong.frag");

    bool success = shader_program.SetShader(phx::ShaderProgram::VERTEX, vertex);
    success &= shader_program.SetShader(phx::ShaderProgram::FRAGMENT, fragment);

    WHEN("we use this for the first time") {
      THEN(" it is also LinkShaderProgram links it") {
        trompeloeil::sequence seq;
        REQUIRE_CALL(open_gl_mock, glLinkProgram(1u)).TIMES(1).IN_SEQUENCE(seq);
        REQUIRE_CALL(open_gl_mock, glUseProgram(1u)).TIMES(1).IN_SEQUENCE(seq);

        // the first time the question for linked status should return false,
        // that it is linked. The second time it should be already linked,
        // so true is returned to avoid a "not linked correctly"-warning
        ALLOW_CALL(open_gl_mock, glGetProgramiv(1u, _, _))
            .SIDE_EFFECT(static bool firstCall = true; if (firstCall) {
              firstCall = false;
              *_3 = GL_FALSE;
            } else { *_3 = GL_TRUE; });

        shader_program.Link();
        shader_program.use();

        WHEN("we use this for the second time") {
          THEN("LinkShaderProgram does not link it again") {
            FORBID_CALL(open_gl_mock, glLinkProgram(1u));
            REQUIRE_CALL(open_gl_mock, glUseProgram(1u)).TIMES(1);
            ALLOW_CALL(open_gl_mock, glGetProgramiv(1u, _, _))
                .SIDE_EFFECT(*_3 = GL_TRUE);

            shader_program.Link();
            shader_program.use();

            WHEN("we unuse it") {
              THEN(" the program is set to an invalid one") {
                REQUIRE_CALL(open_gl_mock, glUseProgram(0u)).TIMES(1);
                shader_program.unuse();
              }
            }
          }
        }
      }
    }
  }
}

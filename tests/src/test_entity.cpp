//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include "catch/catch.hpp"

#include "phx/core/component.hpp"
#include "phx/core/entity.hpp"
#include "phx/rendering/components/mesh_handle.hpp"

SCENARIO("An entity can keep track of components.", "[phx][phx::Entity]") {
  GIVEN("A new entity.") {
    phx::Entity entity;

    THEN("Our entity has 0 components.") {
      REQUIRE(entity.GetNumberOfComponents() == 0);
    }

    THEN("It is not associated with a scene if setup this way") {
      REQUIRE(entity.GetScene() == nullptr);
    }

    WHEN("We add a component.") {
      phx::MeshHandle* component_handle =
          entity.AddComponent<phx::MeshHandle>();

      THEN("You get a valid handle.") { REQUIRE(component_handle != nullptr); }

      THEN("Component is part of the entity.") {
        REQUIRE(entity.HasComponent(component_handle));
        REQUIRE(component_handle->GetEntity() == &entity);
      }

      THEN("Our entity has exactly one component.") {
        REQUIRE(entity.GetNumberOfComponents() == 1);
      }

      WHEN("We ask for the first mesh component.") {
        phx::MeshHandle* mesh_handle =
            entity.GetFirstComponent<phx::MeshHandle>();
        THEN("We get the mesh component we added earlier.") {
          REQUIRE(mesh_handle != nullptr);
          REQUIRE(mesh_handle == component_handle);
        }
      }

      WHEN("We remove that component.") {
        entity.RemoveComponent(component_handle);

        THEN("The component is no longer part of the entity.") {
          REQUIRE(!entity.HasComponent(component_handle));
        }

        THEN("Our entity shall have 0 components.") {
          REQUIRE(entity.GetNumberOfComponents() == 0);
        }
      }
    }
  }
}

//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include <array>
#include <string>

#include "catch/catch.hpp"

#include "phx/rendering/backend/opengl_image_buffer_data.hpp"
#include "phx/resources_path.hpp"
#include "test_utilities/opengl_buffer_data_comparison.hpp"

namespace {

constexpr std::size_t some_extent = 64;

struct AnyPixel {
  AnyPixel(std::size_t xx, std::size_t yy) : x{xx}, y{yy} {}
  std::size_t x{32};
  std::size_t y{32};
};
const AnyPixel any_pixel(32, 32);
const AnyPixel another_pixel(16, 16);

const phx::OpenGLImageBufferDataType_RGB black_rgb(0, 0, 0);
const phx::OpenGLImageBufferDataType_RGBA black_rgba(0, 0, 0, 0);
const phx::OpenGLImageBufferDataType_Byte black_byte(0);
const phx::OpenGLImageBufferDataType_Float32 black_float32(0.0f);
const phx::OpenGLImageBufferDataType_RGB any_rgb_color(255, 128, 64);
const phx::OpenGLImageBufferDataType_RGBA any_rgba_color(255, 128, 64, 142);
const phx::OpenGLImageBufferDataType_Byte any_rgb_colors_luminance_byte(149);
const phx::OpenGLImageBufferDataType_Float32 any_rgb_colors_luminance_float32(
    149.0f / 255.0f);
const phx::OpenGLImageBufferDataType_Byte neutral_byte_color(122);
const phx::OpenGLImageBufferDataType_RGB neutral_rgb_color(122, 122, 122);
const phx::OpenGLImageBufferDataType_RGBA neutral_rgba_color(122, 122, 122,
                                                             255);
const phx::OpenGLImageBufferDataType_Float32 neutral_float32_color(122.0f /
                                                                   255.0f);

}  // namespace

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-macros"
#endif

#define REQUIRE_PIXEL_RGB_COLOR(pixel, color) \
  REQUIRE(pixel.r_ == color.r_);              \
  REQUIRE(pixel.g_ == color.g_);              \
  REQUIRE(pixel.b_ == color.b_);

#define REQUIRE_PIXEL_RGBA_COLOR(pixel, color) \
  REQUIRE_PIXEL_RGB_COLOR(pixel, color)        \
  REQUIRE(pixel.a_ == color.a_);

#define REQUIRE_PIXEL_V_COLOR(pixel, color) \
  REQUIRE(pixel.value_ == Approx(color.value_).epsilon(0.01));

#ifdef __clang__
#pragma clang diagnostic pop
#endif

SCENARIO(
    "OpenGLImageBufferData can be initialized and its pixels can be written "
    "and read",
    "[phx][phx::OpenGLImageBufferData]") {
  GIVEN("An empty OpenGLImageBufferData with some extent and RGB format") {
    phx::OpenGLImageBufferData<phx::OpenGLImageBufferDataType_RGB> buffer(
        ::some_extent, ::some_extent);

    THEN("its area is correct") {
      REQUIRE(buffer.GetArea() == ::some_extent * ::some_extent);
    }

    THEN("Its size in bytes is correct") {
      REQUIRE(buffer.GetSizeInBytes() == ::some_extent * ::some_extent * 3);
    }

    WHEN("We read any of its pixels") {
      phx::OpenGLImageBufferDataType_RGB pixel =
          buffer.GetPixel(::any_pixel.x, ::any_pixel.y);

      THEN("It is black") { REQUIRE_PIXEL_RGB_COLOR(pixel, ::black_rgb); }
    }

    WHEN("We set that pixel to any color") {
      buffer.SetPixel(::any_pixel.x, ::any_pixel.y, ::any_rgb_color);

      THEN("It has the correct color") {
        phx::OpenGLImageBufferDataType_RGB pixel =
            buffer.GetPixel(::any_pixel.x, ::any_pixel.y);
        REQUIRE_PIXEL_RGB_COLOR(pixel, ::any_rgb_color);
      }
    }
  }

  GIVEN("An empty OpenGLImageBufferData with some extent and RGBA format") {
    phx::OpenGLImageBufferData<phx::OpenGLImageBufferDataType_RGBA> buffer(
        ::some_extent, ::some_extent);

    WHEN("We read any of its pixels") {
      phx::OpenGLImageBufferDataType_RGBA pixel =
          buffer.GetPixel(::any_pixel.x, ::any_pixel.y);

      THEN("It is black") { REQUIRE_PIXEL_RGBA_COLOR(pixel, ::black_rgba); }
    }

    WHEN("We set that pixel to any color") {
      buffer.SetPixel(::any_pixel.x, ::any_pixel.y, ::any_rgba_color);

      THEN("It has the correct color") {
        phx::OpenGLImageBufferDataType_RGBA pixel =
            buffer.GetPixel(::any_pixel.x, ::any_pixel.y);
        REQUIRE_PIXEL_RGBA_COLOR(pixel, ::any_rgba_color);
      }
    }
  }

  GIVEN("An empty OpenGLImageBufferData with some extent and Byte format") {
    phx::OpenGLImageBufferData<phx::OpenGLImageBufferDataType_Byte> buffer(
        ::some_extent, ::some_extent);

    WHEN("We read any of its pixels") {
      phx::OpenGLImageBufferDataType_Byte pixel =
          buffer.GetPixel(::any_pixel.x, ::any_pixel.y);

      THEN("It is black") { REQUIRE_PIXEL_V_COLOR(pixel, ::black_byte); }
    }

    WHEN("We set that pixel to any color") {
      buffer.SetPixel(::any_pixel.x, ::any_pixel.y, ::neutral_byte_color);

      THEN("It has the correct color") {
        phx::OpenGLImageBufferDataType_Byte pixel =
            buffer.GetPixel(::any_pixel.x, ::any_pixel.y);
        REQUIRE_PIXEL_V_COLOR(pixel, ::neutral_byte_color);
      }
    }
  }

  GIVEN("An empty OpenGLImageBufferData with some extent and Float32 format") {
    phx::OpenGLImageBufferData<phx::OpenGLImageBufferDataType_Float32> buffer(
        ::some_extent, ::some_extent);

    WHEN("We read any of its pixels") {
      phx::OpenGLImageBufferDataType_Float32 pixel =
          buffer.GetPixel(::any_pixel.x, ::any_pixel.y);

      THEN("It is black") { REQUIRE_PIXEL_V_COLOR(pixel, ::black_float32); }
    }

    WHEN("We set that pixel to any color") {
      buffer.SetPixel(::any_pixel.x, ::any_pixel.y, ::neutral_float32_color);

      THEN("It has the correct color") {
        phx::OpenGLImageBufferDataType_Float32 pixel =
            buffer.GetPixel(::any_pixel.x, ::any_pixel.y);
        REQUIRE_PIXEL_V_COLOR(pixel, ::neutral_float32_color);
      }
    }
  }
}

SCENARIO("OpenGLImageBufferData can be written to and read from disk",
         "[phx][phx::OpenGLImageBufferData]") {
  GIVEN("An empty OpenGLImageBufferData with some extent and RGBA format") {
    phx::OpenGLImageBufferData<phx::OpenGLImageBufferDataType_RGBA> buffer(
        ::some_extent, ::some_extent);

    WHEN(
        "We set a pixel to any color, save it to disk, and read "
        "it again") {
      buffer.SetPixel(::any_pixel.x, ::any_pixel.y, ::any_rgba_color);
      buffer.SaveToFileBinary("testbufferfile.bin");

      auto buffer2 =
          phx::OpenGLImageBufferData<phx::OpenGLImageBufferDataType_RGBA>::
              ReadFromFileBinary("testbufferfile.bin");
      THEN("They are identical") { REQUIRE(buffer == buffer2); }

      // clean up: delete file
      std::remove("testbufferfile.bin");
    }

    WHEN(
        "We set a pixel to any RGBA color, save it to disk as a png, and read "
        "it again") {
      std::string buffer_file_name{"testbufferfile_rgba.png"};
      buffer.SetPixel(::any_pixel.x, ::any_pixel.y, ::any_rgba_color);
      buffer.SaveToFilePNG(phx::resources_root + buffer_file_name);

      auto buffer2 =
          phx::OpenGLImageBufferData<phx::OpenGLImageBufferDataType_RGBA>::
              ReadFromFilePNG(phx::resources_root + buffer_file_name);
      THEN("They are identical") { REQUIRE(buffer == buffer2); }

      // clean up: delete file
      std::remove((phx::resources_root + buffer_file_name).c_str());
    }
  }

  GIVEN("An empty OpenGLImageBufferData with some extent and RGB format") {
    phx::OpenGLImageBufferData<phx::OpenGLImageBufferDataType_RGB> buffer(
        ::some_extent, ::some_extent);

    WHEN(
        "We set a pixel to any color, save it to disk, and read "
        "it again") {
      buffer.SetPixel(::any_pixel.x, ::any_pixel.y, ::any_rgb_color);
      buffer.SaveToFileBinary("testbufferfile.bin");

      auto buffer2 =
          phx::OpenGLImageBufferData<phx::OpenGLImageBufferDataType_RGB>::
              ReadFromFileBinary("testbufferfile.bin");
      THEN("They are identical") { REQUIRE(buffer == buffer2); }

      // clean up: delete file
      std::remove("testbufferfile.bin");
    }

    WHEN(
        "We set a pixel to any color, save it to disk as a png, and "
        "read it again") {
      buffer.SetPixel(::any_pixel.x, ::any_pixel.y, ::any_rgb_color);

      // we can leave out the extension, it will be added

      buffer.SaveToFilePNG(std::string(phx::resources_root) +
                           "testbufferfile_rgb");

      auto buffer2 =
          phx::OpenGLImageBufferData<phx::OpenGLImageBufferDataType_RGB>::
              ReadFromFilePNG(std::string(phx::resources_root) +
                              "testbufferfile_rgb.png");
      THEN("They are identical") { REQUIRE(buffer == buffer2); }

      // clean up: delete file
      std::remove((std::string(phx::resources_root) + "testbufferfile_rgb.png")
                      .c_str());
    }
  }

  GIVEN("An OpenGLImageBufferData with some content and Float32 format") {
    auto buffer =
        phx::OpenGLImageBufferData<phx::OpenGLImageBufferDataType_Float32>(
            ::some_extent, ::some_extent);
    buffer.SetPixel(::any_pixel.x, ::any_pixel.y, neutral_float32_color);

    WHEN("We save it to PNG and load it again") {
      buffer.SaveToFilePNG("testbufferfile_float.png");
      auto buffer2 =
          phx::OpenGLImageBufferData<phx::OpenGLImageBufferDataType_Float32>::
              ReadFromFilePNG("testbufferfile_float.png");

      THEN("They are identical") { REQUIRE(buffer == buffer2); }

      // clean up: delete file
      std::remove("testbufferfile_float.png");
    }
  }

  GIVEN("An OpenGLImageBufferData with some content and byte format") {
    auto buffer =
        phx::OpenGLImageBufferData<phx::OpenGLImageBufferDataType_Byte>(
            ::some_extent, ::some_extent);
    buffer.SetPixel(::any_pixel.x, ::any_pixel.y, neutral_byte_color);

    WHEN("We save it to PNG and load it again") {
      buffer.SaveToFilePNG("testbufferfile_byte.png");
      auto buffer2 =
          phx::OpenGLImageBufferData<phx::OpenGLImageBufferDataType_Byte>::
              ReadFromFilePNG("testbufferfile_byte.png");

      THEN("They are identical") { REQUIRE(buffer == buffer2); }

      // clean up: delete file
      std::remove("testbufferfile_byte.png");
    }
  }
}

SCENARIO(
    "OpenGLImageBufferData can be written to and read from disk in different "
    "formats",
    "[phx][phx::OpenGLImageBufferData]") {
  GIVEN("An OpenGLImageBufferData with some content and RGB format") {
    phx::OpenGLImageBufferData<phx::OpenGLImageBufferDataType_RGB> buffer(
        ::some_extent, ::some_extent);
    buffer.SetPixel(::any_pixel.x, ::any_pixel.y, ::any_rgb_color);

    WHEN("We save it to PNG and load it as RGBA") {
      buffer.SaveToFilePNG("testbufferfile_conversion_rgb.png");

      auto buffer2 =
          phx::OpenGLImageBufferData<phx::OpenGLImageBufferDataType_RGBA>::
              ReadFromFilePNG("testbufferfile_conversion_rgb.png");

      THEN("The pixel is still there and the conversion worked.") {
        auto pixel = buffer2.GetPixel(::any_pixel.x, ::any_pixel.y);
        REQUIRE_PIXEL_RGB_COLOR(pixel, ::any_rgb_color);
        REQUIRE(pixel.a_ == 255);
      }

      std::remove("testbufferfile_conversion_rgb.png");
    }

    WHEN("We save it to PNG and load it as 8bit") {
      buffer.SaveToFilePNG("testbufferfile_conversion_rgb2.png");

      auto buffer2 =
          phx::OpenGLImageBufferData<phx::OpenGLImageBufferDataType_Byte>::
              ReadFromFilePNG("testbufferfile_conversion_rgb2.png");

      THEN("The pixel is there, but gray") {
        auto pixel = buffer2.GetPixel(::any_pixel.x, ::any_pixel.y);
        REQUIRE_PIXEL_V_COLOR(pixel, ::any_rgb_colors_luminance_byte);
      }

      std::remove("testbufferfile_conversion_rgb2.png");
    }

    WHEN("We save it to PNG and load it as Float32") {
      buffer.SaveToFilePNG("testbufferfile_conversion_rgb3.png");

      auto buffer2 =
          phx::OpenGLImageBufferData<phx::OpenGLImageBufferDataType_Float32>::
              ReadFromFilePNG("testbufferfile_conversion_rgb3.png");

      THEN("The pixel is there, but gray") {
        auto pixel = buffer2.GetPixel(::any_pixel.x, ::any_pixel.y);
        REQUIRE_PIXEL_V_COLOR(pixel, ::any_rgb_colors_luminance_float32);
      }

      std::remove("testbufferfile_conversion_rgb3.png");
    }
  }

  GIVEN("An OpenGLImageBufferData with some content and RGBA format") {
    phx::OpenGLImageBufferData<phx::OpenGLImageBufferDataType_RGBA> buffer(
        ::some_extent, ::some_extent);
    buffer.SetPixel(::any_pixel.x, ::any_pixel.y, ::any_rgba_color);

    WHEN("We save it to PNG and load it as RGB") {
      buffer.SaveToFilePNG("testbufferfile_conversion_rgba.png");

      auto buffer2 =
          phx::OpenGLImageBufferData<phx::OpenGLImageBufferDataType_RGB>::
              ReadFromFilePNG("testbufferfile_conversion_rgba.png");

      THEN("The pixel is still there (without alpha)") {
        auto pixel = buffer2.GetPixel(::any_pixel.x, ::any_pixel.y);
        REQUIRE_PIXEL_RGB_COLOR(pixel, ::any_rgb_color);
      }

      std::remove("testbufferfile_conversion_rgba.png");
    }

    WHEN("We save it to PNG and load it as 8bit") {
      buffer.SaveToFilePNG("testbufferfile_conversion_rgba2.png");

      auto buffer2 =
          phx::OpenGLImageBufferData<phx::OpenGLImageBufferDataType_Byte>::
              ReadFromFilePNG("testbufferfile_conversion_rgba2.png");

      THEN("The pixel is there, but gray") {
        auto pixel = buffer2.GetPixel(::any_pixel.x, ::any_pixel.y);
        REQUIRE_PIXEL_V_COLOR(pixel, ::any_rgb_colors_luminance_byte);
      }

      std::remove("testbufferfile_conversion_rgba2.png");
    }

    WHEN("We save it to PNG and load it as Float32") {
      buffer.SaveToFilePNG("testbufferfile_conversion_rgba3.png");

      auto buffer2 =
          phx::OpenGLImageBufferData<phx::OpenGLImageBufferDataType_Float32>::
              ReadFromFilePNG("testbufferfile_conversion_rgba3.png");

      THEN("The pixel is there, but gray") {
        auto pixel = buffer2.GetPixel(::any_pixel.x, ::any_pixel.y);
        REQUIRE_PIXEL_V_COLOR(pixel, ::any_rgb_colors_luminance_float32);
      }

      std::remove("testbufferfile_conversion_rgba3.png");
    }
  }

  GIVEN("An OpenGLImageBufferData with some content and grayscale format") {
    phx::OpenGLImageBufferData<phx::OpenGLImageBufferDataType_Byte> buffer(
        ::some_extent, ::some_extent);
    buffer.SetPixel(::any_pixel.x, ::any_pixel.y,
                    phx::OpenGLImageBufferDataType_Byte(::neutral_byte_color));

    WHEN("We save it to PNG and load it as RGBA") {
      buffer.SaveToFilePNG("testbufferfile_conversion_byte.png");

      auto buffer2 =
          phx::OpenGLImageBufferData<phx::OpenGLImageBufferDataType_RGBA>::
              ReadFromFilePNG("testbufferfile_conversion_byte.png");

      THEN("The pixel is still there and the conversion worked.") {
        auto pixel = buffer2.GetPixel(::any_pixel.x, ::any_pixel.y);
        REQUIRE_PIXEL_RGBA_COLOR(pixel, ::neutral_rgba_color);
      }

      std::remove("testbufferfile_conversion_byte.png");
    }

    WHEN("We save it to PNG and load it as RGB") {
      buffer.SaveToFilePNG("testbufferfile_conversion_byte2.png");

      auto buffer2 =
          phx::OpenGLImageBufferData<phx::OpenGLImageBufferDataType_RGB>::
              ReadFromFilePNG("testbufferfile_conversion_byte2.png");

      THEN("The pixel is there, but gray") {
        auto pixel = buffer2.GetPixel(::any_pixel.x, ::any_pixel.y);
        REQUIRE_PIXEL_RGB_COLOR(pixel, ::neutral_rgb_color);
      }

      std::remove("testbufferfile_conversion_byte2.png");
    }

    WHEN("We save it to PNG and load it as Float32") {
      buffer.SaveToFilePNG("testbufferfile_conversion_byte3.png");

      auto buffer2 =
          phx::OpenGLImageBufferData<phx::OpenGLImageBufferDataType_Float32>::
              ReadFromFilePNG("testbufferfile_conversion_byte3.png");

      THEN("The pixel is there") {
        auto pixel = buffer2.GetPixel(::any_pixel.x, ::any_pixel.y);
        REQUIRE_PIXEL_V_COLOR(pixel, ::neutral_float32_color);
      }

      std::remove("testbufferfile_conversion_byte3.png");
    }
  }
}
SCENARIO("Distance between two OpenGLImageBufferDataType can be computed",
         "[phx][phx::OpenGLImageBufferDataType]") {
  GIVEN("Two OpenGLImageBufferDataType_RGB") {
    phx::OpenGLImageBufferDataType_RGB A{3, 4, 5};
    phx::OpenGLImageBufferDataType_RGB B{25, 1, 13};
    THEN("The distance is correct") {
      REQUIRE(phx::PixelDistance(A, B) == Approx(23.60085f));
    }
  }

  GIVEN("Two OpenGLImageBufferDataType_RGBA") {
    phx::OpenGLImageBufferDataType_RGBA A{3, 4, 5, 4};
    phx::OpenGLImageBufferDataType_RGBA B{25, 1, 13, 188};
    THEN("The distance is correct") {
      REQUIRE(phx::PixelDistance(A, B) == Approx(185.507f));
    }
  }

  GIVEN("Two OpenGLImageBufferDataType_Byte") {
    phx::OpenGLImageBufferDataType_Byte A{17};
    phx::OpenGLImageBufferDataType_Byte B{4};
    THEN("The distance is correct") {
      REQUIRE(phx::PixelDistance(A, B) == Approx(13.0f));
    }
  }

  GIVEN("Two OpenGLImageBufferDataType_Float32") {
    phx::OpenGLImageBufferDataType_Float32 A{3.1415f};
    phx::OpenGLImageBufferDataType_Float32 B{2.7181f};
    THEN("Their distance is correct") {
      REQUIRE(phx::PixelDistance(A, B) == Approx(0.4234f));
    }
  }
}

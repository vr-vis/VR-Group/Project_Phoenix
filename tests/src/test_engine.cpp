//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include <chrono>
#include <memory>
#include <string>
#include <thread>
#include <vector>

#include "catch/catch.hpp"

#include "phx/core/engine.hpp"
#include "phx/core/logger.hpp"
#include "phx/core/system.hpp"
#include "phx/display/display_system_window.hpp"
#include "phx/input/input_system.hpp"
#include "phx/rendering/rendering_system.hpp"
#include "phx/scripting/behavior.hpp"
#include "phx/setup.hpp"

#include "test_utilities/log_capture.hpp"

#include "trompeloeil.hpp"

#include "mocks/opengl_mock.hpp"
#include "mocks/openvr_mock.hpp"
#include "mocks/sdl_mock.hpp"

using trompeloeil::_;
using trompeloeil::ne;

extern template struct trompeloeil::reporter<trompeloeil::specialized>;

class TimeTrackerSystem : public phx::System {
 public:
  explicit TimeTrackerSystem(phx::Engine* engine) : phx::System(engine) {}

  void Update(const phx::FrameTimer::TimeInfo& time_info) override {
    time_info_ = &time_info;
  }

  const phx::FrameTimer::TimeInfo* time_info_ = nullptr;
};

class EngineStopTestSystem : public phx::System {
 public:
  explicit EngineStopTestSystem(phx::Engine* engine, int stop_in_frame = 1)
      : phx::System(engine), stop_on_update_(stop_in_frame) {}

  void Update(const phx::FrameTimer::TimeInfo& time_info) override {
    update_counter_++;
    if (update_counter_ >= stop_on_update_) {
      GetEngine()->Stop();
    }
    time_info_ = &time_info;
  }

  const phx::FrameTimer::TimeInfo* time_info_ = nullptr;

 private:
  int update_counter_ = 0;
  int stop_on_update_ = 1;
};

class WaitSystem : public phx::System {
 public:
  explicit WaitSystem(phx::Engine* engine) : phx::System(engine) {}
  void Update(const phx::FrameTimer::TimeInfo&) override {
    std::this_thread::sleep_for(std::chrono::milliseconds(5));
  }
};

template <typename T>
class TestOrderSystem : public phx::System {
 public:
  explicit TestOrderSystem(phx::Engine* engine) : phx::System(engine) {}
  void Update(const phx::FrameTimer::TimeInfo&) override {
    if (call_on_update_ != nullptr) call_on_update_();
  }

  void SetCallbackOnUpdate(const std::function<void(void)>& callback) {
    call_on_update_ = callback;
  }

 private:
  std::function<void(void)> call_on_update_;
};

class AnyBehavior : public phx::Behavior {
  void OnUpdate() override {}
};

SCENARIO("Only one engine should be created", "[phx][phx::Engine]") {
  auto log_capture = std::make_shared<test_utilities::LogCapture>();
  phx::logger = std::make_shared<spdlog::logger>("logcapture", log_capture);
  phx::logger->set_pattern("%w");
  GIVEN("An engine and later on a second engine") {
    // combined into one THEN case, because otherwise the way catch serializes
    // the test cases contradicts what we are about to test
    THEN("no warning is issued for the first but for the second one") {
      phx::Engine engine;
      REQUIRE(*log_capture == "");

      phx::Engine engine2;
      REQUIRE(*log_capture ==
              "WARNING: Only 1 engine should be used. Make sure you know "
              "what you are doing if you want to use more!!!!");
    }
  }
}

SCENARIO("An engine is a container of systems.", "[phx][phx::Engine]") {
  GIVEN("A new engine.") {
    phx::Engine engine;

    THEN("It does not contain a test system.") {
      REQUIRE(engine.GetSystem<EngineStopTestSystem>() == nullptr);

      WHEN("We add a test system to it.") {
        engine.CreateSystem<EngineStopTestSystem>();

        THEN("It contains a test system.") {
          REQUIRE(engine.GetSystem<EngineStopTestSystem>() != nullptr);
        }

        WHEN("We remove the test system from it.") {
          engine.RemoveSystem<EngineStopTestSystem>();

          THEN("It does not contain a test system.") {
            REQUIRE(engine.GetSystem<EngineStopTestSystem>() == nullptr);
          }
        }

        WHEN("We add another test system") {
          engine.CreateSystem<EngineStopTestSystem>();
          THEN("It contains two test systems") {
            std::vector<EngineStopTestSystem*> systems =
                engine.GetSystems<EngineStopTestSystem>();
            REQUIRE(systems.size() == 2);
          }
        }
      }
    }

    THEN("It is in stopped state.") {
      REQUIRE(engine.IsRunning() == false);

      WHEN("We add a test system and run it.") {
        engine.CreateSystem<EngineStopTestSystem>();
        engine.Run();

        // TODO(acd): Engine takes over the flow at this point until a system
        // stops it, hence we can't check if it is running from the outside.
        // Test system stops the engine on update to prevent infinite loop.

        THEN("It is in stopped state.") {
          REQUIRE(engine.IsRunning() == false);
        }
      }
    }
  }
}

SCENARIO("The active scene in an engine can be switched",
         "[phx][phx::Engine]") {
  GIVEN("An engine") {
    phx::Engine engine;
    auto first_scene = std::make_shared<phx::Scene>();

    // setup a way to test the scene changed signal
    phx::Scene* s1;
    phx::Scene* s2;
    engine.AddSceneChangedCallback(
        [&s1, &s2](std::shared_ptr<phx::Scene> param_s1,
                   std::shared_ptr<phx::Scene> param_s2) {
          s1 = param_s1.get();
          s2 = param_s2.get();
        });

    engine.SetScene(first_scene);
    THEN("It has a scene") { REQUIRE(engine.GetScene() != nullptr); }
    THEN("The old scene is nullptr.") { REQUIRE(s1 == nullptr); }
    THEN("The new scene is the one we just set.") {
      REQUIRE(s2 == first_scene.get());
    }
    WHEN("The scene is exchanged for a new scene") {
      auto new_scene = std::make_shared<phx::Scene>();
      engine.SetScene(new_scene);

      THEN(
          "The new scene is attached to the engine and the old scene is "
          "detached") {
        REQUIRE(engine.GetScene() == new_scene);
      }
      THEN("The signal is triggered with the appropriate values.") {
        REQUIRE(s1 == first_scene.get());
        REQUIRE(s2 == new_scene.get());
      }
    }
  }
}

SCENARIO("The engine provides simple time access", "[phx][phx::Engine]") {
  GIVEN("A new engine") {
    phx::Engine engine;
    WHEN(
        "We run it (we attach a system that makes it stop directly so we get "
        "back control)") {
      engine.CreateSystem<EngineStopTestSystem>();
      auto time_tracker_system = engine.CreateSystem<TimeTrackerSystem>();
      auto start_time_measured = std::chrono::high_resolution_clock::now();
      engine.Run();
      THEN(
          "We can get the clock time of the start of the frame (and it's about "
          "the current time)") {
        const auto frame_start_time =
            time_tracker_system->time_info_->frame_start_time;
        const auto now = std::chrono::high_resolution_clock::now();
        auto diff_in_us =
            std::abs(std::chrono::duration_cast<std::chrono::microseconds>(
                         now - frame_start_time)
                         .count());
        REQUIRE(diff_in_us < 5000);  // 5ms should be enough time by far
      }
      THEN(
          "We can get the start up time (= the time the engine was started), "
          "and it's about the time point we called Run()") {
        const auto start_time = time_tracker_system->time_info_->startup_time;
        auto diff_in_us =
            std::abs(std::chrono::duration_cast<std::chrono::microseconds>(
                         start_time - start_time_measured)
                         .count());
        REQUIRE(diff_in_us < 5000);
      }
      THEN("We can get time duration since startup, and it's close to 0") {
        auto seconds_since_startup =
            time_tracker_system->time_info_->time_since_startup;
        REQUIRE(seconds_since_startup.count() < 0.001);  // <1ms
      }

      THEN(
          "We can get the frame count, and it's 2, since 1 frame passed and 1 "
          "frame passed in initializing to get a valid delta") {
        REQUIRE(time_tracker_system->time_info_->frame_count == 2);
      }

      THEN(
          "The frame delta is the same as the time since startup, since only a "
          "single frame has passed (and there is no valid delta between two "
          "frames, yet)") {
        auto frame_delta =
            time_tracker_system->time_info_->time_since_last_frame;
        auto seconds_since_startup =
            time_tracker_system->time_info_->time_since_startup;
        double diff = frame_delta.count() - seconds_since_startup.count();
        REQUIRE(Approx(diff) == 0.0);
      }

      WHEN(
          "We let the engine run two frames instead of one, and add a system "
          "that consumes time") {
        engine.RemoveSystem<EngineStopTestSystem>();
        engine.CreateSystem<EngineStopTestSystem>(2);
        engine.CreateSystem<WaitSystem>();
        engine.Run();
        THEN("We get a frame delta > 0") {
          auto frame_delta =
              time_tracker_system->time_info_->time_since_last_frame;
          REQUIRE(frame_delta.count() > 0.0);
        }
        THEN("We can get the frame count, and it's 3") {
          REQUIRE(time_tracker_system->time_info_->frame_count == 3);
        }
      }
    }
  }
}

SCENARIO("An engine provides access to the behaviors", "[phx][phx::Engine]") {
  GIVEN("An engine with a scene and an entity") {
    phx::Engine engine;
    auto scene = std::make_shared<phx::Scene>();
    engine.SetScene(scene);
    auto entity = scene->CreateEntity();

    THEN("the engine returns no behaviors") {
      auto behaviors{engine.GetFirstComponents<phx::Behavior>()};
      REQUIRE(behaviors.size() == 0u);
    }

    WHEN("any behavior is added") {
      entity->AddComponent<AnyBehavior>();
      THEN("there the engine returns 1 AnyBehavior") {
        auto behaviors{engine.GetFirstComponents<AnyBehavior>()};
        REQUIRE(behaviors.size() == 1u);
      }
    }
  }
}

SCENARIO("An engine can be setup by the default setup", "[phx][phx::Engine]") {
  SDL_MOCK_ALLOW_ANY_CALL
  OPENGL_MOCK_ALLOW_ANY_CALL
  ALLOW_CALL(openvr_mock.Get(), VR_IsHmdPresent()).RETURN(false);
  GIVEN("Nothing") {
    WHEN("An engine is setup with the default setup") {
      auto engine = phx::Setup::CreateDefaultEngine();

      THEN("It is not null") {
        REQUIRE(engine != nullptr);
        THEN("It has a window") {
          auto window =
              engine->GetSystem<phx::DisplaySystemWindow>()->GetWindow();
          REQUIRE(window != nullptr);
        }
        THEN("It has a RenderingSystem") {
          phx::RenderingSystem* rendering_system =
              engine->GetSystem<phx::RenderingSystem>();
          REQUIRE(rendering_system != nullptr);
          REQUIRE(rendering_system->GetEngine() == engine.get());
        }
        THEN("It has an input system") {
          phx::InputSystem* input_system =
              engine->GetSystem<phx::InputSystem>();
          REQUIRE(input_system != nullptr);
          REQUIRE(input_system->GetEngine() == engine.get());
        }
        THEN("A default empty scene has been created") {
          REQUIRE(engine->GetScene() != nullptr);
        }
      }
    }
  }
}

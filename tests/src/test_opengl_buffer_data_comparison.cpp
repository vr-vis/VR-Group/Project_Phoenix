//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include <iostream>
#include "catch/catch.hpp"

#include "phx/rendering/backend/opengl_image_buffer_data.hpp"
#include "test_utilities/opengl_buffer_data_comparison.hpp"

SCENARIO("OpenGLImageBufferData can be compared", "[tests][test::utils]") {
  GIVEN("An OpenGLImageBufferData with size 64x64 and RGB format") {
    phx::OpenGLImageBufferData<phx::OpenGLImageBufferDataType_RGB> buffer(64,
                                                                          64);
    WHEN("We set one pixel brown (255, 128, 64)") {
      buffer.SetPixel(32, 32, {255, 128, 64});

      WHEN("We copy the buffer") {
        phx::OpenGLImageBufferData<phx::OpenGLImageBufferDataType_RGB> buffer2 =
            buffer;

        THEN("It will be identical") { REQUIRE(buffer == buffer2); }
      }

      WHEN("We compare it to an empty buffer of the same size") {
        phx::OpenGLImageBufferData<phx::OpenGLImageBufferDataType_RGB> buffer2(
            64, 64);

        THEN("It will not be identical") { REQUIRE(buffer != buffer2); }
      }
    }

    WHEN("We compare it to a different empty buffer with a different size") {
      phx::OpenGLImageBufferData<phx::OpenGLImageBufferDataType_RGB> buffer2(
          32, 32);

      THEN("They will not be equal") { REQUIRE(buffer != buffer2); }
    }
  }
}

SCENARIO(
    "The similarity between two OpenGLImageBufferData objects can be computed",
    "[tests][test::utils]") {
  GIVEN(
      "Two identical buffers with size 64x64, RGB format and one pixel not "
      "black") {
    phx::OpenGLImageBufferData<phx::OpenGLImageBufferDataType_RGB> buffer(64,
                                                                          64);
    buffer.SetPixel(32, 32, {255, 128, 64});
    auto buffer2 = buffer;

    THEN("Their similarity is 1") {
      REQUIRE(test_utilities::OpenGLBufferComparison::Similarity(
                  buffer, buffer2) == 1.0);
    }
  }

  GIVEN("Two buffers with different sizes") {
    phx::OpenGLImageBufferData<phx::OpenGLImageBufferDataType_RGB> buffer(64,
                                                                          64);
    phx::OpenGLImageBufferData<phx::OpenGLImageBufferDataType_RGB> buffer2(32,
                                                                           32);

    THEN("Their similarity is 0") {
      REQUIRE(test_utilities::OpenGLBufferComparison::Similarity(
                  buffer, buffer2) == 0.0);
    }
  }

  GIVEN("Two (black) 64x64 RGB buffers, and one has a white pixel") {
    phx::OpenGLImageBufferData<phx::OpenGLImageBufferDataType_RGB> buffer(64,
                                                                          64);
    phx::OpenGLImageBufferData<phx::OpenGLImageBufferDataType_RGB> buffer2(64,
                                                                           64);
    buffer.SetPixel(32, 32, {255, 255, 255});

    THEN("Their similarity is below 1") {
      REQUIRE(test_utilities::OpenGLBufferComparison::Similarity(buffer,
                                                                 buffer2) < 1);
    }

    THEN("They have a similarity of > 0.999") {
      REQUIRE(test_utilities::OpenGLBufferComparison::Similarity(
                  buffer, buffer2) > 0.999);
    }

    WHEN("We compare the black buffer against one with a gray pixel") {
      phx::OpenGLImageBufferData<phx::OpenGLImageBufferDataType_RGB> buffer3(
          64, 64);
      buffer3.SetPixel(32, 32, {128, 128, 128});
      THEN(
          "They will be more similar than the black buffer and the one with "
          "one white pixel") {
        REQUIRE(test_utilities::OpenGLBufferComparison::Similarity(buffer,
                                                                   buffer3) >
                test_utilities::OpenGLBufferComparison::Similarity(buffer,
                                                                   buffer2));
      }
    }
  }

  GIVEN(
      "Two (black) 64x64 RGBA buffers, and one has a white, full-alpha pixel") {
    phx::OpenGLImageBufferData<phx::OpenGLImageBufferDataType_RGBA> buffer(64,
                                                                           64);
    phx::OpenGLImageBufferData<phx::OpenGLImageBufferDataType_RGBA> buffer2(64,
                                                                            64);
    buffer.SetPixel(32, 32, {255, 255, 255, 255});

    THEN("Their similarity is below 1") {
      REQUIRE(test_utilities::OpenGLBufferComparison::Similarity(buffer,
                                                                 buffer2) < 1);
    }

    THEN("They have a similarity of > 0.999") {
      REQUIRE(test_utilities::OpenGLBufferComparison::Similarity(
                  buffer, buffer2) > 0.999);
    }

    WHEN(
        "We compare the black buffer against one with a gray, full-alpha "
        "pixel") {
      phx::OpenGLImageBufferData<phx::OpenGLImageBufferDataType_RGBA> buffer3(
          64, 64);
      buffer3.SetPixel(32, 32, {128, 128, 128, 255});
      THEN(
          "They will be more similar than the black buffer and the one with "
          "one white pixel") {
        REQUIRE(test_utilities::OpenGLBufferComparison::Similarity(buffer,
                                                                   buffer3) >
                test_utilities::OpenGLBufferComparison::Similarity(buffer,
                                                                   buffer2));
      }
    }
  }

  GIVEN("Two black 64x64 8bit buffers, and one has a white pixel") {
    phx::OpenGLImageBufferData<phx::OpenGLImageBufferDataType_Byte> buffer(64,
                                                                           64);
    phx::OpenGLImageBufferData<phx::OpenGLImageBufferDataType_Byte> buffer2(64,
                                                                            64);
    buffer.SetPixel(32, 32, phx::OpenGLImageBufferDataType_Byte(255));

    THEN("Their similarity is below 1") {
      REQUIRE(test_utilities::OpenGLBufferComparison::Similarity(buffer,
                                                                 buffer2) < 1);
    }

    THEN("They have a similarity of > 0.999") {
      REQUIRE(test_utilities::OpenGLBufferComparison::Similarity(
                  buffer, buffer2) > 0.999);
    }

    WHEN("We compare the black buffer against one with a gray pixel") {
      phx::OpenGLImageBufferData<phx::OpenGLImageBufferDataType_Byte> buffer3(
          64, 64);
      buffer3.SetPixel(32, 32, phx::OpenGLImageBufferDataType_Byte(128));
      THEN(
          "They will be more similar than the black buffer and the one with "
          "one white pixel") {
        REQUIRE(test_utilities::OpenGLBufferComparison::Similarity(buffer,
                                                                   buffer3) >
                test_utilities::OpenGLBufferComparison::Similarity(buffer,
                                                                   buffer2));
      }
    }
  }

  GIVEN("Two black 64x64 float buffers, and one has a white pixel") {
    phx::OpenGLImageBufferData<phx::OpenGLImageBufferDataType_Float32> buffer(
        64, 64);
    phx::OpenGLImageBufferData<phx::OpenGLImageBufferDataType_Float32> buffer2(
        64, 64);
    buffer.SetPixel(32, 32, phx::OpenGLImageBufferDataType_Float32(1.f));

    THEN("Their similarity is below 1") {
      REQUIRE(test_utilities::OpenGLBufferComparison::Similarity(buffer,
                                                                 buffer2) < 1);
    }

    THEN("They have a similarity of > 0.999") {
      REQUIRE(test_utilities::OpenGLBufferComparison::Similarity(
                  buffer, buffer2) > 0.999);
    }

    WHEN("We compare the black buffer against one with a gray pixel") {
      phx::OpenGLImageBufferData<phx::OpenGLImageBufferDataType_Float32>
          buffer3(64, 64);
      buffer3.SetPixel(32, 32, phx::OpenGLImageBufferDataType_Float32(0.5f));
      THEN(
          "They will be more similar than the black buffer and the one with "
          "one white pixel") {
        REQUIRE(test_utilities::OpenGLBufferComparison::Similarity(buffer,
                                                                   buffer3) >
                test_utilities::OpenGLBufferComparison::Similarity(buffer,
                                                                   buffer2));
      }
    }
  }
}

SCENARIO(
    "A difference magnitude image/buffer of two OpenGLImageBufferData can be "
    "computed",
    "[tests][test::utils]") {
  GIVEN("Two OpenGLImageBufferData of unequal size") {
    phx::OpenGLImageBufferData<phx::OpenGLImageBufferDataType_RGB> buffer1(64,
                                                                           64);
    phx::OpenGLImageBufferData<phx::OpenGLImageBufferDataType_RGB> buffer2(64,
                                                                           32);

    THEN("The difference magnitude image is null") {
      auto result =
          phx::OpenGLImageBufferData<phx::OpenGLImageBufferDataType_RGB>::
              CreateDifferenceMagnitudeBuffer(buffer1, buffer2);
      REQUIRE(result == nullptr);
    }
  }

  GIVEN(
      "Two OpenGLImageBufferData with size 64x64 and RGB format that differ in "
      "some pixels") {
    phx::OpenGLImageBufferData<phx::OpenGLImageBufferDataType_RGB> buffer1(64,
                                                                           64);
    phx::OpenGLImageBufferData<phx::OpenGLImageBufferDataType_RGB> buffer2(64,
                                                                           64);
    buffer1.SetPixel(5, 6, {128, 128, 128});
    buffer2.SetPixel(5, 6, {190, 120, 130});
    buffer2.SetPixel(18, 18, {116, 90, 22});

    THEN(
        "The difference magnitude image is greater than 0 at these locations") {
      auto result =
          phx::OpenGLImageBufferData<phx::OpenGLImageBufferDataType_RGB>::
              CreateDifferenceMagnitudeBuffer(buffer1, buffer2);
      REQUIRE(result != nullptr);

      REQUIRE(result->GetPixel(5, 6).value_ > 0.f);
      REQUIRE(result->GetPixel(18, 18).value_ > 0.f);
      REQUIRE(result->GetPixel(55, 21).value_ == 0.f);
    }
  }
}

SCENARIO(
    "REQUIRE_SIMILARITY can be used to check for similarity and on failure "
    "write the images to disk",
    "[tests][test::utils]") {
  GIVEN("Two buffer images") {
    phx::OpenGLImageBufferData<phx::OpenGLImageBufferDataType_RGB> buffer1(64,
                                                                           64);
    phx::OpenGLImageBufferData<phx::OpenGLImageBufferDataType_RGB> buffer2(64,
                                                                           64);

    WHEN("They are compared using REQUIRE_SIMILARITY") {
      THEN(
          "If they are not similar enough, they are written to disk and the "
          "filenames are output.") {
        test_utilities::OpenGLBufferComparison::REQUIRE_SIMILARITY(
            buffer1, buffer2, 0.99);
      }
    }
  }
}

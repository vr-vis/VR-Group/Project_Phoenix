//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include <string>

#include "catch/catch.hpp"

#include "phx/resources/loaders/assimp_model_loader.hpp"
#include "phx/resources/resource_manager.hpp"
#include "phx/resources/resource_utils.hpp"
#include "phx/resources/types/model.hpp"

#include "mocks/opengl_mock.hpp"
#include "trompeloeil.hpp"

extern template struct trompeloeil::reporter<trompeloeil::specialized>;

SCENARIO("A Model can be loaded that contains multiple meshes",
         "[phx][phx::Model]") {
  OPENGL_MOCK_ALLOW_ANY_CALL
  GIVEN("A fresh resource manager...") {
    GIVEN("A file name of an .obj file...") {
      std::string model_file_name{"models/2MeshTest/2meshTest.obj"};
      WHEN("A model resource is declared and loaded") {
        auto model = phx::ResourceUtils::LoadResourceFromFile<phx::Model>(
            model_file_name);
        THEN("the returned model contains 2 meshes") {
          REQUIRE(model->GetMeshes().size() == 2u);
        }
        THEN(
            "the returned model contains 3 materials - 2 from the file, plus "
            "one default material") {
          REQUIRE(model->GetMaterials().size() == 3u);
        }
        THEN(
            "Mesh no. 0 is connected to material no. 2, and mesh no. 1 to "
            "material no. 1") {
          REQUIRE(model->GetMaterialForMesh(model->GetMeshes()[0]) ==
                  model->GetMaterials()[2]);
          REQUIRE(model->GetMaterialForMesh(model->GetMeshes()[1]) ==
                  model->GetMaterials()[1]);
        }
      }
    }
  }
}

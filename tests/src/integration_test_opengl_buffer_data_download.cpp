//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include <iostream>
#include <memory>
#include <utility>
#include <vector>

#include "catch/catch.hpp"

#include "phx/suppress_warnings.hpp"

#include "phx/core/engine.hpp"
#include "phx/core/entity.hpp"
#include "phx/core/scene.hpp"
#include "phx/display/window.hpp"
#include "phx/rendering/backend/opengl_image_buffer_data.hpp"
#include "phx/rendering/components/mesh_handle.hpp"
#include "phx/rendering/rendering_system.hpp"
#include "phx/resources/resource_declaration.hpp"
#include "phx/resources/resource_manager.hpp"
#include "phx/resources/resource_pointer.hpp"
#include "phx/resources/types/mesh.hpp"
#include "phx/setup.hpp"

SUPPRESS_WARNINGS_BEGIN
#include "mocks/openvr_mock.hpp"
SUPPRESS_WARNINGS_END

#include "test_utilities/dummy_mesh_generator.hpp"
#include "test_utilities/opengl_buffer_data_comparison.hpp"

#include "trompeloeil.hpp"

extern template struct trompeloeil::reporter<trompeloeil::specialized>;

class EngineStopTestSystem : public phx::System {
 public:
  explicit EngineStopTestSystem(phx::Engine* engine, int stop_in_frame = 1)
      : phx::System(engine), stop_on_update_(stop_in_frame) {}

  void Update(const phx::FrameTimer::TimeInfo&) override {
    update_counter_++;
    if (update_counter_ >= stop_on_update_) {
      GetEngine()->Stop();
    }
  }

 private:
  int update_counter_ = 0;
  int stop_on_update_ = 1;
};

class SceneSetupEmpty {
 public:
  SceneSetupEmpty() : engine_(phx::Setup::CreateDefaultEngine()) {
    engine_->CreateSystem<EngineStopTestSystem>();
    engine_->Run();
  }
  ~SceneSetupEmpty() = default;

 private:
  std::unique_ptr<phx::Engine> engine_;
};

class SceneSetupSimple {
 public:
  SceneSetupSimple() : engine_(phx::Setup::CreateDefaultEngine()) {
    auto scene = engine_->GetScene();

    phx::Entity* triangle = scene->CreateEntity();
    triangle->AddComponent<phx::Transform>();

    auto material = phx::ResourceUtils::LoadGenericMaterial(
        "custommaterial", glm::vec3(), glm::vec3(), glm::vec3(1.f, 0.5f, 0.25f),
        500.f);

    phx::MaterialHandle* material_handle =
        triangle->AddComponent<phx::MaterialHandle>();
    material_handle->SetMaterial(material);

    std::vector<glm::vec3> vertices = {
        {-4.0f, -3.0f, -0.05f}, {4.0f, -3.0f, -0.05f}, {4.0f, 3.0f, -0.05f}};
    std::vector<glm::vec3> normals = {
        {0.f, 0.f, 1.f}, {0.f, 0.f, 1.f}, {0.f, 0.f, 1.f}};
    std::vector<unsigned int> indices = {0u, 1u, 2u};

    phx::ResourceManager::instance().RegisterResourceType(
        "staticmesh",
        std::make_unique<phx::DummyMeshLoader>(
            std::move(vertices), std::move(normals), std::move(indices)));
    auto mesh = phx::ResourceManager::instance().DeclareResource<phx::Mesh>(
        {{"TYPE", "staticmesh"}});
    mesh.Load();

    auto mesh_handle = triangle->AddComponent<phx::MeshHandle>();
    mesh_handle->SetMesh(mesh);

    phx::Entity* main_light = scene->CreateEntity();
    main_light->AddComponent<phx::Transform>();
    phx::Light* light = main_light->AddComponent<phx::Light>();
    light->SetType(phx::Light::Type::kDirectional);
    light->SetColor(glm::vec3(1.0f, 1.0f, 1.0f));
    light->SetIntensity(1.0f);

    engine_->CreateSystem<EngineStopTestSystem>(180);
    engine_->Run();
  }

  ~SceneSetupSimple() {}

 private:
  std::unique_ptr<phx::Engine> engine_;
  std::unique_ptr<phx::Mesh> test_mesh_;
};

SCENARIO("OpenGLImageBufferData can download pixels from the frame buffer",
         ".") {
  ALLOW_CALL(openvr_mock.Get(), VR_IsHmdPresent()).RETURN(false);
  GIVEN("An empty, black frame buffer") {
    using test_utilities::OpenGLBufferComparison;
    // setup empty scene
    SceneSetupEmpty setup;

    WHEN("We download its RGB data") {
      phx::OpenGLImageBufferData<phx::OpenGLImageBufferDataType_RGB> buffer(
          1024, 768);
      buffer.ReadColorPixels(true);
      THEN("all its pixels correspond to 0 (black)") {
        phx::OpenGLImageBufferData<phx::OpenGLImageBufferDataType_RGB>
            blackBuffer(1024, 768);
        OpenGLBufferComparison::REQUIRE_SIMILARITY(buffer, blackBuffer, 1.0);
      }
    }

    WHEN("We download its RGBA data") {
      phx::OpenGLImageBufferData<phx::OpenGLImageBufferDataType_RGBA> buffer(
          1024, 768);
      buffer.ReadColorPixels(true);
      THEN("all its pixels correspond to 0 (black), with alpha=1") {
        phx::OpenGLImageBufferData<phx::OpenGLImageBufferDataType_RGBA>
            blackBuffer(1024, 768);
        phx::OpenGLImageBufferDataType_RGBA blackPixel = {0, 0, 0, 255};
        for (std::size_t y = 0; y < 768; ++y) {
          for (std::size_t x = 0; x < 1024; ++x) {
            blackBuffer.SetPixel(x, y, blackPixel);
          }
        }
        OpenGLBufferComparison::REQUIRE_SIMILARITY(buffer, blackBuffer, 1.0);
      }
    }

    WHEN("We download its depth data (8bit)") {
      phx::OpenGLImageBufferData<phx::OpenGLImageBufferDataType_Byte> buffer(
          1024, 768);
      buffer.ReadDepthPixels(true);
      THEN("all depth pixels correspond to 255 (max distance)") {
        phx::OpenGLImageBufferData<phx::OpenGLImageBufferDataType_Byte>
            whiteBuffer(1024, 768);
        phx::OpenGLImageBufferDataType_Byte whitePixel(255u);
        for (std::size_t y = 0; y < 768; ++y) {
          for (std::size_t x = 0; x < 1024; ++x) {
            whiteBuffer.SetPixel(x, y, whitePixel);
          }
        }
        OpenGLBufferComparison::REQUIRE_SIMILARITY(buffer, whiteBuffer, 1.0);
      }
    }

    WHEN("We download its depth data (32bit float)") {
      phx::OpenGLImageBufferData<phx::OpenGLImageBufferDataType_Float32> buffer(
          1024, 768);
      buffer.ReadDepthPixels(true);
      THEN("all depth pixels correspond to 1.f (max distance)") {
        phx::OpenGLImageBufferData<phx::OpenGLImageBufferDataType_Float32>
            whiteBuffer(1024, 768);
        phx::OpenGLImageBufferDataType_Float32 whitePixel(1.f);
        for (std::size_t y = 0; y < 768; ++y) {
          for (std::size_t x = 0; x < 1024; ++x) {
            whiteBuffer.SetPixel(x, y, whitePixel);
          }
        }
        OpenGLBufferComparison::REQUIRE_SIMILARITY(buffer, whiteBuffer, 1.0);
      }
    }
  }

  GIVEN(
      "A simple scene featuring a single triangle, rendered into a frame "
      "buffer") {
    using test_utilities::OpenGLBufferComparison;
    // setup simple scene
    SceneSetupSimple setup;

    WHEN("We download its RGB data") {
      phx::OpenGLImageBufferData<phx::OpenGLImageBufferDataType_RGB> buffer(
          1024, 768);
      buffer.ReadColorPixels(true);
      THEN(
          "The lower right half of the image is colored, the other half is "
          "black") {
        phx::OpenGLImageBufferData<phx::OpenGLImageBufferDataType_RGB>
            triangleBuffer(1024, 768);
        for (std::size_t y = 0; y < 768; ++y) {
          for (std::size_t x = 0; x < 1024; ++x) {
            // in lower right half?
            bool lower_right_half =
                static_cast<float>(x) / 1024.f > static_cast<float>(y) / 768.f;
            if (lower_right_half) {
              triangleBuffer.SetPixel(x, y, {255, 128, 64});
            }
          }
        }
        OpenGLBufferComparison::REQUIRE_SIMILARITY(buffer, triangleBuffer,
                                                   0.995);
      }
    }

    WHEN("We download its depth data") {
      phx::OpenGLImageBufferData<phx::OpenGLImageBufferDataType_Float32> buffer(
          1024, 768);
      buffer.ReadDepthPixels(true);

      THEN(
          "The lower right half of the image is at depth 0.8, the other half "
          "is at 1.0") {
        phx::OpenGLImageBufferData<phx::OpenGLImageBufferDataType_Float32>
            triangleBuffer(1024, 768);
        for (std::size_t y = 0; y < 768; ++y) {
          for (std::size_t x = 0; x < 1024; ++x) {
            // in lower right half?
            bool lower_right_half =
                static_cast<float>(x) / 1024.f > static_cast<float>(y) / 768.f;
            if (lower_right_half) {
              triangleBuffer.SetPixel(
                  x, y, phx::OpenGLImageBufferDataType_Float32(0.8f));
            } else {
              triangleBuffer.SetPixel(
                  x, y, phx::OpenGLImageBufferDataType_Float32(1.f));
            }
          }
        }
        OpenGLBufferComparison::REQUIRE_SIMILARITY(buffer, triangleBuffer,
                                                   0.995);
      }
    }
  }
}

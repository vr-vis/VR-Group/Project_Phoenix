//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include <fstream>
#include <memory>
#include <string>

#include "phx/suppress_warnings.hpp"

SUPPRESS_WARNINGS_BEGIN
#include "glm/glm.hpp"
SUPPRESS_WARNINGS_END

#include "catch/catch.hpp"

#include "assimp/postprocess.h"  // Post processing flags

#include "phx/resources/loaders/image_loader.hpp"
#include "phx/resources/types/image.hpp"
#include "phx/resources/types/model.hpp"
#include "phx/resources/types/shader_source.hpp"
#include "phx/resources/resource_declaration.hpp"
#include "phx/resources/resource_manager.hpp"
#include "phx/resources/resource_utils.hpp"

SCENARIO(
    "Check the declaration phase of a resource life cycle. A resource needs to "
    "be declared first and it can only be declared once.",
    "[phx][phx::ResourceManager]") {
  GIVEN("A fresh resource manager...") {
    auto &manager = phx::ResourceManager::instance();
    GIVEN("A file name of an .obj file...") {
      std::string model_file_name{"models/bunny.obj"};
      phx::ResourceDeclaration model_declaration =
          phx::ResourceUtils::DeclarationFromFile(model_file_name);
      WHEN("A resource is declared") {
        auto model = manager.DeclareResource<phx::Model>(model_declaration);
        THEN("the returned resource pointer represents an unloaded resource") {
          REQUIRE(model == nullptr);
        }
        WHEN("The same resource declaration is issued again") {
          auto redundant_model_resource_ptr =
              manager.DeclareResource<phx::Model>(model_declaration);
          THEN(
              "An equal resource pointer is returned (i.e. each resource "
              "exists only "
              "once!).") {
            REQUIRE(redundant_model_resource_ptr == model);
          }
        }
      }
    }
  }
}

SCENARIO(
    "Check the load/unload phases of a resource life cycle. A resource is "
    "offline after declaration and can be loaded in order to bring it online.",
    "[phx][phx::ResourceManager]") {
  GIVEN("A fresh resource manager and a declared model resource") {
    auto &manager = phx::ResourceManager::instance();
    std::string model_file_name{"models/bunny.obj"};
    phx::ResourceDeclaration model_declaration =
        phx::ResourceUtils::DeclarationFromFile(model_file_name);
    auto model = manager.DeclareResource<phx::Model>(model_declaration);
    WHEN("A resource has not yet been loaded") {
      THEN(
          "its state is still \"offline\", i.e., its resource pointer is equal "
          "to nullptr") {
        REQUIRE(model == nullptr);
      }
      THEN("accessing the resource directly yields a nullptr.") {
        REQUIRE(model.Get() == nullptr);
      }
    }
    WHEN("A resource is loaded through its resource pointer") {
      model.Load();
      THEN(
          "its state changes to \"online\", i.e., its resource pointer is not "
          "nullptr anymore") {
        REQUIRE(model != nullptr);
      }
      THEN("the resource itself is available through the resource pointer") {
        auto model_resource = model.Get();
        REQUIRE(model_resource != nullptr);
        REQUIRE(model_resource->GetMeshes().size() > 0);

        WHEN("the resource is loaded again") {
          model.Load();
          THEN("the underlying resource remains the same") {
            auto redundant_model = model.Get();
            REQUIRE(model != nullptr);
            REQUIRE(redundant_model == model_resource);
          }
        }
      }
      WHEN("a resource pointer is used to unload a resource again") {
        model.Unload();
        THEN("its state changes to \"offline\" (again)") {
          REQUIRE(model == nullptr);
        }
        THEN("its resource reverts to nullptr") {
          REQUIRE(model.Get() == nullptr);
        }
        WHEN("an unloaded resource is unloaded again") {
          model.Unload();
          THEN("its state remains unchanged") {
            REQUIRE(model == nullptr);
            REQUIRE(model.Get() == nullptr);
          }
        }
      }
    }
  }
}

SCENARIO("Shaders are resources", "[phx][phx::ResourceManager]") {
  GIVEN("An empty resource manager") {
    auto &manager = phx::ResourceManager::instance();
    GIVEN("A shader file name") {
      const std::string shader_file{"shader/test.frag"};
      WHEN("A shader is declared") {
        auto shader = manager.DeclareResource<phx::ShaderSource>(
            phx::ResourceUtils::DeclarationFromFile(shader_file));
        WHEN("The shader is loaded through its resource pointer") {
          shader.Load();
          THEN("It is now online") { REQUIRE(shader != nullptr); }
          THEN(
              "the resource (shader source) is available through the resource "
              "pointer") {
            auto shader_resource = shader.Get();
            REQUIRE(shader_resource != nullptr);
            REQUIRE(shader->GetSourceLength() > 0);
          }
          WHEN("The shader is unloaded again") {
            shader.Unload();
            THEN("its resource pointer compares to null again") {
              REQUIRE(shader == nullptr);
            }
            THEN("the resource (shader source) is gone") {
              REQUIRE(shader.Get() == nullptr);
            }
          }
        }
      }
    }
  }
}

SCENARIO("Images are resources", "[phx][phx::ResourceManager]") {
  GIVEN("An empty resource manager") {
    auto &manager = phx::ResourceManager::instance();
    GIVEN("An image file name") {
      const std::string image_file{"textures/test.jpg"};
      WHEN("An image is declared") {
        auto image = manager.DeclareResource<phx::Image>(
            phx::ResourceUtils::DeclarationFromFile(image_file));
        WHEN("The image is loaded through its resource pointer") {
          image.Load();
          THEN("The resource pointer is not null anymore") {
            REQUIRE(image != nullptr);
          }
          THEN("the image data is available through the resource pointer") {
            auto image_resource = image.Get();
            REQUIRE(image_resource != nullptr);
            REQUIRE(image->GetDimensions()[0] > 0);
          }
          WHEN("The image is unloaded again") {
            image.Unload();
            THEN("the resource pointer equals null again") {
              REQUIRE(image == nullptr);
            }
            THEN("the image data is gone") { REQUIRE(image.Get() == nullptr); }
          }
        }
      }
    }
    GIVEN(
        "An image file name containing uppercase letters in the file "
        "extension") {
      const std::string image_file{"textures/test_uppercase.JPG"};
      WHEN("An image is declared and loaded") {
        auto image = manager.DeclareResource<phx::Image>(
            phx::ResourceUtils::DeclarationFromFile(image_file));
        image.Load();
        THEN("the image data is available through the resource pointer") {
          auto image_resource = image.Get();
          REQUIRE(image_resource != nullptr);
          REQUIRE(image->GetDimensions()[0] > 0);
        }
      }
    }
  }
}

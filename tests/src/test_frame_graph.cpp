//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include <memory>
#include <utility>

#include "catch/catch.hpp"

#include "trompeloeil.hpp"

#include "phx/rendering/render_passes/render_pass.hpp"
#include "phx/rendering/frame_graph.hpp"

extern template struct trompeloeil::reporter<trompeloeil::specialized>;

class MockRenderPass : public phx::RenderPass {
 public:
  MAKE_MOCK0(Initialize, void());
  MAKE_MOCK0(Execute, void());
};

SCENARIO("A frame graph manages render passes.", "[phx][phx::FrameGraph]") {
  GIVEN("An empty frame graph") {
    phx::FrameGraph frame_graph;

    THEN("It should have 0 render passes.") {
      REQUIRE(frame_graph.GetNumberOfPasses() == 0);
    }

    WHEN("We add a mock render pass") {
      std::unique_ptr<MockRenderPass> pass = std::make_unique<MockRenderPass>();
      MockRenderPass* pass_ptr = static_cast<MockRenderPass*>(
          frame_graph.AddRenderPass(std::move(pass)));

      THEN("the frame graph has 1 render pass.") {
        REQUIRE(frame_graph.GetNumberOfPasses() == 1);
      }

      THEN("we can get the mock render pass out again") {
        auto mockrenderpasses = frame_graph.GetRenderPasses<MockRenderPass>();
        REQUIRE(mockrenderpasses.size() == 1);
        REQUIRE(mockrenderpasses.front() == pass_ptr);
      }

      WHEN("we call create") {
        THEN("the mock render pass was initialized but not executed") {
          REQUIRE_CALL(*pass_ptr, Initialize()).TIMES(1);
          FORBID_CALL(*pass_ptr, Execute());
          frame_graph.Initialize();
        }
      }

      WHEN("we traverse the graph") {
        THEN("the mock render pass was executed") {
          REQUIRE_CALL(*pass_ptr, Execute()).TIMES(1);
          FORBID_CALL(*pass_ptr, Initialize());
          frame_graph.Execute();
        }
      }
    }
  }
}

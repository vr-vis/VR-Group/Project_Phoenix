//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include <vector>

#include "catch/catch.hpp"

#include "phx/display/display_system_window.hpp"
#include "phx/core/engine.hpp"
#include "phx/core/entity.hpp"
#include "phx/core/scene.hpp"
#include "phx/rendering/components/transform.hpp"
#include "phx/rendering/render_passes/geometry_pass.hpp"
#include "phx/rendering/rendering_system.hpp"
#include "phx/resources/types/mesh.hpp"

#include "trompeloeil.hpp"

#include "mocks/opengl_mock.hpp"
#include "mocks/sdl_mock.hpp"

extern template struct trompeloeil::reporter<trompeloeil::specialized>;

SCENARIO(
    "The rendering system holds the render passes and calls the initialize and "
    "execute methods.",
    "[phx][phx::RenderingSystem]") {
  OPENGL_MOCK_ALLOW_ANY_CALL
  SDL_MOCK_ALLOW_ANY_CALL
  phx::Engine engine;

  GIVEN("A rendering system.") {
    auto diplay_system = engine.CreateSystem<phx::DisplaySystemWindow>();
    phx::RenderingSystem* rendering_system =
        engine.CreateSystem<phx::RenderingSystem>(diplay_system);

    WHEN("We initialize the system") {
      THEN(
          "a default frame graph is created, that has 0 render passes, "
          "since there is currently to much inter-relation between the "
          "systems") {
        REQUIRE(rendering_system->GetFrameGraph()->GetNumberOfPasses() == 0);
        // TODO(@all) Improve this?
      }
    }
  }
  // The rest of the functionality testing should be done in an integration test
}

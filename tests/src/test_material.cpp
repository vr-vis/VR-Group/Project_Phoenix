//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include <array>
#include <vector>

#include "catch/catch.hpp"

#include "phx/core/component.hpp"
#include "phx/core/logger.hpp"
#include "phx/rendering/components/material_handle.hpp"

#include "test_utilities/glm_vec.hpp"

SCENARIO("The material component keeps track of its attributes",
         "[phx][phx::Material]") {
  GIVEN("An empty material component") {
    phx::CreateDefaultLogger();

    phx::Material material;

    THEN("it is by default red") {
      REQUIRE(material.GetAmbientColor() == glm::vec3(0, 0, 0));
      REQUIRE(material.GetSpecularColor() == glm::vec3(1, 1, 1));
      REQUIRE(material.GetDiffuseColor() == glm::vec3(1, 0, 0));
      REQUIRE(material.GetShininess() == 64.0f);
    }
    WHEN("we assign it to be some other material") {
      glm::vec3 blue(0, 0, 1), white(1, 1, 1), green(0, 1, 0);

      material.SetAmbientColor(blue);
      material.SetDiffuseColor(green);
      material.SetSpecularColor(white);
      material.SetShininess(16.0f);

      THEN("we expect these values to be stored") {
        REQUIRE(material.GetAmbientColor() == blue);
        REQUIRE(material.GetSpecularColor() == white);
        REQUIRE(material.GetDiffuseColor() == green);
        REQUIRE(material.GetShininess() == 16.0f);
      }
      WHEN("we copy it") {
        phx::Material material2 = material;
        THEN("we expect these values to be copied as well") {
          REQUIRE(material2.GetAmbientColor() == blue);
          REQUIRE(material2.GetSpecularColor() == white);
          REQUIRE(material2.GetDiffuseColor() == green);
          REQUIRE(material2.GetShininess() == 16.0f);
        }
      }
    }
    WHEN("we set shininess to an invalid value") {
      material.SetShininess(-1.0f);

      THEN("it is not set and stays >= 0") {
        REQUIRE(material.GetShininess() >= 0.0f);
      }
    }
  }
}

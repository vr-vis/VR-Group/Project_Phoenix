//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include <memory>
#include <string>

#include "catch/catch.hpp"

#include "trompeloeil.hpp"

#include "phx/display/hmd.hpp"
#include "phx/suppress_warnings.hpp"

SUPPRESS_WARNINGS_BEGIN
#include "mocks/openvr_mock.hpp"
SUPPRESS_WARNINGS_END
#include "mocks/sdl_mock.hpp"

#include "phx/core/entity.hpp"
#include "phx/core/runtime_component.hpp"
#include "phx/core/scene.hpp"
#include "phx/input/device_system.hpp"
#include "phx/rendering/components/projection.hpp"
#include "phx/rendering/components/transform.hpp"
#include "phx/tracking/tracking_system_openvr.hpp"

#include "test_utilities/glm_mat4.hpp"

using trompeloeil::_;
using trompeloeil::ne;

extern template struct trompeloeil::reporter<trompeloeil::specialized>;

#if defined __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wmissing-prototypes"
#endif

namespace {

void SetGlmToArrayMatrix_3_4(glm::mat4 m, float *array) {
  std::size_t index = 0;
  for (int x = 0; x < 3; x++) {
    for (int y = 0; y < 4; y++) {
      array[index++] = m[y][x];
    }
  }
}

template <typename ComponentType>
bool Has(phx::Entity *entity) {
  return entity->GetFirstComponent<ComponentType>() != nullptr;
}

template <phx::RuntimeComponentType type_id>
bool Has(phx::Entity *entity) {
  return Has<phx::RuntimeComponent<type_id>>(entity);
}

template <phx::RuntimeComponentType type_id>
bool ParentOfTransformIsTransformOf(phx::Entity *entity) {
  const auto *parent_of_transform =
      entity->GetFirstComponent<phx::Transform>()->GetParent();
  REQUIRE(parent_of_transform != nullptr);
  return Has<type_id>(parent_of_transform->GetEntity());
}

std::string RuntimeComponentName(phx::RuntimeComponentType type_id) {
  switch (type_id) {
    case phx::USER_PLATFORM:
      return "user platform";
    case phx::HEAD:
      return "HMD";
    case phx::LEFT_EYE:
      return "left eye";
    case phx::RIGHT_EYE:
      return "right eye";
    case phx::LEFT_CONTROLLER:
      return "left controller";
    case phx::RIGHT_CONTROLLER:
      return "right controller";
  }
  return "unknown runtime component";
}

template <phx::RuntimeComponentType type_id>
void ThenRequireThereIsOnlyOne(bool is_present) {
  THEN("There is only one " + RuntimeComponentName(type_id) + ".") {
    REQUIRE_FALSE(is_present);
  }
}

void ThenRequireItHasTransformComponent(phx::Entity *entity) {
  THEN("It has a transform component.") {
    REQUIRE(Has<phx::Transform>(entity));
  }
}

void ThenRequireItHasProjectionComponent(phx::Entity *entity) {
  THEN("It has a transform component.") {
    REQUIRE(Has<phx::Projection>(entity));
  }
}

template <phx::RuntimeComponentType type_id>
void ThenRequireParentOfItsTranformIsTransformOf(phx::Entity *entity) {
  THEN("Its transform's parent is the " + RuntimeComponentName(type_id) +
       "'s transform") {
    REQUIRE(ParentOfTransformIsTransformOf<type_id>(entity));
  }
}

template <bool expression>
using BoolIf = typename std::enable_if<expression, bool>::type;

template <phx::RuntimeComponentType type_id>
BoolIf<type_id == phx::USER_PLATFORM> Validate(phx::Entity *entity,
                                               bool is_present) {
  if (Has<type_id>(entity)) {
    ThenRequireThereIsOnlyOne<type_id>(is_present);
    return true;
  }
  return false;
}

template <phx::RuntimeComponentType type_id>
BoolIf<type_id == phx::HEAD || type_id == phx::LEFT_CONTROLLER ||
       type_id == phx::RIGHT_CONTROLLER>
Validate(phx::Entity *entity, bool is_present) {
  if (Has<type_id>(entity)) {
    ThenRequireThereIsOnlyOne<type_id>(is_present);
    ThenRequireItHasTransformComponent(entity);
    ThenRequireParentOfItsTranformIsTransformOf<phx::USER_PLATFORM>(entity);
    return true;
  }
  return false;
}

template <phx::RuntimeComponentType type_id>
BoolIf<type_id == phx::LEFT_EYE || type_id == phx::RIGHT_EYE> Validate(
    phx::Entity *entity, bool is_present) {
  if (Has<type_id>(entity)) {
    ThenRequireThereIsOnlyOne<type_id>(is_present);
    ThenRequireItHasTransformComponent(entity);
    ThenRequireItHasProjectionComponent(entity);
    ThenRequireParentOfItsTranformIsTransformOf<phx::HEAD>(entity);
    return true;
  }
  return false;
}

test_utilities::Approx<glm::mat4> GetApproxLocalMatrix(phx::Entity *entity) {
  return test_utilities::Approx<glm::mat4>(
      entity->GetFirstComponent<phx::Transform>()->GetLocalMatrix());
}

template <phx::RuntimeComponentType type_id>
bool ValidateTransform(phx::Entity *entity, glm::mat4 trans_mat,
                       const std::string &then) {
  if (Has<type_id>(entity)) {
    THEN("For the " + RuntimeComponentName(type_id) + " " + then) {
      REQUIRE(GetApproxLocalMatrix(entity) == trans_mat);
    }
    return true;
  }
  return false;
}

void TestRuntimeEntityStructure(std::shared_ptr<phx::Scene> scene) {
  THEN("The HMD and the user's eyes are represented in the scene.") {
    auto entities = scene->GetEntitiesWithComponents<phx::Transform>();
    bool platform_present = false;
    bool hmd_present = false;
    bool left_eye_present = false;
    bool right_eye_present = false;
    bool left_controller_present = false;
    bool right_controller_present = false;

    for (auto entity : entities) {
      platform_present |=
          Validate<phx::USER_PLATFORM>(entity, platform_present);

      hmd_present |= Validate<phx::HEAD>(entity, hmd_present);

      left_eye_present |= Validate<phx::LEFT_EYE>(entity, left_eye_present);
      right_eye_present |= Validate<phx::RIGHT_EYE>(entity, right_eye_present);

      left_controller_present |=
          Validate<phx::LEFT_CONTROLLER>(entity, left_controller_present);
      right_controller_present |=
          Validate<phx::RIGHT_CONTROLLER>(entity, right_controller_present);
    }

    THEN(
        "There are six entities: The platform, the HMD, the left and "
        "the right eyes, the left and right controllers.") {
      REQUIRE(entities.size() == 6);
      REQUIRE(platform_present);
      REQUIRE(hmd_present);
      REQUIRE(left_eye_present);
      REQUIRE(right_eye_present);
      REQUIRE(left_controller_present);
      REQUIRE(right_controller_present);
    }
  }
}

void InitMatrices(glm::mat4 *platform_trans_mat, glm::mat4 *hmd_trans_mat,
                  glm::mat4 *left_eye_trans_mat,
                  glm::mat4 *right_eye_trans_mat) {
  *platform_trans_mat =
      glm::mat4(1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f,
                1.0f, 0.0f, 1.0f, -1.0f, 0.5f, 1.0f);
  *hmd_trans_mat = glm::mat4(1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,
                             0.0f, 0.0f, 1.0f, 0.0f, 0.5f, 1.8f, 0.2f, 1.0f);
  *left_eye_trans_mat =
      glm::mat4(1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f,
                1.0f, 0.0f, -0.04f, 0.0f, 0.0f, 1.0f);
  *right_eye_trans_mat =
      glm::mat4(1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f,
                1.0f, 0.0f, 0.04f, 0.0f, 0.0f, 1.0f);
  SetGlmToArrayMatrix_3_4(*hmd_trans_mat, openvr_mock.head_transformation_);
  SetGlmToArrayMatrix_3_4(*left_eye_trans_mat, openvr_mock.eye_to_head_left_);
  SetGlmToArrayMatrix_3_4(*right_eye_trans_mat, openvr_mock.eye_to_head_right_);
}

void TestRuntimeEntityUpdate(std::shared_ptr<phx::Scene> scene,
                             phx::TrackingSystemOpenVR *tracking_system) {
  glm::mat4 platform_trans_mat, hmd_trans_mat, left_eye_trans_mat,
      right_eye_trans_mat;
  InitMatrices(&platform_trans_mat, &hmd_trans_mat, &left_eye_trans_mat,
               &right_eye_trans_mat);

  WHEN("The tracking system is updated.") {
    tracking_system->Update(phx::FrameTimer::TimeInfo());
    THEN(
        "It sets the transforms of all the runtime entities to the "
        "correct values.") {
      auto entities = scene->GetEntitiesWithComponents<phx::Transform>();
      bool platform_present = false;
      bool hmd_present = false;
      bool left_eye_present = false;
      bool right_eye_present = false;
      bool left_controller_present = false;
      bool right_controller_present = false;

      for (auto entity : entities) {
        platform_present |= ValidateTransform<phx::USER_PLATFORM>(
            entity, platform_trans_mat, "the transformation did not change.");

        hmd_present |= ValidateTransform<phx::HEAD>(
            entity, hmd_trans_mat,
            "the transformation changes to the one provided by OpenVR.");

        left_eye_present |= ValidateTransform<phx::LEFT_EYE>(
            entity, left_eye_trans_mat,
            "the transformation changes to the one provided by OpenVR.");
        right_eye_present |= ValidateTransform<phx::RIGHT_EYE>(
            entity, right_eye_trans_mat,
            "the transformation changes to the one provided by OpenVR.");

        left_controller_present |= Has<phx::LEFT_CONTROLLER>(entity);
        right_controller_present |= Has<phx::RIGHT_CONTROLLER>(entity);
      }
      THEN(
          "There are still six entities: The platform, the HMD, the "
          "left and the right eyes, the left and the right controllers.") {
        REQUIRE(entities.size() == 6);
        REQUIRE(platform_present);
        REQUIRE(hmd_present);
        REQUIRE(left_eye_present);
        REQUIRE(right_eye_present);
        REQUIRE(left_controller_present);
        REQUIRE(right_controller_present);
      }
    }
  }
}

void SetPlatformMatrix(const phx::Scene *scene, glm::mat4 matrix) {
  auto *transform = scene
                        ->GetEntitiesWithComponents<
                            phx::RuntimeComponent<phx::USER_PLATFORM>>()[0]
                        ->GetFirstComponent<phx::Transform>();

  transform->SetLocalMatrix(matrix);
}

template <phx::RuntimeComponentType type_id>
std::size_t NumberOfContained(const phx::Scene *scene) {
  const auto &entities =
      scene->GetEntitiesWithComponents<phx::RuntimeComponent<type_id>>();
  return entities.size();
}

}  // namespace

#if defined __clang__
#pragma clang diagnostic pop
#endif

SCENARIO(
    "The tracking system tracks hardware and updates their software "
    "counterparts.",
    "[phx][phx::TrackingSystemOpenVR]") {
  OPENVR_MOCK_ALLOW_ANY_CALL;
  SDL_MOCK_ALLOW_ANY_CALL;

  phx::Engine engine;
  auto device_system = engine.CreateSystem<phx::DeviceSystem>();

  GIVEN(
      "The display system has an HMD and the engine has a scene with a "
      "user platform.") {
    device_system->AddDevice<phx::HMD>();
    device_system->AddDevice<phx::VRController>(
        phx::VRController::LEFT_CONTROLLER);
    device_system->AddDevice<phx::VRController>(
        phx::VRController::RIGHT_CONTROLLER);
    device_system->Update(phx::FrameTimer::TimeInfo());
    auto first_scene = std::make_shared<phx::Scene>();
    engine.SetScene(first_scene);
    auto platform_trans_mat =
        glm::mat4(1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f,
                  1.0f, 0.0f, 1.0f, -1.0f, 0.5f, 1.0f);
    ::SetPlatformMatrix(first_scene.get(), platform_trans_mat);
    WHEN("A tracking system is created and initialized.") {
      auto tracking_system =
          engine.CreateSystem<phx::TrackingSystemOpenVR>(device_system);

      ::TestRuntimeEntityStructure(first_scene);
      ::TestRuntimeEntityUpdate(first_scene, tracking_system);

      WHEN("The scene is then changed.") {
        auto new_scene = std::make_shared<phx::Scene>();
        ::SetPlatformMatrix(new_scene.get(), platform_trans_mat);
        engine.SetScene(new_scene);

        THEN(
            "The first scene does not contain runtime entities except for "
            "exactly one user platform.") {
          REQUIRE(::NumberOfContained<phx::USER_PLATFORM>(first_scene.get()) ==
                  1);
          REQUIRE(::NumberOfContained<phx::HEAD>(first_scene.get()) == 0);
          REQUIRE(::NumberOfContained<phx::LEFT_EYE>(first_scene.get()) == 0);
          REQUIRE(::NumberOfContained<phx::RIGHT_EYE>(first_scene.get()) == 0);
          REQUIRE(::NumberOfContained<phx::LEFT_CONTROLLER>(
                      first_scene.get()) == 0);
          REQUIRE(::NumberOfContained<phx::RIGHT_CONTROLLER>(
                      first_scene.get()) == 0);
        }

        ::TestRuntimeEntityStructure(new_scene);
        ::TestRuntimeEntityUpdate(new_scene, tracking_system);
      }
    }
  }
}

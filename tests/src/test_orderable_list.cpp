//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include <memory>
#include <string>

#include "catch/catch.hpp"
#include "trompeloeil.hpp"

#include "phx/utility/orderable_list.hpp"

using trompeloeil::_;
using trompeloeil::ne;

extern template struct trompeloeil::reporter<trompeloeil::specialized>;

SCENARIO("An OderableList can be used as a list", "[phx][phx::OrderableList]") {
  GIVEN("An empty OrderableList") {
    phx::OrderableList<int> orderable_list;

    THEN("It does not contain anything") {
      REQUIRE(orderable_list.size() == 0);

      WHEN("we add one element") {
        orderable_list.PushBack(std::make_unique<int>(1));
        THEN("the number of elements is correct") {
          REQUIRE(orderable_list.size() == 1);
          WHEN("we add one more") {
            orderable_list.PushBack(std::make_unique<int>(2));
            THEN("the number of elements is correct") {
              REQUIRE(orderable_list.size() == 2);
              WHEN("We remove it again") {
                orderable_list.erase(orderable_list.begin() + 1,
                                     orderable_list.end());
                THEN("it has 1 element again") {
                  REQUIRE(orderable_list.size() == 1);
                }
              }
            }
          }
        }
      }
    }
  }
}

SCENARIO("An OderableList can looped through", "[phx][phx::OrderableList]") {
  GIVEN("An OrderableList with 3 elements") {
    phx::OrderableList<int> orderable_list;
    orderable_list.PushBack(std::make_unique<int>(1));
    orderable_list.PushBack(std::make_unique<int>(2));
    orderable_list.PushBack(std::make_unique<int>(3));

    THEN("it can be iterated over the elements using a for each loop") {
      int elements = 0;
      int correct_order = true;
      for (auto& it : orderable_list) {
        elements++;
        if (*it != elements) correct_order = false;
      }
      REQUIRE(correct_order);
      REQUIRE(elements == 3);
    }
  }
}

#if defined __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wmissing-prototypes"
#endif

namespace {

std::string GetOrderString(
    const phx::OrderableList<std::string>& orderable_list) {
  std::string order_string;
  for (auto& it : orderable_list) {
    order_string += *it;
  }
  return order_string;
}

}  // namespace

#if defined __clang__
#pragma clang diagnostic pop
#endif

SCENARIO("The order in which elements are ordered can be changed",
         "[phx][phx::OrderableList]") {
  GIVEN("An OrderableList set up with 4 number of elements") {
    phx::OrderableList<std::string> orderable_list;
    auto elem1 = orderable_list.PushBack(std::make_unique<std::string>("A"));
    auto elem2 = orderable_list.PushBack(std::make_unique<std::string>("B"));
    auto elem3 = orderable_list.PushBack(std::make_unique<std::string>("C"));
    auto elem4 = orderable_list.PushBack(std::make_unique<std::string>("D"));

    THEN("The elements are in the same order in which they were added") {
      REQUIRE(::GetOrderString(orderable_list) == "ABCD");
    }

    WHEN("We change the second element to be first") {
      orderable_list.MoveToFront(elem2);
      THEN("The elements are in order 2-1-3-4") {
        REQUIRE(::GetOrderString(orderable_list) == "BACD");
      }
    }

    WHEN("We change the second element to update last") {
      orderable_list.MoveToBack(elem2);
      THEN("The elements are in order 1-3-4-2") {
        REQUIRE(::GetOrderString(orderable_list) == "ACDB");
      }
    }

    WHEN("We change order to have element 2 after element 3") {
      orderable_list.MoveAfter(elem2, elem3);
      THEN("The elements are in order 1-3-2-4") {
        REQUIRE(::GetOrderString(orderable_list) == "ACBD");
      }
    }

    WHEN("We change order to have element 4 before element 1") {
      orderable_list.MoveBefore(elem4, elem1);
      THEN("The elements are in order 4-1-2-3") {
        REQUIRE(::GetOrderString(orderable_list) == "DABC");
      }
    }
  }
}

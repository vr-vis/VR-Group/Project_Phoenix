//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include <memory>

#include "catch/catch.hpp"

#include "phx/suppress_warnings.hpp"

SUPPRESS_WARNINGS_BEGIN
#include "GL/glew.h"
SUPPRESS_WARNINGS_END

#include "trompeloeil.hpp"

#include "phx/core/entity.hpp"
#include "phx/core/scene.hpp"
#include "phx/input/device_system.hpp"
#include "phx/input/openvr_controller_model_system.hpp"
#include "phx/input/vr_controller.hpp"
#include "phx/rendering/components/material_handle.hpp"
#include "phx/rendering/components/mesh_handle.hpp"
#include "phx/rendering/rendering_system.hpp"

#include "mocks/opengl_mock.hpp"
#include "mocks/openvr_mock.hpp"
#include "mocks/sdl_mock.hpp"

using trompeloeil::_;
using trompeloeil::ne;

extern template struct trompeloeil::reporter<trompeloeil::specialized>;

class EngineStopTestSystem : public phx::System {
 public:
  explicit EngineStopTestSystem(phx::Engine* engine, int stop_in_frame = 1)
      : System(engine), stop_on_update_(stop_in_frame) {}

  void Update(const phx::FrameTimer::TimeInfo& time_info) override {
    update_counter_++;
    if (update_counter_ >= stop_on_update_) {
      GetEngine()->Stop();
    }
    time_info_ = &time_info;
  }

  const phx::FrameTimer::TimeInfo* time_info_ = nullptr;

 private:
  int update_counter_ = 0;
  int stop_on_update_ = 1;
};

SCENARIO(
    "The OpenVRControllerSystem automatically inserts controller entities into "
    "a scene if controllers are connected",
    "[phx][phx::OpenVRControllerModelSystem]") {
  OPENGL_MOCK_ALLOW_ANY_CALL;
  OPENVR_MOCK_ALLOW_ANY_CALL;
  SDL_MOCK_ALLOW_ANY_CALL;

  phx::Engine engine;
  auto device_system = engine.CreateSystem<phx::DeviceSystem>();

  GIVEN("a device system with two vr controllers") {
    device_system->AddDevice<phx::VRController>(
        phx::VRController::LEFT_CONTROLLER);
    device_system->AddDevice<phx::VRController>(
        phx::VRController::RIGHT_CONTROLLER);
    auto scene = std::make_shared<phx::Scene>();
    engine.SetScene(scene);
    engine.CreateSystem<phx::OpenVRControllerModelSystem>(device_system);
    engine.CreateSystem<EngineStopTestSystem>();

    WHEN("We run the engine for once frame (updating each system once)") {
      engine.Run();

      THEN(
          "There are controller entities in the scene with controller "
          "behaviors attached to them, as well as mesh and material handles, "
          "and the controllers are left and right") {
        auto controller_entities =
            scene->GetEntitiesWithComponents<phx::OpenVRControllerBehavior>();
        REQUIRE(controller_entities.size() == 2);

        for (auto entity : controller_entities) {
          REQUIRE(entity->GetFirstComponent<phx::MeshHandle>() != nullptr);
          REQUIRE(entity->GetFirstComponent<phx::MaterialHandle>() != nullptr);
        }

        auto side0 = controller_entities[0]
                         ->GetFirstComponent<phx::OpenVRControllerBehavior>()
                         ->GetSide();
        auto side1 = controller_entities[1]
                         ->GetFirstComponent<phx::OpenVRControllerBehavior>()
                         ->GetSide();
        REQUIRE(side0 != side1);
      }
    }
  }
}

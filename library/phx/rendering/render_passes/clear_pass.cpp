//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include "gl/framebuffer.hpp"

#include "phx/suppress_warnings.hpp"

SUPPRESS_WARNINGS_BEGIN
#include "glm/gtc/type_ptr.hpp"
SUPPRESS_WARNINGS_END

#include "phx/rendering/render_passes/clear_pass.hpp"

namespace phx {

ClearPass::ClearPass(RenderTarget* render_target)
    : render_target_(render_target) {}

void ClearPass::SetClearColor(const glm::vec4& color) const {
  render_target_->bind();
  gl::set_clear_color(
      std::array<float, 4>{{color[0], color[1], color[2], color[3]}});
  render_target_->unbind();
}

void ClearPass::Initialize() {
  SetClearColor(glm::vec4(0.0f, 0.0f, 0.0f, 1.0f));
}

void ClearPass::Execute() {
  render_target_->bind();
  gl::clear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  render_target_->unbind();
}
}  // namespace phx

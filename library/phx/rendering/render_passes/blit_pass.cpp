//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include "phx/rendering/render_passes/blit_pass.hpp"

#include <memory>

namespace phx {

BlitPass::BlitPass(RenderTarget* render_target)
    : render_target_(render_target) {}

void BlitPass::Initialize() {
  default_framebuffer_ = std::make_unique<gl::framebuffer>(0);
}

void BlitPass::Execute() {
  const glm::uvec2& source_dims = render_target_->GetDimensions();
  const GLint source_width = static_cast<int>(source_dims.x);
  const GLint source_height = static_cast<int>(source_dims.y);

  GLint target_dims[4] = {0};
  glGetIntegerv(GL_VIEWPORT, target_dims);
  const GLint target_width = target_dims[2];
  const GLint target_height = target_dims[3];

  GLint source_x = 0;
  GLint source_y = 0;
  if (target_width < source_width) {
    source_x = (source_width - target_width) / 2;
  }
  if (target_height < source_height) {
    source_y = (source_height - target_height) / 2;
  }

  default_framebuffer_->blit(*render_target_, source_x, source_y, target_width,
                             target_height, 0, 0, target_width, target_height,
                             GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT,
                             GL_NEAREST);
}
}  // namespace phx

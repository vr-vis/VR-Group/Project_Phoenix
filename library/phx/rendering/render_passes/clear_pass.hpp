//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef LIBRARY_PHX_RENDERING_RENDER_PASSES_CLEAR_PASS_HPP_
#define LIBRARY_PHX_RENDERING_RENDER_PASSES_CLEAR_PASS_HPP_

#include "phx/suppress_warnings.hpp"

SUPPRESS_WARNINGS_BEGIN
#include "glm/glm.hpp"
#include "glm/vec4.hpp"
SUPPRESS_WARNINGS_END

#include "phx/rendering/backend/render_target.hpp"
#include "phx/rendering/render_passes/render_pass.hpp"
#include "phx/export.hpp"

namespace phx {

class PHOENIX_EXPORT ClearPass : public RenderPass {
 public:
  explicit ClearPass(RenderTarget* render_target);
  ClearPass(const ClearPass&) = default;
  ClearPass(ClearPass&&) = default;

  ClearPass& operator=(const ClearPass&) = default;
  ClearPass& operator=(ClearPass&&) = default;

  void SetClearColor(const glm::vec4& color = glm::vec4(0)) const;

  void Initialize() override;
  void Execute() override;

 private:
  RenderTarget* render_target_;
};

}  // namespace phx

#endif  // LIBRARY_PHX_RENDERING_RENDER_PASSES_CLEAR_PASS_HPP_

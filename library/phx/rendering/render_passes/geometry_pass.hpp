//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef LIBRARY_PHX_RENDERING_RENDER_PASSES_GEOMETRY_PASS_HPP_
#define LIBRARY_PHX_RENDERING_RENDER_PASSES_GEOMETRY_PASS_HPP_

#include <cstddef>
#include <map>
#include <memory>
#include <utility>
#include <vector>

#include "phx/suppress_warnings.hpp"

SUPPRESS_WARNINGS_BEGIN
#include "gl/buffer.hpp"
#include "gl/vertex_array.hpp"
SUPPRESS_WARNINGS_END

#include "phx/export.hpp"
#include "phx/rendering/backend/render_target.hpp"
#include "phx/rendering/backend/shader_program.hpp"
#include "phx/rendering/components/light.hpp"
#include "phx/rendering/components/material_handle.hpp"
#include "phx/rendering/components/mesh_render_settings.hpp"
#include "phx/rendering/components/projection.hpp"
#include "phx/rendering/components/transform.hpp"
#include "phx/rendering/render_passes/render_pass.hpp"
#include "phx/resources/resource_manager.hpp"
#include "phx/resources/types/mesh.hpp"

namespace phx {

class PHOENIX_EXPORT GeometryPass : public RenderPass {
 public:
  explicit GeometryPass(RenderTarget* render_target);
  GeometryPass(const GeometryPass&) = default;
  GeometryPass(GeometryPass&&) = default;

  GeometryPass& operator=(const GeometryPass&) = default;
  GeometryPass& operator=(GeometryPass&&) = default;

  struct RenderingInstance {
    Mesh* mesh = nullptr;
    Material* material = nullptr;
    Transform* transform = nullptr;
    MeshRenderSettings* mesh_render_settings = nullptr;
  };
  struct RenderOffset {
    std::size_t vertex_offset;
    std::size_t index_offset;
  };

  void SetData(
      const std::vector<RenderingInstance>& rendering_instances,
      const std::vector<std::pair<Light*, Transform*>>& light_transform_pairs =
          std::vector<std::pair<Light*, Transform*>>());

  void UploadMeshData(
      const std::vector<RenderingInstance>& rendering_instances);

  bool CheckMeshValidity(Mesh* mesh);

  void Initialize() override;
  void Execute() override;

  bool IsValid() const;

 private:
  struct RenderingResource {
    RenderingResource() = default;
    RenderingResource(const RenderingResource&) = delete;
    RenderingResource(RenderingResource&&) = default;
    ~RenderingResource() = default;

    RenderingResource& operator=(const RenderingResource&) = delete;
    RenderingResource& operator=(RenderingResource&&) = default;

    gl::buffer vertex_buffer;
    gl::buffer normal_buffer;
    gl::buffer tex_coords_buffer;
    gl::buffer index_buffer;
    gl::vertex_array vertex_array;

    unsigned int vertex_buffer_size;
    unsigned int index_buffer_size;
  };

  void SetUpShaders();
  void CreateRenderingResource();
  void CreateAttachVertexArrayVertexBuffer(const gl::buffer& buffer_id,
                                           GLuint attribute_index,
                                           GLint num_components,
                                           GLsizei stride);
  void BindResources();
  void Draw(const RenderingInstance& rendering_instance);
  void UnbindResources();

  void SetTransformShaderUniforms(const glm::mat4& model_matrix,
                                  const glm::mat4& view_matrix,
                                  const glm::mat4& projection_matrix);
  void SetLightShaderUniforms();
  void SetMaterialShaderUniforms(const Material* material);

  std::unique_ptr<ShaderProgram> shader_program_;

  std::unique_ptr<RenderingResource> rendering_resource_;
  std::vector<RenderingInstance> rendering_instances_;
  std::vector<std::pair<Light*, Transform*>> light_transform_pairs_;
  gl::buffer light_buffer_;

  std::map<Mesh*, RenderOffset> mesh_cache_;
  RenderTarget* render_target_;
};

}  // namespace phx

#endif  // LIBRARY_PHX_RENDERING_RENDER_PASSES_GEOMETRY_PASS_HPP_

//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include "phx/rendering/auxiliary/splash_screen.hpp"

#include <cmath>
#include <memory>
#include <string>

#include "phx/resources/types/image.hpp"
#include "phx/resources/resource_manager.hpp"
#include "phx/resources/resource_utils.hpp"
#include "phx/resources_path.hpp"

namespace phx {

phx::SplashScreen::SplashScreen(Engine* engine, Window* window)
    : System(engine), window_(window) {
  splash_image_ = ResourceUtils::LoadResourceFromFile<Image>(
      "textures/splash_progress.png");

  progressbar_image_ =
      std::make_unique<Image>(std::array<std::size_t, 2>{{1ull, 1ull}}, 32);
  progressbar_image_->SetPixelColor({{0, 0}}, {{255, 255, 255, 255}});

  default_framebuffer_ = std::make_unique<gl::framebuffer>(0);
}

void SplashScreen::Update(const FrameTimer::TimeInfo&) { Draw(); }

void SplashScreen::Draw() {
  auto splash_dimensions = splash_image_->GetDimensions();
  auto progress_dimensions = progressbar_image_->GetDimensions();
  gl::initialize();
  gl::framebuffer splash_buff;
  gl::texture_2d splash_tex;
  splash_buff.bind();
  splash_tex.bind();
  splash_tex.set_storage(1, GL_RGBA8,
                         static_cast<GLsizei>(splash_dimensions[0]),
                         static_cast<GLsizei>(splash_dimensions[1]));
  splash_buff.attach_texture<GL_TEXTURE_2D>(GL_COLOR_ATTACHMENT0, splash_tex,
                                            0);
  splash_tex.set_sub_image(0, 0, 0, static_cast<GLsizei>(splash_dimensions[0]),
                           static_cast<GLsizei>(splash_dimensions[1]), GL_RGBA,
                           GL_UNSIGNED_BYTE, splash_image_->GetPixels().first);
  gl::print_error("OpenGl Error creating Splash Screen texture: ");
  splash_buff.unbind();
  splash_tex.unbind();

  gl::framebuffer progress_buff;
  gl::texture_2d progress_tex;
  progress_buff.bind();
  progress_tex.bind();
  progress_tex.set_storage(1, GL_RGBA8,
                           static_cast<GLsizei>(progress_dimensions[0]),
                           static_cast<GLsizei>(progress_dimensions[1]));
  progress_buff.attach_texture<GL_TEXTURE_2D>(GL_COLOR_ATTACHMENT0,
                                              progress_tex, 0);
  progress_tex.set_sub_image(
      0, 0, 0, static_cast<GLsizei>(progress_dimensions[0]),
      static_cast<GLsizei>(progress_dimensions[1]), GL_RGBA, GL_UNSIGNED_BYTE,
      progressbar_image_->GetPixels().first);
  gl::print_error("OpenGl Error creating Progress Bar texture: ");
  progress_buff.unbind();
  progress_tex.unbind();

  auto win_size = window_->GetSize();

  auto x_offset = (win_size.x - splash_dimensions[0]) / 2;
  auto y_offset = (win_size.y - splash_dimensions[1]) / 2;

  default_framebuffer_->blit(
      splash_buff, 0, 0, static_cast<GLsizei>(splash_dimensions[0]),
      static_cast<GLsizei>(splash_dimensions[1]),
      static_cast<GLsizei>(x_offset), static_cast<GLsizei>(y_offset),
      static_cast<GLsizei>(splash_dimensions[0] + x_offset),
      static_cast<GLsizei>(splash_dimensions[1] + y_offset),
      GL_COLOR_BUFFER_BIT, GL_LINEAR);

  gl::print_error("OpenGl Error blitting Splash Screen: ");

  constexpr float progressbar_max_pixel_width = 198.f;
  unsigned int progressbar_current_pixel_width;
  {
    std::lock_guard<std::mutex> lock_guard(load_progress_mutex_);
    progressbar_current_pixel_width = static_cast<unsigned int>(
        std::round(load_progress_ * progressbar_max_pixel_width));
  }
  const int progressbar_pixel_height = 9;

  auto progressbar_x_offset = x_offset + 98;
  auto progressbar_y_offset = y_offset + 15;
  default_framebuffer_->blit(
      progress_buff, 0, 0, static_cast<GLsizei>(progress_dimensions[0]),
      static_cast<GLsizei>(progress_dimensions[1]),
      static_cast<GLsizei>(progressbar_x_offset),
      static_cast<GLsizei>(progressbar_y_offset),
      static_cast<GLsizei>(progressbar_current_pixel_width +
                           progressbar_x_offset),
      static_cast<GLsizei>(progressbar_pixel_height + progressbar_y_offset),
      GL_COLOR_BUFFER_BIT, GL_LINEAR);
}

void SplashScreen::SetLoadProgress(float progress) {
  std::lock_guard<std::mutex> lock_guard(load_progress_mutex_);
  load_progress_ = progress;
}

}  // namespace phx

//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef LIBRARY_PHX_RENDERING_AUXILIARY_SPLASH_SCREEN_HPP_
#define LIBRARY_PHX_RENDERING_AUXILIARY_SPLASH_SCREEN_HPP_

#include <memory>
#include <mutex>

#include "phx/suppress_warnings.hpp"

#include "phx/core/system.hpp"
#include "phx/display/window.hpp"
#include "phx/export.hpp"
#include "phx/resources/resource_pointer.hpp"
#include "phx/resources/types/image.hpp"

#include "gl/framebuffer.hpp"

namespace phx {

class PHOENIX_EXPORT SplashScreen : public System {
 public:
  SplashScreen() = delete;
  SplashScreen(Engine* engine, Window* window);
  virtual ~SplashScreen() = default;

  void Update(const FrameTimer::TimeInfo&) override;
  void Draw();
  void SetLoadProgress(float progress);

 protected:
  SplashScreen(const SplashScreen&) = delete;
  SplashScreen(SplashScreen&&) = default;

  SplashScreen& operator=(const SplashScreen&) = delete;
  SplashScreen& operator=(SplashScreen&&) = default;

 private:
  std::unique_ptr<gl::framebuffer> default_framebuffer_;
  ResourcePointer<Image> splash_image_;
  std::unique_ptr<Image> progressbar_image_;
  Window* window_;

  std::mutex load_progress_mutex_;
  float load_progress_ = 0.f;
};

}  // namespace phx

#endif  // LIBRARY_PHX_RENDERING_AUXILIARY_SPLASH_SCREEN_HPP_

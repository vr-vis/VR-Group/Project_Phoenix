//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef LIBRARY_PHX_RENDERING_FRAME_GRAPH_HPP_
#define LIBRARY_PHX_RENDERING_FRAME_GRAPH_HPP_

#include <list>
#include <memory>
#include <utility>
#include <vector>

#include "phx/export.hpp"
#include "phx/rendering/render_passes/render_pass.hpp"

namespace phx {
class PHOENIX_EXPORT FrameGraph {
 public:
  FrameGraph() = default;
  FrameGraph(const FrameGraph&) = delete;
  FrameGraph(FrameGraph&&) = default;
  virtual ~FrameGraph() = default;

  FrameGraph& operator=(const FrameGraph&) = delete;
  FrameGraph& operator=(FrameGraph&&) = default;

  void Initialize();
  void Execute();

  std::size_t GetNumberOfPasses();
  RenderPass* AddRenderPass(std::unique_ptr<RenderPass> render_pass);

  template <typename RenderPassType>
  std::vector<RenderPassType*> GetRenderPasses() const;

 private:
  std::list<std::unique_ptr<RenderPass>> render_passes_;
};

template <typename RenderPassType>
std::vector<RenderPassType*> phx::FrameGraph::GetRenderPasses() const {
  static_assert(std::is_base_of<RenderPass, RenderPassType>::value,
                "The type does not inherit from RenderPass.");
  std::vector<RenderPassType*> render_passes;
  for (const auto& render_pass : render_passes_) {
    auto pass_pointer = dynamic_cast<RenderPassType*>(render_pass.get());
    if (pass_pointer != nullptr) {
      render_passes.push_back(pass_pointer);
    }
  }
  return render_passes;
}

}  // namespace phx

#endif  // LIBRARY_PHX_RENDERING_FRAME_GRAPH_HPP_

//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef LIBRARY_PHX_RENDERING_COMPONENTS_MATERIAL_HANDLE_HPP_
#define LIBRARY_PHX_RENDERING_COMPONENTS_MATERIAL_HANDLE_HPP_

#include <string>
#include <vector>

#include "phx/suppress_warnings.hpp"

SUPPRESS_WARNINGS_BEGIN
#include "glm/vec3.hpp"
SUPPRESS_WARNINGS_END

#include "phx/core/component.hpp"
#include "phx/resources/types/material.hpp"
#include "phx/resources/resource_pointer.hpp"
#include "phx/utility/aspects/nameable.hpp"
#include "phx/export.hpp"

namespace phx {

class PHOENIX_EXPORT MaterialHandle final : public Component, public Nameable {
 public:
  void SetMaterial(ResourcePointer<Material> material);
  ResourcePointer<Material> GetMaterial() const;

  std::string ToString() const override;

 protected:
  friend Entity;
  MaterialHandle() = default;
  MaterialHandle(const MaterialHandle&) = delete;
  MaterialHandle(MaterialHandle&&) = default;

  MaterialHandle& operator=(const MaterialHandle&) = delete;
  MaterialHandle& operator=(MaterialHandle&&) = default;

 private:
  ResourcePointer<Material> material_{nullptr};
};

}  // namespace phx

#endif  // LIBRARY_PHX_RENDERING_COMPONENTS_MATERIAL_HANDLE_HPP_

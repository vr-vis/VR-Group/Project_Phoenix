//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include "phx/rendering/components/light.hpp"

#include <string>

namespace phx {

Light::Type Light::GetType() const { return type_; }
void Light::SetType(Light::Type type) { type_ = type; }

const glm::vec3& Light::GetColor() const { return color_; }
void Light::SetColor(const glm::vec3& color) { color_ = color; }

float Light::GetIntensity() const { return intensity_; }
void Light::SetIntensity(float intensity) { intensity_ = intensity; }

float Light::GetRange() const { return range_; }
void Light::SetRange(float range) { range_ = range; }

float Light::GetSpotAngle() const { return spot_angle_; }
void Light::SetSpotAngle(float spot_angle) { spot_angle_ = spot_angle; }

std::string Light::ToString() const {
  std::string desc = "LightComponent: type: ";
  switch (type_) {
    case phx::Light::Type::kPoint:
      desc += "point";
      break;
    case phx::Light::Type::kDirectional:
      desc += "directional";
      break;
    case phx::Light::Type::kSpot:
      desc += "spot";
      break;
  }
  return desc;
}

}  // namespace phx

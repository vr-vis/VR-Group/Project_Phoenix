//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include "phx/rendering/components/material_handle.hpp"

#include <iostream>
#include <string>

#include "phx/core/logger.hpp"

namespace phx {

void MaterialHandle::SetMaterial(ResourcePointer<Material> material) {
  material_ = material;
}

ResourcePointer<Material> MaterialHandle::GetMaterial() const {
  return material_;
}

std::string MaterialHandle::ToString() const {
  return GetName() + " (MaterialComponent)";
}
}  // namespace phx

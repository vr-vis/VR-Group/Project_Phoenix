//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef LIBRARY_PHX_RENDERING_COMPONENTS_LIGHT_HPP_
#define LIBRARY_PHX_RENDERING_COMPONENTS_LIGHT_HPP_

#include <string>

#include "phx/suppress_warnings.hpp"

SUPPRESS_WARNINGS_BEGIN
#include "glm/glm.hpp"
SUPPRESS_WARNINGS_END

#include "phx/core/component.hpp"
#include "phx/export.hpp"

namespace phx {

class PHOENIX_EXPORT Light final : public Component {
 public:
  enum class Type {
    kPoint,
    kDirectional,
    kSpot,
  };

  Type GetType() const;
  void SetType(Type type);

  const glm::vec3& GetColor() const;
  void SetColor(const glm::vec3& color);

  float GetIntensity() const;
  void SetIntensity(float intensity);

  float GetRange() const;
  void SetRange(float range);

  float GetSpotAngle() const;
  void SetSpotAngle(float spot_angle);

  std::string ToString() const override;

 protected:
  friend Entity;
  Light() = default;
  Light(const Light&) = delete;
  Light(Light&&) = default;

  Light& operator=(const Light&) = delete;
  Light& operator=(Light&&) = default;

 private:
  Type type_ = Type::kDirectional;
  glm::vec3 color_ = glm::vec3(1.0f, 1.0f, 1.0f);
  float intensity_ = 1.0f;
  float range_ = 10.0f;       // Only for point and spot lights.
  float spot_angle_ = 45.0f;  // Only for spot lights.
};

}  // namespace phx

#endif  // LIBRARY_PHX_RENDERING_COMPONENTS_LIGHT_HPP_

//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include "phx/rendering/components/projection.hpp"

#include <string>

#include "phx/suppress_warnings.hpp"

SUPPRESS_WARNINGS_BEGIN
#include "glm/mat4x4.hpp"
SUPPRESS_WARNINGS_END

#include "glm/gtc/matrix_transform.hpp"

namespace phx {

const glm::mat4& Projection::GetMatrix() const { return matrix_; }

void Projection::SetPerspective(float fovy, float aspect, float near,
                                float far) {
  matrix_ = glm::perspective(fovy, aspect, near, far);
}

void Projection::SetOrthogonal(float left, float right, float bottom, float top,
                               float near, float far) {
  matrix_ = glm::ortho(left, right, bottom, top, near, far);
}

void Projection::SetMatrix(const glm::mat4& matrix) { matrix_ = matrix; }

std::string Projection::ToString() const { return "ProjectionComponent"; }

}  // namespace phx

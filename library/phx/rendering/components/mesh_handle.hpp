//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------
#ifndef LIBRARY_PHX_RENDERING_COMPONENTS_MESH_HANDLE_HPP_
#define LIBRARY_PHX_RENDERING_COMPONENTS_MESH_HANDLE_HPP_

#include <string>

#include "phx/core/component.hpp"
#include "phx/export.hpp"
#include "phx/resources/resource_pointer.hpp"
#include "phx/resources/types/mesh.hpp"
#include "phx/utility/aspects/nameable.hpp"

namespace phx {
/**
 * A component holding a simple pointer to a mesh
 */
class PHOENIX_EXPORT MeshHandle final : public Component, public Nameable {
 public:
  ~MeshHandle() override = default;

  void SetMesh(ResourcePointer<Mesh> mesh);
  ResourcePointer<Mesh> GetMesh() const;

  std::string ToString() const override;

 protected:
  friend Entity;
  MeshHandle() = default;
  MeshHandle(const MeshHandle&) = delete;
  MeshHandle(MeshHandle&&) = default;

  MeshHandle& operator=(const MeshHandle&) = delete;
  MeshHandle& operator=(MeshHandle&&) = default;

 private:
  ResourcePointer<Mesh> mesh_{nullptr};
};
}  // namespace phx

#endif  // LIBRARY_PHX_RENDERING_COMPONENTS_MESH_HANDLE_HPP_

//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include <sstream>
#include <string>

#include "phx/suppress_warnings.hpp"

SUPPRESS_WARNINGS_BEGIN
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtx/matrix_decompose.hpp"
#include "glm/gtx/quaternion.hpp"
#include "glm/gtx/transform.hpp"
SUPPRESS_WARNINGS_END

#include "phx/core/entity.hpp"
#include "phx/core/logger.hpp"
#include "phx/rendering/components/transform.hpp"
#include "phx/utility/stream_helpers.hpp"

namespace phx {
Transform::Transform(const glm::vec3& translation, const glm::quat& rotation,
                     const glm::vec3& scale)
    : local_translation_(translation),
      local_rotation_(rotation),
      local_scale_(scale) {
  UpdateLocalMatrix();
}
Transform::Transform(const glm::vec3& translation,
                     const glm::vec3& rotation_euler, const glm::vec3& scale)
    : Transform(translation, glm::quat(glm::radians(rotation_euler)), scale) {}

const glm::vec3& Transform::GetLocalTranslation() const {
  return local_translation_;
}
Transform& Transform::SetLocalTranslation(const glm::vec3& translation) {
  local_translation_ = translation;
  UpdateLocalMatrix();
  return *this;
}

const glm::quat& Transform::GetLocalRotation() const { return local_rotation_; }
Transform& Transform::SetLocalRotation(const glm::quat& rotation) {
  local_rotation_ = rotation;
  UpdateLocalMatrix();
  return *this;
}

glm::vec3 Transform::GetLocalRotationEuler() const {
  return glm::degrees(glm::eulerAngles(local_rotation_));
}
Transform& Transform::SetLocalRotationEuler(const glm::vec3& rotation_euler) {
  local_rotation_ = glm::quat(glm::radians(rotation_euler));
  UpdateLocalMatrix();
  return *this;
}

const glm::vec3& Transform::GetLocalScale() const { return local_scale_; }
Transform& Transform::SetLocalScale(const glm::vec3& scale) {
  local_scale_ = scale;
  UpdateLocalMatrix();
  return *this;
}

const glm::mat4& Transform::GetLocalMatrix() const { return local_matrix_; }
Transform& Transform::SetLocalMatrix(const glm::mat4& matrix) {
  local_matrix_ = matrix;
  glm::vec3 skew;
  glm::vec4 perspective;
  glm::decompose(local_matrix_, local_scale_, local_rotation_,
                 local_translation_, skew, perspective);
  UpdateGlobalMatricesRecursively();
  return *this;
}

const glm::vec3& Transform::GetGlobalTranslation() const {
  return global_translation_;
}
Transform& Transform::SetGlobalTranslation(const glm::vec3& translation) {
  local_translation_ =
      parent_ ? translation - parent_->global_translation_ : translation;
  UpdateLocalMatrix();
  return *this;
}

const glm::quat& Transform::GetGlobalRotation() const {
  return global_rotation_;
}
Transform& Transform::SetGlobalRotation(const glm::quat& rotation) {
  local_rotation_ =
      parent_ ? glm::inverse(parent_->global_rotation_) * rotation : rotation;
  UpdateLocalMatrix();
  return *this;
}

glm::vec3 Transform::GetGlobalRotationEuler() const {
  return glm::degrees(glm::eulerAngles(global_rotation_));
}
Transform& Transform::SetGlobalRotationEuler(const glm::vec3& rotation_euler) {
  local_rotation_ = parent_ ? glm::inverse(parent_->global_rotation_) *
                                  glm::quat(glm::radians(rotation_euler))
                            : glm::quat(glm::radians(rotation_euler));
  UpdateLocalMatrix();
  return *this;
}

const glm::vec3& Transform::GetGlobalScale() const { return global_scale_; }
Transform& Transform::SetGlobalScale(const glm::vec3& scale) {
  local_scale_ = parent_ ? scale / parent_->GetGlobalScale() : scale;
  UpdateLocalMatrix();
  return *this;
}

const glm::mat4& Transform::GetGlobalMatrix() const { return global_matrix_; }
Transform& Transform::SetGlobalMatrix(const glm::mat4& matrix) {
  local_matrix_ =
      parent_ ? glm::inverse(parent_->global_matrix_) * matrix : matrix;
  glm::vec3 skew;
  glm::vec4 perspective;
  glm::decompose(local_matrix_, local_scale_, local_rotation_,
                 local_translation_, skew, perspective);
  UpdateGlobalMatricesRecursively();
  return *this;
}

// Convenience functions.
Transform& Transform::Translate(const glm::vec3& amount) {
  return SetLocalTranslation(amount + local_translation_);
}
Transform& Transform::Rotate(const glm::quat& amount) {
  return SetLocalRotation(local_rotation_ * amount);
}
Transform& Transform::RotateEuler(const glm::vec3& amount) {
  return SetLocalRotation(local_rotation_ * glm::quat(glm::radians(amount)));
}
Transform& Transform::Scale(const glm::vec3& amount) {
  return SetLocalScale(amount + local_scale_);
}
Transform& Transform::LookAt(const glm::vec3& target,
                             const glm::vec3& up_vector) {
  return SetLocalRotation(glm::conjugate(
      glm::toQuat(glm::lookAt(local_translation_, target, up_vector))));
}
Transform& Transform::Reset() {
  local_translation_ = glm::vec3();
  local_rotation_ = glm::quat();
  local_scale_ = glm::vec3(1.0f);
  local_matrix_ = glm::mat4();
  return *this;
}

glm::vec3 Transform::Right() const {
  return local_rotation_ * glm::vec3(1, 0, 0);
}
glm::vec3 Transform::Up() const { return local_rotation_ * glm::vec3(0, 1, 0); }
glm::vec3 Transform::Forward() const {
  return local_rotation_ * glm::vec3(0, 0, -1);
}

void Transform::SetParent(Transform* parent) { SetParent(parent, true); }

void Transform::SetParent(Transform* parent, bool maintainGlobalPosition) {
  if (!CheckIfParentIsValid(parent)) {
    error("The transform to be set as parent is not within the same scene.");
    return;
  }
  glm::mat4 global_matrix_cache = GetGlobalMatrix();
  Hierarchical::SetParent(parent);
  if (maintainGlobalPosition) {
    SetGlobalMatrix(global_matrix_cache);
  } else {
    UpdateGlobalMatricesRecursively();
  }
}

bool Transform::CheckIfParentIsValid(Transform* parent) {
  if (!parent) return true;
  auto entity_this = GetEntity();
  auto entity_parent = parent->GetEntity();
  if (entity_this && entity_parent &&
      entity_this->GetScene() == entity_parent->GetScene()) {
    return true;
  }
  return false;
}

std::string Transform::ToString() const {
  std::ostringstream sstr;
  sstr << "TransformComponent: " << std::endl;
  sstr << local_matrix_;
  return sstr.str();
}

void Transform::UpdateLocalMatrix() {
  local_matrix_ = glm::translate(local_translation_) *
                  glm::mat4_cast(local_rotation_) * glm::scale(local_scale_);
  UpdateGlobalMatricesRecursively();
}
void Transform::UpdateGlobalMatricesRecursively() {
  global_translation_ = parent_
                            ? parent_->global_translation_ + local_translation_
                            : local_translation_;
  global_rotation_ =
      parent_ ? parent_->global_rotation_ * local_rotation_ : local_rotation_;
  global_scale_ =
      parent_ ? parent_->global_scale_ * local_scale_ : local_scale_;
  global_matrix_ =
      parent_ ? parent_->global_matrix_ * local_matrix_ : local_matrix_;
  for (auto child : children_) child->UpdateGlobalMatricesRecursively();
}

}  // namespace phx

//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef LIBRARY_PHX_RENDERING_COMPONENTS_TRANSFORM_HPP_
#define LIBRARY_PHX_RENDERING_COMPONENTS_TRANSFORM_HPP_

#include <ostream>
#include <string>

#include "phx/suppress_warnings.hpp"

SUPPRESS_WARNINGS_BEGIN
#include "glm/gtc/quaternion.hpp"
#include "glm/mat4x4.hpp"
#include "glm/vec4.hpp"
SUPPRESS_WARNINGS_END

#include "phx/core/component.hpp"
#include "phx/utility/aspects/hierarchical.hpp"
#include "phx/export.hpp"

namespace phx {

// Quaternions are in radians. Euler angles are in degrees.
class PHOENIX_EXPORT Transform : public Component,
                                 public Hierarchical<Transform> {
 public:
  ~Transform() override = default;

  // Getting/Setting Local transform
  const glm::vec3& GetLocalTranslation() const;
  Transform& SetLocalTranslation(const glm::vec3& translation);

  const glm::quat& GetLocalRotation() const;
  Transform& SetLocalRotation(const glm::quat& rotation);

  glm::vec3 GetLocalRotationEuler() const;
  Transform& SetLocalRotationEuler(const glm::vec3& rotation_euler);

  const glm::vec3& GetLocalScale() const;
  Transform& SetLocalScale(const glm::vec3& scale);

  const glm::mat4& GetLocalMatrix() const;
  Transform& SetLocalMatrix(const glm::mat4& matrix);

  // Getting/Setting Global transform
  const glm::vec3& GetGlobalTranslation() const;
  Transform& SetGlobalTranslation(const glm::vec3& translation);

  const glm::quat& GetGlobalRotation() const;
  Transform& SetGlobalRotation(const glm::quat& rotation);

  glm::vec3 GetGlobalRotationEuler() const;
  Transform& SetGlobalRotationEuler(const glm::vec3& rotation_euler);

  const glm::vec3& GetGlobalScale() const;
  Transform& SetGlobalScale(const glm::vec3& scale);

  const glm::mat4& GetGlobalMatrix() const;
  Transform& SetGlobalMatrix(const glm::mat4& matrix);

  // Convenience functions.
  Transform& Translate(const glm::vec3& amount);
  Transform& Rotate(const glm::quat& amount);
  Transform& RotateEuler(const glm::vec3& amount);
  Transform& Scale(const glm::vec3& amount);
  Transform& LookAt(const glm::vec3& target,
                    const glm::vec3& up_vector = glm::vec3(0.0f, 1.0f, 0.0f));
  Transform& Reset();

  glm::vec3 Right() const;
  glm::vec3 Up() const;
  glm::vec3 Forward() const;

  void SetParent(Transform* parent) override;
  void SetParent(Transform* parent, bool maintainGlobalPosition);

  std::string ToString() const override;

 protected:
  friend Entity;
  Transform(const glm::vec3& translation = glm::vec3(),
            const glm::quat& rotation = glm::quat(),
            const glm::vec3& scale = glm::vec3(1.0f));
  Transform(const glm::vec3& translation, const glm::vec3& rotation_euler,
            const glm::vec3& scale = glm::vec3(1.0f));
  Transform(const Transform&) = delete;
  Transform(Transform&&) = default;

  Transform& operator=(const Transform&) = delete;
  Transform& operator=(Transform&&) = default;

 private:
  bool CheckIfParentIsValid(Transform* parent);
  void UpdateLocalMatrix();
  void UpdateGlobalMatricesRecursively();

  glm::vec3 local_translation_;
  glm::quat local_rotation_;
  glm::vec3 local_scale_;
  glm::mat4 local_matrix_;
  glm::vec3 global_translation_;
  glm::quat global_rotation_;
  glm::vec3 global_scale_;
  glm::mat4 global_matrix_;
};

}  // namespace phx

#endif  // LIBRARY_PHX_RENDERING_COMPONENTS_TRANSFORM_HPP_

//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef LIBRARY_PHX_RENDERING_COMPONENTS_PROJECTION_HPP_
#define LIBRARY_PHX_RENDERING_COMPONENTS_PROJECTION_HPP_

#include <string>

#include "phx/suppress_warnings.hpp"

SUPPRESS_WARNINGS_BEGIN
#include "glm/mat4x4.hpp"
SUPPRESS_WARNINGS_END

#include "phx/core/component.hpp"
#include "phx/export.hpp"

namespace phx {

class PHOENIX_EXPORT Projection final : public Component {
 public:
  const glm::mat4& GetMatrix() const;

  void SetPerspective(float fovy, float aspect, float near, float far);

  void SetOrthogonal(float left, float right, float bottom, float top,
                     float near, float far);
  void SetMatrix(const glm::mat4& matrix);

  std::string ToString() const override;

 protected:
  friend Entity;
  Projection() = default;
  Projection(const Projection&) = delete;
  Projection(Projection&&) = default;

  Projection& operator=(const Projection&) = delete;
  Projection& operator=(Projection&&) = default;

 private:
  glm::mat4 matrix_;
};

}  // namespace phx

#endif  // LIBRARY_PHX_RENDERING_COMPONENTS_PROJECTION_HPP_

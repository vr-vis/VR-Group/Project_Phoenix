//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------
#ifndef LIBRARY_PHX_RENDERING_COMPONENTS_MESH_RENDER_SETTINGS_HPP_
#define LIBRARY_PHX_RENDERING_COMPONENTS_MESH_RENDER_SETTINGS_HPP_

#include "phx/core/component.hpp"
#include "phx/export.hpp"

namespace phx {
/**
 * A component holding a simple pointer to a mesh
 */
class PHOENIX_EXPORT MeshRenderSettings final : public Component {
 public:
  virtual ~MeshRenderSettings() = default;

  void SetWireframeMode(bool enabled);
  bool GetWireframeMode() const;

  static const MeshRenderSettings* GetDefault();

 private:
  bool wireframe_mode_ = false;

  static const MeshRenderSettings default_settings_;
};
}  // namespace phx

#endif  // LIBRARY_PHX_RENDERING_COMPONENTS_MESH_RENDER_SETTINGS_HPP_

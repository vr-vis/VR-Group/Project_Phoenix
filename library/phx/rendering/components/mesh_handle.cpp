//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include "phx/rendering/components/mesh_handle.hpp"

#include <string>

#include "phx/resources/types/mesh.hpp"

namespace phx {

void MeshHandle::SetMesh(ResourcePointer<Mesh> mesh) { mesh_ = mesh; }

ResourcePointer<Mesh> MeshHandle::GetMesh() const { return mesh_; }

std::string MeshHandle::ToString() const {
  if (mesh_ == nullptr) return GetName() + " (MeshHandle <empty>)";

  auto mesh = this->GetMesh();
  return GetName() + " (MeshHandle" +
         " #Vertices: " + std::to_string(mesh->GetVertices().size()) +
         " #Normals: " + std::to_string(mesh->GetNormals().size()) +
         " #TexCoords: " + std::to_string(mesh->GetTextureCoords().size()) +
         " #Tangents: " + std::to_string(mesh->GetTangents().size()) +
         " #Bitangents: " + std::to_string(mesh->GetBitangents().size()) +
         " #Indices: " + std::to_string(mesh->GetIndices().size()) + ")";
}

}  // namespace phx

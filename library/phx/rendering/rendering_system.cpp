//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include "phx/rendering/rendering_system.hpp"

#include <memory>
#include <string>
#include <utility>
#include <vector>

#include "phx/core/engine.hpp"
#include "phx/core/logger.hpp"
#include "phx/core/scene.hpp"
#include "phx/core/system.hpp"
#include "phx/rendering/components/light.hpp"
#include "phx/rendering/components/material_handle.hpp"
#include "phx/rendering/components/mesh_handle.hpp"
#include "phx/rendering/components/mesh_render_settings.hpp"
#include "phx/rendering/components/transform.hpp"

#include "gl/opengl.hpp"

namespace phx {

RenderingSystem::RenderingSystem(Engine* engine, DisplaySystem* display_system)
    : System(engine) {
  if (!display_system) error("RenderingSystem needs a valid DisplaySystem.");
  InitializeOpenGL();
  SetupFramegraph();
}

void RenderingSystem::InitializeOpenGL() {
  if (!gl::initialize()) {
    error("Initializing gl failed");
  }
  std::string prefix = "[RenderingSystem] OpenGl Error: ";
  gl::print_error(prefix.c_str());
}

void RenderingSystem::SetupFramegraph() {
  // set up an empty frame graph, just so we always have one...
  frame_graph_ = std::make_unique<FrameGraph>();
  frame_graph_->Initialize();
}

void RenderingSystem::Update(const FrameTimer::TimeInfo&) {
  std::vector<GeometryPass::RenderingInstance> rendering_instances;
  std::vector<std::pair<Light*, Transform*>> light_transform_pairs;

  if (GetEngine()->GetScene() == nullptr) {
    return;
  }

  for (auto& entity : GetEngine()->GetEntities()) {
    auto mesh_handle = entity->GetFirstComponent<MeshHandle>();
    auto light = entity->GetFirstComponent<Light>();
    auto transform = entity->GetFirstComponent<Transform>();
    auto material_handle = entity->GetFirstComponent<MaterialHandle>();
    Material* material = nullptr;
    if (material_handle != nullptr)
      material = material_handle->GetMaterial().Get();
    auto mesh_render_settings = entity->GetFirstComponent<MeshRenderSettings>();

    if (transform != nullptr) {
      if (mesh_handle != nullptr) {
        rendering_instances.push_back({mesh_handle->GetMesh().Get(), material,
                                       transform, mesh_render_settings});
      } else if (light != nullptr) {
        light_transform_pairs.push_back(
            std::pair<Light*, Transform*>(light, transform));
      }
    }
  }

  auto geometry_passes = frame_graph_->GetRenderPasses<GeometryPass>();
  for (auto geometry_pass : geometry_passes) {
    geometry_pass->SetData(rendering_instances, light_transform_pairs);
  }

  frame_graph_->Execute();
}

FrameGraph* RenderingSystem::GetFrameGraph() const {
  return frame_graph_.get();
}

void RenderingSystem::SetFrameGraph(std::unique_ptr<FrameGraph> frame_graph) {
  frame_graph_ = std::move(frame_graph);
}

std::string RenderingSystem::ToString() const {
  return "RenderingSystem with #FrameGraph-Passes: " +
         std::to_string(frame_graph_->GetNumberOfPasses());
}

}  // namespace phx

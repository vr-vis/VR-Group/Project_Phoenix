//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include "phx/rendering/frame_graph.hpp"

#include <memory>
#include <utility>

#include "phx/rendering/render_passes/clear_pass.hpp"
#include "phx/rendering/render_passes/geometry_pass.hpp"

namespace phx {

void FrameGraph::Initialize() {
  for (auto& pass : render_passes_) {
    pass->Initialize();
  }
}

void FrameGraph::Execute() {
  for (auto& pass : render_passes_) {
    pass->Execute();
  }
}


std::size_t FrameGraph::GetNumberOfPasses() { return render_passes_.size(); }

phx::RenderPass* FrameGraph::AddRenderPass(
    std::unique_ptr<RenderPass> render_pass) {
  auto it = render_passes_.insert(render_passes_.end(), std::move(render_pass));
  return (*it).get();
}

}  // namespace phx

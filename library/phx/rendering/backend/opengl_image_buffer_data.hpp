//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef LIBRARY_PHX_RENDERING_BACKEND_OPENGL_IMAGE_BUFFER_DATA_HPP_
#define LIBRARY_PHX_RENDERING_BACKEND_OPENGL_IMAGE_BUFFER_DATA_HPP_

#include <algorithm>
#include <cmath>
#include <fstream>
#include <locale>
#include <memory>
#include <string>
#include <vector>

#include "GL/glew.h"
#include "gl/texture.hpp"

#include "phx/resources/types/image.hpp"
#include "phx/resources/resource_manager.hpp"
#include "phx/resources/resource_utils.hpp"
#include "phx/export.hpp"

namespace phx {

struct PHOENIX_EXPORT OpenGLImageBufferDataType {};

struct PHOENIX_EXPORT OpenGLImageBufferDataType_RGB
    : public OpenGLImageBufferDataType {
 public:
  OpenGLImageBufferDataType_RGB(unsigned char r, unsigned char g,
                                unsigned char b)
      : r_(r), g_(g), b_(b) {}
  unsigned char r_;
  unsigned char g_;
  unsigned char b_;
  static std::string GetFormatString() { return "RGB"; }
  static OpenGLImageBufferDataType_RGB GetMaxValue() { return {255, 255, 255}; }
  static OpenGLImageBufferDataType_RGB GetMinValue() { return {0, 0, 0}; }
};

struct PHOENIX_EXPORT OpenGLImageBufferDataType_RGBA
    : public OpenGLImageBufferDataType {
 public:
  OpenGLImageBufferDataType_RGBA(unsigned char r, unsigned char g,
                                 unsigned char b, unsigned char a)
      : r_(r), g_(g), b_(b), a_(a) {}
  unsigned char r_;
  unsigned char g_;
  unsigned char b_;
  unsigned char a_;
  static std::string GetFormatString() { return "RGBA"; }
  static OpenGLImageBufferDataType_RGBA GetMaxValue() {
    return {255, 255, 255, 255};
  }
  static OpenGLImageBufferDataType_RGBA GetMinValue() { return {0, 0, 0, 0}; }
};

struct PHOENIX_EXPORT OpenGLImageBufferDataType_Float32
    : public OpenGLImageBufferDataType {
 public:
  explicit OpenGLImageBufferDataType_Float32(float f) : value_(f) {}
  float value_;
  static std::string GetFormatString() { return "Float"; }
  static OpenGLImageBufferDataType_Float32 GetMaxValue() {
    return OpenGLImageBufferDataType_Float32(1.f);
  }
  static OpenGLImageBufferDataType_Float32 GetMinValue() {
    return OpenGLImageBufferDataType_Float32(0.f);
  }
};

struct PHOENIX_EXPORT OpenGLImageBufferDataType_Byte
    : public OpenGLImageBufferDataType {
 public:
  explicit OpenGLImageBufferDataType_Byte(unsigned char value)
      : value_(value) {}
  unsigned char value_;
  static std::string GetFormatString() { return "Byte"; }
  static OpenGLImageBufferDataType_Byte GetMaxValue() {
    return OpenGLImageBufferDataType_Byte(255);
  }
  static OpenGLImageBufferDataType_Byte GetMinValue() {
    return OpenGLImageBufferDataType_Byte(0);
  }
};

inline PHOENIX_EXPORT double PixelDistance(
    OpenGLImageBufferDataType_Float32 a, OpenGLImageBufferDataType_Float32 b) {
  return std::abs(static_cast<double>(a.value_ - b.value_));
}

inline PHOENIX_EXPORT double PixelDistance(OpenGLImageBufferDataType_Byte a,
                                           OpenGLImageBufferDataType_Byte b) {
  return std::abs(static_cast<double>(a.value_) -
                  static_cast<double>(b.value_));
}

inline PHOENIX_EXPORT double PixelDistance(OpenGLImageBufferDataType_RGB a,
                                           OpenGLImageBufferDataType_RGB b) {
  const double diffR = a.r_ - b.r_;
  const double diffG = a.g_ - b.g_;
  const double diffB = a.b_ - b.b_;
  return std::sqrt(diffR * diffR + diffG * diffG + diffB * diffB);
}

inline PHOENIX_EXPORT double PixelDistance(OpenGLImageBufferDataType_RGBA a,
                                           OpenGLImageBufferDataType_RGBA b) {
  const double diffR = a.r_ - b.r_;
  const double diffG = a.g_ - b.g_;
  const double diffB = a.b_ - b.b_;
  const double diffA = a.a_ - b.a_;
  return std::sqrt(diffR * diffR + diffG * diffG + diffB * diffB +
                   diffA * diffA);
}

template <typename T>
class PHOENIX_EXPORT OpenGLImageBufferData {
 public:
  OpenGLImageBufferData(std::size_t width, std::size_t height)
      : width_(width), height_(height) {
    static_assert(
        std::is_base_of<phx::OpenGLImageBufferDataType, T>::value,
        "Data type is not derived from phx::OpenGLImageBufferDataType.");
    // allocate space
    buffer_data_.resize(width_ * height_ * sizeof(T));
  }
  OpenGLImageBufferData(const OpenGLImageBufferData&) = default;
  OpenGLImageBufferData(OpenGLImageBufferData&&) = default;
  virtual ~OpenGLImageBufferData() = default;

  OpenGLImageBufferData& operator=(const OpenGLImageBufferData&) = default;
  OpenGLImageBufferData& operator=(OpenGLImageBufferData&&) = default;

  static std::unique_ptr<OpenGLImageBufferData<T>> CreateFromImage(
      phx::Image* image);

  static std::unique_ptr<OpenGLImageBufferData<T>> CreateFromTexture(
      gl::texture_2d* texture);

  std::unique_ptr<phx::Image> CreateImage() const;

  inline std::size_t GetSizeInBytes() const { return buffer_data_.size(); }
  inline std::size_t GetWidth() const { return width_; }
  inline std::size_t GetHeight() const { return height_; }
  inline std::size_t GetArea() const { return width_ * height_; }
  inline const std::vector<unsigned char>& GetBufferData() const {
    return buffer_data_;
  }

  inline T GetPixel(std::size_t x, std::size_t y) const {
    const T* pixel_pointer =
        reinterpret_cast<const T*>(&buffer_data_[(y * width_ + x) * sizeof(T)]);
    return *pixel_pointer;
  }

  inline void SetPixel(std::size_t x, std::size_t y, T pixel) {
    T* pixel_pointer =
        reinterpret_cast<T*>(&buffer_data_[(y * width_ + x) * sizeof(T)]);
    *pixel_pointer = pixel;
  }

  void SaveToFileBinary(const std::string& filename) const;

  void SaveToFilePNG(const std::string& filename) const;

  static OpenGLImageBufferData<T> ReadFromFileBinary(
      const std::string& filename);

  static OpenGLImageBufferData<T> ReadFromFilePNG(const std::string& filename);

  void ReadColorPixels(bool read_front_buffer = false);

  void ReadDepthPixels(bool read_front_buffer = false);

  bool operator==(const OpenGLImageBufferData<T>& other) const {
    return (width_ == other.width_ && height_ == other.height_ &&
            buffer_data_ == other.buffer_data_);
  }
  bool operator!=(const OpenGLImageBufferData<T>& other) const {
    return (width_ != other.width_ || height_ != other.height_ ||
            buffer_data_ != other.buffer_data_);
  }

  static std::unique_ptr<
      OpenGLImageBufferData<OpenGLImageBufferDataType_Float32>>
  CreateDifferenceMagnitudeBuffer(const OpenGLImageBufferData<T>& first,
                                  const OpenGLImageBufferData<T>& second);

 private:
  std::size_t width_;
  std::size_t height_;

  std::vector<unsigned char> buffer_data_;
};

template <typename T>
std::unique_ptr<phx::OpenGLImageBufferData<OpenGLImageBufferDataType_Float32>>
phx::OpenGLImageBufferData<T>::CreateDifferenceMagnitudeBuffer(
    const OpenGLImageBufferData<T>& first,
    const OpenGLImageBufferData<T>& second) {
  if (first.GetWidth() != second.GetWidth() ||
      first.GetHeight() != second.GetHeight())
    return nullptr;

  // create new buffer
  auto diff = std::make_unique<
      OpenGLImageBufferData<OpenGLImageBufferDataType_Float32>>(
      first.GetWidth(), first.GetHeight());
  // compute each pixel as the pixel distance, normalized by the max possible
  // distance
  double maxdist = phx::PixelDistance(T::GetMaxValue(), T::GetMinValue());
  for (std::size_t y = 0; y < first.GetHeight(); ++y) {
    for (std::size_t x = 0; x < first.GetWidth(); ++x) {
      double dist =
          phx::PixelDistance(first.GetPixel(x, y), second.GetPixel(x, y)) /
          maxdist;
      diff->SetPixel(
          x, y,
          phx::OpenGLImageBufferDataType_Float32(static_cast<float>(dist)));
    }
  }

  return diff;
}

template <>
void phx::OpenGLImageBufferData<OpenGLImageBufferDataType_RGB>::ReadColorPixels(
    bool read_front_buffer);
template <>
void phx::OpenGLImageBufferData<
    OpenGLImageBufferDataType_RGBA>::ReadColorPixels(bool read_front_buffer);
template <>
void phx::OpenGLImageBufferData<
    OpenGLImageBufferDataType_Byte>::ReadDepthPixels(bool read_front_buffer);
template <>
void phx::OpenGLImageBufferData<
    OpenGLImageBufferDataType_Float32>::ReadDepthPixels(bool read_front_buffer);

template <>
std::unique_ptr<phx::Image> phx::OpenGLImageBufferData<
    phx::OpenGLImageBufferDataType_Float32>::CreateImage() const;

template <>
std::unique_ptr<OpenGLImageBufferData<OpenGLImageBufferDataType_Float32>>
phx::OpenGLImageBufferData<OpenGLImageBufferDataType_Float32>::CreateFromImage(
    phx::Image* image);

template <typename T>
std::unique_ptr<OpenGLImageBufferData<T>>
phx::OpenGLImageBufferData<T>::CreateFromImage(phx::Image* image) {
  if (image->GetBitsPerPixel() != sizeof(T) * 8) {
    switch (sizeof(T)) {
      case 1:
        image->To8Bits();
        break;
      case 3:
        image->To24Bits();
        break;
      case 4:
        image->To32Bits();
        break;
      default:
        return nullptr;
    }
  }
  // copy width, height and data outright
  auto dimensions = image->GetDimensions();
  std::unique_ptr<OpenGLImageBufferData<T>> buffer =
      std::make_unique<OpenGLImageBufferData<T>>(dimensions[0], dimensions[1]);
  const auto image_data_pointer = image->GetPixels();
  OpenGLImageBufferData<T>& buffer_ref = *buffer.get();
  for (std::size_t i = 0; i < image_data_pointer.second; ++i)
    buffer_ref.buffer_data_[i] = image_data_pointer.first[i];
  return buffer;
}

template <typename T>
std::unique_ptr<OpenGLImageBufferData<T>>
phx::OpenGLImageBufferData<T>::CreateFromTexture(gl::texture_2d* texture) {
  if (texture == nullptr) {
    warn("OpenGLImageBufferData<T>::CreateFromTexture called with nullptr");
    return nullptr;
  }
  if (texture->internal_format() != GL_RGB8) {
    warn(
        "OpenGLImageBufferData<T>::CreateFromTexture not implemented for "
        "anything else than RGB8, tried internal format: {}",
        texture->internal_format());
    return nullptr;
  }
  // copy width, height and data outright
  std::size_t width = static_cast<std::size_t>(texture->width());
  std::size_t height = static_cast<std::size_t>(texture->height());
  auto buffer = std::make_unique<OpenGLImageBufferData<T>>(width, height);
  // std::vector<GLubyte> tex_data = texture->image(0, GL_RGB,
  // GL_UNSIGNED_BYTE);
  buffer->buffer_data_ = static_cast<std::vector<unsigned char>>(
      texture->image(0, GL_RGB, GL_UNSIGNED_BYTE));

  return buffer;
}

// different behavior for Float32!
template <>
std::unique_ptr<OpenGLImageBufferData<OpenGLImageBufferDataType_Float32>>
phx::OpenGLImageBufferData<OpenGLImageBufferDataType_Float32>::CreateFromImage(
    phx::Image* image);

template <typename T>
std::unique_ptr<phx::Image> phx::OpenGLImageBufferData<T>::CreateImage() const {
  return std::make_unique<phx::Image>(
      const_cast<std::uint8_t*>(buffer_data_.data()),
      std::array<std::size_t, 2>{{width_, height_}}, sizeof(T) * 8);
}

// different behavior for Float32!
template <>
std::unique_ptr<phx::Image> phx::OpenGLImageBufferData<
    phx::OpenGLImageBufferDataType_Float32>::CreateImage() const;

template <typename T>
void phx::OpenGLImageBufferData<T>::SaveToFileBinary(
    const std::string& filename) const {
  std::ofstream stream(filename, std::ios::out | std::ios::binary);
  // width, height, buffer data
  stream.write(reinterpret_cast<const char*>(&width_), sizeof(std::size_t));
  stream.write(reinterpret_cast<const char*>(&height_), sizeof(std::size_t));
  const char* data_pointer = reinterpret_cast<const char*>(buffer_data_.data());
  stream.write(data_pointer, static_cast<std::streamsize>(buffer_data_.size()));
}

template <typename T>
phx::OpenGLImageBufferData<T> phx::OpenGLImageBufferData<T>::ReadFromFileBinary(
    const std::string& filename) {
  std::ifstream stream(filename, std::ios::in | std::ios::binary);
  std::size_t width, height;
  stream.read(reinterpret_cast<char*>(&width), sizeof(std::size_t));
  stream.read(reinterpret_cast<char*>(&height), sizeof(std::size_t));
  OpenGLImageBufferData<T> buffer(width, height);
  stream.read(reinterpret_cast<char*>(buffer.buffer_data_.data()),
              static_cast<std::streamsize>(width * height * sizeof(T)));
  return buffer;
}

template <typename T>
void phx::OpenGLImageBufferData<T>::SaveToFilePNG(
    const std::string& filename) const {
  std::string filename_corrected = filename;
  // check extension: should be .png
  if (filename.length() < 4) {
    filename_corrected = filename_corrected + ".png";
  } else {
    std::string extension = filename.substr(filename.length() - 4);
    std::transform(extension.begin(), extension.end(), extension.begin(),
                   [](char c) { return std::tolower(c, std::locale()); });
    if (extension != ".png") {
      filename_corrected = filename_corrected + ".png";
    }
  }

  const auto image = CreateImage();
  image->Save(filename_corrected);
}

template <typename T>
phx::OpenGLImageBufferData<T> phx::OpenGLImageBufferData<T>::ReadFromFilePNG(
    const std::string& filename) {
  auto image =
      phx::ResourceUtils::LoadResourceFromFile<Image>(filename, {}, true);
  auto buffer = OpenGLImageBufferData<T>::CreateFromImage(image.Get());
  auto* buffer_pointer = buffer.release();
  return *buffer_pointer;
}

}  // namespace phx

#endif  // LIBRARY_PHX_RENDERING_BACKEND_OPENGL_IMAGE_BUFFER_DATA_HPP_

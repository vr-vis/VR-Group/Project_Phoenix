//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef LIBRARY_PHX_RENDERING_BACKEND_SHADER_PROGRAM_HPP_
#define LIBRARY_PHX_RENDERING_BACKEND_SHADER_PROGRAM_HPP_

#include <cstddef>
#include <memory>
#include <string>
#include <utility>
#include <vector>

#include "phx/suppress_warnings.hpp"

SUPPRESS_WARNINGS_BEGIN
#include "gl/auxiliary/glm_uniforms.hpp"
SUPPRESS_WARNINGS_END
#include "gl/program.hpp"
#include "gl/shader.hpp"

#include "phx/export.hpp"
#include "phx/rendering/components/transform.hpp"
#include "phx/rendering/render_passes/render_pass.hpp"
#include "phx/resources/resource_pointer.hpp"
#include "phx/resources/types/mesh.hpp"
#include "phx/resources/types/shader_source.hpp"

namespace phx {

class PHOENIX_EXPORT ShaderProgram : public gl::program {
 public:
  enum ShaderChannel {
    VERTEX = 0,
    TESSELLATION_CONTROL,
    TESSELLATION_EVALUATION,
    GEOMETRY,
    FRAGMENT,
    COMPUTE,
    MAX_SHADER_CHANNEL
  };

  ShaderProgram() = default;
  ShaderProgram(const ShaderProgram&) = delete;
  ShaderProgram(ShaderProgram&&) = default;
  ~ShaderProgram() = default;

  ShaderProgram& operator=(const ShaderProgram&) = delete;
  ShaderProgram& operator=(ShaderProgram&&) = default;

  bool SetShader(ShaderChannel channel,
                 ResourcePointer<ShaderSource> shader_source);

  template <typename T>
  void SetUniform(const std::string& name, T value) {
    set_uniform(uniform_location(name), value);
  }

  // checks if program was already linked and if not does so
  void Link();

 private:
  bool CompileAndAttachShader(gl::shader* shader);

  GLenum ConvertChannelToGL(ShaderChannel channel) const;

  using ShaderHandle =
      std::pair<ResourcePointer<ShaderSource>, std::unique_ptr<gl::shader>>;
  std::array<ShaderHandle, MAX_SHADER_CHANNEL> shaders_;
};

}  // namespace phx

#endif  // LIBRARY_PHX_RENDERING_BACKEND_SHADER_PROGRAM_HPP_

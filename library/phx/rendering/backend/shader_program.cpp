//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include "phx/rendering/backend/shader_program.hpp"

#include <fstream>
#include <iostream>
#include <memory>
#include <string>

#include "phx/core/logger.hpp"
#include "phx/resources/types/shader_source.hpp"
#include "phx/resources/resource.hpp"

namespace phx {

bool ShaderProgram::SetShader(ShaderChannel channel,
                              ResourcePointer<ShaderSource> shader_source) {
  auto& entry = shaders_[channel];
  entry.first = shader_source;
  entry.second =
      std::make_unique<gl::shader>(this->ConvertChannelToGL(channel));
  gl::shader* shader = entry.second.get();
  shader->set_source(shader_source->GetSource());

  return this->CompileAndAttachShader(shader);
}

void ShaderProgram::Link() {
  if (!link_status()) {
    auto status = link();
    if (!status) {
      phx::warn("WARNING: Unable to link program with id: {} with message: {}",
                id(), info_log());
    }
  }
}

bool ShaderProgram::CompileAndAttachShader(gl::shader* shader) {
  auto compile_result = shader->compile();
  if (!compile_result) {
    phx::warn("WARNING: Unable to compile shader with message: {}",
              shader->info_log());
  }
  attach_shader(*shader);
  return compile_result;
}

GLenum ShaderProgram::ConvertChannelToGL(ShaderChannel channel) const {
  switch (channel) {
    case VERTEX:
      return GL_VERTEX_SHADER;
    case TESSELLATION_CONTROL:
      return GL_TESS_CONTROL_SHADER;
    case TESSELLATION_EVALUATION:
      return GL_TESS_EVALUATION_SHADER;
    case GEOMETRY:
      return GL_GEOMETRY_SHADER;
    case FRAGMENT:
      return GL_FRAGMENT_SHADER;
    case COMPUTE:
      return GL_COMPUTE_SHADER;
    case MAX_SHADER_CHANNEL:
      break;
  }
  phx::warn("WARNING: Invalid internal shader channel {}!", channel);
  return GL_INVALID_ENUM;
}

}  // namespace phx

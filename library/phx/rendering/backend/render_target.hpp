//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef LIBRARY_PHX_RENDERING_BACKEND_RENDER_TARGET_HPP_
#define LIBRARY_PHX_RENDERING_BACKEND_RENDER_TARGET_HPP_

#include <memory>

#include "phx/suppress_warnings.hpp"

SUPPRESS_WARNINGS_BEGIN
#include "glm/mat4x4.hpp"
#include "glm/vec2.hpp"
SUPPRESS_WARNINGS_END

SUPPRESS_WARNINGS_BEGIN
#include "gl/framebuffer.hpp"
SUPPRESS_WARNINGS_END
#include "gl/renderbuffer.hpp"
#include "gl/texture.hpp"

#include "phx/core/component.hpp"
#include "phx/export.hpp"

namespace phx {

class PHOENIX_EXPORT RenderTarget : public Component, public gl::framebuffer {
 public:
  RenderTarget() = delete;
  explicit RenderTarget(const glm::uvec2 dimensions);
  RenderTarget(RenderTarget&) = delete;
  RenderTarget(RenderTarget&&) = default;
  ~RenderTarget() = default;

  RenderTarget& operator=(const RenderTarget&) = delete;
  RenderTarget& operator=(RenderTarget&&) = default;

  const glm::uvec2& GetDimensions() const;
  gl::texture_2d* GetColorTexture() const;

  const glm::mat4 GetProjection() const;
  const glm::mat4 GetView() const;

  void SetViewport() const;

 private:
  std::unique_ptr<gl::texture_2d> color_texture_;
  std::unique_ptr<gl::texture_2d> depth_texture_;

  glm::uvec2 dimensions_;
};

}  // namespace phx

#endif  // LIBRARY_PHX_RENDERING_BACKEND_RENDER_TARGET_HPP_

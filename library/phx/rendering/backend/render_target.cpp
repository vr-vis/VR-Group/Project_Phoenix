//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include "phx/rendering/backend/render_target.hpp"

#include <memory>

#include "gl/command_execution.hpp"
#include "gl/viewport.hpp"

#include "phx/core/entity.hpp"
#include "phx/core/logger.hpp"
#include "phx/rendering/components/projection.hpp"
#include "phx/rendering/components/transform.hpp"

namespace phx {
phx::RenderTarget::RenderTarget(const glm::uvec2 dimensions)
    : framebuffer(), dimensions_(dimensions) {
  bind();

  color_texture_ = std::make_unique<gl::texture_2d>();
  color_texture_->bind();
  const GLint width = static_cast<GLint>(dimensions.x);
  const GLint height = static_cast<GLint>(dimensions.y);
  color_texture_->set_storage(1, GL_RGB8, width, height);
  gl::print_error("OpenGl Error creating Render Target Color Texture: ");

  attach_texture<GL_TEXTURE_2D>(GL_COLOR_ATTACHMENT0, *color_texture_, 0);
  color_texture_->unbind();

  depth_texture_ = std::make_unique<gl::texture_2d>();
  depth_texture_->bind();
  depth_texture_->set_storage(1, GL_DEPTH_COMPONENT24, width, height);
  gl::print_error("OpenGl Error creating Render Target Depth Texture: ");

  attach_texture<GL_TEXTURE_2D>(GL_DEPTH_ATTACHMENT, *depth_texture_, 0);
  depth_texture_->unbind();

  if (!is_valid() || !is_complete()) {
    gl::print_error("OpenGl Error creating Render Target: ");
    error(
        "[RenderTarget] Unable to create render target, status: {}, valid: {}",
        status(), is_valid());
    throw std::runtime_error("Framebuffer creation failed.");
  }

  unbind();
}

const glm::uvec2& RenderTarget::GetDimensions() const { return dimensions_; }

gl::texture_2d* RenderTarget::GetColorTexture() const {
  return color_texture_.get();
}

const glm::mat4 RenderTarget::GetProjection() const {
  auto projection = GetEntity()->GetFirstComponent<Projection>();
  if (projection) {
    return projection->GetMatrix();
  } else {
    error(
        "RenderTarget component may not be added to an Entity without a "
        "Projection component!");
    return glm::mat4();
  }
}

const glm::mat4 RenderTarget::GetView() const {
  auto transform = GetEntity()->GetFirstComponent<Transform>();
  if (transform) {
    return inverse(transform->GetGlobalMatrix());
  } else {
    error(
        "RenderTarget component may not be added to an Entity without a "
        "Transform component!");
    return glm::mat4();
  }
}

void RenderTarget::SetViewport() const {
  gl::set_viewport({{0, 0}}, {{static_cast<GLsizei>(dimensions_[0]),
                               static_cast<GLsizei>(dimensions_[1])}});
}

}  // namespace phx

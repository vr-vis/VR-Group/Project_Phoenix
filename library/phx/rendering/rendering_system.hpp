//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef LIBRARY_PHX_RENDERING_RENDERING_SYSTEM_HPP_
#define LIBRARY_PHX_RENDERING_RENDERING_SYSTEM_HPP_

#include <memory>
#include <ostream>
#include <string>
#include <vector>

#include "phx/core/engine.hpp"
#include "phx/core/scene.hpp"
#include "phx/core/system.hpp"
#include "phx/display/display_system.hpp"
#include "phx/export.hpp"
#include "phx/rendering/backend/render_target.hpp"
#include "phx/rendering/frame_graph.hpp"
#include "phx/rendering/render_passes/geometry_pass.hpp"

namespace phx {

class PHOENIX_EXPORT RenderingSystem : public System {
 public:
  RenderingSystem() = delete;
  RenderingSystem(const RenderingSystem&) = delete;
  RenderingSystem(RenderingSystem&&) = default;
  ~RenderingSystem() override = default;

  void Update(const FrameTimer::TimeInfo& time_info) override;

  FrameGraph* GetFrameGraph() const;
  void SetFrameGraph(std::unique_ptr<FrameGraph> frame_graph);

  std::string ToString() const override;

  RenderingSystem& operator=(const RenderingSystem&) = delete;
  RenderingSystem& operator=(RenderingSystem&&) = default;

 protected:
  template <typename SystemType, typename... SystemArguments>
  friend SystemType* Engine::CreateSystem(SystemArguments&&... arguments);
  RenderingSystem(Engine* engine, DisplaySystem* display_system);

 private:
  void InitializeOpenGL();
  void SetupFramegraph();

  std::unique_ptr<FrameGraph> frame_graph_;
};

}  // namespace phx

#endif  // LIBRARY_PHX_RENDERING_RENDERING_SYSTEM_HPP_

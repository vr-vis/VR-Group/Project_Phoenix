//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef LIBRARY_PHX_UTILITY_STREAM_HELPERS_HPP_
#define LIBRARY_PHX_UTILITY_STREAM_HELPERS_HPP_

#include <ostream>

#include "phx/suppress_warnings.hpp"

SUPPRESS_WARNINGS_BEGIN
#include "glm/gtc/quaternion.hpp"
#include "glm/mat4x4.hpp"
#include "glm/vec2.hpp"
#include "glm/vec3.hpp"
#include "glm/vec4.hpp"
SUPPRESS_WARNINGS_END

#include "phx/export.hpp"

// this is put into glm namespace, so they can be used for glm:: objects even in
// namespace like e.g. phx, leaving them in global namespace would not work
namespace glm {

class PHOENIX_EXPORT PhxStreamSettings {
 public:
  explicit PhxStreamSettings(std::ostream& stream);
  PhxStreamSettings(const PhxStreamSettings&) = default;
  PhxStreamSettings(PhxStreamSettings&&) = default;
  ~PhxStreamSettings();

  PhxStreamSettings& operator=(const PhxStreamSettings&) = default;
  PhxStreamSettings& operator=(PhxStreamSettings&&) = default;

 private:
  std::ostream& stream_;
  std::streamsize stream_precision_;
  std::ios::fmtflags stream_flags_;
};

PHOENIX_EXPORT std::ostream& operator<<(std::ostream& out,
                                        const glm::vec2& vec);

PHOENIX_EXPORT std::ostream& operator<<(std::ostream& out,
                                        const glm::vec3& vec);

PHOENIX_EXPORT std::ostream& operator<<(std::ostream& out,
                                        const glm::vec4& vec);

PHOENIX_EXPORT std::ostream& operator<<(std::ostream& out,
                                        const glm::quat& quat);

PHOENIX_EXPORT std::ostream& operator<<(std::ostream& out,
                                        const glm::mat4& mat);

}  // namespace glm
#endif  // LIBRARY_PHX_UTILITY_STREAM_HELPERS_HPP_

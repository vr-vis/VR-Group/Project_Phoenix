//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef LIBRARY_PHX_UTILITY_ORDERABLE_LIST_HPP_
#define LIBRARY_PHX_UTILITY_ORDERABLE_LIST_HPP_

#include <memory>
#include <utility>
#include <vector>

#include "phx/core/logger.hpp"
#include "phx/export.hpp"

namespace phx {

template <typename ElementType>
class PHOENIX_EXPORT OrderableList {
 public:
  typedef typename std::vector<std::unique_ptr<ElementType>>::iterator
      orderable_list_iterator;
  typedef typename std::vector<std::unique_ptr<ElementType>>::const_iterator
      orderable_list_const_iterator;

  std::size_t size() const { return data_.size(); }
  orderable_list_iterator begin() { return data_.begin(); }
  orderable_list_iterator end() { return data_.end(); }
  orderable_list_const_iterator begin() const { return data_.begin(); }
  orderable_list_const_iterator end() const { return data_.end(); }
  orderable_list_iterator erase(orderable_list_const_iterator first,
                                orderable_list_const_iterator last) {
    return data_.erase(first, last);
  }

  ElementType* PushBack(std::unique_ptr<ElementType>&& element) {
    data_.push_back(std::move(element));
    return data_.back().get();
  }

  void MoveToFront(ElementType* element);
  void MoveToBack(ElementType* element);
  void MoveAfter(ElementType* element, ElementType* after) {
    MoveRelativeTo(element, after, 1);
  }
  void MoveBefore(ElementType* element, ElementType* before) {
    MoveRelativeTo(element, before, 0);
  }

 private:
  orderable_list_iterator FindElement(ElementType* element);
  void MoveRelativeTo(ElementType* element, ElementType* relative_to,
                      int distance);

  std::vector<std::unique_ptr<ElementType>> data_;
};

template <typename ElementType>
void phx::OrderableList<ElementType>::MoveToFront(ElementType* element) {
  auto it = FindElement(element);
  if (it == data_.end()) {
    phx::error(
        "Cannot move Element to front of orderable list: element not found");
    return;
  }
  auto tmp = std::move(*it);
  data_.erase(it);
  data_.insert(data_.begin(), std::move(tmp));
}

template <typename ElementType>
void phx::OrderableList<ElementType>::MoveToBack(ElementType* element) {
  auto it = FindElement(element);
  if (it == data_.end()) {
    phx::error(
        "Cannot move element to the back of the orderable list: element not "
        "found");
    return;
  }
  auto tmp = std::move(*it);
  data_.erase(it);
  data_.insert(data_.end(), std::move(tmp));
}

template <typename ElementType>
typename std::vector<std::unique_ptr<ElementType>>::iterator
phx::OrderableList<ElementType>::FindElement(ElementType* element) {
  auto find_func = [element](const std::unique_ptr<ElementType>& elem) {
    return elem.get() == element;
  };
  return std::find_if(data_.begin(), data_.end(), find_func);
}

template <typename ElementType>
void phx::OrderableList<ElementType>::MoveRelativeTo(ElementType* element,
                                                     ElementType* relative_to,
                                                     int distance) {
  if (element == relative_to) return;
  auto it_element = FindElement(element);
  if (it_element == data_.end()) {
    phx::error("Cannot change ordered list: element not found");
    return;
  }
  auto it_target = FindElement(relative_to);
  if (it_target == data_.end()) {
    phx::error("Cannot change ordered list: target element not found");
    return;
  }
  auto tmp = std::move(*it_element);
  data_.erase(it_element);
  it_target = FindElement(relative_to);
  it_target += distance;
  data_.insert(it_target, std::move(tmp));
}

}  // namespace phx

#endif  // LIBRARY_PHX_UTILITY_ORDERABLE_LIST_HPP_

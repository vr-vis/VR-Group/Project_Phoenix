//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include "phx/utility/stream_helpers.hpp"

namespace glm {
PhxStreamSettings::PhxStreamSettings(std::ostream& stream) : stream_(stream) {
  stream_precision_ = stream.precision(3);
  stream_flags_ = stream.flags();

  // set fix point notation
  stream.setf(std::ios::fixed | std::ios::showpos);
}

PhxStreamSettings::~PhxStreamSettings() {
  stream_.precision(stream_precision_);
  stream_.flags(stream_flags_);
}

std::ostream& operator<<(std::ostream& out, const glm::vec2& vec) {
  PhxStreamSettings streamSettings(out);
  out << "(" << vec.x << ", " << vec.y << ")";
  return out;
}

std::ostream& operator<<(std::ostream& out, const glm::vec3& vec) {
  PhxStreamSettings streamSettings(out);
  out << "(" << vec.x << ", " << vec.y << ", " << vec.z << ")";
  return out;
}

std::ostream& operator<<(std::ostream& out, const glm::vec4& vec) {
  PhxStreamSettings streamSettings(out);
  out << "(" << vec.r << ", " << vec.g << "," << vec.b << ", " << vec.a << ")";
  return out;
}

std::ostream& operator<<(std::ostream& out, const glm::quat& quat) {
  PhxStreamSettings streamSettings(out);
  out << "(" << quat.x << ", " << quat.y << "," << quat.z << ", w: " << quat.w
      << ")";
  return out;
}

std::ostream& operator<<(std::ostream& out, const glm::mat4& mat) {
  PhxStreamSettings streamSettings(out);
  for (int row = 0; row < 4; ++row) {
    for (int col = 0; col < 4; ++col) {
      out << mat[col][row];
      if (col < 3) out << "  ";
    }
    if (row < 3) out << std::endl;
  }
  return out;
}

}  // namespace glm

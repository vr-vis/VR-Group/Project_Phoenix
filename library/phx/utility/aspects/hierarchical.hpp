//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef LIBRARY_PHX_UTILITY_ASPECTS_HIERARCHICAL_HPP_
#define LIBRARY_PHX_UTILITY_ASPECTS_HIERARCHICAL_HPP_

#include <algorithm>
#include <cstddef>
#include <vector>

#include "phx/export.hpp"

namespace phx {

template <typename Derived>
class PHOENIX_EXPORT Hierarchical {
 public:
  virtual ~Hierarchical() {
    Hierarchical::SetParent(nullptr);
    while (children_.size() != 0) {
      children_[0]->SetParent(nullptr);
    }
  }

  virtual void SetParent(Derived* parent) {
    if (parent_) {
      parent_->children_.erase(
          std::remove(parent_->children_.begin(), parent_->children_.end(),
                      static_cast<Derived*>(this)),
          parent_->children_.end());
    }
    parent_ = parent;

    if (parent_) parent_->children_.push_back(static_cast<Derived*>(this));
  }

  virtual const Derived* GetParent() const { return parent_; }
  virtual Derived* GetParent() { return parent_; }

  virtual const Derived* GetChild(const std::size_t index) const {
    return index < children_.size() ? children_[index] : nullptr;
  }
  virtual Derived* GetChild(const std::size_t index) {
    return index < children_.size() ? children_[index] : nullptr;
  }

  virtual std::size_t GetChildCount() { return children_.size(); }

  virtual typename std::vector<Derived*>::iterator begin() {
    return children_.begin();
  }
  virtual typename std::vector<Derived*>::const_iterator begin() const {
    return children_.begin();
  }
  virtual typename std::vector<Derived*>::iterator end() {
    return children_.end();
  }
  virtual typename std::vector<Derived*>::const_iterator end() const {
    return children_.end();
  }

 protected:
  Hierarchical() = default;
  Hierarchical(const Hierarchical&) = default;
  Hierarchical(Hierarchical&&) = default;

  Hierarchical& operator=(const Hierarchical&) = default;
  Hierarchical& operator=(Hierarchical&&) = default;

  Derived* parent_ = nullptr;
  std::vector<Derived*> children_;
};

}  // namespace phx

#endif  // LIBRARY_PHX_UTILITY_ASPECTS_HIERARCHICAL_HPP_

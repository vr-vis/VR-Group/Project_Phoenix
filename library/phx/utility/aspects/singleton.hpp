//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef LIBRARY_PHX_UTILITY_ASPECTS_SINGLETON_HPP_
#define LIBRARY_PHX_UTILITY_ASPECTS_SINGLETON_HPP_

#include <memory>
#include <mutex>
#include <utility>

namespace phx {
template <class type>
class singleton {
 public:
  template <typename... arguments>
  static type& instance(arguments&&... args) {
    std::call_once(
        get_once_flag(),
        [](arguments&&... lambda_args) {
          instance_.reset(new type(std::forward<arguments>(lambda_args)...));
        },
        std::forward<arguments>(args)...);
    return *instance_.get();
  }

 protected:
  explicit singleton<type>() = default;
  singleton(const singleton&) = delete;
  singleton(singleton&&) = default;
  virtual ~singleton<type>() = default;

  singleton& operator=(const singleton&) = delete;
  singleton& operator=(singleton&&) = default;

 private:
  static std::once_flag& get_once_flag() {
    static std::once_flag once;
    return once;
  }
  static std::unique_ptr<type> instance_;
};

template <class type>
std::unique_ptr<type> singleton<type>::instance_ = nullptr;

}  // namespace phx
#endif  // LIBRARY_PHX_UTILITY_ASPECTS_SINGLETON_HPP_

//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef LIBRARY_PHX_UTILITY_ASPECTS_LOGGABLE_HPP_
#define LIBRARY_PHX_UTILITY_ASPECTS_LOGGABLE_HPP_

#include <ostream>
#include <string>
#include <typeinfo>

#include "phx/export.hpp"

namespace phx {

class PHOENIX_EXPORT Loggable {
 public:
  virtual ~Loggable() = default;

  virtual std::string ToString() const;

 protected:
  Loggable() = default;
  Loggable(const Loggable&) = default;
  Loggable(Loggable&&) = default;

  Loggable& operator=(const Loggable&) = default;
  Loggable& operator=(Loggable&&) = default;
};

inline std::string Loggable::ToString() const { return typeid(*this).name(); }

template <typename T>
T& PHOENIX_EXPORT operator<<(T& out, const Loggable& loggable) {
  out << loggable.ToString();
  return out;
}

}  // namespace phx

#endif  // LIBRARY_PHX_UTILITY_ASPECTS_LOGGABLE_HPP_

//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef LIBRARY_PHX_UTILITY_ASPECTS_NAMEABLE_HPP_
#define LIBRARY_PHX_UTILITY_ASPECTS_NAMEABLE_HPP_

#include <string>

#include "phx/export.hpp"

namespace phx {
class PHOENIX_EXPORT Nameable {
 public:
  virtual ~Nameable() = default;

  void SetName(const std::string& name) { name_ = name; }
  std::string GetName() const { return name_; }

 protected:
  Nameable() = default;
  explicit Nameable(const std::string& name) { SetName(name); }
  Nameable(const Nameable&) = default;
  Nameable(Nameable&&) = default;

  Nameable& operator=(const Nameable&) = default;
  Nameable& operator=(Nameable&&) = default;

 private:
  std::string name_;
};

}  // namespace phx

#endif  // LIBRARY_PHX_UTILITY_ASPECTS_NAMEABLE_HPP_

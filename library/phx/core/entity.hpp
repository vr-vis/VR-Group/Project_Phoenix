//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef LIBRARY_PHX_CORE_ENTITY_HPP_
#define LIBRARY_PHX_CORE_ENTITY_HPP_

#include <algorithm>
#include <memory>
#include <ostream>
#include <string>
#include <type_traits>
#include <vector>

#include "phx/core/component.hpp"
#include "phx/utility/aspects/loggable.hpp"
#include "phx/utility/aspects/nameable.hpp"
#include "phx/export.hpp"

namespace phx {

class Scene;  // necessary forward declaration to avoid circular #include

class PHOENIX_EXPORT Entity : public Nameable, public Loggable {
  friend class Scene;

 public:
  Entity() : Nameable() {}
  explicit Entity(const std::string& name) : Nameable(name) {}
  Entity(const Entity&) = delete;
  Entity(Entity&&) = default;
  ~Entity() override = default;

  Entity& operator=(const Entity&) = delete;
  Entity& operator=(Entity&&) = default;

  template <class ComponentType, typename... ComponentArguments>
  ComponentType* AddComponent(ComponentArguments&&... arguments) {
    static_assert(std::is_base_of<phx::Component, ComponentType>::value,
                  "ComponentType is not derived from phx::Component.");
    auto component_ptr = new ComponentType(arguments...);
    components_.push_back(std::unique_ptr<ComponentType>(component_ptr));
    component_ptr->entity_ = this;
    return component_ptr;
  }
  void RemoveComponent(Component* component_handle) {
    components_.erase(FindComponent(component_handle));
  }
  std::size_t GetNumberOfComponents() const { return components_.size(); }

  bool HasComponent(Component* component_handle) const {
    return FindComponent(component_handle) != components_.end();
  }

  template <class ComponentType>
  ComponentType* GetFirstComponent() const {
    auto it =
        std::find_if(components_.begin(), components_.end(),
                     [](const std::unique_ptr<phx::Component>& a) {
                       return dynamic_cast<ComponentType*>(a.get()) != nullptr;
                     });
    if (it == components_.end()) {
      return nullptr;
    }
    return static_cast<ComponentType*>(it->get());
  }

  std::string ToString() const override;

  Scene* GetScene() const;

 private:
  std::vector<std::unique_ptr<Component>>::const_iterator FindComponent(
      Component* component_handle) const {
    return std::find_if(
        components_.begin(), components_.end(),
        [component_handle](const std::unique_ptr<phx::Component>& a) {
          return a.get() == component_handle;
        });
  }

  std::vector<std::unique_ptr<Component>> components_;
  Scene* scene_ = nullptr;
};

}  // namespace phx

#endif  // LIBRARY_PHX_CORE_ENTITY_HPP_

//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include "phx/core/scene.hpp"

#include <cstddef>

#include <algorithm>
#include <memory>
#include <string>
#include <utility>
#include <vector>

#include "phx/core/runtime_component.hpp"
#include "phx/rendering/components/transform.hpp"

namespace phx {

Scene::Scene() {
  phx::Entity* virtual_platform = CreateEntity();
  virtual_platform->AddComponent<phx::Transform>();
  virtual_platform
      ->AddComponent<phx::RuntimeComponent<phx::USER_PLATFORM>>();
}

Entity* Scene::CreateEntity() {
  entities_.push_back(std::make_unique<Entity>());
  auto new_entity = entities_.back().get();
  new_entity->scene_ = this;
  return new_entity;
}

void Scene::RemoveEntity(Entity* entity) {
  auto iterator =
      std::remove_if(entities_.begin(), entities_.end(),
                     [&entity](const std::unique_ptr<Entity>& iteratee) {
                       return entity == iteratee.get();
                     });
  entities_.erase(iterator, entities_.end());
}

std::size_t Scene::GetNumberOfEntities() const { return entities_.size(); }

std::vector<Entity*> Scene::GetEntities() const {
  std::vector<Entity*> entities(entities_.size());
  std::transform(
      entities_.begin(), entities_.end(), entities.begin(),
      [](const std::unique_ptr<Entity>& iteratee) { return iteratee.get(); });
  return entities;
}

std::string Scene::ToString() const {
  return GetName() +
         "(Scene #Entities: " + std::to_string(GetNumberOfEntities()) + ")";
}

}  // namespace phx

//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef LIBRARY_PHX_CORE_COMPONENT_CHECK_HPP_
#define LIBRARY_PHX_CORE_COMPONENT_CHECK_HPP_

#include <tuple>

#include "boost/optional.hpp"

#include "phx/core/entity.hpp"
#include "phx/export.hpp"

namespace phx {

template <typename... Components>
struct PHOENIX_EXPORT ComponentCheck {};
template <>
struct PHOENIX_EXPORT ComponentCheck<> {
  static bool Check(Entity*) { return true; }
};
template <typename Head, typename... Tail>
struct PHOENIX_EXPORT ComponentCheck<Head, Tail...> {
  static bool Check(Entity* entity) {
    return entity->GetFirstComponent<Head>() &&
           ComponentCheck<Tail...>::Check(entity);
  }
};

template <typename... Components>
struct PHOENIX_EXPORT ComponentGatherInternal {};
template <>
struct ComponentGatherInternal<> {
  template <typename Result>
  static void Gather(Entity*, Result*) {}
};
template <typename Head, typename... Tail>
struct PHOENIX_EXPORT ComponentGatherInternal<Head, Tail...> {
  template <typename Result>
  static void Gather(Entity* entity, Result* result) {
    auto component = entity->GetFirstComponent<Head>();
    if (!component) {
      *result = boost::none;
      return;
    }
    std::get<Head*>(result->value()) = component;
    ComponentGatherInternal<Tail...>::template Gather<Result>(entity, result);
  }
};
template <typename... Components>
struct PHOENIX_EXPORT ComponentGather {
  typedef boost::optional<std::tuple<Components*...>> ReturnType;

  static ReturnType Gather(Entity* entity) {
    ReturnType components = std::tuple<Components*...>();
    ComponentGatherInternal<Components...>::template Gather<ReturnType>(
        entity, &components);
    return components;
  }
};

}  // namespace phx

#endif  // LIBRARY_PHX_CORE_COMPONENT_CHECK_HPP_

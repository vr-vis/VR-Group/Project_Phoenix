//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef LIBRARY_PHX_CORE_RUNTIME_COMPONENT_HPP_
#define LIBRARY_PHX_CORE_RUNTIME_COMPONENT_HPP_

#include "phx/core/component.hpp"
#include "phx/export.hpp"

namespace phx {

enum RuntimeComponentType {
  USER_PLATFORM,
  HEAD,
  LEFT_EYE,
  RIGHT_EYE,
  LEFT_CONTROLLER,
  RIGHT_CONTROLLER
};

template <RuntimeComponentType t>
class PHOENIX_EXPORT RuntimeComponent : public Component {
 public:
  RuntimeComponent<t>() = default;
  RuntimeComponent<t>(const RuntimeComponent<t>&) = delete;
  RuntimeComponent<t>(RuntimeComponent<t>&&) = default;
  virtual ~RuntimeComponent<t>() = default;
  RuntimeComponent<t>& operator=(const RuntimeComponent<t>&) = delete;
  RuntimeComponent<t>& operator=(RuntimeComponent<t>&&) = default;
};

}  // namespace phx

#endif  // LIBRARY_PHX_CORE_RUNTIME_COMPONENT_HPP_

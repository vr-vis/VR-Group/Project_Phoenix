//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef LIBRARY_PHX_CORE_FRAME_TIMER_HPP_
#define LIBRARY_PHX_CORE_FRAME_TIMER_HPP_

#include <chrono>

#include "phx/export.hpp"
#include "phx/core/time_info.hpp"

namespace phx {

class PHOENIX_EXPORT FrameTimer final {
  friend class Engine;

 public:
  using Clock = std::chrono::high_resolution_clock;
  using Seconds = std::chrono::duration<double, std::ratio<1, 1>>;
  using TimePoint = std::chrono::time_point<Clock, Seconds>;
  using TimeInfo = phx::TimeInfo<Seconds, TimePoint>;

  const TimeInfo& GetTimeInfo() const { return time_info_; }

 private:
  void Start() {
    time_info_.startup_time = Clock::now();
    time_info_.frame_start_time = time_info_.startup_time;
    time_info_.time_since_startup = Seconds::zero();
    time_info_.time_since_last_frame = Seconds::zero();
    time_info_.frame_count = 0;
    // Ensure deltas are not equal to zero.
    Tick();
  }

  void Tick() {
    const auto last_frame_start_time = time_info_.frame_start_time;
    time_info_.frame_start_time = Clock::now();
    time_info_.time_since_startup =
        time_info_.frame_start_time - time_info_.startup_time;
    time_info_.time_since_last_frame =
        time_info_.frame_start_time - last_frame_start_time;
    time_info_.frame_count++;
  }

  TimeInfo time_info_;
};

}  // namespace phx

#endif  // LIBRARY_PHX_CORE_FRAME_TIMER_HPP_

//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include "phx/core/logger.hpp"

#include <memory>
#include <vector>

#ifdef WIN32
#include "spdlog/sinks/wincolor_sink.h"
#else
#include "spdlog/sinks/ansicolor_sink.h"
#endif
#include "spdlog/spdlog.h"

namespace phx {

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wexit-time-destructors"
#pragma clang diagnostic ignored "-Wglobal-constructors"
#endif

std::shared_ptr<spdlog::logger> logger;

#ifdef __clang__
#pragma clang diagnostic pop
#endif

void CreateDefaultLogger() {
  if (spdlog::get("main")) {
    if (!logger) {
      logger = spdlog::get("main");
    }
    return;
  }
  std::vector<spdlog::sink_ptr> sinks;
#ifdef WIN32
  sinks.push_back(std::make_shared<spdlog::sinks::wincolor_stdout_sink_mt>());
#else
  sinks.push_back(std::make_shared<spdlog::sinks::ansicolor_stdout_sink_mt>());
#endif
  sinks.push_back(
      std::make_shared<spdlog::sinks::simple_file_sink_mt>("log.txt"));
  logger = std::make_shared<spdlog::logger>("main", sinks.begin(), sinks.end());
  logger->set_pattern("[%T][%l] %v");
  spdlog::register_logger(logger);
}

void DestroyLogger() { logger.reset(); }
}  // namespace phx

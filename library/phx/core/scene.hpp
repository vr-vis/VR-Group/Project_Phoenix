//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef LIBRARY_PHX_CORE_SCENE_HPP_
#define LIBRARY_PHX_CORE_SCENE_HPP_

#include <cstddef>

#include <algorithm>
#include <memory>
#include <string>
#include <tuple>
#include <utility>
#include <vector>

#include "phx/core/component_check.hpp"
#include "phx/core/entity.hpp"
#include "phx/utility/aspects/loggable.hpp"
#include "phx/utility/aspects/nameable.hpp"
#include "phx/export.hpp"

namespace phx {

class Engine;

class PHOENIX_EXPORT Scene : public Nameable, public Loggable {
  friend class Engine;

 public:
  Scene();
  Scene(const Scene&) = delete;
  Scene(Scene&&) = default;
  ~Scene() override = default;

  Scene& operator=(const Scene&) = delete;
  Scene& operator=(Scene&&) = default;

  Entity* CreateEntity();

  void RemoveEntity(Entity* entity);

  std::size_t GetNumberOfEntities() const;

  std::vector<Entity*> GetEntities() const;
  template <typename... Components>
  std::vector<Entity*> GetEntitiesWithComponents() const;

  template <typename Component>
  std::vector<Component*> GetFirstComponents() const;
  template <typename... Components>
  std::vector<std::tuple<Components*...>> GetFirstComponentsMany() const;

  std::string ToString() const override;

  // Access the engine currently working on this scene.
  // This is nullptr if the scene is not rendered by any engine right now.
  Engine* GetEngine() const;

 private:
  std::vector<std::unique_ptr<Entity>> entities_;
  Engine* engine_ = nullptr;
};

template <typename... Components>
std::vector<Entity*> Scene::GetEntitiesWithComponents() const {
  std::vector<Entity*> filteredEntities;
  const auto& entities = GetEntities();
  filteredEntities.reserve(entities.size());
  std::for_each(entities.begin(), entities.end(),
                [&filteredEntities](Entity* entity) {
                  if (ComponentCheck<Components...>::Check(entity))
                    filteredEntities.push_back(entity);
                });
  filteredEntities.shrink_to_fit();
  return filteredEntities;
}

template <typename Component>
std::vector<Component*> Scene::GetFirstComponents() const {
  std::vector<Component*> components;
  const auto& entities = GetEntities();
  components.reserve(entities.size());
  std::for_each(entities.begin(), entities.end(),
                [&components](Entity* entity) {
                  Component* component = entity->GetFirstComponent<Component>();
                  if (component != nullptr) components.push_back(component);
                });
  components.shrink_to_fit();
  return components;
}
template <typename... Components>
std::vector<std::tuple<Components*...>> Scene::GetFirstComponentsMany() const {
  std::vector<std::tuple<Components*...>> components;
  const auto& entities = GetEntities();
  components.reserve(entities.size());
  std::for_each(entities.begin(), entities.end(),
                [&components](Entity* entity) {
                  auto gatheredComponents =
                      ComponentGather<Components...>::Gather(entity);
                  if (gatheredComponents != boost::none)
                    components.push_back(*gatheredComponents);
                });
  components.shrink_to_fit();
  return components;
}

}  // namespace phx

#endif  // LIBRARY_PHX_CORE_SCENE_HPP_

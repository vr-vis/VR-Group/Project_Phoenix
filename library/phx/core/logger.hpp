//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef LIBRARY_PHX_CORE_LOGGER_HPP_
#define LIBRARY_PHX_CORE_LOGGER_HPP_

#include <memory>
#include <sstream>
#include <utility>

#include "spdlog/fmt/ostr.h"
#include "spdlog/logger.h"

#include "phx/export.hpp"
#include "phx/utility/stream_helpers.hpp"

namespace phx {

PHOENIX_EXPORT void CreateDefaultLogger();
PHOENIX_EXPORT void DestroyLogger();

extern std::shared_ptr<spdlog::logger> logger;

#define PHX_CONVENIENCE_LOGGER_FUNCTIONS(stream_name)                    \
  template <typename... Args>                                            \
  inline PHOENIX_EXPORT void stream_name(const char* fmt,                \
                                         const Args&... args) {          \
    if (!logger) CreateDefaultLogger();                                  \
    logger->stream_name(std::forward<const char*>(fmt),                  \
                        std::forward<const Args&>(args)...);             \
  }                                                                      \
  template <typename... Args>                                            \
  PHOENIX_EXPORT void stream_name##_if(const bool flag, const char* fmt, \
                                       const Args&... args) {            \
    if (flag)                                                            \
      stream_name(std::forward<const char*>(fmt),                        \
                  std::forward<const Args&>(args)...);                   \
  }

PHX_CONVENIENCE_LOGGER_FUNCTIONS(trace)
PHX_CONVENIENCE_LOGGER_FUNCTIONS(debug)
PHX_CONVENIENCE_LOGGER_FUNCTIONS(info)
PHX_CONVENIENCE_LOGGER_FUNCTIONS(warn)
PHX_CONVENIENCE_LOGGER_FUNCTIONS(error)
PHX_CONVENIENCE_LOGGER_FUNCTIONS(critical)

#undef PHX_CONVENIENCE_LOGGER_FUNCTIONS

}  // namespace phx

#endif  // LIBRARY_PHX_CORE_LOGGER_HPP_

//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef LIBRARY_PHX_CORE_COMPONENT_HPP_
#define LIBRARY_PHX_CORE_COMPONENT_HPP_

#include <ostream>
#include <string>

#include "phx/utility/aspects/loggable.hpp"
#include "phx/export.hpp"

namespace phx {

class Entity;

class PHOENIX_EXPORT Component : public Loggable {
 public:
  // Entity should be the only one to construct Components
  // has to be befriended to all derived Components, since friendship is not
  // inheritable
  friend class Entity;
  virtual ~Component() = default;

  Entity* GetEntity() const;

 protected:
  Component() = default;
  Component(const Component&) = delete;
  Component(Component&&) = default;

  Component& operator=(const Component&) = delete;
  Component& operator=(Component&&) = default;

 private:
  Entity* entity_ = nullptr;
};

}  // namespace phx

#endif  // LIBRARY_PHX_CORE_COMPONENT_HPP_

//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include "phx/core/engine.hpp"

#include <iostream>
#include <memory>
#include <string>
#include <utility>
#include <vector>

#include "phx/core/logger.hpp"
#include "phx/core/scene.hpp"

namespace phx {

Engine::Engine() {
  static bool first_engine = true;
  if (!first_engine) {
    warn(
        "WARNING: Only 1 engine should be used. Make sure you know what you "
        "are doing if you want to use more!!!!");
  } else {
    first_engine = false;
  }
}

Engine::~Engine() {
  // if a scene is still attached to this engine, detach it
  if (scene_ != nullptr) scene_->engine_ = nullptr;
  DestroyLogger();
}

void Engine::Run() {
  if (is_running_) {
    warn("Unable to run. Already running.");
    return;
  }

  frame_timer_.Start();

  is_running_ = true;
  while (is_running_) UpdateSystems();
}

void Engine::Stop() { is_running_ = false; }

bool Engine::IsRunning() const { return is_running_; }

void Engine::UpdateSystems() {
  // before updating all systems, update the frame timer
  frame_timer_.Tick();
  for (auto& system : systems_)
    if (system->GetEnabled()) system->Update(frame_timer_.GetTimeInfo());
}

std::string Engine::ToString() const {
  return "(Engine, #Systems: " + std::to_string(systems_.size()) +
         ", running: " + (is_running_ ? "true" : "false") + ")";
}

const phx::FrameTimer& Engine::GetFrameTimer() { return frame_timer_; }

std::vector<Entity*> Engine::GetEntities() const {
  if (!scene_) return std::vector<Entity*>();
  return scene_->GetEntities();
}

std::shared_ptr<Scene> Engine::GetScene() const { return scene_; }

void Engine::SetScene(std::shared_ptr<Scene> new_scene) {
  // detach from current scene
  if (scene_ != nullptr) {
    scene_->engine_ = nullptr;
  }
  scene_changed_signal_(scene_, new_scene);
  // attach to new scene
  scene_ = new_scene;
  scene_->engine_ = this;
}

boost::signals2::connection Engine::AddSceneChangedCallback(
    const std::function<void(std::shared_ptr<Scene>, std::shared_ptr<Scene>)>&
        callback) {
  return scene_changed_signal_.connect(callback);
}

void Engine::MoveSystemToFront(System* system) { systems_.MoveToFront(system); }

void Engine::MoveSystemToBack(System* system) { systems_.MoveToBack(system); }

void Engine::MoveSystemAfter(System* system, System* after) {
  systems_.MoveAfter(system, after);
}

void Engine::MoveSystemBefore(System* system, System* before) {
  systems_.MoveBefore(system, before);
}

}  // namespace phx

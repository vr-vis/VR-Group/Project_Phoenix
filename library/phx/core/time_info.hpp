//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef LIBRARY_PHX_CORE_TIME_INFO_HPP_
#define LIBRARY_PHX_CORE_TIME_INFO_HPP_

#include <cstddef>

#include "phx/export.hpp"

namespace phx {
template <typename Unit, typename TimePoint>
struct PHOENIX_EXPORT TimeInfo {
  TimePoint startup_time;
  TimePoint frame_start_time;
  Unit time_since_startup;
  Unit time_since_last_frame;
  std::size_t frame_count = 0;
};
}  // namespace phx

#endif  // LIBRARY_PHX_CORE_TIME_INFO_HPP_

//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef LIBRARY_PHX_SETUP_HPP_
#define LIBRARY_PHX_SETUP_HPP_

#include <memory>

#include "phx/core/engine.hpp"
#include "phx/core/scene.hpp"
#include "phx/display/window.hpp"
#include "phx/rendering/frame_graph.hpp"
#include "phx/rendering/rendering_system.hpp"
#include "phx/export.hpp"

namespace phx {

class PHOENIX_EXPORT Setup {
 public:
  Setup() = delete;

  // sets up the engine with the most default setup
  // i.e.: RenderingSystem and an InputSystem
  // also creates an empty scene for the engine
  static std::unique_ptr<Engine> CreateDefaultEngine(
      bool use_hmd_if_available = true);

  static void SetupDefaultFrameGraphWindow(RenderingSystem* rendering_system,
                                           Engine* engine);
  static void SetupDefaultFrameGraphOpenVR(RenderingSystem* rendering_system,
                                           Engine* engine);
};

}  // namespace phx

#endif  // LIBRARY_PHX_SETUP_HPP_

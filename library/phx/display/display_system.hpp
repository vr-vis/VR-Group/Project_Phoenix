//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef LIBRARY_PHX_DISPLAY_DISPLAY_SYSTEM_HPP_
#define LIBRARY_PHX_DISPLAY_DISPLAY_SYSTEM_HPP_

#include <memory>
#include <vector>

#include "phx/rendering/backend/render_target.hpp"
#include "phx/core/engine.hpp"
#include "phx/core/system.hpp"
#include "phx/export.hpp"

namespace phx {

class PHOENIX_EXPORT DisplaySystem : public System {
 public:
  DisplaySystem(const DisplaySystem&) = delete;
  DisplaySystem(DisplaySystem&&) = default;
  ~DisplaySystem() override;

  DisplaySystem& operator=(const DisplaySystem&) = delete;
  DisplaySystem& operator=(DisplaySystem&&) = default;

  void Update(const FrameTimer::TimeInfo&) override = 0;

 protected:
  explicit DisplaySystem(Engine* engine);
  friend DisplaySystem* Engine::CreateSystem<DisplaySystem>();
};

}  // namespace phx

#endif  // LIBRARY_PHX_DISPLAY_DISPLAY_SYSTEM_HPP_

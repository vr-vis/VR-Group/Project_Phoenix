//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include "phx/display/display_system_openvr.hpp"

#include <algorithm>
#include <memory>
#include <stdexcept>
#include <string>
#include <utility>
#include <vector>

#include "phx/core/engine.hpp"
#include "phx/core/logger.hpp"
#include "phx/core/runtime_component.hpp"
#include "phx/core/scene.hpp"
#include "phx/input/device_system.hpp"
#include "phx/rendering/rendering_system.hpp"

#undef CreateWindow

namespace phx {
DisplaySystemOpenVR::DisplaySystemOpenVR(Engine* engine)
    : DisplaySystem(engine),
      left_render_target_(nullptr),
      right_render_target_(nullptr) {
  scene_changed_connection_ = engine->AddSceneChangedCallback(
      [this](std::shared_ptr<Scene>, std::shared_ptr<Scene> new_scene) {
        RemoveRenderTargets();
        CreateRenderTargets(new_scene.get());
      });
}
DisplaySystemOpenVR::~DisplaySystemOpenVR() {}

phx::HMD* DisplaySystemOpenVR::GetHMD() {
  auto hmds = engine_->GetSystem<DeviceSystem>()->GetDevices<HMD>();
  if (hmds.size() == 0) return nullptr;
  return hmds[0];
}

void DisplaySystemOpenVR::Update(const FrameTimer::TimeInfo&) {
  HMD* hmd = GetHMD();
  if (hmd != nullptr) {
    if (left_render_target_ != nullptr && right_render_target_ != nullptr) {
      auto right_texture = right_render_target_->GetColorTexture();
      hmd->Submit(HMD::RIGHT_EYE, right_texture);
      auto left_texture = left_render_target_->GetColorTexture();
      hmd->Submit(HMD::LEFT_EYE, left_texture);
    }
  }
}

void DisplaySystemOpenVR::CreateRenderTargets(Scene* scene) {
  if (GetHMD() == nullptr) {
    error("Cannot create render targets: no HMD.");
    return;
  }
  auto left_eye_components =
      scene->GetEntitiesWithComponents<RuntimeComponent<LEFT_EYE>>();
  if (!left_eye_components.empty()) {
    left_render_target_ = left_eye_components[0]->AddComponent<RenderTarget>(
        GetHMD()->GetViewportSize());
  }
  auto right_eye_components =
      scene->GetEntitiesWithComponents<RuntimeComponent<RIGHT_EYE>>();
  if (!right_eye_components.empty()) {
    right_render_target_ = right_eye_components[0]->AddComponent<RenderTarget>(
        GetHMD()->GetViewportSize());
  }
  SetEyeProjections(scene);
}

void DisplaySystemOpenVR::RemoveRenderTargets() {
  if (left_render_target_)
    left_render_target_->GetEntity()->RemoveComponent(left_render_target_);
  if (right_render_target_)
    right_render_target_->GetEntity()->RemoveComponent(right_render_target_);
}

void DisplaySystemOpenVR::SetEyeProjections(Scene* scene) {
  if (GetHMD() == nullptr) {
    error("Cannot set eye projections: no HMD.");
    return;
  }
  auto left_eye_components =
      scene->GetEntitiesWithComponents<RuntimeComponent<LEFT_EYE>>();
  if (!left_eye_components.empty()) {
    left_eye_components[0]->GetFirstComponent<Projection>()->SetMatrix(
        GetHMD()->GetProjectionMatrix(HMD::LEFT_EYE));
  }
  auto right_eye_components =
      scene->GetEntitiesWithComponents<RuntimeComponent<RIGHT_EYE>>();
  if (!right_eye_components.empty()) {
    right_eye_components[0]->GetFirstComponent<Projection>()->SetMatrix(
        GetHMD()->GetProjectionMatrix(HMD::RIGHT_EYE));
  }
}

}  // namespace phx

//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef LIBRARY_PHX_DISPLAY_DISPLAY_SYSTEM_WINDOW_HPP_
#define LIBRARY_PHX_DISPLAY_DISPLAY_SYSTEM_WINDOW_HPP_

#include <memory>
#include <vector>

#include "phx/core/scene.hpp"
#include "phx/display/display_system.hpp"
#include "phx/display/window.hpp"
#include "phx/rendering/backend/render_target.hpp"
#include "phx/export.hpp"

#undef CreateWindow

namespace phx {

class PHOENIX_EXPORT DisplaySystemWindow : public DisplaySystem {
 public:
  explicit DisplaySystemWindow(Engine* engine);
  DisplaySystemWindow(const DisplaySystemWindow&) = delete;
  DisplaySystemWindow(DisplaySystemWindow&&) = default;
  ~DisplaySystemWindow() override;

  DisplaySystemWindow& operator=(const DisplaySystemWindow&) = delete;
  DisplaySystemWindow& operator=(DisplaySystemWindow&&) = default;

  template <typename... Arguments>
  Window* CreateWindow(Arguments&&... arguments);
  void DestroyWindow();
  Window* GetWindow();

  void Update(const FrameTimer::TimeInfo&) override;

  // field_of_view anglie in degree in the y direction,
  // near_plane, far_plane distance in meters
  void CreateRenderTarget(Scene* scene, float field_of_view, float near_plane,
                          float far_plane);

 protected:
  std::unique_ptr<Window> window_;
};

template <typename... Arguments>
Window* DisplaySystemWindow::CreateWindow(Arguments&&... arguments) {
  // Do not use make_unique due to the friendship to Window.
  window_ = std::unique_ptr<Window>(new Window(arguments...));
  return window_.get();
}
}  // namespace phx

#endif  // LIBRARY_PHX_DISPLAY_DISPLAY_SYSTEM_WINDOW_HPP_

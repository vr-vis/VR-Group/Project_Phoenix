//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef LIBRARY_PHX_DISPLAY_HMD_HPP_
#define LIBRARY_PHX_DISPLAY_HMD_HPP_

#include <memory>
#include <vector>

#include "phx/suppress_warnings.hpp"

SUPPRESS_WARNINGS_BEGIN
#include "glm/mat4x4.hpp"
#include "glm/vec2.hpp"

#include "openvr.h"  //NOLINT
SUPPRESS_WARNINGS_END

#include "phx/export.hpp"
#include "phx/input/tracked_device.hpp"
#include "phx/resources/types/material.hpp"
#include "phx/resources/types/mesh.hpp"

#include "gl/texture.hpp"

namespace phx {

class PHOENIX_EXPORT HMD : public TrackedDevice {
 public:
  HMD();
  ~HMD() = default;

  HMD(const HMD&) = delete;
  HMD(HMD&&) = default;

  HMD& operator=(const HMD&) = delete;
  HMD& operator=(HMD&&) = default;

  enum Side {
    RIGHT_EYE = vr::EVREye::Eye_Right,
    LEFT_EYE = vr::EVREye::Eye_Left
  };

  void Update() override;
  void OnOpenVREvent(const vr::VREvent_t& event) override;

  const glm::uvec2& GetViewportSize() const;

  const glm::mat4& GetProjectionMatrix(Side side) const;
  const glm::mat4& GetEyeToHeadMatrix(Side side) const;

  static bool IsHMDPresent();

  void Submit(Side side, gl::texture_2d* texture);

  void UpdateDeviceIndex() override;

 private:
  glm::mat4 GetProjectionMatrixFromOpenVR(const vr::Hmd_Eye eye);
  glm::mat4 GetEyeToHeadMatrixFromOpenVR(const vr::Hmd_Eye eye);
  void UpdateEyeToHeadMatrices();

  glm::uvec2 viewport_size_;

  glm::mat4 projection_right_;
  glm::mat4 projection_left_;
  glm::mat4 eye_to_head_right_;
  glm::mat4 eye_to_head_left_;

  glm::mat4 last_head_transformation_;
};

}  // namespace phx

#endif  // LIBRARY_PHX_DISPLAY_HMD_HPP_

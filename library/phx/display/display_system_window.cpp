//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include "phx/display/display_system_window.hpp"

#include <algorithm>
#include <memory>
#include <stdexcept>
#include <string>
#include <utility>
#include <vector>

#include "SDL2/SDL_video.h"

#include "phx/core/scene.hpp"
#include "phx/core/logger.hpp"
#include "phx/core/runtime_component.hpp"
#include "phx/rendering/rendering_system.hpp"

#undef CreateWindow

namespace phx {
DisplaySystemWindow::DisplaySystemWindow(Engine* engine)
    : DisplaySystem(engine) {
  if (SDL_VideoInit(nullptr) != 0)
    throw std::runtime_error(
        "Unable to initialize SDL video subsystem. Error: " +
        std::string(SDL_GetError()));
}
DisplaySystemWindow::~DisplaySystemWindow() { SDL_VideoQuit(); }

void DisplaySystemWindow::DestroyWindow() { window_.reset(); }

Window* DisplaySystemWindow::GetWindow() { return window_.get(); }

void DisplaySystemWindow::Update(const FrameTimer::TimeInfo&) {
  if (window_ != nullptr) {
    window_->Swap();
  }
}

void DisplaySystemWindow::CreateRenderTarget(Scene* scene, float field_of_view,
                                             float near_plane,
                                             float far_plane) {
  Entity* platform =
      scene->GetEntitiesWithComponents<RuntimeComponent<USER_PLATFORM>>()[0];
  Entity* camera = scene->CreateEntity();
  camera->AddComponent<Transform>()->SetParent(
      platform->GetFirstComponent<Transform>());
  auto window_size = window_->GetSize();
  camera->AddComponent<Projection>()->SetPerspective(
      glm::radians(field_of_view),
      static_cast<float>(window_size[0]) / static_cast<float>(window_size[1]),
      near_plane, far_plane);
  camera->AddComponent<RenderTarget>(window_size);
}

}  // namespace phx

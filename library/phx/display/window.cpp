//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include "phx/display/window.hpp"

#include <stdexcept>
#include <string>

namespace phx {
Window::Window(const std::string& title)
    : native_(SDL_CreateWindow(
          title.c_str(), 0, 0, 1, 1,
          static_cast<std::uint32_t>(SetupOpenGLContextFlags() |
                                     SDL_WINDOW_FULLSCREEN_DESKTOP))),
      gl_context_(SDL_GL_CreateContext(native_)) {
  Validate();
}
Window::Window(const std::string& title, const glm::uvec2& position,
               const glm::uvec2& size, bool hidden)
    : native_(SDL_CreateWindow(
          title.c_str(), static_cast<int>(position.x),
          static_cast<int>(position.y), static_cast<int>(size.x),
          static_cast<int>(size.y),
          SetupOpenGLContextFlags() | (hidden ? SDL_WINDOW_HIDDEN : 0))),
      gl_context_(SDL_GL_CreateContext(native_)) {
  Validate();
}

Window::~Window() {
  SDL_GL_DeleteContext(gl_context_);
  SDL_DestroyWindow(native_);
}

void Window::Swap() const { SDL_GL_SwapWindow(native_); }

glm::uvec2 Window::GetSize() const {
  int w, h;
  SDL_GetWindowSize(native_, &w, &h);
  return glm::uvec2(static_cast<unsigned>(w), static_cast<unsigned>(h));
}

void Window::SetVisible(bool visible) {
  if (visible) {
    SDL_ShowWindow(native_);
  } else {
    SDL_HideWindow(native_);
  }
}

std::uint32_t Window::SetupOpenGLContextFlags() const {
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 5);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
  return SDL_WINDOW_OPENGL;
}

void Window::Validate() const {
  if (!native_)
    throw std::runtime_error("Failed to create SDL window: " +
                             std::string(SDL_GetError()));
  if (!gl_context_)
    throw std::runtime_error("Failed to create SDL OpenGL context: " +
                             std::string(SDL_GetError()));
  SDL_GL_SetSwapInterval(0);
}
}  // namespace phx

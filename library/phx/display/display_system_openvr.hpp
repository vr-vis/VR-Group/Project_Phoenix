//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef LIBRARY_PHX_DISPLAY_DISPLAY_SYSTEM_OPENVR_HPP_
#define LIBRARY_PHX_DISPLAY_DISPLAY_SYSTEM_OPENVR_HPP_

#include <memory>
#include <vector>

#include "phx/core/scene.hpp"
#include "phx/display/display_system.hpp"
#include "phx/display/hmd.hpp"
#include "phx/display/window.hpp"
#include "phx/export.hpp"
#include "phx/rendering/backend/render_target.hpp"

namespace phx {

class PHOENIX_EXPORT DisplaySystemOpenVR : public DisplaySystem {
 public:
  explicit DisplaySystemOpenVR(Engine* engine);
  DisplaySystemOpenVR(const DisplaySystemOpenVR&) = delete;
  DisplaySystemOpenVR(DisplaySystemOpenVR&&) = default;
  ~DisplaySystemOpenVR() override;

  DisplaySystemOpenVR& operator=(const DisplaySystemOpenVR&) = delete;
  DisplaySystemOpenVR& operator=(DisplaySystemOpenVR&&) = default;

  HMD* GetHMD();

  void Update(const FrameTimer::TimeInfo&) override;

  void CreateRenderTargets(Scene* scene);

 private:
  void RemoveRenderTargets();
  void SetEyeProjections(Scene* scene);

  boost::signals2::connection scene_changed_connection_;

  RenderTarget* left_render_target_;
  RenderTarget* right_render_target_;
};

}  // namespace phx

#endif  // LIBRARY_PHX_DISPLAY_DISPLAY_SYSTEM_OPENVR_HPP_

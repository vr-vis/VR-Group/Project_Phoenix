//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef LIBRARY_PHX_DISPLAY_WINDOW_HPP_
#define LIBRARY_PHX_DISPLAY_WINDOW_HPP_

#include <string>

#include "phx/suppress_warnings.hpp"

SUPPRESS_WARNINGS_BEGIN
#include "SDL.h"
#include "glm/vec2.hpp"
SUPPRESS_WARNINGS_END

#include "phx/export.hpp"

namespace phx {

class DisplaySystemWindow;

class PHOENIX_EXPORT Window {
 public:
  Window(const Window&) = delete;
  Window(Window&&) = default;
  virtual ~Window();

  Window& operator=(const Window&) = delete;
  Window& operator=(Window&&) = default;

  virtual void Swap() const;

  glm::uvec2 GetSize() const;

  void SetVisible(bool visible);

 protected:
  friend DisplaySystemWindow;

  explicit Window(const std::string& title);
  Window(const std::string& title, const glm::uvec2& position,
         const glm::uvec2& size = glm::uvec2(1024, 768), bool hidden = false);

  std::uint32_t SetupOpenGLContextFlags() const;
  void Validate() const;

  SDL_Window* native_;
  SDL_GLContext gl_context_;
};

}  // namespace phx

#endif  // LIBRARY_PHX_DISPLAY_WINDOW_HPP_

//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include "phx/display/hmd.hpp"

#include <algorithm>
#include <array>
#include <memory>
#include <string>
#include <utility>
#include <vector>

#include "phx/suppress_warnings.hpp"

SUPPRESS_WARNINGS_BEGIN
#include "glm/glm.hpp"
SUPPRESS_WARNINGS_END

#include "openvr.h"  //NOLINT

#include "gl/texture.hpp"

#include "phx/core/logger.hpp"
#include "phx/resources/resource_manager.hpp"

namespace phx {
HMD::HMD() : TrackedDevice() {
  id_ = vr::k_unTrackedDeviceIndex_Hmd;

  uint32_t x, y;
  vr_system_->GetRecommendedRenderTargetSize(&x, &y);
  viewport_size_ = glm::uvec2(x, y);

  projection_right_ = GetProjectionMatrixFromOpenVR(vr::Hmd_Eye::Eye_Right);
  projection_left_ = GetProjectionMatrixFromOpenVR(vr::Hmd_Eye::Eye_Left);

  UpdateEyeToHeadMatrices();
}

bool HMD::IsHMDPresent() { return vr::VR_IsHmdPresent(); }

void HMD::Submit(Side side, gl::texture_2d* texture) {
  vr::Texture_t vr_texture = {
      reinterpret_cast<void*>(static_cast<uintptr_t>(texture->id())),
      vr::TextureType_OpenGL, vr::ColorSpace_Gamma};
  vr::VRCompositor()->Submit(static_cast<vr::EVREye>(side), &vr_texture);
}

void HMD::UpdateDeviceIndex() { id_ = vr::k_unTrackedDeviceIndex_Hmd; }

const glm::uvec2& HMD::GetViewportSize() const { return viewport_size_; }

const glm::mat4& HMD::GetProjectionMatrix(Side side) const {
  if (side == RIGHT_EYE)
    return projection_right_;
  else
    return projection_left_;
}

const glm::mat4& HMD::GetEyeToHeadMatrix(Side side) const {
  if (side == RIGHT_EYE)
    return eye_to_head_right_;
  else
    return eye_to_head_left_;
}

void HMD::Update() {
  last_wait_get_poses_.resize(vr::k_unMaxTrackedDeviceCount);

  vr::VRCompositor()->WaitGetPoses(&last_wait_get_poses_[0],
                                   vr::k_unMaxTrackedDeviceCount, NULL, 0);
  if (last_wait_get_poses_[vr::k_unTrackedDeviceIndex_Hmd].bPoseIsValid) {
    pose_ = TransformToGlmMatrix(
        last_wait_get_poses_[vr::k_unTrackedDeviceIndex_Hmd]
            .mDeviceToAbsoluteTracking);
  } else {
    debug("[HMD] HMD pose is invalid, use the last valid one");
  }

  TrackedDevice::Update();
}

void HMD::OnOpenVREvent(const vr::VREvent_t& event) {
  if (event.eventType == vr::VREvent_IpdChanged) {
    UpdateEyeToHeadMatrices();
  }
}

glm::mat4 HMD::GetProjectionMatrixFromOpenVR(const vr::Hmd_Eye eye) {
  const vr::HmdMatrix44_t steamvr_proj_matrix =
      vr_system_->GetProjectionMatrix(eye, 0.01f, 1000.f);

  return TransformToGlmMatrix(steamvr_proj_matrix);
}

glm::mat4 HMD::GetEyeToHeadMatrixFromOpenVR(const vr::Hmd_Eye eye) {
  const vr::HmdMatrix34_t steamvr_eye_head_matrix =
      vr_system_->GetEyeToHeadTransform(eye);

  return TransformToGlmMatrix(steamvr_eye_head_matrix);
}

void HMD::UpdateEyeToHeadMatrices() {
  eye_to_head_right_ = GetEyeToHeadMatrixFromOpenVR(vr::Hmd_Eye::Eye_Right);
  eye_to_head_left_ = GetEyeToHeadMatrixFromOpenVR(vr::Hmd_Eye::Eye_Left);
}

}  // namespace phx

//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include "phx/resources/types/mesh.hpp"

#include <string>
#include <utility>
#include <vector>

#include "glm/vec2.hpp"
#include "glm/vec3.hpp"

namespace phx {

const char Mesh::UNNAMED[] = "UnnamedMesh";

void Mesh::SetVertices(std::vector<glm::vec3>&& vertices) {
  vertices_ = std::move(vertices);
}

const std::vector<glm::vec3>& Mesh::GetVertices() const { return vertices_; }

std::size_t Mesh::GetNumberOfVertices() const { return vertices_.size(); }

void Mesh::SetNormals(std::vector<glm::vec3>&& normals) {
  normals_ = std::move(normals);
}

const std::vector<glm::vec3>& Mesh::GetNormals() const { return normals_; }

void Mesh::SetTangents(std::vector<glm::vec3>&& tangents) {
  tangents_ = std::move(tangents);
}
const std::vector<glm::vec3>& Mesh::GetTangents() const { return tangents_; }

void Mesh::SetBitangents(std::vector<glm::vec3>&& bitangents) {
  bitangents_ = std::move(bitangents);
}

const std::vector<glm::vec3>& Mesh::GetBitangents() const {
  return bitangents_;
}

void Mesh::SetTextureCoords(std::vector<glm::vec2>&& tcoords) {
  texture_coords_ = std::move(tcoords);
}

const std::vector<glm::vec2>& Mesh::GetTextureCoords() const {
  return texture_coords_;
}

void Mesh::SetIndices(std::vector<unsigned int>&& indices) {
  indices_ = std::move(indices);
}

const std::vector<unsigned int>& Mesh::GetIndices() const { return indices_; }

void Mesh::SetBoundingBox(const std::array<glm::vec3, 2>& boundingbox) {
  boundingbox_ = std::move(boundingbox);
}

const std::array<glm::vec3, 2>& Mesh::GetBoundingBox() const {
  return boundingbox_;
}

Mesh Mesh::GenerateBoundingBoxMesh() {
  Mesh boundingbox;

  boundingbox.SetVertices(
      {glm::vec3{boundingbox_[0][0], boundingbox_[0][1], boundingbox_[0][2]},
       glm::vec3{boundingbox_[0][0], boundingbox_[1][1], boundingbox_[0][2]},
       glm::vec3{boundingbox_[0][0], boundingbox_[0][1], boundingbox_[1][2]},
       glm::vec3{boundingbox_[0][0], boundingbox_[1][1], boundingbox_[1][2]},
       glm::vec3{boundingbox_[1][0], boundingbox_[1][1], boundingbox_[1][2]},
       glm::vec3{boundingbox_[1][0], boundingbox_[0][1], boundingbox_[0][2]},
       glm::vec3{boundingbox_[1][0], boundingbox_[1][1], boundingbox_[0][2]},
       glm::vec3{boundingbox_[1][0], boundingbox_[0][1], boundingbox_[1][2]}});
  boundingbox.SetIndices({
      0,
  });
  return boundingbox;
}

}  // namespace phx

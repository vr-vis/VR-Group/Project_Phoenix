//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef LIBRARY_PHX_RESOURCES_TYPES_MESH_HPP_
#define LIBRARY_PHX_RESOURCES_TYPES_MESH_HPP_

#include <array>
#include <string>
#include <vector>

#include "phx/suppress_warnings.hpp"

SUPPRESS_WARNINGS_BEGIN
#include "glm/glm.hpp"
SUPPRESS_WARNINGS_END

#include "phx/resources/resource.hpp"
#include "phx/utility/aspects/nameable.hpp"
#include "phx/export.hpp"

namespace phx {

class PHOENIX_EXPORT Mesh : public Resource, public Nameable {
 public:
  static const char UNNAMED[];

  Mesh() : Nameable(UNNAMED) {}
  Mesh(const Mesh &) = default;
  Mesh(Mesh &&) = default;

  ~Mesh() override = default;

  Mesh &operator=(const Mesh &) = default;
  Mesh &operator=(Mesh &&) = default;

  void SetVertices(std::vector<glm::vec3> &&vertices);
  const std::vector<glm::vec3> &GetVertices() const;
  std::size_t GetNumberOfVertices() const;

  void SetNormals(std::vector<glm::vec3> &&normals);
  const std::vector<glm::vec3> &GetNormals() const;

  void SetTangents(std::vector<glm::vec3> &&tangents);
  const std::vector<glm::vec3> &GetTangents() const;

  void SetBitangents(std::vector<glm::vec3> &&bitangents);
  const std::vector<glm::vec3> &GetBitangents() const;

  void SetTextureCoords(std::vector<glm::vec2> &&tcoords);
  const std::vector<glm::vec2> &GetTextureCoords() const;

  void SetIndices(std::vector<unsigned int> &&indices);
  const std::vector<unsigned int> &GetIndices() const;

  void SetBoundingBox(const std::array<glm::vec3, 2> &boundingbox);
  const std::array<glm::vec3, 2> &GetBoundingBox() const;

  Mesh GenerateBoundingBoxMesh();

 private:
  std::vector<glm::vec3> vertices_;
  std::vector<glm::vec3> normals_;
  std::vector<glm::vec3> tangents_;
  std::vector<glm::vec3> bitangents_;
  std::vector<glm::vec2> texture_coords_;
  std::vector<unsigned int> indices_;
  std::array<glm::vec3, 2> boundingbox_;
};

}  // namespace phx

#endif  // LIBRARY_PHX_RESOURCES_TYPES_MESH_HPP_

//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef LIBRARY_PHX_RESOURCES_TYPES_IMAGE_HPP_
#define LIBRARY_PHX_RESOURCES_TYPES_IMAGE_HPP_

#ifdef _WIN32
#define NOMINMAX
#include <windows.h>
#endif  // _WIN32

#include <array>
#include <cstddef>
#include <cstdint>
#include <mutex>
#include <stdexcept>
#include <string>
#include <utility>

#include "FreeImage.h"

#include "phx/resources/resource.hpp"
#include "phx/utility/aspects/loggable.hpp"
#include "phx/export.hpp"

namespace phx {
class PHOENIX_EXPORT Image : public Resource, public Loggable {
 public:
  explicit Image(const std::array<std::size_t, 2>& dimensions,
                 const std::size_t bits_per_pixel = 8);
  template <typename ColorType>
  explicit Image(const ColorType& color,
                 const std::array<std::size_t, 2>& dimensions,
                 const std::size_t bits_per_pixel = 8) {
    Initialize();
    native_ = FreeImage_AllocateExT(FIT_BITMAP,
                                    static_cast<std::int32_t>(dimensions[0]),
                                    static_cast<std::int32_t>(dimensions[1]),
                                    static_cast<std::int32_t>(bits_per_pixel),
                                    &color[0], 0, nullptr, 0, 0, 0);
    if (!native_) {
      throw std::runtime_error("FreeImage_AllocateExT failed.");
    }
  }
  template <typename DataType>
  explicit Image(DataType* data, const std::array<std::size_t, 2>& dimensions,
                 const std::size_t bits_per_pixel = 8) {
    Initialize();
    native_ = FreeImage_ConvertFromRawBitsEx(
        true, reinterpret_cast<std::uint8_t*>(data), FIT_BITMAP,
        static_cast<std::int32_t>(dimensions[0]),
        static_cast<std::int32_t>(dimensions[1]),
        static_cast<std::int32_t>((bits_per_pixel * dimensions[0] + 31) / 32 *
                                  4),
        static_cast<std::uint32_t>(bits_per_pixel), FI_RGBA_RED_MASK,
        FI_RGBA_GREEN_MASK, FI_RGBA_BLUE_MASK, false);
    if (!native_) {
      throw std::runtime_error("FreeImage_ConvertFromRawBitsEx failed.");
    }
  }
  explicit Image(const std::string& filepath,
                 const std::int32_t native_flags = 0);
  Image(const Image& that);
  Image(Image&& temp) noexcept;
  ~Image() override;
  Image& operator=(const Image& that);
  Image& operator=(Image&& temp) noexcept;

  bool Save(const std::string& filepath, const std::int32_t native_flags = 0);

  std::array<std::size_t, 2> GetDimensions() const;
  std::size_t GetPitch() const;
  std::size_t GetBitsPerPixel() const;

  bool IsEmpty() const;

  template <typename type = std::uint8_t>
  std::pair<type*, std::size_t> GetPixels() const {
    return {reinterpret_cast<type*>(FreeImage_GetBits(native_)),
            GetDimensions()[1] * GetPitch() / sizeof(type)};
  }

  void SetPixelColor(const std::array<std::size_t, 2>& position,
                     const std::array<std::uint8_t, 4>& color);
  std::array<std::uint8_t, 4> GetPixelColor(
      const std::array<std::size_t, 2>& position) const;

  void To4Bits();
  void To8Bits();
  void To16Bits();
  void To24Bits();
  void To32Bits();

  std::string ToString() const override;

 protected:
  static void Initialize();

  void Replace(FIBITMAP* native);
  bool SwapRedBlue(FIBITMAP* dib);

 private:
  FIBITMAP* native_;
  static std::once_flag once_flag_;
};  // namespace phx
}  // namespace phx

#endif  // LIBRARY_PHX_RESOURCES_TYPES_IMAGE_HPP_

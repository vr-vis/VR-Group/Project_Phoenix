//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef LIBRARY_PHX_RESOURCES_TYPES_MATERIAL_HPP_
#define LIBRARY_PHX_RESOURCES_TYPES_MATERIAL_HPP_

#include <memory>
#include <string>
#include <vector>

#include "phx/suppress_warnings.hpp"

SUPPRESS_WARNINGS_BEGIN
#include "gl/texture.hpp"
#include "gl/texture_handle.hpp"
#include "glm/vec3.hpp"
SUPPRESS_WARNINGS_END

#include "phx/export.hpp"
#include "phx/resources/resource.hpp"
#include "phx/resources/resource_pointer.hpp"
#include "phx/resources/types/image.hpp"
#include "phx/utility/aspects/nameable.hpp"

namespace phx {

// TODO(acd): Move render backend specific functionality out.
class PHOENIX_EXPORT Material : public Resource, public Nameable {
 public:
  static const char UNNAMED[];

  Material() : Nameable(UNNAMED) {}
  Material(const Material&) = default;
  Material(Material&&) = default;

  ~Material() override = default;

  Material& operator=(const Material&) = default;
  Material& operator=(Material&&) = default;

  glm::vec3 GetDiffuseColor() const;
  void SetDiffuseColor(glm::vec3 color);

  ResourcePointer<Image> GetDiffuseImage() const;
  void SetDiffuseImage(ResourcePointer<Image> image);
  gl::texture_2d* GetDiffuseTexture() const;

  glm::vec3 GetAmbientColor() const;
  void SetAmbientColor(glm::vec3 color);

  ResourcePointer<Image> GetAmbientImage() const;
  void SetAmbientImage(ResourcePointer<Image> image);
  gl::texture_2d* GetAmbientTexture() const;

  glm::vec3 GetSpecularColor() const;
  void SetSpecularColor(glm::vec3 color);

  ResourcePointer<Image> GetSpecularImage() const;
  void SetSpecularImage(ResourcePointer<Image> image);
  gl::texture_2d* GetSpecularTexture() const;

  float GetShininess() const;
  void SetShininess(float shininess);

  static const Material* GetDefault();

  // unless this was called Get..Texture return nullptr
  void UploadTextures();

 private:
  void SetTexture(ResourcePointer<Image> image,
                  std::shared_ptr<gl::texture_2d>* texture);
  void ResetTexture(std::shared_ptr<gl::texture_2d>* texture);

  ResourcePointer<Image> ambient_image_{nullptr};
  ResourcePointer<Image> diffuse_image_{nullptr};
  ResourcePointer<Image> specular_image_{nullptr};
  std::shared_ptr<gl::texture_2d> ambient_texture_ = nullptr;
  std::shared_ptr<gl::texture_2d> diffuse_texture_ = nullptr;
  std::shared_ptr<gl::texture_2d> specular_texture_ = nullptr;
  glm::vec3 ambient_color_ = glm::vec3(0, 0, 0);
  glm::vec3 diffuse_color_ = glm::vec3(1, 0, 0);
  glm::vec3 specular_color_ = glm::vec3(1, 1, 1);
  float shininess_ = 64.0f;

  static const Material default_material_;
};

}  // namespace phx

#endif  // LIBRARY_PHX_RESOURCES_TYPES_MATERIAL_HPP_

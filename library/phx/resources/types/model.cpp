//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include "phx/resources/types/model.hpp"

#include <string>
#include <vector>

#include "phx/core/logger.hpp"
#include "phx/resources/types/material.hpp"

namespace phx {

const std::vector<ResourcePointer<Mesh>>& Model::GetMeshes() const {
  return mesh_proxies_;
}

void Model::AddMesh(ResourcePointer<Mesh> mesh) {
  mesh_proxies_.push_back(mesh);
}

const std::vector<ResourcePointer<Material>>& Model::GetMaterials() const {
  return material_proxies_;
}

void Model::AddMaterial(ResourcePointer<Material> material) {
  material_proxies_.push_back(material);
}

ResourcePointer<Material> Model::GetMaterialForMesh(
    ResourcePointer<Mesh> mesh) const {
  auto it = material_by_mesh_.find(mesh);
  if (it == material_by_mesh_.end()) return ResourcePointer<Material>(nullptr);
  return it->second;
}

void Model::SetMaterialForMesh(ResourcePointer<Mesh> mesh,
                               ResourcePointer<Material> material) {
  material_by_mesh_[mesh] = material;
}

ResourcePointer<Mesh> Model::GetMesh(std::string mesh_name) {
  if (mesh_proxies_.size() == 0) {
    warn("Model contains no mesh.");
    return ResourcePointer<Mesh>(nullptr);
  }

  if (mesh_name.empty() == true) {
    if (mesh_proxies_.size() > 1) {
      info(
          "No mesh_name defined and multiple meshes were found, returning "
          "first mesh in model.");
    }
    return mesh_proxies_[0];
  }

  for (auto resource_pointer : mesh_proxies_) {
    if (resource_pointer->GetName() == mesh_name) {
      return resource_pointer;
    }
  }

  warn("Couldn't find mesh_name in model.");
  return ResourcePointer<Mesh>(nullptr);
}

}  // namespace phx

//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include "phx/resources/types/image.hpp"

#include <string>
#include <utility>

namespace phx {
std::once_flag Image::once_flag_;

Image::Image(const std::array<std::size_t, 2>& dimensions,
             const std::size_t bits_per_pixel) {
  Initialize();
  native_ =
      FreeImage_AllocateT(FIT_BITMAP, static_cast<std::int32_t>(dimensions[0]),
                          static_cast<std::int32_t>(dimensions[1]),
                          static_cast<std::int32_t>(bits_per_pixel), 0, 0, 0);
  if (!native_) {
    throw std::runtime_error("FreeImage_AllocateT failed.");
  }
}
Image::Image(const std::string& filepath, const std::int32_t native_flags) {
  Initialize();
  FREE_IMAGE_FORMAT format = FreeImage_GetFileType(filepath.c_str(), 0);
  if (format == FIF_UNKNOWN)
    format = FreeImage_GetFIFFromFilename(filepath.c_str());

  native_ = FreeImage_Load(format, filepath.c_str(), native_flags);
  if (!native_) throw std::runtime_error("FreeImage_Load failed.");
  SwapRedBlue(native_);
}
Image::Image(const Image& that)
    : Resource(that), Loggable(that), native_(FreeImage_Clone(that.native_)) {
  if (!native_) throw std::runtime_error("FreeImage_Clone failed.");
  FreeImage_CloneMetadata(native_, that.native_);
}  // namespace phx
Image::Image(Image&& temp) noexcept
    : Resource(std::move(static_cast<Resource&&>(temp))),
      Loggable(std::move(static_cast<Loggable&&>(temp))),
      native_(temp.native_) {
  temp.native_ = nullptr;
}
Image::~Image() {
  if (native_) FreeImage_Unload(native_);
}
Image& Image::operator=(const Image& that) {
  native_ = FreeImage_Clone(that.native_);
  if (!native_) throw std::runtime_error("FreeImage_Clone failed.");
  FreeImage_CloneMetadata(native_, that.native_);
  return *this;
}
Image& Image::operator=(Image&& temp) noexcept {
  if (this != &temp) {
    native_ = temp.native_;
    temp.native_ = nullptr;
  }
  return *this;
}

bool Image::Save(const std::string& filepath, const std::int32_t native_flags) {
  SwapRedBlue(native_);
  const auto result =
      FreeImage_Save(FreeImage_GetFIFFromFilename(filepath.c_str()), native_,
                     filepath.c_str(), native_flags) != 0;
  SwapRedBlue(native_);
  return result;
}

std::array<std::size_t, 2> Image::GetDimensions() const {
  return {{static_cast<std::size_t>(FreeImage_GetWidth(native_)),
           static_cast<std::size_t>(FreeImage_GetHeight(native_))}};
}
std::size_t Image::GetPitch() const {
  return static_cast<std::size_t>(FreeImage_GetPitch(native_));
}
std::size_t Image::GetBitsPerPixel() const {
  return static_cast<std::size_t>(FreeImage_GetBPP(native_));
}

bool Image::IsEmpty() const { return FreeImage_HasPixels(native_) == 0; }

void Image::SetPixelColor(const std::array<std::size_t, 2>& position,
                          const std::array<std::uint8_t, 4>& color) {
  FreeImage_SetPixelColor(
      native_, static_cast<std::uint32_t>(position[0]),
      static_cast<std::uint32_t>(position[1]),
      reinterpret_cast<RGBQUAD*>(const_cast<std::uint8_t*>(color.data())));
}
std::array<std::uint8_t, 4> Image::GetPixelColor(
    const std::array<std::size_t, 2>& position) const {
  std::array<std::uint8_t, 4> color;
  FreeImage_GetPixelColor(native_, static_cast<std::uint32_t>(position[0]),
                          static_cast<std::uint32_t>(position[1]),
                          reinterpret_cast<RGBQUAD*>(color.data()));
  return color;
}

void Image::To4Bits() {
  SwapRedBlue(native_);
  Replace(FreeImage_ConvertTo4Bits(native_));
}
void Image::To8Bits() {
  SwapRedBlue(native_);
  Replace(FreeImage_ConvertToGreyscale(native_));
}
void Image::To16Bits() {
  SwapRedBlue(native_);
  Replace(FreeImage_ConvertTo16Bits555(native_));
}
void Image::To24Bits() {
  SwapRedBlue(native_);
  Replace(FreeImage_ConvertTo24Bits(native_));
}
void Image::To32Bits() {
  SwapRedBlue(native_);
  Replace(FreeImage_ConvertTo32Bits(native_));
}

std::string Image::ToString() const {
  return "Image (dimensions: " + std::to_string(GetDimensions()[0]) + " " +
         std::to_string(GetDimensions()[1]) +
         ", bits per pixel: " + std::to_string(GetBitsPerPixel());
}

void Image::Initialize() {
  std::call_once(once_flag_, []() { FreeImage_Initialise(); });
}

void Image::Replace(FIBITMAP* native) {
  if (native_) FreeImage_Unload(native_);
  native_ = native;
  SwapRedBlue(native_);
  if (!native_) throw std::runtime_error("Replace failed.");
}

bool Image::SwapRedBlue(FIBITMAP* dib) {
  if (FreeImage_GetImageType(dib) != FIT_BITMAP) return false;
  const auto bytes_per_pixel = FreeImage_GetBPP(dib) / 8;
  if (bytes_per_pixel > 4 || bytes_per_pixel < 3) return false;

  auto height = FreeImage_GetHeight(dib);
  auto pitch = FreeImage_GetPitch(dib);
  auto line_size = FreeImage_GetLine(dib);
  auto line = FreeImage_GetBits(dib);
  for (unsigned y = 0; y < height; ++y, line += pitch) {
    for (auto pixel = line; pixel < line + line_size;
         pixel += bytes_per_pixel) {
      pixel[0] ^= pixel[2];
      pixel[2] ^= pixel[0];
      pixel[0] ^= pixel[2];
    }
  }
  return true;
}
}  // namespace phx

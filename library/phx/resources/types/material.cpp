//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include "phx/resources/types/material.hpp"

#include <math.h>
#include <algorithm>
#include <memory>
#include <string>

#include "phx/core/logger.hpp"

namespace phx {

const char Material::UNNAMED[] = "UnnamedMaterial";
const Material Material::default_material_;

glm::vec3 Material::GetDiffuseColor() const { return diffuse_color_; }
void Material::SetDiffuseColor(glm::vec3 color) { diffuse_color_ = color; }

ResourcePointer<Image> Material::GetDiffuseImage() const {
  return diffuse_image_;
}
void Material::SetDiffuseImage(ResourcePointer<Image> image) {
  diffuse_image_ = image;
  ResetTexture(&diffuse_texture_);
}

gl::texture_2d* Material::GetDiffuseTexture() const {
  return diffuse_texture_.get();
}

glm::vec3 Material::GetAmbientColor() const { return ambient_color_; }
void Material::SetAmbientColor(glm::vec3 color) { ambient_color_ = color; }

ResourcePointer<Image> Material::GetAmbientImage() const {
  return ambient_image_;
}
void Material::SetAmbientImage(ResourcePointer<Image> image) {
  ambient_image_ = image;
  ResetTexture(&ambient_texture_);
}

gl::texture_2d* Material::GetAmbientTexture() const {
  return ambient_texture_.get();
}

glm::vec3 Material::GetSpecularColor() const { return specular_color_; }
void Material::SetSpecularColor(glm::vec3 color) { specular_color_ = color; }

ResourcePointer<Image> Material::GetSpecularImage() const {
  return specular_image_;
}
void Material::SetSpecularImage(ResourcePointer<Image> image) {
  specular_image_ = image;
  ResetTexture(&specular_texture_);
}

gl::texture_2d* Material::GetSpecularTexture() const {
  return specular_texture_.get();
}

float Material::GetShininess() const { return shininess_; }
void Material::SetShininess(float shininess) {
  if (shininess >= 0.0f) {
    shininess_ = shininess;
  } else {
    info(
        "WARNING: Shininess values < 0.0 are not allowed, desired value of {} "
        "has not been set!",
        shininess);
  }
}

const Material* Material::GetDefault() { return &default_material_; }

void Material::UploadTextures() {
  if (!diffuse_texture_) {
    SetTexture(diffuse_image_, &diffuse_texture_);
  }
  if (!specular_texture_) {
    SetTexture(specular_image_, &specular_texture_);
  }
  if (!ambient_texture_) {
    SetTexture(ambient_image_, &ambient_texture_);
  }
}

void Material::SetTexture(ResourcePointer<Image> image,
                          std::shared_ptr<gl::texture_2d>* texture) {
  if (image == nullptr) return;
  if (*texture) {
    ResetTexture(texture);
  }

  auto dimensions = image->GetDimensions();
  (*texture) = std::make_shared<gl::texture_2d>();
  (*texture)->set_min_filter(GL_LINEAR_MIPMAP_LINEAR);
  (*texture)->set_mag_filter(GL_LINEAR);
  (*texture)->set_wrap_s(GL_REPEAT);
  (*texture)->set_wrap_t(GL_REPEAT);
  int mipmap_levels = 8;
  int max_level = static_cast<int>(
      std::floor(log2(std::min(dimensions[0], dimensions[1]))));
  mipmap_levels = std::min(mipmap_levels, max_level + 1);
  (*texture)->set_storage(mipmap_levels, GL_RGBA8,
                          static_cast<GLsizei>(dimensions[0]),
                          static_cast<GLsizei>(dimensions[1]));
  (*texture)->set_sub_image(0, 0, 0, static_cast<GLsizei>(dimensions[0]),
                            static_cast<GLsizei>(dimensions[1]), GL_RGBA,
                            GL_UNSIGNED_BYTE, image->GetPixels().first);
  (*texture)->generate_mipmap();
  gl::texture_handle handle(*texture->get());
  handle.set_resident(true);
  gl::print_error("[Material::SetTexture] OpenGl Error code");
}

void Material::ResetTexture(std::shared_ptr<gl::texture_2d>* texture) {
  if (*texture && (*texture)->is_valid()) {
    gl::texture_handle handle(*texture->get());
    handle.set_resident(false);
  }
  texture->reset();
}

}  // namespace phx

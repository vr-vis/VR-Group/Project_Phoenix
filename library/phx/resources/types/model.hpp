//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef LIBRARY_PHX_RESOURCES_TYPES_MODEL_HPP_
#define LIBRARY_PHX_RESOURCES_TYPES_MODEL_HPP_

#include <map>
#include <string>
#include <vector>

#include "phx/suppress_warnings.hpp"

SUPPRESS_WARNINGS_BEGIN
#include "glm/glm.hpp"
SUPPRESS_WARNINGS_END

#include "phx/resources/types/material.hpp"
#include "phx/resources/types/mesh.hpp"
#include "phx/resources/resource.hpp"
#include "phx/resources/resource_pointer.hpp"
#include "phx/export.hpp"

namespace phx {

class PHOENIX_EXPORT Model : public Resource {
 public:
  Model() = default;
  Model(const Model&) = default;
  Model(Model&&) = default;

  ~Model() override = default;

  Model& operator=(const Model&) = default;
  Model& operator=(Model&&) = default;

  const std::vector<ResourcePointer<Mesh>>& GetMeshes() const;
  void AddMesh(ResourcePointer<Mesh> mesh);

  const std::vector<ResourcePointer<Material>>& GetMaterials() const;
  void AddMaterial(ResourcePointer<Material> material);

  ResourcePointer<Material> GetMaterialForMesh(
      ResourcePointer<Mesh> mesh) const;
  void SetMaterialForMesh(ResourcePointer<Mesh> mesh,
                          ResourcePointer<Material> material);

  ResourcePointer<Mesh> GetMesh(std::string mesh_name = "");

 private:
  std::vector<ResourcePointer<Mesh>> mesh_proxies_;
  std::vector<ResourcePointer<Material>> material_proxies_;
  std::map<ResourcePointer<Mesh>, ResourcePointer<Material>> material_by_mesh_;
};

}  // namespace phx

#endif  // LIBRARY_PHX_RESOURCES_TYPES_MODEL_HPP_

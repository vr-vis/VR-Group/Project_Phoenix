//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef LIBRARY_PHX_RESOURCES_RESOURCE_UTILS_HPP_
#define LIBRARY_PHX_RESOURCES_RESOURCE_UTILS_HPP_

#include <map>
#include <memory>
#include <string>
#include <vector>

#include "json.hpp"

#include "phx/core/logger.hpp"
#include "phx/export.hpp"
#include "phx/resources/resource_declaration.hpp"
#include "phx/resources/resource_load_strategy.hpp"
#include "phx/resources/resource_manager.hpp"
#include "phx/resources/resource_pointer.hpp"
#include "phx/resources/types/material.hpp"
#include "phx/utility/aspects/singleton.hpp"

namespace phx {
/**
 * The ResourceUtils class contains static convenience methods to simplify
 * interactions with the resource system. It also holds a user configurable
 * resource search path. When operating on a relative file name, it will
 * search all entries in the search path and return/load the first file that
 * matches the given file name. Per default, the resource search path
 * contains the local directory and phx::resources_root.
 */
class PHOENIX_EXPORT ResourceUtils final {
 public:
  static ResourceDeclaration GenericMaterialDeclaration(
      const std::string& material_name, glm::vec3 diffuse_color,
      glm::vec3 specular_color, glm::vec3 ambient_color, float shininess);

  static void AddResourceSearchPath(const std::string& path_to_add);
  static void ResetResourceSearchPath();
  static std::vector<std::string> GetResourceSearchPath();

  static std::string ExtractFileExtension(const std::string& file_name);

  static ResourceDeclaration DeclarationFromFile(
      const std::string& file_name, nlohmann::json additional_info = {},
      bool absolute_path = false);

  static ResourcePointer<Material> LoadGenericMaterial(
      const std::string& material_name, glm::vec3 diffuse_color,
      glm::vec3 specular_color = Material::GetDefault()->GetSpecularColor(),
      glm::vec3 ambient_color = Material::GetDefault()->GetAmbientColor(),
      float shininess = Material::GetDefault()->GetShininess());

  template <class ResourceType>
  static ResourcePointer<ResourceType> LoadResourceFromFile(
      const std::string& file_name, nlohmann::json additional_info = {},
      bool absolute_path = false) {
    auto res_ptr =
        phx::ResourceManager::instance().DeclareResource<ResourceType>(
            DeclarationFromFile(file_name, additional_info, absolute_path));
    res_ptr.Load();
    return res_ptr;
  }

 private:
  static std::string FindFileInSearchPath(const std::string& file_name);
  static std::vector<std::string> resource_search_paths_;
};

}  // namespace phx

#endif  // LIBRARY_PHX_RESOURCES_RESOURCE_UTILS_HPP_

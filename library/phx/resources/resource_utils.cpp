//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------
#include "phx/resources/resource_utils.hpp"

#include <algorithm>
#include <cassert>
#include <fstream>
#include <locale>
#include <string>
#include <vector>

#include "boost/filesystem.hpp"

#include "phx/resources/resource_manager.hpp"
#include "phx/resources_path.hpp"

namespace phx {
namespace {
std::vector<std::string> default_resource_search_paths = {
    {std::string("./"), resources_root}};
}

std::vector<std::string> ResourceUtils::resource_search_paths_{
    default_resource_search_paths};

ResourceDeclaration ResourceUtils::GenericMaterialDeclaration(
    const std::string& material_name, glm::vec3 diffuse_color,
    glm::vec3 specular_color, glm::vec3 ambient_color, float shininess) {
  ResourceDeclaration declaration = {{"TYPE", "GEN_MATERIAL"}};
  declaration["material_name"] = material_name;

  declaration["diffuse_color"] = {diffuse_color[0], diffuse_color[1],
                                  diffuse_color[2]};
  declaration["specular_color"] = {specular_color[0], specular_color[1],
                                   specular_color[2]};
  declaration["ambient_color"] = {ambient_color[0], ambient_color[1],
                                  ambient_color[2]};
  declaration["shininess"] = shininess;
  return declaration;
}

void ResourceUtils::AddResourceSearchPath(const std::string& path_to_add) {
  assert(!path_to_add.empty());
  auto p = (path_to_add.back() == '/' ? path_to_add
                                      : path_to_add + std::string("/"));
  resource_search_paths_.push_back(p);
}

void ResourceUtils::ResetResourceSearchPath() {
  resource_search_paths_ = default_resource_search_paths;
}

std::vector<std::string> ResourceUtils::GetResourceSearchPath() {
  return resource_search_paths_;
}

ResourceDeclaration ResourceUtils::DeclarationFromFile(
    const std::string& file_name, nlohmann::json additional_info /*= {}*/,
    bool absolute_path /*= false*/) {
  ResourceDeclaration declaration = {{"TYPE", ExtractFileExtension(file_name)}};
  if (absolute_path) {
    declaration["file_name"] = file_name;
  } else {
    declaration["file_name"] = FindFileInSearchPath(file_name);
  }
  for (nlohmann::json::iterator it = additional_info.begin();
       it != additional_info.end(); ++it) {
    declaration[it.key()] = it.value();
  }
  return declaration;
}

std::string ResourceUtils::ExtractFileExtension(const std::string& file_name) {
  std::string extension = file_name.substr(file_name.rfind('.'));
  std::transform(extension.begin(), extension.end(), extension.begin(),
                 [](char c) { return std::tolower(c, std::locale()); });

  return extension;
}

ResourcePointer<Material> ResourceUtils::LoadGenericMaterial(
    const std::string& material_name, glm::vec3 diffuse_color,
    glm::vec3 specular_color /*= Material::GetDefault()->GetSpecularColor()*/,
    glm::vec3 ambient_color /*= Material::GetDefault()->GetAmbientColor()*/,
    float shininess /*= Material::GetDefault()->GetShininess()*/) {
  auto res_ptr = phx::ResourceManager::instance().DeclareResource<Material>(
      GenericMaterialDeclaration(material_name, diffuse_color, specular_color,
                                 ambient_color, shininess));
  res_ptr.Load();
  return res_ptr;
}

std::string ResourceUtils::FindFileInSearchPath(const std::string& file_name) {
  for (auto& entry : resource_search_paths_) {
    auto full_name = entry + file_name;
    if (boost::filesystem::exists(full_name.c_str())) return full_name;
  }
  return "";
}

}  // namespace phx

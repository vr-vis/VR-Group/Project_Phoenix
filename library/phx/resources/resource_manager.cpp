//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------
#include "phx/resources/resource_manager.hpp"

#include <algorithm>
#include <memory>
#include <string>
#include <utility>

#include "phx/resources/loaders/assimp_model_loader.hpp"
#include "phx/resources/loaders/generic_material_loader.hpp"
#include "phx/resources/loaders/image_loader.hpp"
#include "phx/resources/loaders/shader_loader.hpp"

namespace phx {

ResourceManager::ResourceManager() {
  // per convention over configuration, we register default extensions here.
  this->RegisterMeshResourceExtensions();
  this->RegisterShaderResourceExtensions();
  this->RegisterImageResourceExtensions();
  this->RegisterResourceGenerators();
}

void ResourceManager::RegisterResourceType(
    const std::string &type, std::unique_ptr<ResourceLoadStrategy> loader) {
  // we intentionally overwrite loaders if they exist (assuming the caller
  // knows what she's doing).
  loaders_by_type_[type] = std::move(loader);
}

std::unique_ptr<phx::Resource> ResourceManager::Load(
    const ResourceDeclaration &declaration) {
  ResourceLoadStrategy *loader = nullptr;
  if (declaration.find("TYPE") != declaration.end()) {
    loader = GetLoaderForType(declaration["TYPE"]);
  } else {
    warn(
        "No Loader for Resource Declaration {}, can be determined, since it "
        " does not contain a TYPE key",
        declaration.dump());
  }
  if (loader == nullptr) return nullptr;
  return loader->Load(declaration);
}

phx::ResourceLoadStrategy *ResourceManager::GetLoaderForType(
    const std::string &type) const {
  auto loader_iterator = loaders_by_type_.find(type);
  if (loader_iterator == loaders_by_type_.end()) {
    phx::warn("No matching loader for TYPE {}", type);
    return nullptr;
  }
  return loader_iterator->second.get();
}

void ResourceManager::RegisterMeshResourceExtensions() {
  this->RegisterResourceType(".obj", std::make_unique<AssimpModelLoader>());
  this->RegisterResourceType(".stl", std::make_unique<AssimpModelLoader>());
}

void ResourceManager::RegisterShaderResourceExtensions() {
  this->RegisterResourceType(".frag", std::make_unique<ShaderLoader>());
  this->RegisterResourceType(".vert", std::make_unique<ShaderLoader>());
  this->RegisterResourceType(".geom", std::make_unique<ShaderLoader>());
  this->RegisterResourceType(".tesc", std::make_unique<ShaderLoader>());
  this->RegisterResourceType(".tese", std::make_unique<ShaderLoader>());
  this->RegisterResourceType(".comp", std::make_unique<ShaderLoader>());
}

void ResourceManager::RegisterImageResourceExtensions() {
  this->RegisterResourceType(".jpg", std::make_unique<ImageLoader>());
  this->RegisterResourceType(".jpeg", std::make_unique<ImageLoader>());
  this->RegisterResourceType(".png", std::make_unique<ImageLoader>());
}

void ResourceManager::RegisterResourceGenerators() {
  this->RegisterResourceType("GEN_MATERIAL",
                             std::make_unique<GenericMaterialLoader>());
}

}  // namespace phx

//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef LIBRARY_PHX_RESOURCES_RESOURCE_MANAGER_HPP_
#define LIBRARY_PHX_RESOURCES_RESOURCE_MANAGER_HPP_

#include <map>
#include <memory>
#include <string>

#include "phx/core/logger.hpp"
#include "phx/export.hpp"
#include "phx/resources/resource_declaration.hpp"
#include "phx/resources/resource_load_strategy.hpp"
#include "phx/resources/resource_pointer.hpp"
#include "phx/resources/resource_proxy.hpp"
#include "phx/utility/aspects/singleton.hpp"

namespace phx {
/**
 *  The ResourceManager is the central instance for the management of all
 *  externally provided content, aka resources. Resources have to be declared
 *  first before being available in the system.
 */
class PHOENIX_EXPORT ResourceManager final : public singleton<ResourceManager> {
 public:
  // intentionally limit access to the load logic only through the ResourceProxy
  friend void ResourceProxy::Load();

  ~ResourceManager() = default;

  void RegisterResourceType(const std::string& type,
                            std::unique_ptr<ResourceLoadStrategy> loader);

  template <typename ResourceType>
  ResourcePointer<ResourceType> DeclareResource(
      const ResourceDeclaration& declaration);

  ResourceLoadStrategy* GetLoaderForType(const std::string& type) const;

 protected:
  friend ResourceManager& phx::singleton<ResourceManager>::instance<>();
  ResourceManager();
  ResourceManager(const ResourceManager&) = delete;
  ResourceManager(ResourceManager&&) = delete;

  ResourceManager& operator=(const ResourceManager&) = delete;
  ResourceManager& operator=(ResourceManager&&) = delete;

  std::unique_ptr<Resource> Load(const ResourceDeclaration& declaration);

 private:
  void RegisterShaderResourceExtensions();
  void RegisterMeshResourceExtensions();
  void RegisterImageResourceExtensions();

  void RegisterResourceGenerators();

  std::map<std::string, std::unique_ptr<ResourceLoadStrategy>> loaders_by_type_;
  std::map<std::size_t, std::unique_ptr<ResourceProxy>> resources_;
};

template <typename ResourceType>
ResourcePointer<ResourceType> phx::ResourceManager::DeclareResource(
    const ResourceDeclaration& declaration) {
  auto resource_hash = std::hash<std::string>{}(declaration.dump());

  auto resource_entry = resources_.find(resource_hash);
  if (resource_entry != resources_.end()) {
    return ResourcePointer<ResourceType>(resource_entry->second.get());
  }

  auto entry = resources_.insert(std::make_pair(
      resource_hash, std::make_unique<ResourceProxy>(this, declaration)));
  return ResourcePointer<ResourceType>(entry.first->second.get());
}

}  // namespace phx

#endif  // LIBRARY_PHX_RESOURCES_RESOURCE_MANAGER_HPP_

//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef LIBRARY_PHX_RESOURCES_RESOURCE_LOAD_STRATEGY_HPP_
#define LIBRARY_PHX_RESOURCES_RESOURCE_LOAD_STRATEGY_HPP_

#include <memory>

#include "phx/resources/resource_declaration.hpp"
#include "phx/resources/resource.hpp"

namespace phx {
/**
 * ResourceLoadStrategy serves as an interface for all load strategies that are
 * used to obtain resources. Derived classes have to implement the pure virtual
 * Load interface.
 */
class ResourceLoadStrategy {
 public:
  ResourceLoadStrategy() = default;
  ResourceLoadStrategy(const ResourceLoadStrategy &) = delete;
  ResourceLoadStrategy(ResourceLoadStrategy &&) = delete;
  virtual ~ResourceLoadStrategy() = default;

  ResourceLoadStrategy &operator=(const ResourceLoadStrategy &) = delete;
  ResourceLoadStrategy &operator=(ResourceLoadStrategy &&) = delete;

  virtual std::unique_ptr<Resource> Load(
      const ResourceDeclaration &declaration) = 0;

 protected:
 private:
};
}  // namespace phx

#endif  // LIBRARY_PHX_RESOURCES_RESOURCE_LOAD_STRATEGY_HPP_

//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef LIBRARY_PHX_RESOURCES_RESOURCE_POINTER_HPP_
#define LIBRARY_PHX_RESOURCES_RESOURCE_POINTER_HPP_

#include <cstddef>

#include "phx/resources/resource_proxy.hpp"
#include "phx/export.hpp"

namespace phx {

template <class ResourceType>
class PHOENIX_EXPORT ResourcePointer {
 public:
  ResourcePointer() : proxy_(nullptr) {}
  explicit ResourcePointer(std::nullptr_t) : proxy_(nullptr) {}
  explicit ResourcePointer(ResourceProxy *proxy) : proxy_(proxy) {}
  ResourcePointer(const ResourcePointer &) = default;
  ResourcePointer(ResourcePointer &&) = default;
  ResourcePointer &operator=(const ResourcePointer &) = default;
  ResourcePointer &operator=(ResourcePointer &&) = default;
  ~ResourcePointer() = default;

  ResourceType *operator->() { return proxy_->GetAs<ResourceType>(); }
  bool operator==(const ResourcePointer<ResourceType> &a) const {
    return a.proxy_ == proxy_;
  }
  bool operator==(std::nullptr_t) const {
    return proxy_ == nullptr || !proxy_->IsOnline();
  }
  bool operator!=(const ResourcePointer<ResourceType> &a) const {
    return a.proxy_ != proxy_;
  }
  bool operator!=(std::nullptr_t) const {
    return proxy_ != nullptr && proxy_->IsOnline();
  }
  bool operator<(const ResourcePointer<ResourceType> &a) const {
    return a.proxy_ < proxy_;
  }
  operator bool() const { return proxy_ != nullptr && proxy_->IsOnline(); }

  ResourceType *Get() const { return proxy_->GetAs<ResourceType>(); }

  void Load() { if (proxy_) proxy_->Load(); }
  void Unload() { if (proxy_) proxy_->Unload(); }

 private:
  ResourceProxy *proxy_;
};

}  // namespace phx

#endif  // LIBRARY_PHX_RESOURCES_RESOURCE_POINTER_HPP_

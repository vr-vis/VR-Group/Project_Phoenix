//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include "phx/resources/resource_proxy.hpp"

#include "phx/resources/resource_manager.hpp"

namespace phx {

ResourceProxy::ResourceProxy(ResourceManager *mng,
                             const ResourceDeclaration &declaration)
    : resource_manager_(mng), declaration_(declaration) {}

bool ResourceProxy::IsOnline() const { return resource_.get() != nullptr; }

void ResourceProxy::Load() {
  if (IsOnline()) return;
  resource_ = resource_manager_->Load(declaration_);
}

void ResourceProxy::Unload() { resource_.release(); }

}  // namespace phx

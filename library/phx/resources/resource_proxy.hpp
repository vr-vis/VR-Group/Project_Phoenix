//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef LIBRARY_PHX_RESOURCES_RESOURCE_PROXY_HPP_
#define LIBRARY_PHX_RESOURCES_RESOURCE_PROXY_HPP_

#include <memory>

#include "phx/resources/resource.hpp"
#include "phx/resources/resource_declaration.hpp"

namespace phx {
class ResourceManager;
/**
 * A ResourceProxy acts as a generic handle to pass arbitrary resources
 * throughout the system. The proxy defines a resource's explicit state machine.
 * A resource has to be declared through the ResourceManager first. This
 * generates a resource proxy for said resource. Subsequently, the resource can
 * be loaded and unloaded. A load will bring it from offline to online. The
 * actual resource can only be accessed when the corresponding proxy is in its
 * online state. In this way, memory management is explicitly left in the
 * client's hands.
 */
class ResourceProxy final {
 public:
  ResourceProxy(ResourceManager *mng, const ResourceDeclaration &declaration);
  ResourceProxy(const ResourceProxy &) = delete;
  ResourceProxy(ResourceProxy &&) = delete;
  ResourceProxy &operator=(const ResourceProxy &) = delete;
  ResourceProxy &operator=(ResourceProxy &&) = delete;
  ~ResourceProxy() = default;

  bool IsOnline() const;

  void Load();
  void Unload();

  template <typename ResourceTargetType>
  ResourceTargetType *GetAs() const;

 private:
  ResourceManager *resource_manager_;
  ResourceDeclaration declaration_;
  std::unique_ptr<Resource> resource_{nullptr};
};

template <typename ResourceTargetType>
ResourceTargetType *phx::ResourceProxy::GetAs() const {
  return dynamic_cast<ResourceTargetType *>(resource_.get());
}

}  // namespace phx

#endif  // LIBRARY_PHX_RESOURCES_RESOURCE_PROXY_HPP_

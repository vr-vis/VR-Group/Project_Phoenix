//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include "phx/resources/loaders/generic_material_loader.hpp"

#include <memory>
#include <vector>

#include "phx/resources/types/material.hpp"

namespace phx {
std::unique_ptr<Resource> GenericMaterialLoader::Load(
    const ResourceDeclaration& declaration) {
  auto resource = std::make_unique<phx::Material>();

  resource->SetName(declaration["material_name"]);

  std::vector<float> diffuseColor = declaration["diffuse_color"];
  resource->SetDiffuseColor(
      glm::vec3(diffuseColor[0], diffuseColor[1], diffuseColor[2]));
  std::vector<float> specularColor = declaration["specular_color"];
  resource->SetSpecularColor(
      glm::vec3(specularColor[0], specularColor[1], specularColor[2]));
  std::vector<float> ambientColor = declaration["ambient_color"];
  resource->SetAmbientColor(
      glm::vec3(ambientColor[0], ambientColor[1], ambientColor[2]));
  float shininess = declaration["shininess"];
  resource->SetShininess(shininess);

  return resource;
}
}  // namespace phx

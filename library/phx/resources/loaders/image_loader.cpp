//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include "phx/resources/loaders/image_loader.hpp"

#include <memory>
#include <string>
#include <utility>

#include "phx/core/logger.hpp"
#include "phx/resources/types/image.hpp"
#include "phx/resources_path.hpp"

namespace phx {
std::unique_ptr<Resource> ImageLoader::Load(
    const ResourceDeclaration& declaration) {
  if (declaration.find("file_name") == declaration.end() ||
      !declaration["file_name"].is_string()) {
    warn(
        "declaration {} cannot be parsed by ImageLoader, it does not "
        "contain a file_name key",
        declaration.dump());
    return nullptr;
  }
  auto image =
      std::make_unique<Image>(declaration["file_name"].get<std::string>());
  int bit_format = 32;
  if (declaration.find("bit_format") == declaration.end() ||
      !declaration["bit_format"].is_number_integer()) {
    debug("declaration {} does not contain bit_format, use 32",
          declaration.dump());
  } else {
    bit_format = declaration["bit_format"].get<int>();
  }

  switch (bit_format) {
    case 4:
      image->To4Bits();
      break;
    case 8:
      image->To8Bits();
      break;
    case 16:
      image->To16Bits();
      break;
    case 24:
      image->To24Bits();
      break;
    case 32:
      image->To32Bits();
      break;
    default:
      warn("[ImageLoader] bit_format {} not supported, use 32 bits instead",
           bit_format);
      image->To32Bits();
      break;
  }

  return std::move(image);
}
}  // namespace phx

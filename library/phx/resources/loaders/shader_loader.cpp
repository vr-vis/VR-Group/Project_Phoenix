//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include "phx/resources/loaders/shader_loader.hpp"

#include <fstream>
#include <memory>
#include <string>
#include <utility>

#include "phx/core/logger.hpp"
#include "phx/resources/types/shader_source.hpp"
#include "phx/resources_path.hpp"

namespace phx {

std::unique_ptr<phx::Resource> ShaderLoader::Load(
    const ResourceDeclaration& declaration) {
  auto shader_source = this->LoadShaderSource(declaration["file_name"]);
  if (shader_source.empty()) return nullptr;

  auto shader = std::make_unique<ShaderSource>();
  shader->SetSource(shader_source);
  return shader;
}

std::string ShaderLoader::LoadShaderSource(const std::string& file_name) {
  std::ifstream infile{file_name.c_str(), std::ios::in};
  if (!infile.good()) {
    phx::warn("Unable to read shader from requested file {}", file_name);
    return "";
  }

  std::string shader_source{std::istreambuf_iterator<char>(infile),
                            std::istreambuf_iterator<char>()};
  return shader_source;
}

}  // namespace phx

//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef LIBRARY_PHX_RESOURCES_LOADERS_ASSIMP_MODEL_LOADER_HPP_
#define LIBRARY_PHX_RESOURCES_LOADERS_ASSIMP_MODEL_LOADER_HPP_

#include <functional>
#include <memory>
#include <string>
#include <vector>

#include "assimp/Importer.hpp"
#include "assimp/ProgressHandler.hpp"
#include "assimp/scene.h"

#include "phx/suppress_warnings.hpp"

SUPPRESS_WARNINGS_BEGIN
#include "glm/glm.hpp"
SUPPRESS_WARNINGS_END

#include "phx/export.hpp"
#include "phx/resources/resource_declaration.hpp"
#include "phx/resources/resource_load_strategy.hpp"
#include "phx/resources/resource_pointer.hpp"
#include "phx/resources/types/material.hpp"
#include "phx/resources/types/mesh.hpp"
#include "phx/resources/types/model.hpp"

namespace phx {

class PHOENIX_EXPORT AssimpModelLoader final : public ResourceLoadStrategy {
 public:
  AssimpModelLoader();
  AssimpModelLoader(const AssimpModelLoader&) = delete;
  AssimpModelLoader(AssimpModelLoader&&) = delete;
  ~AssimpModelLoader() override;

  AssimpModelLoader& operator=(const AssimpModelLoader&) = delete;
  AssimpModelLoader& operator=(AssimpModelLoader&&) = delete;

  std::unique_ptr<Resource> Load(
      const ResourceDeclaration& declaration) override;
  void SetProgressUpdateCallback(const std::function<void(float)>& callback);

 protected:
  std::unique_ptr<phx::Model> LoadModel(const aiScene* scene,
                                        const std::string& file_name);
  void LoadModelMaterials(const aiScene* scene, const std::string& file_name,
                          phx::Model* resource);
  void LoadModelMeshes(const aiScene* scene, const std::string& file_name,
                       phx::Model* resource);

  std::unique_ptr<phx::Material> LoadSingleMaterial(
      const aiScene* scene, const ResourceDeclaration& declaration,
      const std::string& file_name);

  std::unique_ptr<phx::Mesh> LoadSingleMesh(
      const aiScene* scene, const ResourceDeclaration& declaration);

  std::vector<glm::vec3> LoadVertices(const aiMesh* mesh);
  std::vector<unsigned int> LoadIndices(const aiMesh* mesh);
  std::vector<glm::vec3> LoadNormals(const aiMesh* mesh);
  std::vector<glm::vec3> LoadTangents(const aiMesh* mesh);
  std::vector<glm::vec3> LoadBitangents(const aiMesh* mesh);
  std::vector<glm::vec2> LoadTextureCoords(const aiMesh* mesh);

  template <typename FileSourceType>
  std::vector<glm::vec3> LoadVectorData(FileSourceType* source,
                                        const std::size_t num_entries);

  unsigned int GetSceneMaterialIndex(const aiScene* scene,
                                     const std::string& material_name);
  std::string GetSceneMaterialNameFromIndex(const aiScene*,
                                            const unsigned int mesh_index);

 private:
  class AssimpProgressHandler : public Assimp::ProgressHandler {
   public:
    bool Update(float percentage) override;
    AssimpModelLoader* parent_ = nullptr;
  };

  bool LoadFile(const std::string& filename);

  std::array<glm::vec3, 2> ComputeBoundingBox(
      const std::vector<glm::vec3>& vertices);

  static const int kInvalidMaterialIndex = -1;

  Assimp::Importer importer_;
  AssimpProgressHandler progress_handler_;
  std::string last_loaded_scene_;
  int last_material_index_ = kInvalidMaterialIndex;
  std::function<void(float)> on_progress_update_callback_;
};

template <typename FileSourceType>
std::vector<glm::vec3> phx::AssimpModelLoader::LoadVectorData(
    FileSourceType* source, const std::size_t num_entries) {
  std::vector<glm::vec3> result(num_entries);
  for (unsigned int i = 0; i < num_entries; ++i) {
    result[i] = glm::vec3{static_cast<float>(source[i][0]),
                          static_cast<float>(source[i][1]),
                          static_cast<float>(source[i][2])};
  }
  return result;
}

}  // namespace phx

#endif  // LIBRARY_PHX_RESOURCES_LOADERS_ASSIMP_MODEL_LOADER_HPP_

//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include "phx/resources/loaders/assimp_model_loader.hpp"

#include <iostream>
#include <memory>
#include <string>
#include <vector>

#include "assimp/Importer.hpp"   // C++ importer interface
#include "assimp/postprocess.h"  // Post processing flags

#include "phx/resources/loaders/image_loader.hpp"
#include "phx/resources/types/model.hpp"
#include "phx/resources/resource_declaration.hpp"
#include "phx/resources/resource_manager.hpp"
#include "phx/resources/resource_utils.hpp"
#include "phx/resources_path.hpp"
#include "phx/core/logger.hpp"

namespace phx {

AssimpModelLoader::AssimpModelLoader() {
  progress_handler_.parent_ = this;
  importer_.SetProgressHandler(&progress_handler_);
}

std::unique_ptr<phx::Resource> AssimpModelLoader::Load(
    const ResourceDeclaration &declaration) {
  if (declaration.find("file_name") == declaration.end() ||
      !declaration["file_name"].is_string()) {
    warn(
        "declaration {} cannot be parsed by AssimpModelLoader, it does not "
        "contain a file_name key",
        declaration.dump());
    return nullptr;
  }
  std::string file_name = declaration["file_name"];
  if (last_loaded_scene_ != file_name || importer_.GetScene() == nullptr) {
    if (!LoadFile(file_name)) {
      return nullptr;
    }
  }

  const aiScene *scene = importer_.GetScene();

  if (declaration.find("mesh_index") == declaration.end() &&
      declaration.find("material_name") == declaration.end()) {
    return LoadModel(scene, file_name);
  } else if (declaration.find("material_name") != declaration.end()) {
    return LoadSingleMaterial(scene, declaration, file_name);
  } else if (declaration.find("mesh_index") != declaration.end()) {
    return LoadSingleMesh(scene, declaration);
  }
  return nullptr;
}

std::unique_ptr<phx::Model> AssimpModelLoader::LoadModel(
    const aiScene *scene, const std::string &file_name) {
  auto resource = std::make_unique<phx::Model>();

  LoadModelMaterials(scene, file_name, resource.get());

  LoadModelMeshes(scene, file_name, resource.get());

  return resource;
}

void AssimpModelLoader::LoadModelMaterials(const aiScene *scene,
                                           const std::string &file_name,
                                           phx::Model *resource) {
  for (unsigned int i = 0; i < scene->mNumMaterials; ++i) {
    auto material = phx::ResourceUtils::LoadResourceFromFile<Material>(
        file_name, {{"material_name", GetSceneMaterialNameFromIndex(scene, i)}},
        true);
    resource->AddMaterial(material);
  }
}

void AssimpModelLoader::LoadModelMeshes(const aiScene *scene,
                                        const std::string &file_name,
                                        phx::Model *resource) {
  for (unsigned int i = 0; i < scene->mNumMeshes; i++) {
    auto mesh = phx::ResourceUtils::LoadResourceFromFile<Mesh>(
        file_name, {{"mesh_index", i}}, true);
    resource->AddMesh(mesh);

    for (auto mesh_material : resource->GetMaterials()) {
      if (mesh_material->GetName() ==
          GetSceneMaterialNameFromIndex(scene,
                                        scene->mMeshes[i]->mMaterialIndex)) {
        resource->SetMaterialForMesh(mesh, mesh_material);
        break;
      }
    }
  }
}  // namespace phx

std::unique_ptr<phx::Material> AssimpModelLoader::LoadSingleMaterial(
    const aiScene *scene, const ResourceDeclaration &declaration,
    const std::string &file_name) {
  auto filepath = file_name.substr(0, file_name.find_last_of("/\\"));
  auto resource = std::make_unique<phx::Material>();

  aiMaterial *material = scene->mMaterials[GetSceneMaterialIndex(
      scene, declaration["material_name"])];
  aiColor4D color_diffuse(0.f, 0.f, 0.f, 1.f);
  material->Get(AI_MATKEY_COLOR_DIFFUSE, color_diffuse);
  aiColor4D color_specular(0.f, 0.f, 0.f, 1.f);
  material->Get(AI_MATKEY_COLOR_SPECULAR, color_specular);
  aiColor4D color_ambient(0.f, 0.f, 0.f, 1.f);
  material->Get(AI_MATKEY_COLOR_AMBIENT, color_ambient);

  aiString name;
  material->Get(AI_MATKEY_NAME, name);

  float shininess;
  material->Get(AI_MATKEY_SHININESS, shininess);

  resource->SetDiffuseColor(
      glm::vec3(color_diffuse.r, color_diffuse.g, color_diffuse.b));
  resource->SetSpecularColor(
      glm::vec3(color_specular.r, color_specular.g, color_specular.b));
  resource->SetAmbientColor(
      glm::vec3(color_ambient.r, color_ambient.g, color_ambient.b));
  resource->SetShininess(shininess);
  resource->SetName(name.C_Str());

  aiString relative_path;
  if (AI_SUCCESS ==
      material->GetTexture(aiTextureType_AMBIENT, 0, &relative_path)) {
    auto image = ResourceUtils::LoadResourceFromFile<Image>(
        filepath + "/" + std::string(relative_path.C_Str()), {}, true);
    resource->SetAmbientImage(image);
  }
  if (AI_SUCCESS ==
      material->GetTexture(aiTextureType_DIFFUSE, 0, &relative_path)) {
    auto image = ResourceUtils::LoadResourceFromFile<Image>(
        filepath + "/" + std::string(relative_path.C_Str()), {}, true);
    resource->SetDiffuseImage(image);
  }
  if (AI_SUCCESS ==
      material->GetTexture(aiTextureType_SPECULAR, 0, &relative_path)) {
    auto image = ResourceUtils::LoadResourceFromFile<Image>(
        filepath + "/" + std::string(relative_path.C_Str()), {}, true);
    resource->SetSpecularImage(image);
  }

  return resource;
}

std::unique_ptr<phx::Mesh> AssimpModelLoader::LoadSingleMesh(
    const aiScene *scene, const ResourceDeclaration &declaration) {
  auto resource = std::make_unique<phx::Mesh>();
  std::size_t mesh_index = declaration["mesh_index"];

  aiMesh *mesh = scene->mMeshes[mesh_index];
  resource->SetVertices(LoadVertices(mesh));
  resource->SetIndices(LoadIndices(mesh));
  resource->SetNormals(LoadNormals(mesh));
  resource->SetTangents(LoadTangents(mesh));
  resource->SetBitangents(LoadBitangents(mesh));
  resource->SetTextureCoords(LoadTextureCoords(mesh));
  resource->SetBoundingBox(ComputeBoundingBox(resource->GetVertices()));

  auto mesh_name = std::string(mesh->mName.C_Str());
  // If an object is unnamed, assimp names it "defaultobject"
  if (mesh_name != "defaultobject") {
    resource->SetName(mesh_name);
  }

  return resource;
}

std::vector<glm::vec3> AssimpModelLoader::LoadVertices(const aiMesh *mesh) {
  return LoadVectorData(mesh->mVertices, mesh->mNumVertices);
}

std::vector<unsigned int> AssimpModelLoader::LoadIndices(const aiMesh *mesh) {
  const std::size_t num_faces = mesh->mNumFaces;
  std::vector<unsigned int> result;
  result.reserve(num_faces);
  for (unsigned int i = 0; i < num_faces; i++) {
    if (mesh->mFaces[i].mNumIndices != 3) {
      // Meshes are triangulated, so there should not be faces with more than
      // three indices
      phx::warn("Assimp loaded face with {} indices. The face was ignored.",
                mesh->mFaces[i].mNumIndices);
      continue;
    }
    for (unsigned int j = 0; j < 3; j++)
      result.push_back(mesh->mFaces[i].mIndices[j]);
  }
  return result;
}

std::vector<glm::vec3> AssimpModelLoader::LoadTangents(const aiMesh *mesh) {
  if (!mesh->HasTangentsAndBitangents()) {
    std::vector<glm::vec3> dummy;
    return dummy;
  }
  return LoadVectorData(mesh->mTangents, mesh->mNumVertices);
}

std::vector<glm::vec3> AssimpModelLoader::LoadBitangents(const aiMesh *mesh) {
  if (!mesh->HasTangentsAndBitangents()) {
    std::vector<glm::vec3> dummy;
    return dummy;
  }
  return LoadVectorData(mesh->mBitangents, mesh->mNumVertices);
}

std::vector<glm::vec2> AssimpModelLoader::LoadTextureCoords(
    const aiMesh *mesh) {
  std::vector<glm::vec2> result;
  if (!mesh->HasTextureCoords(0)) {
    // since our shaders currently require texture coordinates to exist, we
    // load dummy ones
    return std::vector<glm::vec2>(mesh->mNumVertices, glm::vec2());
  }
  for (unsigned int i = 0; i < mesh->mNumVertices; i++) {
    result.push_back(glm::vec2(
        // Note: Assimp allows for multiple texture coordinates per vertex.
        // Currently only the first set is loaded, the others are ignored.
        static_cast<float>(mesh->mTextureCoords[0][i][0]),
        static_cast<float>(mesh->mTextureCoords[0][i][1])));
  }
  return result;
}

std::vector<glm::vec3> AssimpModelLoader::LoadNormals(const aiMesh *mesh) {
  return LoadVectorData(mesh->mNormals, mesh->mNumVertices);
}

unsigned int AssimpModelLoader::GetSceneMaterialIndex(
    const aiScene *scene, const std::string &material_name) {
  if (material_name.empty() == true) {
    info("No material name defined, returning first material in file.");
    return 0;
  }
  for (unsigned int i = 0; i < scene->mNumMaterials; ++i) {
    if (GetSceneMaterialNameFromIndex(scene, i) == material_name) {
      return i;
    }
  }

  warn("No Material named {} found, returning material with index 0 instead.",
       material_name);
  return 0;
}

std::string AssimpModelLoader::GetSceneMaterialNameFromIndex(
    const aiScene *scene, const unsigned int mesh_index) {
  aiString mat_name;
  scene->mMaterials[mesh_index]->Get(AI_MATKEY_NAME, mat_name);
  return std::string(mat_name.C_Str());
}

bool AssimpModelLoader::LoadFile(const std::string &filename) {
  info("Load Model: {}", filename);
  const aiScene *scene = importer_.ReadFile(
      filename, aiProcess_Triangulate | aiProcess_GenSmoothNormals |
                    aiProcess_JoinIdenticalVertices | aiProcess_SortByPType);
  std::cout << std::endl;
  if (!scene) {
    phx::warn("Error loading model file \"{}\".", filename);
    return false;
  }
  info("Finished Loading Model");
  last_loaded_scene_ = filename;
  last_material_index_ = kInvalidMaterialIndex;
  return true;
}

AssimpModelLoader::~AssimpModelLoader() {
  importer_.SetProgressHandler(nullptr);
}

void AssimpModelLoader::SetProgressUpdateCallback(
    const std::function<void(float)> &callback) {
  on_progress_update_callback_ = callback;
}

bool AssimpModelLoader::AssimpProgressHandler::Update(float percentage) {
  if (percentage < 0.0f) return true;
  if (percentage > 1.0f) percentage = 1.0f;
  const int precision = 50;
  const int progress = static_cast<int>(percentage * precision);
  std::cout << "\r[" << std::string(static_cast<std::size_t>(progress), '=')
            << std::string(static_cast<std::size_t>(precision - progress), ' ')
            << "] (" << (progress * 100 / precision) << "%) ";

  if (parent_ != nullptr && parent_->on_progress_update_callback_ != nullptr)
    parent_->on_progress_update_callback_(percentage);

  return true;
}
std::array<glm::vec3, 2> AssimpModelLoader::ComputeBoundingBox(
    const std::vector<glm::vec3> &vertices) {
  std::array<glm::vec3, 2> boundingbox = {
      {glm::vec3(INFINITY), glm::vec3(-INFINITY)}};
  for (auto &vertex : vertices) {
    for (int i = 0; i < 3; i++) {
      if (vertex[i] < boundingbox[0][i]) {
        boundingbox[0][i] = vertex[i];
      } else if (vertex[i] > boundingbox[1][i]) {
        boundingbox[1][i] = vertex[i];
      }
    }
  }
  return boundingbox;
}

}  // namespace phx

//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef LIBRARY_PHX_RESOURCES_LOADERS_OPENVR_RESOURCE_LOADER_HPP_
#define LIBRARY_PHX_RESOURCES_LOADERS_OPENVR_RESOURCE_LOADER_HPP_

#include <memory>
#include <vector>

#include "phx/suppress_warnings.hpp"

SUPPRESS_WARNINGS_BEGIN
#include "glm/glm.hpp"
SUPPRESS_WARNINGS_END

#include "phx/export.hpp"
#include "phx/input/device_system.hpp"
#include "phx/input/vr_controller.hpp"
#include "phx/resources/resource_declaration.hpp"
#include "phx/resources/resource_load_strategy.hpp"
#include "phx/resources/types/mesh.hpp"

namespace phx {
class Mesh;

class PHOENIX_EXPORT OpenVRResourceLoader final : public ResourceLoadStrategy {
 public:
  explicit OpenVRResourceLoader(DeviceSystem *device_system);
  OpenVRResourceLoader(const OpenVRResourceLoader &) = delete;
  OpenVRResourceLoader(OpenVRResourceLoader &&) = delete;
  ~OpenVRResourceLoader() override = default;

  OpenVRResourceLoader &operator=(const OpenVRResourceLoader &) = delete;
  OpenVRResourceLoader &operator=(OpenVRResourceLoader &&) = delete;

  std::unique_ptr<Resource> Load(const ResourceDeclaration &file_name) override;

  vr::RenderModel_t *GetModel(VRController::ControllerSide side);
  std::unique_ptr<Mesh> GetMesh(VRController::ControllerSide side);
  std::unique_ptr<Material> GetMaterial(VRController::ControllerSide side);
  std::unique_ptr<Image> GetTexture(int id);

  DeviceSystem *device_system_ = nullptr;
};

}  // namespace phx

#endif  // LIBRARY_PHX_RESOURCES_LOADERS_OPENVR_RESOURCE_LOADER_HPP_

//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include "phx/resources/loaders/openvr_resource_loader.hpp"

#include <algorithm>
#include <memory>
#include <string>
#include <utility>
#include <vector>

#include "phx/core/logger.hpp"
#include "phx/input/device_system.hpp"
#include "phx/input/vr_controller.hpp"
#include "phx/resources/resource_manager.hpp"
#include "phx/resources/types/material.hpp"

namespace phx {

OpenVRResourceLoader::OpenVRResourceLoader(DeviceSystem* device_system)
    : device_system_(device_system) {}

std::unique_ptr<phx::Resource> OpenVRResourceLoader::Load(
    const ResourceDeclaration& declaration) {
  if (declaration.find("OpenVR_type") == declaration.end() ||
      !declaration["OpenVR_type"].is_string()) {
    warn(
        "declaration {} cannot be parsed by OpenVRResourceLoader, it does not "
        "contain a OpenVR_type key",
        declaration.dump());
    return nullptr;
  }
  std::string type = declaration["OpenVR_type"];
  VRController::ControllerSide controller_side = VRController::LEFT_CONTROLLER;
  if (declaration.find("side") != declaration.end() &&
      declaration["side"] == "right") {
    controller_side = VRController::RIGHT_CONTROLLER;
  }
  // otherwise default left

  if (type == "material") {
    return GetMaterial(controller_side);
  }
  if (type == "texture") {
    int texture_id = 0;
    if (declaration.find("texture_id") == declaration.end() ||
        !declaration["texture_id"].is_number_integer()) {
      warn(
          "declaration {} cannot be parsed by OpenvrResourceLoader, it does "
          "not contain a texture_id key",
          declaration.dump());
    } else {
      texture_id = declaration["texture_id"];
    }
    return GetTexture(texture_id);
  }
  if (type == "mesh") {
    return GetMesh(controller_side);
  }
  warn(
      "OpenVRResource with OpenVR_type {}, cannot be loaded. Full declaration: "
      "{}",
      type, declaration.dump());
  return nullptr;
}

vr::RenderModel_t* OpenVRResourceLoader::GetModel(
    VRController::ControllerSide side) {
  for (auto controller : device_system_->GetDevices<VRController>()) {
    if (controller->GetSide() == side) return controller->GetModel();
  }

  warn(
      "[OpenVRResourceLoader::GetModel] unable to find controller with side {}",
      side == VRController::RIGHT_CONTROLLER ? "right" : "left");
  return nullptr;
}

std::unique_ptr<phx::Mesh> OpenVRResourceLoader::GetMesh(
    VRController::ControllerSide side) {
  auto model = GetModel(side);
  if (model == nullptr) {
    return nullptr;
  }
  auto mesh = std::make_unique<phx::Mesh>();
  std::vector<glm::vec3> vertices;
  std::vector<glm::vec3> normals;
  std::vector<glm::vec2> texcoords;
  for (std::size_t i = 0; i < model->unVertexCount; i++) {
    vertices.push_back(glm::vec3(model->rVertexData[i].vPosition.v[0],
                                 model->rVertexData[i].vPosition.v[1],
                                 model->rVertexData[i].vPosition.v[2]));
    normals.push_back(glm::vec3(model->rVertexData[i].vNormal.v[0],
                                model->rVertexData[i].vNormal.v[1],
                                model->rVertexData[i].vNormal.v[2]));
    texcoords.push_back(glm::vec2(model->rVertexData[i].rfTextureCoord[0],
                                  model->rVertexData[i].rfTextureCoord[1]));
  }
  std::vector<unsigned int> indices;
  for (std::size_t i = 0; i < model->unTriangleCount * 3; i++) {
    indices.push_back(model->rIndexData[i]);
  }
  mesh->SetVertices(std::move(vertices));
  mesh->SetNormals(std::move(normals));
  mesh->SetTextureCoords(std::move(texcoords));
  mesh->SetIndices(std::move(indices));
  return mesh;
}

std::unique_ptr<Material> OpenVRResourceLoader::GetMaterial(
    VRController::ControllerSide side) {
  auto model = GetModel(side);
  if (model == nullptr) return nullptr;

  auto material = std::make_unique<phx::Material>();
  material->SetAmbientColor(glm::vec3(0.1, 0.1, 0.1));
  material->SetSpecularColor(glm::vec3(0.3, 0.3, 0.3));

  auto texture = ResourceManager::instance().DeclareResource<Image>(
      {{"TYPE", "openVR"},
       {"OpenVR_type", "texture"},
       {"texture_id", model->diffuseTextureId},
       {"side", side == VRController::LEFT_CONTROLLER ? "left" : "right"}});
  texture.Load();
  material->SetDiffuseImage(texture);

  return material;
}

std::unique_ptr<Image> OpenVRResourceLoader::GetTexture(int id) {
  vr::RenderModel_TextureMap_t* texture_map;
  while (vr::VRRenderModels()->LoadTexture_Async(id, &texture_map) ==
         vr::VRRenderModelError_Loading) {
    std::this_thread::sleep_for(std::chrono::milliseconds(50));
  }
  if (texture_map == nullptr) {
    return nullptr;
  }

  std::vector<unsigned char> image_data(texture_map->unWidth *
                                        texture_map->unHeight * 4);
  std::copy(texture_map->rubTextureMapData,
            texture_map->rubTextureMapData + image_data.size(),
            image_data.begin());
  auto image = std::make_unique<phx::Image>(
      &image_data[0],
      std::array<std::size_t, 2>{
          {static_cast<std::size_t>(texture_map->unWidth),
           static_cast<std::size_t>(texture_map->unHeight)}},
      32);

  return image;
}

}  // namespace phx

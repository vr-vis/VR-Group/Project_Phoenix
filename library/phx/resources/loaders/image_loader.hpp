//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef LIBRARY_PHX_RESOURCES_LOADERS_IMAGE_LOADER_HPP_
#define LIBRARY_PHX_RESOURCES_LOADERS_IMAGE_LOADER_HPP_

#include <memory>
#include <string>
#include <vector>

#include "phx/resources/resource_load_strategy.hpp"

namespace phx {
class ImageLoader final : public ResourceLoadStrategy {
 public:
  ImageLoader() = default;
  ImageLoader(const ImageLoader &) = delete;
  ImageLoader(ImageLoader &&) = delete;
  ~ImageLoader() override = default;

  ImageLoader &operator=(const ImageLoader &) = delete;
  ImageLoader &operator=(ImageLoader &&) = delete;

  std::unique_ptr<Resource> Load(
      const ResourceDeclaration &declaration) override;
};
}  // namespace phx

#endif  // LIBRARY_PHX_RESOURCES_LOADERS_IMAGE_LOADER_HPP_

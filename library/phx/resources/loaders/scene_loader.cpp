//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include "phx/resources/loaders/scene_loader.hpp"

#include <string>

#include "phx/rendering/components/material_handle.hpp"
#include "phx/rendering/components/mesh_handle.hpp"
#include "phx/rendering/components/transform.hpp"
#include "phx/resources/types/model.hpp"
#include "phx/resources/resource_manager.hpp"
#include "phx/resources/resource_utils.hpp"

namespace phx {

phx::Entity* SceneLoader::InsertModelIntoScene(const std::string& file_name,
                                               Scene* scene) {
  auto model = ResourceUtils::LoadResourceFromFile<Model>(file_name);
  if (model == nullptr) {
    return nullptr;
  }

  auto rootEntity = scene->CreateEntity();
  rootEntity->SetName(ConstructRootName(file_name));
  auto root_transform = rootEntity->AddComponent<Transform>();

  for (auto mesh : model->GetMeshes()) {
    auto entity = scene->CreateEntity();
    entity->AddComponent<Transform>()->SetParent(root_transform);
    entity->AddComponent<MeshHandle>()->SetMesh(mesh);
    ResourcePointer<Material> material = model->GetMaterialForMesh(mesh);
    if (material != nullptr) {
      entity->AddComponent<MaterialHandle>()->SetMaterial(material);
    }
  }

  return rootEntity;
}

std::string SceneLoader::ConstructRootName(const std::string& file_name) {
  const auto last_slash = file_name.find_last_of('/');
  const auto last_dot = file_name.find_last_of('.');
  const std::size_t model_name_first_pos =
      last_slash == std::string::npos ? 0 : last_slash + 1;
  const std::size_t model_name_last_pos =
      last_dot == std::string::npos ? file_name.length() - 1 : last_dot;
  return file_name.substr(model_name_first_pos,
                          model_name_last_pos - model_name_first_pos);
}

}  // namespace phx

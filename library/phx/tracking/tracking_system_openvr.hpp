//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef LIBRARY_PHX_TRACKING_TRACKING_SYSTEM_OPENVR_HPP_
#define LIBRARY_PHX_TRACKING_TRACKING_SYSTEM_OPENVR_HPP_

#include <memory>
#include <string>

#include "phx/suppress_warnings.hpp"

#include "phx/core/engine.hpp"
#include "phx/core/system.hpp"
#include "phx/export.hpp"
#include "phx/input/vr_controller.hpp"

SUPPRESS_WARNINGS_BEGIN
#define BOOST_BIND_NO_PLACEHOLDERS
#include "boost/signals2/connection.hpp"
SUPPRESS_WARNINGS_END

namespace phx {
class DeviceSystem;

class PHOENIX_EXPORT TrackingSystemOpenVR : public System {
 public:
  TrackingSystemOpenVR() = delete;
  TrackingSystemOpenVR(const TrackingSystemOpenVR&) = delete;
  TrackingSystemOpenVR(TrackingSystemOpenVR&&) = default;
  ~TrackingSystemOpenVR() override;

  TrackingSystemOpenVR& operator=(const TrackingSystemOpenVR&) = delete;
  TrackingSystemOpenVR& operator=(TrackingSystemOpenVR&&) = default;

  void Update(const FrameTimer::TimeInfo&) override;

  std::string ToString() const override;

 protected:
  TrackingSystemOpenVR(Engine* engine, DeviceSystem* device_system);

 private:
  template <typename SystemType, typename... SystemArguments>
  friend SystemType* Engine::CreateSystem(SystemArguments&&... arguments);

  void CreateRuntimeEntities(Scene* scene);
  void RemoveRuntimeEntities(Scene* scene);
  void OnSceneChanged(std::shared_ptr<Scene> old_scene,
                      std::shared_ptr<Scene> new_scene);

  VRController* GetController(VRController::ControllerSide side);

  Entity* hmd_entity_ = nullptr;
  Entity* left_eye_entity_ = nullptr;
  Entity* right_eye_entity_ = nullptr;
  Entity* left_controller_entity_ = nullptr;
  Entity* right_controller_entity_ = nullptr;

  boost::signals2::connection scene_changed_connection_;
};

}  // namespace phx

#endif  // LIBRARY_PHX_TRACKING_TRACKING_SYSTEM_OPENVR_HPP_

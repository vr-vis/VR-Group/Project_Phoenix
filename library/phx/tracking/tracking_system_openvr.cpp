//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include "phx/tracking/tracking_system_openvr.hpp"

#include <memory>
#include <string>

#include "phx/core/runtime_component.hpp"
#include "phx/display/display_system_openvr.hpp"
#include "phx/display/hmd.hpp"
#include "phx/input/device_system.hpp"
#include "phx/input/vr_controller.hpp"
#include "phx/rendering/components/projection.hpp"
#include "phx/rendering/components/transform.hpp"

namespace phx {

TrackingSystemOpenVR::TrackingSystemOpenVR(Engine* engine,
                                           DeviceSystem* device_system)
    : System(engine) {
  if (device_system->GetDevices<HMD>().size() != 0) {
    CreateRuntimeEntities(engine_->GetScene().get());
    scene_changed_connection_ = engine_->AddSceneChangedCallback(
        [this](std::shared_ptr<Scene> old_scene,
               std::shared_ptr<Scene> new_scene) {
          OnSceneChanged(old_scene, new_scene);
        });
  }
}

TrackingSystemOpenVR::~TrackingSystemOpenVR() {
  scene_changed_connection_.disconnect();
}

void TrackingSystemOpenVR::Update(const FrameTimer::TimeInfo&) {
  const auto device_system = engine_->GetSystem<DeviceSystem>();
  if (device_system == nullptr) {
    return;
  }

  auto hmds = device_system->GetDevices<HMD>();
  if (hmds.size() != 1) return;
  auto hmd = hmds[0];

  hmd_entity_->GetFirstComponent<Transform>()->SetLocalMatrix(hmd->GetPose());
  left_eye_entity_->GetFirstComponent<Transform>()->SetLocalMatrix(
      hmd->GetEyeToHeadMatrix(HMD::LEFT_EYE));
  right_eye_entity_->GetFirstComponent<Transform>()->SetLocalMatrix(
      hmd->GetEyeToHeadMatrix(HMD::RIGHT_EYE));

  left_controller_entity_->GetFirstComponent<Transform>()->SetLocalMatrix(
      GetController(VRController::LEFT_CONTROLLER)->GetPose());

  right_controller_entity_->GetFirstComponent<Transform>()->SetLocalMatrix(
      GetController(VRController::RIGHT_CONTROLLER)->GetPose());
}

void TrackingSystemOpenVR::CreateRuntimeEntities(Scene* scene) {
  const auto device_system = engine_->GetSystem<DeviceSystem>();
  if (device_system == nullptr || scene == nullptr) {
    return;
  }

  const auto virtual_platforms = scene->GetEntitiesWithComponents<
      phx::RuntimeComponent<phx::USER_PLATFORM>>();
  if (!virtual_platforms.empty()) {
    const auto virtual_platform = virtual_platforms[0];
    const auto virtual_platform_transform =
        virtual_platform->GetFirstComponent<Transform>();

    hmd_entity_ = scene->CreateEntity();
    hmd_entity_->AddComponent<RuntimeComponent<HEAD>>();
    auto hmd_transform = hmd_entity_->AddComponent<Transform>();
    hmd_transform->SetParent(virtual_platform_transform);

    left_eye_entity_ = scene->CreateEntity();
    left_eye_entity_->AddComponent<RuntimeComponent<LEFT_EYE>>();
    auto left_eye_transform = left_eye_entity_->AddComponent<Transform>();
    left_eye_transform->SetParent(hmd_transform);
    left_eye_entity_->AddComponent<Projection>();

    right_eye_entity_ = scene->CreateEntity();
    right_eye_entity_->AddComponent<RuntimeComponent<RIGHT_EYE>>();
    auto right_eye_transform = right_eye_entity_->AddComponent<Transform>();
    right_eye_transform->SetParent(hmd_transform);
    right_eye_entity_->AddComponent<Projection>();

    left_controller_entity_ = scene->CreateEntity();
    left_controller_entity_->AddComponent<RuntimeComponent<LEFT_CONTROLLER>>();
    auto left_controller_transform =
        left_controller_entity_->AddComponent<Transform>();
    left_controller_transform->SetParent(virtual_platform_transform);

    right_controller_entity_ = scene->CreateEntity();
    right_controller_entity_
        ->AddComponent<RuntimeComponent<RIGHT_CONTROLLER>>();
    auto right_controller_transform =
        right_controller_entity_->AddComponent<Transform>();
    right_controller_transform->SetParent(virtual_platform_transform);
  }
}

void TrackingSystemOpenVR::RemoveRuntimeEntities(Scene* scene) {
  scene->RemoveEntity(hmd_entity_);
  hmd_entity_ = nullptr;
  scene->RemoveEntity(left_eye_entity_);
  left_eye_entity_ = nullptr;
  scene->RemoveEntity(right_eye_entity_);
  right_eye_entity_ = nullptr;
  scene->RemoveEntity(left_controller_entity_);
  left_controller_entity_ = nullptr;
  scene->RemoveEntity(right_controller_entity_);
  right_controller_entity_ = nullptr;
}

void TrackingSystemOpenVR::OnSceneChanged(std::shared_ptr<Scene> old_scene,
                                          std::shared_ptr<Scene> new_scene) {
  RemoveRuntimeEntities(old_scene.get());
  CreateRuntimeEntities(new_scene.get());
}

VRController* TrackingSystemOpenVR::GetController(
    VRController::ControllerSide side) {
  for (auto controller :
       engine_->GetSystem<DeviceSystem>()->GetDevices<VRController>()) {
    if (controller->GetSide() == side) {
      return controller;
    }
  }
  return nullptr;
}

std::string TrackingSystemOpenVR::ToString() const { return "Tracking System"; }

}  // namespace phx

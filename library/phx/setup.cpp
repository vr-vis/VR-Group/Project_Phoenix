//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include "setup.hpp"

#include <cassert>
#include <memory>
#include <string>
#include <utility>

#include "input/device_system.hpp"
#include "input/vr_controller.hpp"
#include "phx/core/component.hpp"
#include "phx/core/engine.hpp"
#include "phx/core/logger.hpp"
#include "phx/core/runtime_component.hpp"
#include "phx/core/scene.hpp"
#include "phx/display/display_system_openvr.hpp"
#include "phx/display/display_system_window.hpp"
#include "phx/display/hmd.hpp"
#include "phx/input/input_system.hpp"
#include "phx/input/openvr_controller_model_system.hpp"
#include "phx/rendering/backend/render_target.hpp"
#include "phx/rendering/render_passes/blit_pass.hpp"
#include "phx/rendering/render_passes/clear_pass.hpp"
#include "phx/rendering/rendering_system.hpp"
#include "phx/scripting/behavior_system.hpp"
#include "phx/tracking/tracking_system_openvr.hpp"

#undef CreateWindow
namespace phx {

std::unique_ptr<Engine> Setup::CreateDefaultEngine(bool use_hmd_if_available) {
  auto engine = std::make_unique<Engine>();
  auto engine_ptr = engine.get();
  engine->SetScene(std::make_shared<Scene>());

  auto behavior_system = engine->CreateSystem<BehaviorSystem>();
  engine->CreateSystem<InputSystem>()->AddQuitCallback(
      [engine_ptr]() { engine_ptr->Stop(); });

  auto device_system = engine->CreateSystem<DeviceSystem>();

  auto displaysys_window = engine->CreateSystem<DisplaySystemWindow>();
  DisplaySystemOpenVR* displaysys_openVR = nullptr;
  bool using_hmd = false;

  if (HMD::IsHMDPresent() && use_hmd_if_available) {
    info("An HMD is present so we use it");
    using_hmd = true;

    device_system->AddDevice<HMD>();
    device_system->AddDevice<VRController>(VRController::LEFT_CONTROLLER);
    device_system->AddDevice<VRController>(VRController::RIGHT_CONTROLLER);

    displaysys_openVR = engine->CreateSystem<DisplaySystemOpenVR>();
  }

  const std::string window_title{using_hmd ? "Phoenix -- HMD Companion"
                                           : "Phoenix"};
  const glm::uvec2 window_position{100, 100};
  const glm::uvec2 window_size{1024, 768};
  displaysys_window->CreateWindow(window_title, window_position, window_size);

  auto rendering_system =
      engine->CreateSystem<RenderingSystem>(engine->GetSystem<DisplaySystem>());

  // fix update order
  engine->MoveSystemToBack(displaysys_window);
  if (displaysys_openVR != nullptr) engine->MoveSystemToBack(displaysys_openVR);
  engine->MoveSystemBefore(rendering_system, displaysys_window);
  engine->MoveSystemBefore(device_system, rendering_system);

  // setup rendering and frame graph
  if (HMD::IsHMDPresent() && use_hmd_if_available) {
    auto controller_model_system =
        engine->CreateSystem<OpenVRControllerModelSystem>(
            engine->GetSystem<DeviceSystem>());
    auto tracking_system = engine->CreateSystem<TrackingSystemOpenVR>(
        engine->GetSystem<DeviceSystem>());

    displaysys_openVR->CreateRenderTargets(engine->GetScene().get());
    SetupDefaultFrameGraphOpenVR(rendering_system, engine.get());

    engine->MoveSystemBefore(tracking_system, rendering_system);
    engine->MoveSystemAfter(behavior_system, tracking_system);
    engine->MoveSystemAfter(controller_model_system, tracking_system);
  } else {
    displaysys_window->CreateRenderTarget(engine->GetScene().get(), 68.0f,
                                          0.01f, 1000.0f);
    SetupDefaultFrameGraphWindow(rendering_system, engine.get());
  }

  return engine;
}

void Setup::SetupDefaultFrameGraphWindow(RenderingSystem* rendering_system,
                                         Engine* engine) {
  auto frame_graph = std::make_unique<FrameGraph>();
  auto render_target = engine->GetScene()
                           ->GetEntitiesWithComponents<RenderTarget>()[0]
                           ->GetFirstComponent<RenderTarget>();
  if (!render_target) {
    error("Cannot setup default frame graph (window): no render targets.");
    return;
  }

  frame_graph->AddRenderPass(std::make_unique<ClearPass>(render_target));
  frame_graph->AddRenderPass(std::make_unique<GeometryPass>(render_target));
  frame_graph->AddRenderPass(std::make_unique<BlitPass>(render_target));

  frame_graph->Initialize();

  rendering_system->SetFrameGraph(std::move(frame_graph));
}

void Setup::SetupDefaultFrameGraphOpenVR(RenderingSystem* rendering_system,
                                         Engine* engine) {
  auto frame_graph = std::make_unique<FrameGraph>();
  auto left_render_target =
      engine->GetScene()
          ->GetEntitiesWithComponents<RuntimeComponent<LEFT_EYE>>()[0]
          ->GetFirstComponent<RenderTarget>();
  auto right_render_target =
      engine->GetScene()
          ->GetEntitiesWithComponents<RuntimeComponent<RIGHT_EYE>>()[0]
          ->GetFirstComponent<RenderTarget>();
  if (!left_render_target || !right_render_target) {
    error(
        "Cannot setup default frame graph (OpenVR): not enough render "
        "targets.");
    return;
  }

  frame_graph->AddRenderPass(std::make_unique<ClearPass>(right_render_target));
  frame_graph->AddRenderPass(std::make_unique<ClearPass>(left_render_target));
  frame_graph->AddRenderPass(
      std::make_unique<GeometryPass>(right_render_target));
  frame_graph->AddRenderPass(
      std::make_unique<GeometryPass>(left_render_target));

  frame_graph->AddRenderPass(std::make_unique<BlitPass>(right_render_target));

  frame_graph->Initialize();

  rendering_system->SetFrameGraph(std::move(frame_graph));
}

}  // namespace phx

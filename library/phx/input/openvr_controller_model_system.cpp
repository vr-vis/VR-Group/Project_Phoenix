//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include "phx/input/openvr_controller_model_system.hpp"

#include <memory>
#include <string>
#include <utility>

#include "phx/core/logger.hpp"
#include "phx/display/display_system_openvr.hpp"
#include "phx/input/openvr_controller_behavior.hpp"
#include "phx/input/vr_controller.hpp"
#include "phx/rendering/components/material_handle.hpp"
#include "phx/rendering/components/mesh_handle.hpp"
#include "phx/rendering/components/transform.hpp"
#include "phx/resources/loaders/openvr_resource_loader.hpp"
#include "phx/resources/resource_manager.hpp"
#include "phx/resources/resource_pointer.hpp"

namespace phx {

OpenVRControllerModelSystem::OpenVRControllerModelSystem(
    Engine* engine, DeviceSystem* device_system)
    : System(engine), device_system_(device_system) {
  auto& resource_manager = ResourceManager::instance();
  auto openvr_loader = std::make_unique<OpenVRResourceLoader>(device_system);
  resource_manager.RegisterResourceType("openVR", std::move(openvr_loader));
}

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wmissing-prototypes"
#endif

Entity* OpenVRControllerModelSystem::AddController(
    phx::Scene* scene, VRController::ControllerSide side) {
  auto& resource_manager = ResourceManager::instance();
  std::string side_string =
      side == VRController::LEFT_CONTROLLER ? "left" : "right";

  ResourceDeclaration mesh_declaration{
      {"TYPE", "openVR"}, {"OpenVR_type", "mesh"}, {"side", side_string}};
  auto mesh =
      ResourceManager::instance().DeclareResource<Mesh>(mesh_declaration);
  mesh.Load();

  ResourceDeclaration material_declaration{
      {"TYPE", "openVR"}, {"OpenVR_type", "material"}, {"side", side_string}};
  auto material =
      resource_manager.DeclareResource<Material>(material_declaration);
  material.Load();

  if (mesh != nullptr) {
    return AddControllerEntity(scene, mesh, material, side);
  }
  return nullptr;
}

Entity* OpenVRControllerModelSystem::AddControllerEntity(
    phx::Scene* scene, ResourcePointer<Mesh> mesh,
    ResourcePointer<Material> material, VRController::ControllerSide side) {
  Entity* controller = scene->CreateEntity();
  controller->AddComponent<MeshHandle>()->SetMesh(mesh);
  controller->AddComponent<Transform>();
  controller->AddComponent<MaterialHandle>()->SetMaterial(material);
  controller->AddComponent<OpenVRControllerBehavior>()->SetSide(side);

  return controller;
}

#ifdef __clang__
#pragma clang diagnostic pop
#endif

void OpenVRControllerModelSystem::Update(const FrameTimer::TimeInfo&) {
  // check which controllers are active and update their scene representation,
  // if necessary
  auto scene = GetEngine()->GetScene();
  if (scene == nullptr) return;

  // get controller entities in the scene
  Entity* left_controller_entity = nullptr;
  Entity* right_controller_entity = nullptr;
  auto controller_entities =
      GetEngine()->GetEntitiesWithComponents<OpenVRControllerBehavior>();
  for (auto entity : controller_entities) {
    if (entity->GetFirstComponent<OpenVRControllerBehavior>()->GetSide() ==
        VRController::LEFT_CONTROLLER) {
      left_controller_entity = entity;
    } else {
      right_controller_entity = entity;
    }
  }

  bool left_controller_active = false;
  bool right_controller_active = false;

  for (auto controller : device_system_->GetDevices<VRController>()) {
    if (!controller->IsActive()) continue;
    if (controller->GetSide() == VRController::LEFT_CONTROLLER) {
      // do we have a left controller in the scene?
      if (left_controller_entity == nullptr) {
        // create that controller
        left_controller_entity =
            AddController(scene.get(), VRController::LEFT_CONTROLLER);
      }
      left_controller_active = true;
    } else if (controller->GetSide() == VRController::RIGHT_CONTROLLER) {
      if (right_controller_entity == nullptr) {
        right_controller_entity =
            AddController(scene.get(), VRController::RIGHT_CONTROLLER);
      }
      right_controller_active = true;
    }
  }

  // remove unnecessary entities
  if (!left_controller_active && left_controller_entity != nullptr) {
    scene->RemoveEntity(left_controller_entity);
  }
  if (!right_controller_active && right_controller_entity != nullptr) {
    scene->RemoveEntity(right_controller_entity);
  }
}

}  // namespace phx

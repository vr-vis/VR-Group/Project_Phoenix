//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef LIBRARY_PHX_INPUT_TRACKED_DEVICE_HPP_
#define LIBRARY_PHX_INPUT_TRACKED_DEVICE_HPP_

#include <vector>

#include "phx/input/device.hpp"
#include "phx/resources/types/material.hpp"
#include "phx/resources/types/mesh.hpp"
#include "phx/suppress_warnings.hpp"

SUPPRESS_WARNINGS_BEGIN
#include "glm/mat4x4.hpp"

#include "openvr.h"  //NOLINT
SUPPRESS_WARNINGS_END

#include "phx/export.hpp"

namespace phx {

class PHOENIX_EXPORT TrackedDevice : public Device {
 public:
  TrackedDevice();
  virtual ~TrackedDevice();

  virtual void Update();

  glm::mat4 GetPose() const;
  bool IsActive() const;

  virtual void UpdateDeviceIndex() = 0;
  virtual void OnOpenVREvent(const vr::VREvent_t& event) = 0;

 protected:
  class OpenVREventDistributer {
   public:
    void Update();
    void AddDevice(TrackedDevice* device);
    void RemoveDevice(TrackedDevice* device);

   private:
    std::vector<TrackedDevice*> tracked_devices_;
  };

  static vr::IVRSystem* vr_system_;
  static OpenVREventDistributer vr_event_distributer;
  static std::vector<vr::TrackedDevicePose_t> last_wait_get_poses_;
  glm::mat4 pose_;

  static glm::mat4 TransformToGlmMatrix(const vr::HmdMatrix34_t& mat);
  static glm::mat4 TransformToGlmMatrix(const vr::HmdMatrix44_t& mat);

  std::vector<vr::TrackedDeviceIndex_t> GetDeviceIndicesForClass(
      vr::ETrackedDeviceClass device_class);

  vr::TrackedDeviceIndex_t id_ = vr::k_unTrackedDeviceIndexInvalid;

 private:
  static int reference_counter_;
  static std::mutex openVR_init_mutex_;
};

}  // namespace phx

#endif  // LIBRARY_PHX_INPUT_TRACKED_DEVICE_HPP_

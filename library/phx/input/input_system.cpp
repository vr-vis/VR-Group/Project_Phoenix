//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include "phx/input/input_system.hpp"

#include <string>

#include "phx/core/logger.hpp"
#include "phx/rendering/rendering_system.hpp"
#include "phx/suppress_warnings.hpp"

SUPPRESS_WARNINGS_BEGIN
#include "SDL.h"
SUPPRESS_WARNINGS_END

namespace phx {
InputSystem::InputSystem(Engine* engine) : System(engine) {
  SDL_Init(SDL_INIT_EVENTS);
}

InputSystem::~InputSystem() { SDL_QuitSubSystem(SDL_INIT_EVENTS); }

void InputSystem::Update(const FrameTimer::TimeInfo&) {
  UpdateSDLEvents();
}

boost::signals2::connection InputSystem::AddQuitCallback(
    const std::function<void()>& callback) {
  return quit_signal_.connect(callback);
}

boost::signals2::connection InputSystem::AddKeyPressCallback(
    const std::function<void(char)>& callback) {
  return key_press_signal_.connect(callback);
}

boost::signals2::connection InputSystem::AddKeyReleaseCallback(
    const std::function<void(char)>& callback) {
  return key_release_signal_.connect(callback);
}

boost::signals2::connection InputSystem::AddMouseMoveCallback(
    const std::function<void(int, int)>& callback) {
  return mouse_move_signal_.connect(callback);
}

boost::signals2::connection InputSystem::AddMousePressCallback(
    const std::function<void(unsigned)>& callback) {
  return mouse_press_signal_.connect(callback);
}

boost::signals2::connection InputSystem::AddMouseReleaseCallback(
    const std::function<void(unsigned)>& callback) {
  return mouse_release_signal_.connect(callback);
}

std::string InputSystem::ToString() const { return "InputSystem"; }

void InputSystem::UpdateSDLEvents() {
  SDL_Event event;
  while (SDL_PollEvent(&event) != 0) {
    switch (event.type) {
      case SDL_QUIT:
        quit_signal_();
        break;
      case SDL_KEYDOWN:
        key_press_signal_(static_cast<char>(event.key.keysym.sym));
        break;
      case SDL_KEYUP:
        key_release_signal_(static_cast<char>(event.key.keysym.sym));
        break;
      case SDL_MOUSEMOTION:
        mouse_move_signal_(static_cast<int>(event.motion.xrel),
                           static_cast<int>(event.motion.yrel));
        break;
      case SDL_MOUSEBUTTONDOWN:
        mouse_press_signal_(event.button.button);
        break;
      case SDL_MOUSEBUTTONUP:
        mouse_release_signal_(event.button.button);
        break;
      default:
        break;
    }
  }
}

}  // namespace phx

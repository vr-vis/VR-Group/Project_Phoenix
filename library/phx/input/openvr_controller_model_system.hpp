//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef LIBRARY_PHX_INPUT_OPENVR_CONTROLLER_MODEL_SYSTEM_HPP_
#define LIBRARY_PHX_INPUT_OPENVR_CONTROLLER_MODEL_SYSTEM_HPP_

#include <memory>
#include <vector>

#include "phx/suppress_warnings.hpp"

SUPPRESS_WARNINGS_BEGIN
#include "glm/glm.hpp"
SUPPRESS_WARNINGS_END

#include "phx/core/engine.hpp"
#include "phx/core/system.hpp"
#include "phx/display/hmd.hpp"
#include "phx/export.hpp"
#include "phx/input/device_system.hpp"
#include "phx/input/openvr_controller_behavior.hpp"
#include "phx/input/vr_controller.hpp"

namespace phx {
class PHOENIX_EXPORT OpenVRControllerModelSystem : public System {
 public:
  OpenVRControllerModelSystem() = delete;
  OpenVRControllerModelSystem(const OpenVRControllerModelSystem&) = delete;
  OpenVRControllerModelSystem(OpenVRControllerModelSystem&&) = default;
  ~OpenVRControllerModelSystem() override = default;

  OpenVRControllerModelSystem& operator=(const OpenVRControllerModelSystem&) =
      delete;
  OpenVRControllerModelSystem& operator=(OpenVRControllerModelSystem&&) =
      default;

  void Update(const FrameTimer::TimeInfo&) override;

 protected:
  template <typename SystemType, typename... SystemArguments>
  friend SystemType* Engine::CreateSystem(SystemArguments&&... arguments);
  OpenVRControllerModelSystem(Engine* engine, DeviceSystem* device_system);

 private:
  static Entity* AddControllerEntity(phx::Scene* scene,
                                     ResourcePointer<Mesh> mesh,
                                     ResourcePointer<Material> material,
                                     VRController::ControllerSide side);
  static Entity* AddController(phx::Scene* scene,
                               VRController::ControllerSide side);

  DeviceSystem* device_system_ = nullptr;
};

}  // namespace phx

#endif  // LIBRARY_PHX_INPUT_OPENVR_CONTROLLER_MODEL_SYSTEM_HPP_

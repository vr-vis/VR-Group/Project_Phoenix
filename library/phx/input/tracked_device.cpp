//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include "phx/input/tracked_device.hpp"

#include <vector>

#include "phx/core/entity.hpp"
#include "phx/core/logger.hpp"

namespace phx {

vr::IVRSystem* TrackedDevice::vr_system_ = nullptr;
int TrackedDevice::reference_counter_ = 0;
std::mutex TrackedDevice::openVR_init_mutex_;
TrackedDevice::OpenVREventDistributer TrackedDevice::vr_event_distributer;
std::vector<vr::TrackedDevicePose_t> TrackedDevice::last_wait_get_poses_;

TrackedDevice::TrackedDevice() {
  openVR_init_mutex_.lock();
  if (vr_system_ == nullptr) {
    vr::EVRInitError init_error = vr::VRInitError_None;
    vr_system_ = vr::VR_Init(&init_error, vr::VRApplication_Scene);
    if (vr_system_ == nullptr || init_error != vr::VRInitError_None) {
      error("OpenVR cannot be initialized with error-code: {}", init_error);
      throw std::runtime_error("OpenVR cannot be initialized!");
    }
  }
  vr_event_distributer.AddDevice(this);
  reference_counter_++;
  openVR_init_mutex_.unlock();
}

TrackedDevice::~TrackedDevice() {
  openVR_init_mutex_.lock();
  reference_counter_--;
  if (reference_counter_ == 0) {
    vr::VR_Shutdown();
    vr_system_ = nullptr;
  }
  vr_event_distributer.RemoveDevice(this);
  openVR_init_mutex_.unlock();
}

void TrackedDevice::Update() {
  if (id_ == vr::k_unTrackedDeviceIndexInvalid) return;

  vr::TrackedDevicePose_t tracked_device_pose;
  if (last_wait_get_poses_.size() == vr::k_unMaxTrackedDeviceCount) {
    // the HMD has aquired the poses of all devices with WaitGetPoses()
    tracked_device_pose = last_wait_get_poses_[id_];
  } else {
    // this tracked device has to get its pose itself directly from openVR
    // this method however has a higher lag
    vr::VRControllerState_t controller_state;
    vr_system_->GetControllerStateWithPose(
        vr::TrackingUniverseStanding, id_, &controller_state,
        sizeof(controller_state), &tracked_device_pose);
  }
  if (tracked_device_pose.bPoseIsValid) {
    pose_ = TransformToGlmMatrix(tracked_device_pose.mDeviceToAbsoluteTracking);
  } else {
    debug(
        "[TrackedDevice] Tracked Device pose is invalid, device is marked as "
        "invalid. The actual implementation has to find another valid index");
    id_ = vr::k_unTrackedDeviceIndexInvalid;
  }

  vr_event_distributer.Update();
}

glm::mat4 TrackedDevice::GetPose() const { return pose_; }

bool TrackedDevice::IsActive() const {
  return id_ != vr::k_unTrackedDeviceIndexInvalid;
}

void TrackedDevice::OpenVREventDistributer::AddDevice(TrackedDevice* device) {
  tracked_devices_.push_back(device);
}

void TrackedDevice::OpenVREventDistributer::RemoveDevice(
    TrackedDevice* device) {
  auto iterator =
      std::remove_if(tracked_devices_.begin(), tracked_devices_.end(),
                     [device](Device* iteratee) { return device == iteratee; });
  tracked_devices_.erase(iterator, tracked_devices_.end());
}

void TrackedDevice::OpenVREventDistributer::Update() {
  vr::VREvent_t event;
  while (vr::VRSystem()->PollNextEvent(&event, sizeof(event))) {
    for (TrackedDevice* device : tracked_devices_) {
      device->OnOpenVREvent(event);
    }
  }
}
glm::mat4 TrackedDevice::TransformToGlmMatrix(const vr::HmdMatrix34_t& mat) {
  return glm::mat4(mat.m[0][0], mat.m[1][0], mat.m[2][0], 0.0f, mat.m[0][1],
                   mat.m[1][1], mat.m[2][1], 0.0f, mat.m[0][2], mat.m[1][2],
                   mat.m[2][2], 0.0f, mat.m[0][3], mat.m[1][3], mat.m[2][3],
                   1.0f);
}

glm::mat4 TrackedDevice::TransformToGlmMatrix(const vr::HmdMatrix44_t& mat) {
  return glm::mat4(mat.m[0][0], mat.m[1][0], mat.m[2][0], mat.m[3][0],
                   mat.m[0][1], mat.m[1][1], mat.m[2][1], mat.m[3][1],
                   mat.m[0][2], mat.m[1][2], mat.m[2][2], mat.m[3][2],
                   mat.m[0][3], mat.m[1][3], mat.m[2][3], mat.m[3][3]);
}

std::vector<vr::TrackedDeviceIndex_t> TrackedDevice::GetDeviceIndicesForClass(
    vr::ETrackedDeviceClass device_class) {
  std::vector<std::uint32_t> indices(vr::k_unMaxTrackedDeviceCount);
  vr::VRSystem()->GetSortedTrackedDeviceIndicesOfClass(
      device_class, indices.data(), static_cast<std::uint32_t>(indices.size()));
  return indices;
}

}  // namespace phx

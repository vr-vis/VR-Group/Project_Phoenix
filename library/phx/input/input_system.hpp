//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef LIBRARY_PHX_INPUT_INPUT_SYSTEM_HPP_
#define LIBRARY_PHX_INPUT_INPUT_SYSTEM_HPP_

#include <functional>
#include <string>

#include "phx/suppress_warnings.hpp"

SUPPRESS_WARNINGS_BEGIN
#define BOOST_BIND_NO_PLACEHOLDERS
#include "boost/signals2/connection.hpp"
#include "boost/signals2/signal.hpp"
SUPPRESS_WARNINGS_END

#include "phx/core/engine.hpp"
#include "phx/core/system.hpp"
#include "phx/export.hpp"

namespace phx {

class PHOENIX_EXPORT InputSystem : public System {
 public:
  InputSystem() = delete;
  InputSystem(const InputSystem&) = delete;
  InputSystem(InputSystem&&) = default;
  ~InputSystem() override;

  void Update(const FrameTimer::TimeInfo& time_info) override;

  InputSystem& operator=(const InputSystem&) = delete;
  InputSystem& operator=(InputSystem&&) = default;

  boost::signals2::connection AddQuitCallback(
      const std::function<void()>& callback);
  boost::signals2::connection AddKeyPressCallback(
      const std::function<void(char)>& callback);
  boost::signals2::connection AddKeyReleaseCallback(
      const std::function<void(char)>& callback);
  boost::signals2::connection AddMouseMoveCallback(
      const std::function<void(int, int)>& callback);
  boost::signals2::connection AddMousePressCallback(
      const std::function<void(unsigned int)>& callback);
  boost::signals2::connection AddMouseReleaseCallback(
      const std::function<void(unsigned int)>& callback);

  std::string ToString() const override;

 private:
  friend InputSystem* Engine::CreateSystem<InputSystem>();
  explicit InputSystem(Engine* engine);

  void UpdateSDLEvents();

  boost::signals2::signal<void()> quit_signal_;
  boost::signals2::signal<void(char)> key_press_signal_;
  boost::signals2::signal<void(char)> key_release_signal_;
  boost::signals2::signal<void(int, int)> mouse_move_signal_;
  boost::signals2::signal<void(unsigned int)> mouse_press_signal_;
  boost::signals2::signal<void(unsigned int)> mouse_release_signal_;
};

}  // namespace phx

#endif  // LIBRARY_PHX_INPUT_INPUT_SYSTEM_HPP_

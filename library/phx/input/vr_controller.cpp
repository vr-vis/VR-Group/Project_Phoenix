//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include "phx/input/vr_controller.hpp"

#include <string>
#include <vector>

#include "phx/core/logger.hpp"

namespace phx {

VRController::VRController(ControllerSide side) : TrackedDevice(), side_(side) {
  VRController::Update();
}

void VRController::Update() {
  if (id_ == vr::k_unTrackedDeviceIndexInvalid ||
      GetControllerRoleForTrackedDeviceIndex(id_) != side_) {
    UpdateDeviceIndex();
  }
  TrackedDevice::Update();
}

void VRController::OnOpenVREvent(const vr::VREvent_t& event) {
  if (event.trackedDeviceIndex != id_) return;
  if (event.eventType == vr::VREvent_ButtonPress) {
    button_signal_(static_cast<ButtonId>(event.data.controller.button),
                   ButtonEvent::BUTTON_PRESSED);
  } else if (event.eventType == vr::VREvent_ButtonUnpress) {
    button_signal_(static_cast<ButtonId>(event.data.controller.button),
                   ButtonEvent::BUTTON_RELEASED);
  } else if (event.eventType == vr::VREvent_ButtonTouch) {
    button_signal_(static_cast<ButtonId>(event.data.controller.button),
                   ButtonEvent::BUTTON_TOUCH);
  } else if (event.eventType == vr::VREvent_ButtonUntouch) {
    button_signal_(static_cast<ButtonId>(event.data.controller.button),
                   ButtonEvent::BUTTON_UNTOUCH);
  }
}

void VRController::UpdateDeviceIndex() {
  auto controller_indices =
      GetDeviceIndicesForClass(vr::TrackedDeviceClass_Controller);
  id_ = vr::k_unTrackedDeviceIndexInvalid;
  for (auto index : controller_indices) {
    if (index != vr::k_unTrackedDeviceIndex_Hmd &&
        GetControllerRoleForTrackedDeviceIndex(index) == side_) {
      id_ = index;
      return;
    }
  }
}

VRController::ControllerSide VRController::GetSide() const { return side_; }

vr::RenderModel_t* VRController::GetModel() {
  if (id_ == vr::k_unTrackedDeviceIndexInvalid) {
    warn("Try to obtain controller model of an invalid controller");
    return nullptr;
  }

  std::string rendermodel_name;
  rendermodel_name.resize(vr::k_unMaxPropertyStringSize);

  vr::VRSystem()->GetStringTrackedDeviceProperty(
      id_, vr::Prop_RenderModelName_String, &rendermodel_name[0],
      vr::k_unMaxPropertyStringSize);

  vr::RenderModel_t* model = nullptr;
  while (vr::VRRenderModels()->LoadRenderModel_Async(
             &rendermodel_name[0], &model) == vr::VRRenderModelError_Loading) {
    std::this_thread::sleep_for(std::chrono::milliseconds(50));
  }

  return model;
}

boost::signals2::connection VRController::RegisterButtonSignal(
    const std::function<void(ButtonId, ButtonEvent)>& callback) {
  return button_signal_.connect(callback);
}

glm::vec2 VRController::GetAxesValue(AxesId axes_id) {
  if (!IsActive()) return glm::vec2(0.0f);

  vr::TrackedDevicePose_t tracked_device_pose;
  vr::VRControllerState_t controller_state;
  vr_system_->GetControllerStateWithPose(
      vr::TrackingUniverseStanding, id_, &controller_state,
      sizeof(controller_state), &tracked_device_pose);

  for (uint32_t i = 0; i < vr::k_unControllerStateAxisCount; i++) {
    vr::EVRControllerAxisType type = static_cast<vr::EVRControllerAxisType>(
        vr_system_->GetInt32TrackedDeviceProperty(
            id_, static_cast<vr::ETrackedDeviceProperty>(
                     vr::Prop_Axis0Type_Int32 + i)));
    if (type == static_cast<vr::EVRControllerAxisType>(axes_id)) {
      vr::VRControllerAxis_t axes = controller_state.rAxis[i];
      return glm::vec2(axes.x, axes.y);
    }
  }
  warn("VRController device  does not provide the requested axes: {} ",
       vr_system_->GetControllerAxisTypeNameFromEnum(
           static_cast<vr::EVRControllerAxisType>(axes_id)));
  return glm::vec2(0.0f);
}

VRController::ControllerSide
VRController::GetControllerRoleForTrackedDeviceIndex(
    vr::TrackedDeviceIndex_t device_index) const {
  switch (vr_system_->GetControllerRoleForTrackedDeviceIndex(device_index)) {
    case vr::TrackedControllerRole_LeftHand:
      return LEFT_CONTROLLER;
    case vr::TrackedControllerRole_RightHand:
      return RIGHT_CONTROLLER;
    case vr::TrackedControllerRole_Invalid:
      break;
  }
  return INVALID_CONTROLLER;
}
}  // namespace phx

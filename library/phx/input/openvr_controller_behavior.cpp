//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include "phx/input/openvr_controller_behavior.hpp"

#include "phx/core/entity.hpp"
#include "phx/core/runtime_component.hpp"
#include "phx/core/scene.hpp"
#include "phx/rendering/components/transform.hpp"

namespace phx {
void OpenVRControllerBehavior::OnUpdate() {
  phx::Scene* scene = GetEntity()->GetScene();
  phx::Entity* runtime_entity = nullptr;
  if (side_ == VRController::LEFT_CONTROLLER) {
    auto runtime_entities = scene->GetEntitiesWithComponents<
        phx::RuntimeComponent<phx::LEFT_CONTROLLER>>();
    if (!runtime_entities.empty()) {
      runtime_entity = runtime_entities[0];
    }
  }
  if (side_ == VRController::RIGHT_CONTROLLER) {
    auto runtime_entities = scene->GetEntitiesWithComponents<
        phx::RuntimeComponent<phx::RIGHT_CONTROLLER>>();
    if (!runtime_entities.empty()) {
      runtime_entity = runtime_entities[0];
    }
  }
  if (runtime_entity &&
      !(GetEntity()->GetFirstComponent<phx::Transform>()->GetParent() ==
        runtime_entity->GetFirstComponent<phx::Transform>())) {
    GetEntity()->GetFirstComponent<phx::Transform>()->SetParent(
        runtime_entity->GetFirstComponent<phx::Transform>(), false);
  }
}

void OpenVRControllerBehavior::SetSide(VRController::ControllerSide side) {
  side_ = side;
}

VRController::ControllerSide OpenVRControllerBehavior::GetSide() const {
  return side_;
}

}  // namespace phx

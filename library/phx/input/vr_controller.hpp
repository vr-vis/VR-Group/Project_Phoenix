//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef LIBRARY_PHX_INPUT_VR_CONTROLLER_HPP_
#define LIBRARY_PHX_INPUT_VR_CONTROLLER_HPP_

#include <memory>

#define BOOST_BIND_NO_PLACEHOLDERS
// otherwise boosts _ placeholders conflict with trompeloeil ones
#include "boost/signals2/connection.hpp"
#include "boost/signals2/signal.hpp"

#include "phx/input/tracked_device.hpp"
#include "phx/resources/types/material.hpp"
#include "phx/resources/types/mesh.hpp"
#include "phx/suppress_warnings.hpp"

SUPPRESS_WARNINGS_BEGIN
#include "glm/mat4x4.hpp"

#include "openvr.h"  //NOLINT
SUPPRESS_WARNINGS_END

#include "phx/export.hpp"

namespace phx {

class OpenVRResourceLoader;

class PHOENIX_EXPORT VRController : public TrackedDevice {
 public:
  enum ControllerSide {
    RIGHT_CONTROLLER = vr::TrackedControllerRole_RightHand,
    LEFT_CONTROLLER = vr::TrackedControllerRole_LeftHand,
    INVALID_CONTROLLER = vr::TrackedControllerRole_Invalid
  };

  typedef vr::EVRButtonId ButtonId;

  enum ButtonEvent {
    BUTTON_PRESSED,
    BUTTON_RELEASED,
    BUTTON_TOUCH,
    BUTTON_UNTOUCH
  };

  enum AxesId {
    AXES_TRACKPAD = vr::k_eControllerAxis_TrackPad,
    AXES_JOYSTICK = vr::k_eControllerAxis_Joystick,
    AXES_TRIGGER = vr::k_eControllerAxis_Trigger
  };

  explicit VRController(ControllerSide side);

  virtual ~VRController() = default;

  void Update() override;
  void OnOpenVREvent(const vr::VREvent_t& event) override;

  void UpdateDeviceIndex() override;

  ControllerSide GetSide() const;

  vr::RenderModel_t* GetModel();

  boost::signals2::connection RegisterButtonSignal(
      const std::function<void(ButtonId, ButtonEvent)>& callback);

  // x ranges from -1.0 to 1.0 for joysticks and track pads and from 0.0 to 1.0
  // for triggers were 0 is fully released.
  // y ranges from -1.0 to 1.0 for joysticks and track pads and is always 0.0
  // for triggers.
  glm::vec2 GetAxesValue(AxesId id);

 private:
  ControllerSide GetControllerRoleForTrackedDeviceIndex(
      vr::TrackedDeviceIndex_t device_index) const;

  boost::signals2::signal<void(ButtonId, ButtonEvent)> button_signal_;

  ControllerSide side_;
};

}  // namespace phx

#endif  // LIBRARY_PHX_INPUT_VR_CONTROLLER_HPP_

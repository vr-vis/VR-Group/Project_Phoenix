//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef LIBRARY_PHX_INPUT_DEVICE_SYSTEM_HPP_
#define LIBRARY_PHX_INPUT_DEVICE_SYSTEM_HPP_

#include <memory>
#include <string>
#include <vector>

#include "phx/core/engine.hpp"
#include "phx/core/system.hpp"
#include "phx/export.hpp"
#include "phx/input/device.hpp"

namespace phx {
class InputSystem;

class PHOENIX_EXPORT DeviceSystem : public System {
 public:
  DeviceSystem() = delete;
  DeviceSystem(const DeviceSystem&) = delete;
  DeviceSystem(DeviceSystem&&) = default;
  virtual ~DeviceSystem();

  DeviceSystem& operator=(const DeviceSystem&) = delete;
  DeviceSystem& operator=(DeviceSystem&&) = default;

  void Update(const FrameTimer::TimeInfo& time_info) override;

  template <typename DeviceType>
  std::vector<DeviceType*> GetDevices() {
    static_assert(std::is_base_of<Device, DeviceType>::value,
                  "The type does not inherit from Device.");

    std::vector<DeviceType*> devices;
    for (const auto& device : devices_) {
      auto casted_device = dynamic_cast<DeviceType*>(device.get());
      if (casted_device != nullptr) {
        devices.push_back(casted_device);
      }
    }
    return devices;
  }

  template <typename DeviceType, typename... ComponentArguments>
  DeviceType* AddDevice(ComponentArguments&&... arguments) {
    devices_.push_back(std::make_unique<DeviceType>(arguments...));
    return dynamic_cast<DeviceType*>(devices_.back().get());
  }

  void RemoveDevice(Device* device);

  std::string ToString() const override;

 private:
  friend DeviceSystem* Engine::CreateSystem<DeviceSystem>();
  explicit DeviceSystem(Engine* engine);

  std::vector<std::unique_ptr<Device>> devices_;
};

}  // namespace phx

#endif  // LIBRARY_PHX_INPUT_DEVICE_SYSTEM_HPP_

//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef LIBRARY_PHX_INPUT_OPENVR_CONTROLLER_BEHAVIOR_HPP_
#define LIBRARY_PHX_INPUT_OPENVR_CONTROLLER_BEHAVIOR_HPP_

#include "phx/suppress_warnings.hpp"

#include "phx/export.hpp"
#include "phx/input/vr_controller.hpp"
#include "phx/scripting/behavior.hpp"

namespace phx {
class PHOENIX_EXPORT OpenVRControllerBehavior : public Behavior {
 public:
  void OnUpdate() override;

  void SetSide(VRController::ControllerSide side);
  VRController::ControllerSide GetSide() const;

 private:
  VRController::ControllerSide side_ = VRController::LEFT_CONTROLLER;
};
}  // namespace phx

#endif  // LIBRARY_PHX_INPUT_OPENVR_CONTROLLER_BEHAVIOR_HPP_

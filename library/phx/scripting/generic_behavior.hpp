//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef LIBRARY_PHX_SCRIPTING_GENERIC_BEHAVIOR_HPP_
#define LIBRARY_PHX_SCRIPTING_GENERIC_BEHAVIOR_HPP_

#include <functional>
#include <string>

#include "phx/scripting/behavior.hpp"

namespace phx {

class PHOENIX_EXPORT GenericBehavior : public Behavior {
 public:
  GenericBehavior(
      const std::function<void(Entity*)>& on_construction,
      const std::function<void(const FrameTimer::TimeInfo* time_info, Entity*)>&
          on_update,
      const std::string& name = "unnamed");
  GenericBehavior(const GenericBehavior&) = default;
  GenericBehavior(GenericBehavior&&) = default;

  GenericBehavior& operator=(const GenericBehavior&) = default;
  GenericBehavior& operator=(GenericBehavior&&) = default;

  ~GenericBehavior() = default;

  void OnUpdate() override;
  std::string ToString() const override;

 protected:
  std::function<void(Entity*)> on_construction_;
  std::function<void(const FrameTimer::TimeInfo* time_info, Entity*)>
      on_update_;
  std::string name_;
};

}  // namespace phx

#endif  // LIBRARY_PHX_SCRIPTING_GENERIC_BEHAVIOR_HPP_

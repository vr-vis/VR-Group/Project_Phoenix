//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef LIBRARY_PHX_SCRIPTING_BEHAVIOR_HPP_
#define LIBRARY_PHX_SCRIPTING_BEHAVIOR_HPP_

#include <string>

#include "phx/core/component.hpp"
#include "phx/core/frame_timer.hpp"
#include "phx/export.hpp"

namespace phx {

class BehaviorSystem;

class PHOENIX_EXPORT Behavior : public Component {
  friend BehaviorSystem;

 public:
  virtual void OnUpdate() = 0;

  std::string ToString() const override;

 protected:
  void Update(const FrameTimer::TimeInfo* time_info) {
    time_info_ = time_info;
    OnUpdate();
  }

  const FrameTimer::TimeInfo* time_info_ = nullptr;
};

}  // namespace phx

#endif  // LIBRARY_PHX_SCRIPTING_BEHAVIOR_HPP_

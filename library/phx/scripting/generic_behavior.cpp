//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include "generic_behavior.hpp"

#include <string>

namespace phx {

GenericBehavior::GenericBehavior(
    const std::function<void(Entity*)>& on_construction,
    const std::function<void(const FrameTimer::TimeInfo* time_info, Entity*)>&
        on_update,
    const std::string& name /*= "unnamed"*/)
    : on_update_(on_update), name_(name) {
  on_construction(GetEntity());
}

void GenericBehavior::OnUpdate() { on_update_(time_info_, GetEntity()); }

std::string GenericBehavior::ToString() const {
  return "GenericBehavior: " + name_;
}

}  // namespace phx

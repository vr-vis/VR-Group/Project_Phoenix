//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include "viewer_system.hpp"

#include <iostream>
#include <string>

#include "phx/core/engine.hpp"
#include "phx/core/logger.hpp"

ViewerSystem::ViewerSystem(phx::Engine* engine) : phx::System(engine) {}

bool ViewerSystem::GetShowFramerate() const { return show_framerate_; }
void ViewerSystem::SetShowFramerate(bool show_framerate) {
  show_framerate_ = show_framerate;
}

void ViewerSystem::Update(const phx::FrameTimer::TimeInfo& time_info) {
  time_since_last_print_ += time_info.time_since_last_frame;
  if (show_framerate_ && time_since_last_print_.count() > 1.0) {
    auto frame_rate = 1.0 / time_info.time_since_last_frame.count();
    phx::info("Framerate: {:.2f}", frame_rate);
    time_since_last_print_ = phx::FrameTimer::Seconds::zero();
  }
}

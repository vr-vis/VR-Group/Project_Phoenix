//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include "vrcontroller_navigation_behavior.hpp"

#include "phx/suppress_warnings.hpp"

SUPPRESS_WARNINGS_BEGIN
#include "glm/detail/type_vec3.hpp"
#include "glm/glm.hpp"
#include "glm/gtc/matrix_access.hpp"
SUPPRESS_WARNINGS_END

#include "phx/core/entity.hpp"
#include "phx/core/logger.hpp"
#include "phx/core/runtime_component.hpp"
#include "phx/display/hmd.hpp"
#include "phx/rendering/components/transform.hpp"

VRControllerNavigationBehavior::VRControllerNavigationBehavior(
    phx::DeviceSystem* device_system, phx::VRController::ControllerSide side)
    : device_system_(device_system), side_(side) {
  phx::VRController* controller = GetController();

  if (controller != nullptr) {
    controller->RegisterButtonSignal(
        [this](phx::VRController::ButtonId id,
               phx::VRController::ButtonEvent event) {
          this->OnButtonSignal(id, event);
        });
  }
}

void VRControllerNavigationBehavior::OnUpdate() {
  const auto transform = GetEntity()->GetFirstComponent<phx::Transform>();
  phx::VRController* controller = GetController();
  if (transform != nullptr && controller != nullptr && button_pressed_) {
    glm::mat4 controller_pos = controller->GetPose();
    glm::vec3 controller_forward = glm::column(controller_pos, 2);
    float speed =
        speed_ * static_cast<float>(time_info_->time_since_last_frame.count());
    speed *=
        -1.0f * controller->GetAxesValue(phx::VRController::AXES_TRACKPAD)[1];
    transform->Translate(speed * controller_forward);
  }

  phx::info("speed_: {}, axes: {}, time: {}, button_pressed: {}", speed_,
            controller->GetAxesValue(phx::VRController::AXES_TRACKPAD),
            time_info_->time_since_last_frame.count(), button_pressed_);
}

void VRControllerNavigationBehavior::OnButtonSignal(
    phx::VRController::ButtonId id, phx::VRController::ButtonEvent event) {
  if (event == phx::VRController::BUTTON_TOUCH) {
    if (id == vr::EVRButtonId::k_EButton_SteamVR_Touchpad) {
      button_pressed_ = true;
    }
  }
  if (event == phx::VRController::BUTTON_UNTOUCH &&
      id == vr::EVRButtonId::k_EButton_SteamVR_Touchpad) {
    button_pressed_ = false;
  }
}

float VRControllerNavigationBehavior::GetNavigationSpeed() const {
  return speed_;
}

void VRControllerNavigationBehavior::SetNavigationSpeed(float speed) {
  speed_ = speed;
}

phx::VRController* VRControllerNavigationBehavior::GetController() {
  for (auto controller : device_system_->GetDevices<phx::VRController>()) {
    if (controller->GetSide() == side_) {
      return controller;
    }
  }
  return nullptr;
}

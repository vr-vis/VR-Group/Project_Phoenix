//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef DEMOS_VIEWER_SRC_VRCONTROLLER_NAVIGATION_BEHAVIOR_HPP_
#define DEMOS_VIEWER_SRC_VRCONTROLLER_NAVIGATION_BEHAVIOR_HPP_

#include "phx/input/device_system.hpp"
#include "phx/input/vr_controller.hpp"
#include "phx/rendering/components/transform.hpp"
#include "phx/scripting/behavior.hpp"

class VRControllerNavigationBehavior : public phx::Behavior {
 public:
  explicit VRControllerNavigationBehavior(
      phx::DeviceSystem* device_system, phx::VRController::ControllerSide side);
  VRControllerNavigationBehavior(const VRControllerNavigationBehavior& that) =
      default;
  VRControllerNavigationBehavior(VRControllerNavigationBehavior&& temp) =
      default;
  virtual ~VRControllerNavigationBehavior() = default;
  VRControllerNavigationBehavior& operator=(
      const VRControllerNavigationBehavior& that) = default;
  VRControllerNavigationBehavior& operator=(
      VRControllerNavigationBehavior&& temp) = default;

  void OnUpdate() override;
  void OnButtonSignal(phx::VRController::ButtonId id,
                      phx::VRController::ButtonEvent event);

  float GetNavigationSpeed() const;
  void SetNavigationSpeed(float speed);

 private:
  phx::DeviceSystem* device_system_;
  phx::VRController* GetController();

  float speed_ = 0.5f;  // m/s
  bool button_pressed_ = false;
  phx::VRController::ControllerSide side_ =
      phx::VRController::INVALID_CONTROLLER;
};

#endif  // DEMOS_VIEWER_SRC_VRCONTROLLER_NAVIGATION_BEHAVIOR_HPP_

//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef DEMOS_VIEWER_SRC_VIEWER_SYSTEM_HPP_
#define DEMOS_VIEWER_SRC_VIEWER_SYSTEM_HPP_

#include <chrono>

#include "phx/suppress_warnings.hpp"

#include "phx/core/engine.hpp"
#include "phx/core/frame_timer.hpp"
#include "phx/core/system.hpp"

class ViewerSystem : public phx::System {
 public:
  ViewerSystem() = delete;
  ViewerSystem(const ViewerSystem&) = delete;
  ViewerSystem(ViewerSystem&&) = default;
  ~ViewerSystem() override = default;

  bool GetShowFramerate() const;
  void SetShowFramerate(bool show_framerate);

  void Update(const phx::FrameTimer::TimeInfo& time_info) override;

  ViewerSystem& operator=(const ViewerSystem&) = delete;
  ViewerSystem& operator=(ViewerSystem&&) = default;

 protected:
  phx::FrameTimer::Seconds time_since_last_print_ =
      phx::FrameTimer::Seconds::zero();
  bool show_framerate_ = true;

 private:
  friend ViewerSystem* phx::Engine::CreateSystem<ViewerSystem>();
  explicit ViewerSystem(phx::Engine* engine);
};

#endif  // DEMOS_VIEWER_SRC_VIEWER_SYSTEM_HPP_

//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include "rotation_behavior.hpp"

#include "phx/suppress_warnings.hpp"

SUPPRESS_WARNINGS_BEGIN
#include "glm/detail/type_vec3.hpp"
#include "glm/glm.hpp"
SUPPRESS_WARNINGS_END

#include "phx/core/engine.hpp"
#include "phx/core/entity.hpp"
#include "phx/core/scene.hpp"
#include "phx/rendering/components/transform.hpp"

void RotationBehavior::OnUpdate() {
  auto transform = GetEntity()->GetFirstComponent<phx::Transform>();
  if (transform) {
    const glm::quat model_rot = glm::angleAxis(
        0.1f * glm::pi<float>() *
            static_cast<float>(time_info_->time_since_startup.count()),
        glm::vec3(0, 1, 0));
    transform->SetLocalRotation(model_rot);
  }
}

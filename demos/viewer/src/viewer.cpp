//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include <chrono>
#include <future>
#include <memory>
#include <vector>

#include "phx/core/engine.hpp"
#include "phx/core/entity.hpp"
#include "phx/core/logger.hpp"
#include "phx/core/runtime_component.hpp"
#include "phx/core/scene.hpp"
#include "phx/display/display_system_openvr.hpp"
#include "phx/display/display_system_window.hpp"
#include "phx/display/window.hpp"
#include "phx/input/device_system.hpp"
#include "phx/input/input_system.hpp"
#include "phx/rendering/auxiliary/splash_screen.hpp"
#include "phx/rendering/components/light.hpp"
#include "phx/rendering/components/transform.hpp"
#include "phx/rendering/rendering_system.hpp"
#include "phx/resources/loaders/assimp_model_loader.hpp"
#include "phx/resources/loaders/scene_loader.hpp"
#include "phx/resources/resource_manager.hpp"
#include "phx/setup.hpp"

#include "viewer_system.hpp"
#include "vrcontroller_navigation_behavior.hpp"

#if defined __clang__
#pragma clang diagnostic ignored "-Wmissing-prototypes"
#endif

int main(int, char**) {
  std::unique_ptr<phx::Engine> engine = phx::Setup::CreateDefaultEngine(true);
  auto scene = engine->GetScene();
  auto rendering_system = engine->GetSystem<phx::RenderingSystem>();
  rendering_system->SetEnabled(false);

  phx::SplashScreen* splash = engine->CreateSystem<phx::SplashScreen>(
      engine->GetSystem<phx::DisplaySystemWindow>()->GetWindow());
  engine->MoveSystemBefore(splash,
                           engine->GetSystem<phx::DisplaySystemWindow>());

  auto assimp_loader = static_cast<phx::AssimpModelLoader*>(
      phx::ResourceManager::instance().GetLoaderForType(".obj"));

  assimp_loader->SetProgressUpdateCallback(
      [splash](float progress) { splash->SetLoadProgress(progress); });

  phx::InputSystem* input_system = engine->GetSystem<phx::InputSystem>();
  ViewerSystem* viewer_system = engine->CreateSystem<ViewerSystem>();

  input_system->AddKeyPressCallback([&engine, &viewer_system](char key) {
    if (key == 'q') engine->Stop();
    if (key == 'f')
      viewer_system->SetShowFramerate(!viewer_system->GetShowFramerate());
  });

  auto handle = std::async([&scene, rendering_system, splash]() {
    phx::SceneLoader::InsertModelIntoScene("models/bunny.obj", scene.get());
    // "models/UniversityScene/Univers20171013.obj"

    rendering_system->SetEnabled(true);
    splash->SetEnabled(false);
  });

  std::vector<glm::quat> light_dirs{
      glm::quat(glm::angleAxis(-0.25f * glm::pi<float>(), glm::vec3(1, 0, 0))),
      glm::quat(glm::angleAxis(0.25f * glm::pi<float>(), glm::vec3(0, 1, 0))),
      glm::quat(glm::angleAxis(-0.25f * glm::pi<float>(), glm::vec3(0, 1, 0))),
      glm::quat(glm::angleAxis(0.75f * glm::pi<float>(), glm::vec3(1, 0, 0)))};
  std::vector<glm::vec3> light_colors{
      glm::vec3(1.0, 1.0, 1.0), glm::vec3(1.0, 1.0, 1.0),
      glm::vec3(1.0, 1.0, 1.0), glm::vec3(1.0, 1.0, 1.0)};
  std::vector<float> light_intensities{1.0f, 0.9f, 0.8f, 0.7f};

  for (std::size_t i = 0; i < light_dirs.size(); i++) {
    phx::Entity* light_entity = scene->CreateEntity();
    phx::Transform* light_transform =
        light_entity->AddComponent<phx::Transform>();
    light_transform->SetLocalRotation(light_dirs[i]);
    phx::Light* light = light_entity->AddComponent<phx::Light>();
    light->SetType(phx::Light::Type::kDirectional);
    light->SetColor(light_colors[i]);
    light->SetIntensity(light_intensities[i]);
  }

  auto virtual_platform = scene->GetEntitiesWithComponents<
      phx::RuntimeComponent<phx::USER_PLATFORM>>()[0];
  auto virtual_platform_transform =
      virtual_platform->GetFirstComponent<phx::Transform>();
  glm::vec3 start_position{0.f, -1.f, -2.f};
  virtual_platform_transform->SetLocalTranslation(start_position);
  phx::info("The  virtual platform's start position is: {}", start_position);

  virtual_platform->AddComponent<VRControllerNavigationBehavior>(
      engine->GetSystem<phx::DeviceSystem>(),
      phx::VRController::RIGHT_CONTROLLER);

  engine->Run();

  return EXIT_SUCCESS;
}

//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include "desktop_navigation_behavior.hpp"

#include "phx/suppress_warnings.hpp"

SUPPRESS_WARNINGS_BEGIN
#include "glm/detail/type_vec3.hpp"
#include "glm/glm.hpp"
SUPPRESS_WARNINGS_END

#include "phx/core/entity.hpp"
#include "phx/rendering/components/transform.hpp"

DesktopNavigationBehavior::DesktopNavigationBehavior(
    phx::InputSystem* input_system)
    : input_system_(input_system) {
  input_system_->AddMouseMoveCallback(
      [this](int x, int y) { OnMouseMove(x, y); });
  input_system_->AddMousePressCallback(
      [this](unsigned btn) { OnMousePress(btn); });
  input_system_->AddMouseReleaseCallback(
      [this](unsigned btn) { OnMouseRelease(btn); });
}

void DesktopNavigationBehavior::OnUpdate() {
  const auto transform = GetEntity()->GetFirstComponent<phx::Transform>();

  if (rotation_mode_) {
    euler_angles_[0] += glm::radians(accumulated_mouse_pos_[1] * 0.0885416666f);
    euler_angles_[1] += glm::radians(accumulated_mouse_pos_[0] * 0.0885416666f);

    transform->SetGlobalRotation(euler_angles_);
  }
  if (strafe_mode_) {
    transform->Translate(transform->Up() *
                         static_cast<float>(accumulated_mouse_pos_[1]) *
                         0.005f);
    transform->Translate(transform->Right() *
                         static_cast<float>(accumulated_mouse_pos_[0]) *
                         -0.005f);
  }
  if (translation_mode_) {
    transform->Translate(transform->Forward() *
                         static_cast<float>(accumulated_mouse_pos_[1]) *
                         0.005f);
    transform->Translate(transform->Right() *
                         static_cast<float>(accumulated_mouse_pos_[0]) *
                         -0.005f);
  }

  accumulated_mouse_pos_ = glm::ivec2{0, 0};
}

void DesktopNavigationBehavior::OnMouseMove(int x, int y) {
  accumulated_mouse_pos_ += glm::ivec2{x, y};
}

void DesktopNavigationBehavior::OnMousePress(unsigned btn) {
  if (btn == 1) {
    rotation_mode_ = true;
  }
  if (btn == 2) {
    strafe_mode_ = true;
  }
  if (btn == 3) {
    translation_mode_ = true;
  }
}

void DesktopNavigationBehavior::OnMouseRelease(unsigned btn) {
  if (btn == 1) {
    rotation_mode_ = false;
  }
  if (btn == 2) {
    strafe_mode_ = false;
  }
  if (btn == 3) {
    translation_mode_ = false;
  }
}

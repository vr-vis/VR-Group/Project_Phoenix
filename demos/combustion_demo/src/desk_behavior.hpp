//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef DEMOS_COMBUSTION_DEMO_SRC_DESK_BEHAVIOR_HPP_
#define DEMOS_COMBUSTION_DEMO_SRC_DESK_BEHAVIOR_HPP_

#include "phx/suppress_warnings.hpp"

SUPPRESS_WARNINGS_BEGIN
#include "glm/glm.hpp"
SUPPRESS_WARNINGS_END

#include "phx/display/display_system_openvr.hpp"
#include "phx/scripting/behavior.hpp"

class DeskBehavior : public phx::Behavior {
 public:
  explicit DeskBehavior(phx::DisplaySystemOpenVR* openvr_system_);
  DeskBehavior(const DeskBehavior& that) = default;
  DeskBehavior(DeskBehavior&& temp) = default;
  virtual ~DeskBehavior() = default;
  DeskBehavior& operator=(const DeskBehavior& that) = default;
  DeskBehavior& operator=(DeskBehavior&& temp) = default;

  void OnUpdate() override;

 protected:
  phx::DisplaySystemOpenVR* openvr_system_;

 private:
  bool position_set_;
};

#endif  // DEMOS_COMBUSTION_DEMO_SRC_DESK_BEHAVIOR_HPP_

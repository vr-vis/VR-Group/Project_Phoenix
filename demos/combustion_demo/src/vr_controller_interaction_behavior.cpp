//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include "vr_controller_interaction_behavior.hpp"

#include <vector>

#include "phx/suppress_warnings.hpp"

SUPPRESS_WARNINGS_BEGIN
#include "glm/detail/type_vec3.hpp"
#include "glm/glm.hpp"
#include "glm/gtc/matrix_access.hpp"
SUPPRESS_WARNINGS_END

#include "phx/core/engine.hpp"
#include "phx/core/entity.hpp"
#include "phx/core/runtime_component.hpp"
#include "phx/core/scene.hpp"
#include "phx/display/display_system_openvr.hpp"
#include "phx/display/hmd.hpp"
#include "phx/rendering/components/transform.hpp"

VRControllerInteractionBehavior::VRControllerInteractionBehavior(
    phx::DeviceSystem* device_system)
    : device_system_(device_system) {}

void VRControllerInteractionBehavior::OnButtonSignal(
    phx::VRController::ButtonId id, phx::VRController::ButtonEvent event) {
  const auto transform = GetEntity()->GetFirstComponent<phx::Transform>();
  if (target_ != nullptr) {
    if (event == phx::VRController::BUTTON_PRESSED &&
        id == vr::EVRButtonId::k_EButton_SteamVR_Trigger) {
      if (transform != nullptr)
        target_->SetParent(GetEntity()->GetFirstComponent<phx::Transform>());
    }
    if (event == phx::VRController::BUTTON_RELEASED &&
        id == vr::EVRButtonId::k_EButton_SteamVR_Trigger) {
      target_->SetParent(nullptr);
    }
  }
}

void VRControllerInteractionBehavior::OnUpdate() {
  if (side_ == phx::VRController::INVALID_CONTROLLER) {
    RegisterOnDeviceSignal();
  }
}

void VRControllerInteractionBehavior::SetTarget(phx::Transform* target) {
  target_ = target;
}

phx::Transform* VRControllerInteractionBehavior::GetTarget() const {
  return target_;
}

void VRControllerInteractionBehavior::RegisterOnDeviceSignal() {
  if (GetEntity()
          ->GetFirstComponent<phx::RuntimeComponent<phx::LEFT_CONTROLLER>>() !=
      nullptr)
    side_ = phx::VRController::LEFT_CONTROLLER;
  else if (GetEntity()
               ->GetFirstComponent<
                   phx::RuntimeComponent<phx::RIGHT_CONTROLLER>>() != nullptr)
    side_ = phx::VRController::RIGHT_CONTROLLER;
  else
    phx::warn(
        "Added VRControllerInteractionBehavior to a non-controller entity");

  phx::VRController* controller = nullptr;
  for (auto cont : device_system_->GetDevices<phx::VRController>()) {
    if (cont->GetSide() == side_) {
      controller = cont;
      break;
    }
  }

  if (controller != nullptr) {
    controller->RegisterButtonSignal(
        [this](phx::VRController::ButtonId id,
               phx::VRController::ButtonEvent event) {
          this->OnButtonSignal(id, event);
        });
  }
}

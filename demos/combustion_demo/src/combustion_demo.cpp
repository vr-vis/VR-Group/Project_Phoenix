//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include <chrono>
#include <future>
#include <iostream>
#include <memory>
#include <string>
#include <utility>
#include <vector>

#include "desk_behavior.hpp"
#include "desktop_navigation_behavior.hpp"
#include "phx/core/runtime_component.hpp"
#include "phx/display/display_system_openvr.hpp"
#include "phx/display/display_system_window.hpp"
#include "phx/input/device_system.hpp"
#include "phx/input/input_system.hpp"
#include "phx/rendering/auxiliary/splash_screen.hpp"
#include "phx/rendering/components/mesh_handle.hpp"
#include "phx/rendering/components/mesh_render_settings.hpp"
#include "phx/resources/loaders/assimp_model_loader.hpp"
#include "phx/resources/loaders/scene_loader.hpp"
#include "phx/setup.hpp"
#include "vr_controller_interaction_behavior.hpp"

#if defined __clang__
#pragma clang diagnostic ignored "-Wmissing-prototypes"
#endif

int main(int, char**) {
  std::unique_ptr<phx::Engine> engine = phx::Setup::CreateDefaultEngine(true);
  auto scene = engine->GetScene();
  auto rendering_system = engine->GetSystem<phx::RenderingSystem>();
  rendering_system->SetEnabled(false);
  auto openvr_system = engine->GetSystem<phx::DisplaySystemOpenVR>();
  auto device_system = engine->GetSystem<phx::DeviceSystem>();

  phx::SplashScreen* splash = engine->CreateSystem<phx::SplashScreen>(
      engine->GetSystem<phx::DisplaySystemWindow>()->GetWindow());
  engine->MoveSystemBefore(splash,
                           engine->GetSystem<phx::DisplaySystemWindow>());

  auto assimp_loader = static_cast<phx::AssimpModelLoader*>(
      phx::ResourceManager::instance().GetLoaderForType(".stl"));

  assimp_loader->SetProgressUpdateCallback(
      [splash](float progress) { splash->SetLoadProgress(progress); });

  phx::InputSystem* input_system = engine->GetSystem<phx::InputSystem>();

  input_system->AddKeyPressCallback([&engine](char key) {
    if (key == 'q') engine->Stop();
  });

  auto right_controller_entities = scene->GetEntitiesWithComponents<
      phx::RuntimeComponent<phx::RIGHT_CONTROLLER>>();
  VRControllerInteractionBehavior* right_interaction_behavior = nullptr;
  if (right_controller_entities.size() >= 1) {
    auto right_controller_entity = right_controller_entities[0];
    right_interaction_behavior =
        right_controller_entity->AddComponent<VRControllerInteractionBehavior>(
            device_system);
  }

  auto handle = std::async([&right_interaction_behavior, &scene,
                            rendering_system, splash, input_system,
                            openvr_system]() {
    auto model_surface_entity = phx::SceneLoader::InsertModelIntoScene(
        "models/combustion/mixture_surface_0.2_lower.stl", scene.get());
    auto surface_transform =
        model_surface_entity->GetFirstComponent<phx::Transform>();
    auto surface_material_handle =
        surface_transform->GetChild(0)
            ->GetEntity()
            ->GetFirstComponent<phx::MaterialHandle>();
    auto surface_material = surface_material_handle->GetMaterial();
    surface_material->SetAmbientColor(glm::vec3(0.1f));
    surface_material->SetDiffuseColor(glm::vec3(1.0f));
    surface_material->SetSpecularColor(glm::vec3(0.4f));
    surface_material->SetShininess(100.0);

    auto model_boundingbox = phx::SceneLoader::InsertModelIntoScene(
        "models/combustion/data_box.stl", scene.get());
    auto boundingbox_transform =
        model_boundingbox->GetFirstComponent<phx::Transform>();
    auto boundigbox_mesh_entity =
        boundingbox_transform->GetChild(0)->GetEntity();
    auto boundingbox_mesh_handle =
        boundigbox_mesh_entity->GetFirstComponent<phx::MeshHandle>();
    auto render_settings =
        boundigbox_mesh_entity->AddComponent<phx::MeshRenderSettings>();
    render_settings->SetWireframeMode(true);

    std::array<glm::vec3, 2> bbox =
        boundingbox_mesh_handle->GetMesh()->GetBoundingBox();
    glm::vec3 bbox_diff = bbox[1] - bbox[0];
    glm::vec3 center_vec = bbox_diff * 0.5f * 0.001f;
    surface_transform->SetLocalTranslation(-center_vec);
    boundingbox_transform->SetLocalTranslation(
        glm::vec3(-center_vec[0], center_vec[1], -center_vec[2]));

    auto vis_root = scene->CreateEntity();
    auto vis_root_transform = vis_root->AddComponent<phx::Transform>();
    surface_transform->SetParent(vis_root_transform, false);
    boundingbox_transform->SetParent(vis_root_transform, false);
    vis_root_transform->SetLocalScale(glm::vec3(0.001f, 0.001f, 0.001f));
    vis_root_transform->SetLocalTranslation(glm::vec3(-0.4f, 0.02f, -0.25f));

    auto floor_entity = phx::SceneLoader::InsertModelIntoScene(
        "models/cube/cube.obj", scene.get());
    auto floor_transform = floor_entity->GetFirstComponent<phx::Transform>();
    floor_transform->SetLocalScale(glm::vec3(1000.0f, 0.05f, 1000.0f));
    auto floor_material = floor_transform->GetChild(0)
                              ->GetEntity()
                              ->GetFirstComponent<phx::MaterialHandle>();
    floor_material->GetMaterial()->SetDiffuseColor(glm::vec3(0.5f, 0.5f, 0.5f));
    floor_material->GetMaterial()->SetSpecularColor(
        glm::vec3(1.0f, 1.0f, 1.0f));
    floor_material->GetMaterial()->SetAmbientColor(glm::vec3(0.1f, 0.1f, 0.1f));

    if (right_interaction_behavior)
      right_interaction_behavior->SetTarget(vis_root_transform);

    auto desk_entity = phx::SceneLoader::InsertModelIntoScene(
        "models/cube/cube2.obj", scene.get());
    auto desk_transform = desk_entity->GetFirstComponent<phx::Transform>();
    desk_transform->SetLocalScale(glm::vec3(2.0f, 0.03f, 1.0f));
    auto desk_material = desk_transform->GetChild(0)
                             ->GetEntity()
                             ->GetFirstComponent<phx::MaterialHandle>();
    desk_material->GetMaterial()->SetDiffuseColor(glm::vec3(0.5f, 1.0f, 0.5f));
    desk_material->GetMaterial()->SetSpecularColor(glm::vec3(1.0f, 1.0f, 1.0f));
    desk_material->GetMaterial()->SetAmbientColor(glm::vec3(0.1f, 0.2f, 0.1f));

    auto desk_root = scene->CreateEntity();
    auto desk_root_transform = desk_root->AddComponent<phx::Transform>();
    desk_root_transform->SetLocalTranslation(glm::vec3(0.0f, 0.8f, 0.0f));
    desk_transform->SetParent(desk_root_transform, false);
    vis_root_transform->SetParent(desk_root_transform, false);
    desk_root->AddComponent<DeskBehavior>(openvr_system);

    rendering_system->SetEnabled(true);
    splash->SetEnabled(false);
  });

  std::vector<glm::quat> light_dirs{
      glm::quat(glm::angleAxis(-0.25f * glm::pi<float>(), glm::vec3(1, 0, 0))),
      glm::quat(glm::angleAxis(0.25f * glm::pi<float>(), glm::vec3(0, 1, 0))),
      glm::quat(glm::angleAxis(-0.25f * glm::pi<float>(), glm::vec3(0, 1, 0))),
      glm::quat(glm::angleAxis(0.75f * glm::pi<float>(), glm::vec3(1, 0, 0)))};
  std::vector<glm::vec3> light_colors{
      glm::vec3(1.0, 1.0, 1.0), glm::vec3(1.0, 1.0, 1.0),
      glm::vec3(1.0, 1.0, 1.0), glm::vec3(1.0, 1.0, 1.0)};
  std::vector<float> light_intensities{1.0f, 0.9f, 0.8f, 0.7f};

  for (std::size_t i = 0; i < light_dirs.size(); i++) {
    phx::Entity* light_entity = scene->CreateEntity();
    phx::Transform* light_transform =
        light_entity->AddComponent<phx::Transform>();
    light_transform->SetLocalRotation(light_dirs[i]);
    phx::Light* light = light_entity->AddComponent<phx::Light>();
    light->SetType(phx::Light::Type::kDirectional);
    light->SetColor(light_colors[i]);
    light->SetIntensity(light_intensities[i]);
  }

  auto virtual_platform_transform =
      scene
          ->GetEntitiesWithComponents<
              phx::RuntimeComponent<phx::USER_PLATFORM>>()[0]
          ->GetFirstComponent<phx::Transform>();

  virtual_platform_transform->SetLocalTranslation(glm::vec3(.0f, .0f, .0f));

  phx::Entity* camera =
      scene->GetEntitiesWithComponents<phx::RenderTarget>()[0];
  if (camera->GetFirstComponent<phx::RuntimeComponent<phx::LEFT_EYE>>() ==
          nullptr &&
      camera->GetFirstComponent<phx::RuntimeComponent<phx::RIGHT_EYE>>() ==
          nullptr) {
    auto camera_projection = camera->GetFirstComponent<phx::Projection>();
    camera_projection->SetPerspective(glm::radians(68.0f), 4.0f / 3.0f, 0.01f,
                                      1000.0f);
  }

  if (!openvr_system) {
    camera->AddComponent<DesktopNavigationBehavior>(input_system);
    camera->GetFirstComponent<phx::Transform>()->Translate(
        glm::vec3(0.0f, 1.0f, 1.0f));
  }

  engine->Run();

  return EXIT_SUCCESS;
}

//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include "desk_behavior.hpp"

#include <vector>

#include "phx/suppress_warnings.hpp"

SUPPRESS_WARNINGS_BEGIN
#include "glm/detail/type_vec3.hpp"
#include "glm/glm.hpp"
SUPPRESS_WARNINGS_END

#include "phx/core/entity.hpp"
#include "phx/core/runtime_component.hpp"
#include "phx/core/scene.hpp"
#include "phx/display/display_system_openvr.hpp"
#include "phx/rendering/components/transform.hpp"

DeskBehavior::DeskBehavior(phx::DisplaySystemOpenVR* openvr_system)
    : openvr_system_(openvr_system), position_set_(false) {}

void DeskBehavior::OnUpdate() {
  if (!openvr_system_ || position_set_) return;

  const auto scene = GetEntity()->GetScene();

  const auto left_controller = scene->GetEntitiesWithComponents<
      phx::RuntimeComponent<phx::LEFT_CONTROLLER>>()[0];
  const auto right_controller = scene->GetEntitiesWithComponents<
      phx::RuntimeComponent<phx::RIGHT_CONTROLLER>>()[0];

  const auto left_controller_translation =
      left_controller->GetFirstComponent<phx::Transform>()
          ->GetGlobalTranslation();
  const auto right_controller_translation =
      right_controller->GetFirstComponent<phx::Transform>()
          ->GetGlobalTranslation();
  const auto difference =
      left_controller_translation - right_controller_translation;
  const auto center = right_controller_translation + difference / 2.0f;

  const auto right = glm::normalize(difference);
  const auto up = glm::vec3(0, 1, 0);
  const auto forward = glm::cross(right, up);

  auto transform = GetEntity()->GetFirstComponent<phx::Transform>();
  transform->LookAt(transform->GetLocalTranslation() + forward);
  transform->SetLocalTranslation(center + 0.3f * forward - 0.075f * up);

  position_set_ = true;
}

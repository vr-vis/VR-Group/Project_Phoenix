//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef DEMOS_COMBUSTION_DEMO_SRC_DESKTOP_NAVIGATION_BEHAVIOR_HPP_
#define DEMOS_COMBUSTION_DEMO_SRC_DESKTOP_NAVIGATION_BEHAVIOR_HPP_

#include "phx/suppress_warnings.hpp"

SUPPRESS_WARNINGS_BEGIN
#include "glm/glm.hpp"
SUPPRESS_WARNINGS_END

#include "phx/input/input_system.hpp"
#include "phx/scripting/behavior.hpp"

class DesktopNavigationBehavior : public phx::Behavior {
 public:
  explicit DesktopNavigationBehavior(phx::InputSystem* input_system);
  DesktopNavigationBehavior(const DesktopNavigationBehavior& that) = default;
  DesktopNavigationBehavior(DesktopNavigationBehavior&& temp) = default;
  virtual ~DesktopNavigationBehavior() = default;
  DesktopNavigationBehavior& operator=(const DesktopNavigationBehavior& that) =
      default;
  DesktopNavigationBehavior& operator=(DesktopNavigationBehavior&& temp) =
      default;

  void OnUpdate() override;
  void OnMouseMove(int x, int y);
  void OnMousePress(unsigned btn);
  void OnMouseRelease(unsigned btn);

 protected:
  phx::InputSystem* input_system_;

 private:
  glm::ivec2 accumulated_mouse_pos_ = glm::ivec2(0);
  glm::vec3 euler_angles_ = glm::vec3(0.0f);
  bool rotation_mode_ = false;
  bool strafe_mode_ = false;
  bool translation_mode_ = false;
};

#endif  // DEMOS_COMBUSTION_DEMO_SRC_DESKTOP_NAVIGATION_BEHAVIOR_HPP_

//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef DEMOS_COMBUSTION_DEMO_SRC_VR_CONTROLLER_INTERACTION_BEHAVIOR_HPP_
#define DEMOS_COMBUSTION_DEMO_SRC_VR_CONTROLLER_INTERACTION_BEHAVIOR_HPP_

#include "phx/input/device_system.hpp"
#include "phx/input/vr_controller.hpp"
#include "phx/rendering/components/transform.hpp"
#include "phx/scripting/behavior.hpp"

class VRControllerInteractionBehavior : public phx::Behavior {
 public:
  explicit VRControllerInteractionBehavior(phx::DeviceSystem* device_system);
  VRControllerInteractionBehavior(const VRControllerInteractionBehavior& that) =
      default;
  VRControllerInteractionBehavior(VRControllerInteractionBehavior&& temp) =
      default;
  virtual ~VRControllerInteractionBehavior() = default;
  VRControllerInteractionBehavior& operator=(
      const VRControllerInteractionBehavior& that) = default;
  VRControllerInteractionBehavior& operator=(
      VRControllerInteractionBehavior&& temp) = default;

  void OnUpdate() override;
  void OnButtonSignal(phx::VRController::ButtonId id,
                      phx::VRController::ButtonEvent event);

  void SetTarget(phx::Transform* target);
  phx::Transform* GetTarget() const;

 protected:
  phx::DeviceSystem* device_system_;

 private:
  void RegisterOnDeviceSignal();

  phx::Transform* target_ = nullptr;
  phx::VRController::ControllerSide side_ =
      phx::VRController::INVALID_CONTROLLER;
};

#endif  // DEMOS_COMBUSTION_DEMO_SRC_VR_CONTROLLER_INTERACTION_BEHAVIOR_HPP_

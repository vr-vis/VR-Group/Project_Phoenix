#------------------------------------------------------------------------------
# Project Phoenix
#
# Copyright (c) 2017-2018 RWTH Aachen University, Germany,
# Virtual Reality & Immersive Visualization Group.
#------------------------------------------------------------------------------
#                                 License
#
# Licensed under the 3-Clause BSD License (the "License");
# you may not use this file except in compliance with the License.
# See the file LICENSE for the full text.
# You may obtain a copy of the License at
#
#     https://opensource.org/licenses/BSD-3-Clause
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#------------------------------------------------------------------------------

from conans import ConanFile, CMake

class ProjectPhoenix(ConanFile):
    name = "phx"
    version = "18.06.0"
    license = "3-Clause BSD License"
    description = """Project Phoenix"""
    settings = "os", "compiler", "build_type", "arch"

    requires = (("assimp/4.1.0@RWTH-VR/thirdparty"),
                ("boost_optional/1.66.0@bincrafters/testing"),
                ("boost_signals2/1.66.0@bincrafters/testing"), #still not working in stable, change whenever available (also boost_optional)
                ("catch/1.12.0@RWTH-VR/thirdparty"),
                ("cppcheck/1.84@RWTH-VR/thirdparty"),
                ("cpplint/e8ffd7c@RWTH-VR/thirdparty"),
                ("freeimage/3.17.0_2@RWTH-VR/thirdparty"),
                ("gl/1.1.1@RWTH-VR/thirdparty"),
                ("glm/0.9.8.5@g-truc/stable"),
                ("jsonformoderncpp/3.0.1@vthiery/stable"),
                ("openvr/1.0.12@RWTH-VR/thirdparty"),
                ("sdl2/2.0.8@bincrafters/stable"),
                ("spdlog/0.16.3@bincrafters/stable"),
                ("trompeloeil/v29@rollbear/stable"))
    generators = "cmake"

    def configure(self):
       self.options["boost"].header_only = True
       self.options["boost_python"].python_version = 2.7
       self.options["freeimage"].shared = True
       self.options["gl"].shared = False
       if self.settings.os == "Linux":
            self.options["sdl2"].alsa = False
            self.options["sdl2"].jack = False
            self.options["sdl2"].pulse = False
            self.options["sdl2"].esd = False
            self.options["sdl2"].nas = False
            self.options["sdl2"].arts = False

    def imports(self):
       self.copy("*.dll", dst="lib", src="bin")
       self.copy("*.dll", dst="tests/Debug", src="bin")
       self.copy("*.dll", dst="tests/Release", src="bin")
       self.copy("*.dll", dst="demos/viewer/Debug", src="bin")
       self.copy("*.dll", dst="demos/viewer/Release", src="bin")
       self.copy("*.dll", dst="demos/combustion_demo/Debug", src="bin")
       self.copy("*.dll", dst="demos/combustion_demo/Release", src="bin")
       self.copy("*.so", dst="lib", src="lib")
       self.copy("*.so.*", dst="lib", src="lib")
       self.copy("*.dylib", dst="lib", src="lib")
       self.copy("*.dylib", dst="tests", src="lib")

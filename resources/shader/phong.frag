//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------
#version 450

layout(location = 0) in vec3 in_position;
layout(location = 1) in vec3 in_normal;
layout(location = 2) in vec2 in_tex_coord;

//material
struct Material {
    uvec4 texture_toggle; // x ambient, y diffuse, z specular, w unused
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
    sampler2D ambient_tex;
	sampler2D diffuse_tex;
	sampler2D specular_tex;
    float shininess;
}; 
uniform Material material;

//light source
struct DirectionalLight {
    vec4 direction;
	vec4 color;
	float intensity;
};

layout(std140, binding = 0) uniform Lights {
	uint num_lights;
	DirectionalLight light[512]; // arbitrarily chosen max value
};

out vec4 frag_color;

void main() {

	vec4 color_out = vec4(0.0, 0.0, 0.0, 1.0);

	//lighting is so far done in world space
	vec3 normal = normalize(in_normal);

	for(int i = 0; i < num_lights; ++i) {
		//light direction should be the one the light is pointing to,so:
		vec3 lightDir = -normalize(light[i].direction.xyz);
	
		float lambertian = max(dot(lightDir,normal), 0.0);
		float specularf = 0.0;

		if(lambertian > 0.0) {

			vec3 viewDir = normalize(-in_position);

			// this is blinn phong
			vec3 halfDir = normalize(lightDir + viewDir);
			float specAngle = max(dot(halfDir, normal), 0.0);
			specularf = pow(specAngle, material.shininess);
	   
		}	
	  vec3 ambient  = light[i].intensity * light[i].color.xyz *               (material.texture_toggle.x == 0 ? material.ambient  : texture(material.ambient_tex , in_tex_coord).xyz);
	  vec3 diffuse  = light[i].intensity * light[i].color.xyz * (lambertian * (material.texture_toggle.y == 0 ? material.diffuse  : texture(material.diffuse_tex , in_tex_coord).xyz));
	  vec3 specular = light[i].intensity * light[i].color.xyz * (specularf  * (material.texture_toggle.z == 0 ? material.specular : texture(material.specular_tex, in_tex_coord).xyz));
  	  frag_color = vec4(ambient + diffuse + specular , 1.0);
  }
}

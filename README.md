# Project Phoenix

**Stable:** [![build status](https://devhub.vr.rwth-aachen.de/VR-Group/Project_Phoenix/badges/stable/build.svg)](https://devhub.vr.rwth-aachen.de/VR-Group/Project_Phoenix/commits/stable)
&emsp;
**Master:** [![build status](https://devhub.vr.rwth-aachen.de/VR-Group/Project_Phoenix/badges/master/build.svg)](https://devhub.vr.rwth-aachen.de/VR-Group/Project_Phoenix/commits/master)

*Project Phoenix* is Copyright (c) 2017-2018 RWTH Aachen University, Germany,
Virtual Reality & Immersive Visualization Group.


## License

Licensed under the 3-Clause BSD License (the "License");
you may not use this file except in compliance with the License.
See the file LICENSE for the full text.
You may obtain a copy of the License at

    https://opensource.org/licenses/BSD-3-Clause

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


## External libraries

Project Phoenix uses external libraries that are listed in the file
[EXTERNALS.md](EXTERNALS.md) along with their corresponding license information.